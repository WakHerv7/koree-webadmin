@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails du commerce</h3>                        
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('marketplace.shop.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste
                        </a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">

                    <div class="py-4 d-flex" style="gap:15px;">
                        <a href="{{ route('marketplace.shop-sub-category.index', ['shop_id' => $data['id']]) }}" class="btn btn-primary btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Categories d'articles du commerce
                        </a>
                        <a href="{{ route('marketplace.article.index', ['shop_id' => $data['id']]) }}" class="btn btn-primary btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Articles du commerce
                        </a>

                        <a href="{{ route('marketplace.order.shop.index', ['shop_id' => $data['id']]) }}" class="btn btn-primary btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Commandes du commerce
                        </a>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['libelle'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Description : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['description'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Téléphone : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['contact'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Adresse : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['adresse'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Catégorie : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['category']['name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">ID Fidelité : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['fidelity_id'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">ID Cashback : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['shop_cashback_id'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Votes : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['votes'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Note : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['note'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['location']['latitude'] }}
                                <br/><br/>
                                Longitude : {{ $data['location']['longitude'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['location']['latitude'],$data['location']['longitude'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Logo : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['logo'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Image Background : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['background'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Accepte des livraisons : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['accept_delivery'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Accepte des livraisons externes : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['accept_external_delivery'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by']['name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by']['name'] }}</span>
                        </div>
                    </div> --}}
                    
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
