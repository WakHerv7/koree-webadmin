@extends('layouts.main')

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Commerces
                    <!-- <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3> -->
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                <a href="#" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouveau Commerce
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table style="overflow-y:auto;" class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>   
                    <th>Nom</th>
                    <th>Telephone</th>
                    <th>Description</th>
                    <th>Categorie</th>
                    <th>Logo</th>
                    <th>Background</th>
                    <th>Adresse</th>
                    <th>Source</th>
                    <th>ID Fidelité</th>
                    <th>ID cashback</th>
                    <th>Localisation</th>
                    <th>Accepte les livraisons</th>
                    <th>Accepte les livraisons externes</th>
                    <th>Localisation</th>
                    <th>Votes</th>
                    <th>Note</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p['libelle'] }}</td>
                        <td>{{ $p['contact'] }}</td>
                        <td>{{ $p['description'] }}</td>
                        <td>{{ $p['category_id'] }}</td>
                        <td><img src="{{ $p['logo'] }}" style="border-radius:50%;" width="100px"/></td>
                        <td><img src="{{ $p['background'] }}" style="border-radius:15px;" width="100px"/></td>
                        <td>{{ $p['adresse'] }}</td>
                        <td>{{ $p['provider'] }}</td>
                        <td>{{ $p['fidelity_id'] }}</td>
                        <td>{{ $p['shop_cashback_id'] }}</td>
                        <td><a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$p['location']['latitude'],$p['location']['longitude'].'&z=17'}}">Open in Google Maps</a></td>
                        <td>
                            @if($p['accept_delivery'])
                                <span class="badge badge-success">Oui</span>
                            @else
                                <span class="badge badge-danger">Non</span>
                            @endif
                        </td>
                        <td>
                            @if($p['accept_external_delivery'])
                                <span class="badge badge-success">Oui</span>
                            @else
                                <span class="badge badge-danger">Non</span>
                            @endif
                        </td>
                        <td>{{ $p['votes'] }}</td>
                        <td>{{ $p['note'] }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="#">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('commerce-edit')
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="#">
                                <i class='flaticon2-edit icon-md'></i>
                            </a>
                            @endcan
                            @can('commerce-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['vendeurs.destroy', $p['id'] ],'id'=>'form-suppression'.$p['id'],'style'=>'display:inline']) !!}
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p['id'] }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
