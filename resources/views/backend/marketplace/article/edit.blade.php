@extends('layouts.main')
@section('content')
    @livewire('backend.marketplace.article.form', ['article'=> $data, 'addons'=> $addons, 'shop'=> $shop])
@endsection

