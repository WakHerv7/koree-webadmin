@extends('layouts.main')
@section('content')
    @livewire('backend.marketplace.article.form', ['shop'=> $shop])
@endsection

@section('end_javascript')
    <script>
        $(document).ready(function(){
            
        }); // fin doc ready
    </script>
@endsection
