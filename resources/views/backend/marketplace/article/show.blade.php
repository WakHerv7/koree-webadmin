@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails du produit</h3>
                    </div>
                    <div class="card-toolbar" style="gap:15px;">
                        <a href="{{ route('marketplace.article.index', ['shop_id' => $data['shop_id']]) }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>
                            Retour à la liste
                        </a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Libellé : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['libelle'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Description : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['description'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quantité en stock : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['stock_quantity'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Prix unitaire : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['unit_per_price'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Categorie : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['product_category_libelle'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date d'expiration : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['expiration_date'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Tags : </label>
                        <div class="col-8">
                            @foreach ($data['tags'] as $key => $p)
                            <span class="badge badge-light mr-4">#{{$p['name']}}</span>
                            @endforeach

                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ticketing ? </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_ticket'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Menu ? </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_menu'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">En rupture ? </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['sold_out'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Image : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div> --}}

                    <br>
                    <br>
                    <br>
                    <br>

                    @if($data['is_menu'] == true)

                    <h6>Composants du menu</h6>
                    <div style="width:100%; height:1px; background:rgb(170, 170, 170); margin:15px 0;"></div>


                    @foreach ($addons as $key => $comp)
                    <div class="my-2" style="position:relative; border:1px solid rgb(170, 170, 170); border-radius:15px; padding:15px; padding-top:35px;">

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Libellé : </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comp['libelle'] }}</span>
                            </div>
                        </div>

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Quantité : </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comp['quantity'] }}</span>
                            </div>
                        </div>

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Type : </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comp['type'] }}</span>
                            </div>
                        </div>

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Obligatoire ? </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">
                                     @if($comp['required'])
                                        <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                    @else
                                        <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                    @endif
                                </span>
                            </div>
                        </div>

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Description : </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comp['description'] }}</span>
                            </div>
                        </div>

                    </div>

                    @endforeach
                    @endif

                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
