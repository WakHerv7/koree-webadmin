@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    @livewire('backend.marketplace.subscription.show-list-deleted', ['data'=> $data])
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollY: '50vh',
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copy',
                'csv', 'pdf',
                'excel', 
                // 'print'
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
