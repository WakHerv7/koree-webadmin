@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails de la commande</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('marketplace.order.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Reference : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['reference'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Statut : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                @if($data['status'] == 'CART')
                                    <span class="label label-lg font-weight-bold label-light-primary label-inline">Panier</span>
                                @elseif($data['status'] == 'PENDING')
                                    <span class="label label-lg font-weight-bold label-light-warning label-inline">En cours</span>
                                @elseif($data['status'] == 'PAID')
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">Payé</span>
                                
                                @elseif($data['status'] == 'REJECT')
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">Rejeté</span>
                                @elseif($data['status'] == 'CANCEL')
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">Annulé</span>
                                @elseif($data['status'] == 'DELIVERY')
                                    <span class="label label-lg font-weight-bold label-light-dark label-inline">Livraison</span>
                                @elseif($data['status'] == 'FINISH')
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">Terminé</span>
                                @endif
                            </span>
                        </div>
                    </div>
                    
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Client : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Nom : {{ $data['customer']['first_name'].' '.$data['customer']['last_name'] }}
                                <br/><br/>
                                Telephone : {{ $data['customer']['phone'] }}
                                <br/><br/>                                
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['total_amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['shop'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ticket ? </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                @if($data['is_ticket'])
                                   <span class="label label-lg font-weight-bold label-light-success label-inline">Oui</span>
                               @else
                                   <span class="label label-lg font-weight-bold label-light-danger label-inline">Non</span>
                               @endif
                           </span>
                        </div>
                    </div>
                    
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Recepteur : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['recipient'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Recepteur : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['recipient'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Mode de retrait : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['witdrawal_mode'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Livraison: Adresse  : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['delivery_adress'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Livraison: Montant : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['delivery_amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Livraison: Localisation : </label>
                        <div class="col-8">
                            @if(isset($data['delivery_location']) && is_array($data['delivery_location']))
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['delivery_location']['latitude'] }}
                                <br/><br/>
                                Longitude : {{ $data['delivery_location']['longitude'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['delivery_location']['latitude'],$data['delivery_location']['longitude'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            @endif
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Description de recouvrement : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['recovery_description'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant cashback : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['cashback_amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nbre total d'articles  : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['total_items'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Qté total d'articles : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['total_articles'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">ID Fidelité : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['fidelity_id'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce: Adresse : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['fidelity_id'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce: Localisation : </label>
                        <div class="col-8">
                            @if(isset($data['shop_location']) && is_array($data['shop_location']))
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['shop_location']['latitude'] }}
                                <br/><br/>
                                Longitude : {{ $data['shop_location']['longitude'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['shop_location']['latitude'],$data['shop_location']['longitude'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            @endif
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce: Logo : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['shop_logo'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Actif : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div> --}}

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by']['name'] }}</span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div> --}}
                    
                </div>
                <!--end::Body-->

                
            </div>

            <div class="card card-custom gutter-b my-4">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Contenu de la commande</h3>                
                    </div>
                    <div class="card-toolbar">
                       <span>Total : {{ $data['total_amount'] }}</span>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin: Datatable-->
                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                        <thead>
                        <tr>   
                            <th>Designation</th>
                            <th>P.U.</th>
                            <th>Qté</th>
                            <th>P.T.</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data['order_items'] as $key => $p)
                            <tr>
                                <td>{{ $p['article'][0]['libelle']}}</td>
                                <td>{{ $p['article'][0]['unit_per_price']}}</td>                                
                                <td>{{ $p['quantity']}}</td>
                                <td>{{ $p['quantity']*$p['article'][0]['unit_per_price']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!--end: Datatable-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollY: '50vh',
            scrollCollapse: true,
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy',
            //     'csv', 'pdf',
            //     'excel', 
            //     // 'print'
            // ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
