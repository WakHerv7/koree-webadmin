@extends('layouts.main')

@section('title', 'Dashboard admin')

@section('content')
    <!--begin::Row-->
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="card card-custom bg-white card-rounded gutter-b pt-2">
                <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 px-2">
                    Temps scan moyen 24 heures
                </a>
                <hr>
                <div class="row m-0">
                    <div class="col px-2 pt-2 pb-4 mr-2">
                        <div
                            class="font-size-h4 font-weight-bolder text-center">{{ $avgScans['dayScansAvg'] }}</div>
                        <div class="font-size-sm text-muted font-weight-bold text-center">min.</div>
                    </div>

                </div>
                <!--end::Row-->
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card card-custom bg-white card-rounded gutter-b pt-2">
                <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 px-2">
                    Temps scan moyen 7 jours
                </a>
                <hr>
                <div class="row m-0">
                    <div class="col px-2 pt-2 pb-4 mr-2">
                        <div
                            class="font-size-h4 font-weight-bolder text-center">{{ $avgScans['weekScansAvg'] }}</div>
                        <div class="font-size-sm text-muted font-weight-bold text-center">min.</div>
                    </div>

                </div>
                <!--end::Row-->
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card card-custom bg-white card-rounded gutter-b pt-2">
                <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 px-2">
                    Temps scan moyen 1 mois
                </a>
                <hr>
                <div class="row m-0">
                    <div class="col px-2 pt-2 pb-4 mr-2">
                        <div
                            class="font-size-h4 font-weight-bolder text-center">{{ $avgScans['monthScansAvg'] }}</div>
                        <div class="font-size-sm text-muted font-weight-bold text-center">min.</div>
                    </div>

                </div>
                <!--end::Row-->
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card card-custom bg-white card-rounded gutter-b pt-2">
                <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 px-2">
                    Temps scan moyen 1 an
                </a>
                <hr>
                <div class="row m-0">
                    <div class="col px-2 pt-2 pb-4 mr-2">
                        <div
                            class="font-size-h4 font-weight-bolder text-center">{{ $avgScans['yearScansAvg'] }}</div>
                        <div class="font-size-sm text-muted font-weight-bold text-center">min.</div>
                    </div>

                </div>
                <!--end::Row-->
            </div>
        </div>
    </div>
    <!--end::Row-->

    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-6 col-xxl-8">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 p-2">Total achat</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($totalAchat,0,'.',' ') }}</div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">Ce mois</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 p-2">Total cashback</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($totalCashback,0,'.',' ') }}</div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">Ce mois</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 p-2">Total topup</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($totalTopup,0,'.',' ') }}</div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">Ce mois</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7 p-2">Total paiement</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($totalPayment,0,'.',' ') }}</div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">Ce mois</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Parrainages</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtParrainage['this_month'] }}
                                    <span @class(['label-danger' => $dtParrainage['taux'] < 0 ,'label-success' => $dtParrainage['taux'] >= 0, 'label label-sm label-pill label-inline mr-2']) >({{ $dtParrainage['taux'] }}% )</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtParrainage['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtParrainage['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtParrainage['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Abonnements
                            récents</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($lastAbonnements,0,'.',' ') }}</div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">&nbsp;</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Commissions
                            sur transactions</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtCommissionsTransactions['this_month'] }}
                                    <span @class(['label-danger' => $dtCommissionsTransactions['taux'] < 0 ,'label-success' => $dtCommissionsTransactions['taux'] >= 0, 'label label-sm label-pill label-inline mr-2']) >({{ $dtCommissionsTransactions['taux'] }}% )</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtCommissionsTransactions['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtCommissionsTransactions['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtCommissionsTransactions['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Commissions
                            sur vente de wallets</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtParrainage['this_month'] }}
                                    <span
                                        class="label label-sm label-success label-pill label-inline mr-2">({{ 0 }}%)</span>
                                </div>
                                <div class="font-size-h4 font-weight-bolder">{{ number_format(0,0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtParrainage['last_month'] }}</div>
                                <div class="font-size-h4 font-weight-bolder">{{ number_format(0,0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Nombre
                            de nouveaux clients</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtNewClient['this_month'] }}
                                    <span @class(['label-danger' => $dtNewClient['taux'] < 0 ,'label-success' => $dtNewClient['taux'] >= 0, 'label label-sm label-pill label-inline mr-2'])>({{ $dtNewClient['taux'] }}%)</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtNewClient['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtNewClient['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtNewClient['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Nombre
                            de clients récurrents</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtAllClient['this_month'] }}
                                    <span @class(['label-danger' => $dtAllClient['taux'] < 0 ,'label-success' => $dtAllClient['taux'] >= 0, 'label label-sm label-pill label-inline mr-2'])>({{ $dtAllClient['taux'] }}%)</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAllClient['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtAllClient['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAllClient['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">&nbsp;&nbsp;Nombre
                            de clients mensuels</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtMensClient['this_month'] }}
                                    <span @class(['label-danger' => $dtMensClient['taux'] < 0 ,'label-success' => $dtMensClient['taux'] >= 0, 'label label-sm label-pill label-inline mr-2'])>({{ $dtMensClient['taux'] }}%)</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtMensClient['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtMensClient['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtMensClient['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h7">
                            Solde SMS
                        </a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-h4 font-weight-bolder text-center">{{ number_format($techsoftBalance,3,'.',' ') }}
                                    €
                                </div>
                                <div class="font-size-sm text-muted font-weight-bold text-center">TECHSOFT SMS</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <!--begin::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">&nbsp;</div>
                                <div class="font-size-h4 font-weight-bolder">&nbsp;</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>

                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h6">&nbsp;&nbsp;Topup
                            moyen par client</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtAvgTopup['this_month'] }}
                                    <span @class(['label-danger' => $dtAvgTopup['taux'] < 0 ,'label-success' => $dtAvgTopup['taux'] >= 0, 'label label-sm label-pill label-inline mr-2']) >({{ $dtAvgTopup['taux'] }}% )</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAvgTopup['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div
                                    class="font-size-sm text-muted font-weight-bold">{{ $dtAvgTopup['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAvgTopup['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-xxl-4">
                    <div class="card card-custom bg-white card-rounded gutter-b">
                        <a href="#" class="text-dark-45 text-hover-primary font-weight-bolder font-size-h6">&nbsp;&nbsp;Average
                            Order Value</a>
                        <hr>
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtAov['this_month'] }}
                                    <span @class(['label-danger' => $dtAov['taux'] < 0 ,'label-success' => $dtAov['taux'] >= 0, 'label label-sm label-pill label-inline mr-2']) >({{ $dtAov['taux'] }}% )</span>
                                </div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAov['this_month_val'],0,'.',' ') }}</div>
                            </div>

                        </div>
                        <!--end::Row-->
                        <div class="row m-0">
                            <div class="col px-2 py-1 mr-2">
                                <div class="font-size-sm text-muted font-weight-bold">{{ $dtAov['last_month'] }}</div>
                                <div
                                    class="font-size-h4 font-weight-bolder">{{ number_format($dtAov['last_month_val'],0,'.',' ') }}</div>
                            </div>
                        </div>
                        <!--end::Row-->
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-6 col-xxl-4">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Dernières transactions</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Example-->
                    <div class="example example-basic">
                        <div class="example-preview">
                            <div class="timeline timeline-justified timeline-4">
                                <div class="timeline-bar"></div>
                                <div class="timeline-items">
                                    @foreach( $dtTrans as $tr)
                                        <div class="timeline-item">
                                            <div class="timeline-badge">
                                                <div class="bg-danger"></div>
                                            </div>
                                            <div class="timeline-label">
                                                <span class="text-primary font-weight-bold">{{ $tr['date'] }}</span>
                                            </div>
                                            <div class="timeline-content">{{ ucfirst($tr['type']) }} -
                                                Montant: {{ $tr['montant'] }}  </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Example-->
                </div>
            </div>

            <!--end::Card-->
        </div>
    </div>
    <!--end::Row-->

    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">TopUp sur les 30 derniers jours</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="chart_topup"></div>
                    <!--end::Chart-->
                </div>
            </div>
            <!--end::Card-->
        </div>
    </div>
    <!--end::Row-->
    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Cashback sur les 30 derniers jours</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="chart_cashback"></div>
                    <!--end::Chart-->
                </div>
            </div>
            <!--end::Card-->
        </div>
    </div>
    <!--end::Row-->

    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Paiements sur les 30 derniers jours</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="chart_paiement"></div>
                    <!--end::Chart-->
                </div>
            </div>
            <!--end::Card-->
        </div>
    </div>
    <!--end::Row-->
@endsection

@section('end_javascript')
    {{--    <script src="{{ asset('assets/js/pages/dashboard.js') }}"></script>--}}
    <script>
        // Class definition

        var KTBootstrapDaterangepicker = function () {

// Private functions
            var demos = function () {
// predefined ranges
                var start = moment().subtract(29, 'days');
                var end = moment();

                $('#kt_daterangepicker_6').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',

                    startDate: start,
                    endDate: end,
                    ranges: {
                        'Aujourdhui': [moment(), moment()],
                        'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                        '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                        'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                        'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, function (start, end, label) {
                    $('#date_periode .form-control').val(start.format('MM/DD/YYYY') + ' / ' + end.format('MM/DD/YYYY'));
                });
            }

            var validationDemos = function () {
// input group and left alignment setup
                $('#kt_daterangepicker_1_validate').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary'
                }, function (start, end, label) {
                    $('#kt_daterangepicker_1_validate .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
                });

// input group and left alignment setup
                $('#kt_daterangepicker_2_validate').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary'
                }, function (start, end, label) {
                    $('#kt_daterangepicker_3_validate .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
                });

// input group and left alignment setup
                $('#kt_daterangepicker_3_validate').daterangepicker({
                    buttonClasses: ' btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary'
                }, function (start, end, label) {
                    $('#kt_daterangepicker_3_validate .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
                });
            }

            return {
// public functions
                init: function () {
                    demos();
                    validationDemos();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTBootstrapDaterangepicker.init();
        });

        // Shared Colors Definition
        const primary = '#6993FF';
        const success = '#1BC5BD';
        const info = '#8950FC';
        const warning = '#FFA800';
        const danger = '#F64E60';

        // affichage des donuts
        var KTApexChartsDemo = function () {
            // Private functions

            var _abonnements = function () {
                const apexChart = "#chart_abonnements";
                var abonDay = {{ $abonJour}};
                var moisAbon = {{$lastAbonnements}};
                var options = {
                    series: [abonDay, moisAbon],
                    chart: {
                        width: 400,
                        type: 'donut',
                    },
                    labels: ['Jour', 'Mois'],
                    responsive: [{
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    colors: [primary, success, warning, danger, info]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            var _commissions = function () {
                const apexChart = "#chart_commissions";
                var comDay = {{ $jourComissions}};
                var comMois = {{$moisComissions}};
                var comAnnee = {{$totCommissions}};
                var options = {
                    series: [comDay, comMois, comAnnee],
                    chart: {
                        width: 400,
                        type: 'donut',
                    },
                    labels: ['Jour', 'Mois', 'Année'],
                    responsive: [{
                        breakpoint: 480,
                        options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    }],
                    colors: [primary, success, warning, danger, info]
                };

                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            var _bar_topup = function () {
                const apexChart = "#chart_topup";
                var lestopval = {{json_encode($tabTopVal)}};
                var lestoplbl = [{!!  $tabTopLbl!!}];
                var options = {
                    series: [{
                        name: 'TopUp',
                        data: lestopval
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                            columnWidth: '55%',
                            endingShape: 'rounded'
                        },
                    },
                    dataLabels: {
                        enabled: true
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: lestoplbl,
                    },
                    yaxis: {
                        title: {
                            text: 'valeur'
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return "F CFA " + val
                            }
                        }
                    },
                    colors: [primary, success, warning]
                };
                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            var _bar_cashback = function () {
                const apexChart = "#chart_cashback";
                var lescashval = {{json_encode($tabCashVal)}};
                var lescashlbl = [{!!  $tabCashLbl!!}];
                var options = {
                    series: [{
                        name: 'Cashback',
                        data: lescashval
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                            columnWidth: '55%',
                            endingShape: 'rounded'
                        },
                    },
                    dataLabels: {
                        enabled: true
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: lescashlbl,
                    },
                    yaxis: {
                        title: {
                            text: 'valeur'
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return "F CFA " + val
                            }
                        }
                    },
                    colors: [primary, success, warning]
                };
                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            const _bar_paiement = () => {
                const apexChart = "#chart_paiement";
                var lespamtval = {{json_encode($tabPaymtVal)}};
                var lespaymtlbl = [{!!  $tabPaymtLbl!!}];
                var options = {
                    series: [{
                        name: 'Paiement',
                        data: lespamtval
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                            columnWidth: '55%',
                            endingShape: 'rounded'
                        },
                    },
                    dataLabels: {
                        enabled: true
                    },
                    stroke: {
                        show: true,
                        width: 2,
                        colors: ['transparent']
                    },
                    xaxis: {
                        categories: lespaymtlbl,
                    },
                    yaxis: {
                        title: {
                            text: 'valeur'
                        }
                    },
                    fill: {
                        opacity: 1
                    },
                    tooltip: {
                        y: {
                            formatter: function (val) {
                                return "F CFA " + val
                            }
                        }
                    },
                    colors: [primary, success, warning]
                };
                var chart = new ApexCharts(document.querySelector(apexChart), options);
                chart.render();
            }

            return {
                // public functions
                init: function () {
                    _abonnements();
                    _commissions();
                    _bar_topup();
                    _bar_cashback();
                    _bar_paiement();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTApexChartsDemo.init();
        });

    </script>
@endsection
