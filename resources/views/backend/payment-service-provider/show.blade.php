@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('payment-service-provider.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commission : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['commission_amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Benefice : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['income_amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Entreprise : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['shop'] }}</span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commande : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order'] }}</span>
                        </div>
                    </div> --}}

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Sens : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                @if($data['sens'] == 'IN')
                                   <span class="label label-lg font-weight-bold label-light-success label-inline">Pay In</span>
                               @else
                                   <span class="label label-lg font-weight-bold label-light-danger label-inline">Pay Out</span>
                               @endif
                           </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Statut : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                @if($data['status'] == 'FINISH')
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">Achevée</span>
                                @elseif($data['status'] == 'RUN')
                                    <span class="label label-lg font-weight-bold label-light-warning label-inline">En cours</span>
                                @elseif($data['status'] == 'WAIT')
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">En attente</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Actif : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div> --}}
                    
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
