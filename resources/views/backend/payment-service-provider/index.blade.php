@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    @livewire('backend.payment-service-provider.show-list', ['data'=> $data])
    <!--end::Card-->

@endsection
@push('css')
<style>
    .status_dot {
    height: 10px;
    width: 10px;
    border-radius: 50%;
    display: inline-block;
   }
   .status_green {
    background-color: #5cb85c;
   }
   .status_red {
    background-color: #aa0000;
   }
   .payment-provider-list {
    width: 100%;
   }
   .payment-provider-service {
    line-height: 30px;
    /* width: 100%; */
   }
   .status_switch.custom-control-input ~ .custom-control-label::before {
    width: 30px;
    height: 20px; 
    border-radius: 15px;
    }
    .status_switch.custom-control-input ~ .custom-control-label::after {
    width: 15px;
    height: 15px;
    border-radius: 15px;
    line-height: 40px;
    }

   .status_switch.custom-control-input:not(:checked) ~ .custom-control-label::before {
    background-color: #aa0000;
    }
    .status_switch.custom-control-input:not(:checked) ~ .custom-control-label::after {
    background-color: white;
    }

    .status_switch.custom-control-input:checked ~ .custom-control-label::before {
    background-color: #5cb85c;
    }
    .status_switch.custom-control-input:checked ~ .custom-control-label::after {
    background-color: white;
    }
    .provider_actions, .service_actions {
        display: none;
    }
    .payment_provider:hover > .provider_actions {
        display: flex;
    }
    .payment-provider-service:hover > .service_actions {
        display: flex;
    }
    .service_libelle {
        display: flex;
        gap:15px;
        justify-content: space-between;
        align-items: center;
    }
</style>
@endpush
@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollY: false,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copy',
                'csv', 'pdf',
                'excel', 
                // 'print'
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
