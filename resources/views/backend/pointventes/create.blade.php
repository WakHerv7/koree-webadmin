@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouveau Point de vente</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('points-ventes.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                <form action="{{ route('points-ventes.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Commerce<span class="text-danger">*</span></label>
                            <select class="form-control" name="commerce_id" id="commerce_id" required>
                                @foreach($commerces as $p)
                                    <option value="{{ $p->id }}">{{ $p->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Code
                                <span class="text-danger">*</span></label>
                            {!! Form::text('code', null, array('placeholder' => 'Code','class' => 'form-control', 'required'=>'required', 'maxlength' => 7)) !!}
                        </div>
                        <div class="form-group">
                            <label>Désignation
                                <span class="text-danger">*</span></label>
                            {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Point de vente global?
                                <span class="text-danger">*</span></label>
                            {!! Form::select('is_global', [ 0 => 'NON', 1=>'OUI' ], null, ['class' => 'form-control', 'required'=>'required']) !!}
                        </div>
                        <div class="form-group">
                            <label>Adresse</label>
                            {!! Form::textArea( 'adresse', null, array('placeholder' => 'Adresse', 'rows' => 5,'class' => 'form-control no-resize')) !!}
                        </div>

                        <div class="form-group">
                            <label>Localisation</label>
                            {!! Form::textArea( 'localisation', null, array('placeholder' => 'Localisation', 'rows' => 2, 'class' => 'form-control no-resize')) !!}
                        </div>

                        <div class="form-group">
                            <label for="country">Pays</label>
                            <select name="country" id="country" class="form-control select2">
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="city">Ville</label>
                            <select name="city" id="city" class="form-control select2">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="district">Quartier</label>
                            <select name="district" id="district" class="form-control select2">
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label" for="horaires">Horaires d'ouverture</label>
                            {!! Form::textArea('horaires', null, array('placeholder' => 'Horaires du point de vente','rows'=>'5', 'id' => 'horaires', 'class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            <label>Nom responsable</label>
                            {!! Form::text('nom_responsable', null, array('placeholder' => 'nom responsable','class' => 'form-control')) !!}

                        </div>
                        <div class="form-group">
                            <label>Tel responsable</label>
                            {!! Form::text('tel_responsable', null, array('placeholder' => 'tel responsable','class' => 'form-control')) !!}

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        const KTSelect2 = function () {
            // Private functions
            const demos = function () {
                // basic
                $('#commerce_id').select2({
                    placeholder: "Selectionnez le commerce"
                });

                $('#country').select2({
                    placeholder: "Selectionnez le pays"
                });

                $('#city').select2({
                    placeholder: "Selectionnez la ville"
                });

                $('#district').select2({
                    placeholder: "Selectionnez le quartier"
                });
            }

            return {
                init: function () {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTSelect2.init();
        });

    </script>

    <script>
        $(document).ready(function() {
            $('#country').on('change', function () {
                var PaysID = $(this).val();
                if (PaysID) {
                    $.ajax({
                        url: '/getVilleParPays/' + PaysID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#city').empty();
                                $('#district').empty();
                                // $('#kt_select2_state').append('<option hidden>Choisir une region</option>');
                                $.each(data, function (key, vale) {
                                    $('select[name="city"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#city').empty();
                                $('#district').empty();
                            }
                        }
                    });
                }
            })

            $('#city').on('change', function () {
                var cityID = $(this).val();
                if (cityID) {
                    $.ajax({
                        url: '/getQuartiers/' + cityID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#district').empty();
                                // $('#kt_select2_state').append('<option hidden>Choisir une region</option>');
                                $.each(data, function (key, vale) {
                                    $('select[name="district"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#district').empty();
                            }
                        }
                    });
                }
            })
        })
    </script>
@endsection
