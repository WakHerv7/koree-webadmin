@extends('layouts.main')
@php
    $wide = true;
@endphp
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail Point de vente
                            <span class="d-block text-muted pt-2 font-size-sm">infos complémentaires</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('points-ventes.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->commerce->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Code : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->code ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Désignation : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Point de vente global? : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">@if($data->is_global==1)
                                    OUI
                                @else
                                    NON
                                @endif</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Adresse : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->adresse ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->city?->name ?? '' }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quartier : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->district?->name ?? '' }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->localisation ?? '' }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom responsable : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->nom_responsable ?? '' }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Tel responsable : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->tel_responsable ?? '' }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-12 col-form-label">Horaires : </label>
                        <div class="col-12">
                            <span class="form-control-plaintext font-weight-bolder">{!! $data->horaires ?? '' !!}</span>
                        </div>
                    </div>


                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
