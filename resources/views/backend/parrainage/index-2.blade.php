@extends('layouts.main')

@section('title', "Historique de parrainage par commerce")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Historique de parrainge par commerce
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les commerces</span></h3>
            </div>
{{--            <div class="card-toolbar">--}}
{{--                <!--begin::Button-->--}}
{{--                @can('commerce-create')--}}
{{--                <a href="{{ route('vendeurs.create') }}" class="btn btn-primary font-weight-bolder">--}}
{{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau Propriétaire--}}
{{--                </a>--}}
{{--                @endcan--}}
{{--                <!--end::Button-->--}}
{{--            </div>--}}
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Parrain</th>
                    <th>Tel parrain</th>
                    <th>Filleul</th>
                    <th>Tel filleul</th>
                    <th>Activé</th>
                    <th>date de parrainage</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $parrainage)
                    <tr>
                        <td>{{ $parrainage->commerce->nom }}</td>
                        <td>{{ $parrainage->parrain->getFullname() }}</td>
                        <td>{{ $parrainage->parrain->user->telephone }}</td>
                        <td>{{ $parrainage->filleul->getFullname() }}</td>
                        <td>{{ $parrainage->filleul->user->telephone }}</td>
                        <td>
                            @if($parrainage->transactions()->where('wallet_vendeur_id', $parrainage->commerce))

                            @endif
                        </td>
                        <td>{{ $parrainage->created_at->format('Y-m-d H:i')}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollY: '50vh',
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            order: [
                [6, 'desc']
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
