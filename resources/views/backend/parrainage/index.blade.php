@extends('layouts.main')
@section('title', "Historique de parrainage")
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Historique de parrainge
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
{{--            <div class="card-toolbar">--}}
{{--                <!--begin::Button-->--}}
{{--                @can('commerce-create')--}}
{{--                <a href="{{ route('vendeurs.create') }}" class="btn btn-primary font-weight-bolder">--}}
{{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau Propriétaire--}}
{{--                </a>--}}
{{--                @endcan--}}
{{--                <!--end::Button-->--}}
{{--            </div>--}}
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Parrain</th>
                    <th>Tel parrain</th>
                    <th>Code de parrainage</th>
                    <th>Filleul</th>
                    <th>tel. filleul</th>
                    <th>date de parrainage</th>
                    <th>date d'activation</th>
                    <th>Payé?</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->parrain->nom. " ".$p->parrain->prenom }}</td>
                        <td>{{ $p->parrain->user->telephone }}</td>
                        <td>{{ $p->parrain->code_parrainage }}</td>
                        <td>{{ $p->filleul->nom. " ".$p->filleul->prenom }}</td>
                        <td>{{ $p->filleul->user->telephone }}</td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td>{{ $p->updated_at ?? ''}}</td>
                        <td> @if($p->status==1)
                                <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                            @else
                                <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                            @endif</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            order: [
                [5, 'desc']
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
