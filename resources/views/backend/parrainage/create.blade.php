@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouveau Propriétaire</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('vendeurs.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::open(array('route' => 'vendeurs.store','method'=>'POST', 'files' => true)) !!}
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Nom(s) <span class="text-danger">*</span></label>
                                {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}

                            </div>
                            <div class="col-lg-6">
                                <label>Pays<span class="text-danger">*</span></label>
                                <select class="form-control" id="country_id"  name="country_id" required>
                                    @foreach($pays as $p)
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Région <span class="text-danger">*</span></label>
                                <select class="form-control" id="state_id" name="state_id" required>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>Ville<span class="text-danger">*</span></label>
                                <select class="form-control" id="city_id" name="city_id" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Email  </label>
                                {!! Form::text('email', null, array('placeholder' => 'Nom','class' => 'form-control')) !!}

                            </div>
                            <div class="col-lg-6">
                                <label>Téléphone  <span class="text-danger">*</span></label>
                                {!! Form::number('telephone', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>RCCM  </label>
                                {!! Form::text('rccm', null, array('placeholder' => 'Contact','class' => 'form-control')) !!}
                            </div>
                            <div class="col-lg-6">
                                <label>Adresse</label>
                                {!! Form::textArea('adresse', null, array('placeholder' => 'Adresse','rows'=>'3', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Tel fixe  </label>
                                {!! Form::text('tel_fixe', null, array('placeholder' => 'telephone fixe','class' => 'form-control')) !!}
                            </div>
                            <div class="col-lg-6">
                                <label>Contact</label>
                                {!! Form::text('contact_compte', null, array('placeholder' => 'contact', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Fonction </label>
                                {!! Form::text('fonction', null, array('placeholder' => 'Fonction','class' => 'form-control')) !!}
                            </div>
                            <div class="col-lg-6">
                                <label>Code de sécurité<span class="text-danger">*</span></label>
                                {!! Form::number('password', null, array('placeholder' => 'Code de sécurité', 'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
            {!! Form::close() !!}
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        $(document).ready(function(){
            $('#country_id').on('change', function(){
                var PaysID = $(this).val();
                if(PaysID){
                    $.ajax({
                        url:'/getRegion/'+PaysID,
                        type: "GET",
                        data: {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success: function(data){
                            if(data){
                                $('#state_id').empty(); $('#city_id').empty();
                                $('#state_id').append('<option hidden>Choisir une region</option>');
                                $.each(data, function(key, vale){
                                    $('select[name="state_id"]').append('<option value="'+ vale.id+'">'+vale.name+'</option>');
                                });
                            }else{
                                $('#state_id').empty(); $('#city_id').empty();
                            }
                        }
                    });
                }
            });
            $('#state_id').on('change', function(){
                var RegionID = $(this).val();
                if(RegionID){
                    $.ajax({
                        url:'/getVille/'+RegionID,
                        type: "GET",
                        data: {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success: function(data){
                            if(data){
                                $('#city_id').empty();
                                $('#city_id').append('<option hidden>Choisir une ville</option>');
                                $.each(data, function(key, vale){
                                    $('select[name="city_id"]').append('<option value="'+ vale.id+'">'+vale.name+'</option>');
                                });
                            }else{
                                $('#city_id').empty();
                            }
                        }
                    });
                }
            });
        }); // fin doc ready

    </script>
@endsection
