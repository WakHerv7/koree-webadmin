@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail Package
                            <span class="d-block text-muted pt-2 font-size-sm">infos complémentaires</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('package.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Intitulé : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Prix : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->prix ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Durée (Jours) : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->duree ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Description : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->description ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Pays : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->pays->name ?? '' }}</span>
                        </div>
                    </div>

                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
