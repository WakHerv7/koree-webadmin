@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des commerces")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des campagnes
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                <a href="{{ route('campagnes.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouvelle campagne
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Id unique</th>
                    <th>Nom</th>
                    <th>Commerce</th>
                    <th>Quantite</th>
                    <th>Forme</th>
                    <th>Taille cm</th>
                    <th>Date Debut</th>
                    <th>Date Fin</th>
                    <th>Créé le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td title="{{$p->id ?? ''}}">{{ substr($p->id ?? '', 0, 8)}}</td>
                        <td>{{ $p->nom ?? ''}}</td>
                        <td>{{ $p->commerce?->nom ?? ''}}</td>
                        <td>{{ $p->quantite ?? '' }}</td>
                        <td>{{ $p->shape ?? '' }}</td>
                        <td>{{ $p->height_in_cm ?? '' }}</td>
                        <td>{{ $p->date_from ?? ''}}</td>
                        <td>{{ $p->date_until ?? ''}}</td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('campagnes.detail',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            <a target="_blank" class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('campagnes.pdf',$p->id) }}">
                                <i class="flaticon-download icon-md"></i>
                            </a>
{{--                            @can('commerce-delete')--}}
{{--                                {!! Form::open(['method' => 'DELETE','route' => ['qrcodes.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}--}}
{{--                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>--}}
{{--                                {!! Form::close() !!}--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            order: [
                [6, 'desc']
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
