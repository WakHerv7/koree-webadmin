<html>
<header>
    <style>
        @page {
            margin: 0 0;
        }

        td {
            padding: 3px;
            /*border: 4px solid black;*/
        }

        .page-break {
            page-break-after: always;
        }

        /*#header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: orange; text-align: center; }*/
        /*#footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px; background-color: lightblue; }*/
        /*#footer .page:after { content: counter(page, upper-roman); }*/

        html{
            margin: 0;
            padding: 0;
        }
        /**{*/
        /*    color: #111;*/
        /*}*/
        /*body {*/
        /*    !*font-size: 11pt;*!*/
        /*    padding: 0;*/
        /*    margin: 0;*/
        /*}*/
    </style>
</header>
<body>

<table>
    <tbody>
    @foreach($images as $group)
        <tr>
            @foreach($group as $img)
                <td>{!! $img !!}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
{{--<header id="header">--}}
{{--    <div class="header" style="position: absolute; top: 0;">--}}
{{--        <table width="100%">--}}
{{--            <tr>--}}
{{--                <td align="right" style="width: 50%;">--}}
{{--                    QRCODE DE CAMPAGNE {{$campagne->nom}}--}}
{{--                </td>--}}

{{--                <td align="right" style="width: 50%;">--}}
{{--                    DU {{$campagne->date_from}}--}}
{{--                    AU {{$campagne->date_until}}--}}
{{--                </td>--}}
{{--            </tr>--}}
{{--        </table>--}}
{{--    </div>--}}
{{--</header>--}}
<footer id="footer">
    <div class="footer" style="position: absolute; bottom: 0;">
        <table width="100%">
            <tr>
                <td align="left" style="width: 100%;">
                    &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved.
                    <br>
                    QRCODE DE CAMPAGNE <b>{{mb_strtoupper($campagne->nom)}}</b>
                    DU {{$campagne->date_from}}
                    AU {{$campagne->date_until}}
                </td>
            </tr>
        </table>
    </div>
</footer>
</body>
</html>
