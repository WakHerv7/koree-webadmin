@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom ">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail Campagne
                            <span class="d-block text-muted pt-2 font-size-sm">infos complémentaires</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <a target="_blank" class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('campagnes.pdf',$data->id) }}">
                            <i class="flaticon-download icon-md"></i>
                        </a>
                        <a href="{{ route('campagnes.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->commerce?->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Intitulé : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quantite : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->quantite ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Forme : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->shape ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Taille : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->height_in_cm ?? '' }} cm</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date debut : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->date_from ?? ''}}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date fin: </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->date_until ?? ''}}</span>
                        </div>
                    </div>

                </div>
                <!--end::Body-->
            </div>

            <div class="card card-custom mt-4">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Qrcodes
                            <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
                    </div>
                    {{--                        <div class="card-toolbar">--}}
                    {{--                            <a href="{{ route('campagnes.index') }}" class="btn btn-success btn-sm font-weight-bold">--}}
                    {{--                                <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>--}}
                    {{--                        </div>--}}
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">

                    <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                        <thead>
                        <tr>
                            <th>ID Unique</th>
                            <th>numero</th>
                            <th>montant</th>
                            <th>Date Debut</th>
                            <th>Date Fin</th>
                            <th>Date Utilisation</th>
                            <th>Créé le</th>
                            <th width="180px">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data->qrcodes as $key => $p)
                            <tr>
                                <td title="{{$p->uuid ?? ''}}">{{ substr($p->uuid ?? '', 0, 8)}}</td>
                               <td>{{ $p->number ?? '' }}</td>
                               <td>{{ $p->montant ?? '' }}</td>
                                <td>{{ $p->date_from ?? ''}}</td>
                                <td>{{ $p->date_until ?? ''}}</td>
                                <td>{{ $p->date_used ?? 'Jamais utilisé'}}</td>
                                <td>{{ $p->created_at ?? ''}}</td>
                                <td nowrap="nowrap">
                                    @can('qrcode-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['qrcodes.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}
                                        <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
                <!--end::Body-->
            </div>

        </div>
    </div>
@endsection


@push('js')

    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    // order: [[9, 'desc']]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>

@endpush
