@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'une campagne</h3>
                </div>
                <!--begin::Form-->
                {!! Form::open(array('route' => 'campagnes.store','method'=>'POST')) !!}
                <div class="card-body">
                    <div class="form-group">
                        <label for="commerce_id">Commerce<span class="text-danger">*</span></label>
                        <select class="form-control" name="commerce_id" id="commerce_id" required>
                            @foreach($commerces as $p)
                                <option value="{{ $p->id }}">{{ $p->nom }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nom
                            <span class="text-danger">*</span></label>
                        {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                    </div>
                    <div class="form-group">
                        <label>Quantite de qrcode a generer
                            <span class="text-danger">*</span></label>
                        {!! Form::number('quantite', null, array('placeholder' => 'quantité','class' => 'form-control', 'required'=>'required')) !!}
                    </div>
                    <div class="form-group">
                        <label for="shape">Forme<span class="text-danger">*</span></label>
                        <select class="form-control" name="shape" id="shape" required>
                            <option value="square">Carré</option>
                            <option value="circle">Circulaire</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Taille en centimetre
                            <span class="text-danger">*</span></label>
                        {!! Form::number('height_in_cm', null, array('placeholder' => 'Taille en cm','class' => 'form-control', 'required'=>'required')) !!}
                    </div>
                    <div class="form-group">
                        <label>Date debut
                            <span class="text-danger">*</span></label>
                        {!! Form::date('date_from', null, array('placeholder' => 'Debut','class' => 'form-control', 'required'=>'required')) !!}
                    </div>

                    <div class="form-group">
                        <label>Durée (Jours) <span class="text-danger">*</span></label>
                        {!! Form::number('duree', null, array('placeholder' => 'duree','class' => 'form-control', 'required'=>'required')) !!}

                        {{--                        {!! Form::select('duree',['15' => '15','30'=>'30','90'=>'90','180'=>'180','360'=>'360'],null, ['class'=>'form-control', 'required'=>'required','placeholder'=>'Durée']) !!}--}}

                    </div>


                    <div class="form-group">
                        <label>Montant <span class="text-danger">*</span></label>
                        {!! Form::number('montant', null, array('placeholder' => 'Montant','class' => 'form-control', 'required'=>'required')) !!}

                        {{--                        {!! Form::select('duree',['15' => '15','30'=>'30','90'=>'90','180'=>'180','360'=>'360'],null, ['class'=>'form-control', 'required'=>'required','placeholder'=>'Durée']) !!}--}}

                    </div>
{{--                    <div class="form-group">--}}
{{--                        <label>Date fin--}}
{{--                            <span class="text-danger">*</span></label>--}}
{{--                        {!! Form::date('date_until', null, array('placeholder' => 'Fin','class' => 'form-control', 'required'=>'required')) !!}--}}
{{--                    </div>--}}
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                {!! Form::close() !!}
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection
