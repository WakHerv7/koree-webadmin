@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des influenceurs
                    <span class="d-block text-muted pt-2 font-size-sm"></span></h3>
            </div>
{{--            <div class="card-toolbar">--}}
{{--                <!--begin::Button-->--}}
{{--                @can('commerce-create')--}}
{{--                <a href="{{ route('vendeurs.create') }}" class="btn btn-primary font-weight-bolder">--}}
{{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau influenceurs--}}
{{--                </a>--}}
{{--                @endcan--}}
{{--                <!--end::Button-->--}}
{{--            </div>--}}
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Code</th>
                    <th>Nb filleuls</th>
                    <th>Nb filleuls en inscription</th>
                    <th>Nb filleuls en commerce</th>
                    <th width="150px">Créé le</th>
                    <th width="80px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->nom }}</td>
                        <td>{{ wordwrap($p->prenom) }}</td>
                        <td>{{ $p->code_parrainage }}</td>
                        <td>{{ $p->parrainages_count }}</td>
                        <td>{{ $p->parrainages_nonactives_count }}</td>
                        <td>{{ $p->parrainages_actives_count }}</td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('clients.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
