@extends('layouts.main')

@section('title', "Historique des transactions")
@php
    $wide = true;
@endphp
@section('content')
    <!--begin::Card-->
    @if(session()->has('info'))
        <div class="py-4 alert alert-info">{{ session()->get('info') }}</div>
    @endif
    @if(session()->has('error'))
        <div class="py-4 alert alert-danger">{{ session()->get('error') }}</div>
    @endif
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Historique de Transactions
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
        </div>
        <div class="card-body">

            <form action="{{ route('transactions.export') }}" method="GET" target="_blank">
                <div class="row mb-3">
                    <div class="col">
                        <select class="form-control" id="client_id" name="client_id">
                            <!-- <option selected disabled>Selectionnez le client</option>
                            <option value="1">romuald</option>
                            <option value="2">youmsi</option>
                            <option value="3">azerty</option>
                            <option value="4">qwerty</option>
                            <option value="5">bastos</option>
                            <option value="6">deido</option>
                            <option value="7">dakar</option> -->
                        </select>
                        <input type="hidden" id="client_id_val" value="0"/>
                    </div>
                    <div class="col">
                        <select class="form-control" id="commerce_id" name="commerce_id">
                            <!-- <option selected disabled>Selectionnez le client</option>
                            <option value="1">romuald</option>
                            <option value="2">youmsi</option>
                            <option value="3">azerty</option>
                            <option value="4">qwerty</option>
                            <option value="5">bastos</option>
                            <option value="6">deido</option>
                            <option value="7">dakar</option> -->
                        </select>
                        <input type="hidden" id="commerce_id_val" value="0"/>
                    </div>
                    <div class="col">
                        <input class="form-control" type="datetime-local" id="start_date" name="start_date"/>
                    </div>
                    <div class="col">
                        <input class="form-control" type="datetime-local" id="end_date" name="end_date"/>
                    </div>
                    <div class="col">
                        <input type="submit" class="btn btn-primary" value="Exporter" id="export_transaction"/>
                    </div>
                </div>
            </form>
            <hr/>
            <!--begin: Datatable Romuald -->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_histo_transaction">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Client</th>
                    <th>vip</th>
                    <th>Telephone</th>
                    <th>Agent</th>
                    <th>Type</th>
                    <th>Montant cde</th>
                    <th>Cashback</th>
                    <th>TopUp</th>
                    <th>Cashback/TopUp</th>
                    <th>Paiement</th>
                    <th>Bonus</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Date</th>
                    <th></th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Commerce</th>
                    <th>Client</th>
                    <th>vip</th>
                    <th>Telephone</th>
                    <th>Agent</th>
                    <th>Type</th>
                    <th>Montant cde</th>
                    <th>Cashback</th>
                    <th>TopUp</th>
                    <th>Cashback/TopUp</th>
                    <th>Paiement</th>
                    <th>Bonus</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Date</th>
                    <th></th>
                </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
            <!--end: Datatable Romuald -->
            <!--end::Card-->
        </div>
    </div>

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                /*var table = $('#kt_datatable1');

                // begin first table
                // dom: 'Bfrtip',
                table.DataTable({
                    scrollY: '50vh',
                    destroy: true,
                    searching: true,
                    processing: true,
                    order: [
                        [10, 'desc']
                    ],
                    lengthMenu: [
                        [5, 10, 25, 50, 100],
                        [5, 10, 25, 50, 100]
                    ],
                    scrollX: true,
                    scrollCollapse: true,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });*/

                // TODO: START Make by Romuald
                window.KtTable = $('#kt_datatable_histo_transaction').DataTable({
                    scrollY: '60vh',
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    order: [12, 'desc'],
                    "pageLength": 50,
                    lengthMenu: [
                        [5, 10, 25, 50, 100],
                        [5, 10, 25, 50, 100]
                    ],
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtlip',
                    // buttons: true,
                    buttons: [
                        'copy',
                        'csv',
                        'excel',
                        // 'pdf',
                        // 'print'
                    ],
                    createdRow: function (row, data, dataIndex) {
                        // console.log("customersDataTable:createdRow row", data, dataIndex);
                        // $(row).attr('data-customerkey', data.customer_key);
                        // $(row).attr('data-customerid', data.customer_id);
                    },
                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: function (d) {
                            d.wallet_client_id = '{{request()->input('wallet_client_id')}}';
                            d.wallet_vendeur_id = '{{request()->input('wallet_vendeur_id')}}';
                            d.client = $("#client_id_val").val();
                            d.commerce = $("#commerce_id_val").val();
                            d.vendeur = $("#commerce_id_val").val();
                            d.start_date = $("#start_date").val();
                            d.end_date = $("#end_date").val();
                        },
                        url: "/liste-transactions/ajaxDatable",
                        type: "POST",
                    },
                    columns: [
                        {
                            data: "commerce"
                        },
                        {
                            data: "client"
                        },
                        {
                            data: "customer_vip",
                            render: function (datum, type, row) {
                                return $("<div/>").html(row.customer_vip ? 'oui' : 'non').text();
                            }
                        },
                        {
                            data: "telephone"
                        },
                        {
                            data: "agent",
                            render: function (datum, type, row) {
                                const data = (row.agent || "").split('||')
                                // console.log(datum);
                                const agent = data[0] || '';
                                const pt_vente = data[1] || '';
                                const adresse_pt_vente = data[2] || '';
                                return $("<div/>").html(`
                                <div title="${adresse_pt_vente}" rel="tooltips">
                                <span>${agent}</span><br>
                                <small>${pt_vente}</sma>
                                </div>
                            `).html();
                            }
                        },
                        {
                            data: "type_transaction"
                        },
                        {
                            data: "montant_facture"
                        },
                        {
                            data: "montant_cashback"
                        },
                        {
                            data: "montant_topup"
                        },
                        {
                            data: "montant_cashback_over_topup"
                        },
                        {
                            data: "montant_paiement"
                        },
                        {
                            data: "montant_parrainage"
                        },
                        {
                            data: "pays"
                        },
                        {
                            data: "ville"
                        },
                        {
                            data: "created_at"
                        },
                        {
                            data: "actions",
                            render: function (datum, type, row) {
                                return $("<div/>").html(row.actions).text();
                            }
                        }
                    ]
                });
            };

            var reloadDatatable = function () {
                window.KtTable && window.KtTable.ajax.reload(null, false); // user paging is not reset on reload
            }
            setInterval(reloadDatatable, 300000); // refresh tous les 5 minutes

            var initClientsSelect2 = function () {
                $("#client_id").select2({
                    ajax: {
                        url: "/ajaxSelect2/clients",
                        dataType: 'json',
                        delay: 250,
                        allowClear: true,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 0;

                            return {
                                results: data,
                                pagination: {
                                    more: false
                                }
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Selectionnez le client',
                    minimumInputLength: 1,
                    templateResult: formatRepo,
                    templateSelection: (repo) => formatRepoSelection(repo, "client_id")
                });

            };
            var initCommercesSelect2 = function () {
                $("#commerce_id").select2({
                    ajax: {
                        url: "/ajaxSelect2/commerces",
                        dataType: 'json',
                        delay: 250,
                        allowClear: true,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 0;

                            return {
                                results: data,
                                pagination: {
                                    more: false
                                }
                            };
                        },
                        cache: true
                    },
                    placeholder: 'Selectionnez le commerce',
                    minimumInputLength: 1,
                    templateResult: formatRepo,
                    templateSelection: (repo) => formatRepoSelection(repo, "commerce_id")
                });

            };

            var formatRepo = function (repo) {
                if (repo.loading) {
                    return repo.text;
                }

                var $container = $(
                    "<div class='select2-result-repository clearfix'>" +
                    "<div class='select2-result-repository__title'></div>" +
                    "<div class='select2-result-repository__description'></div>" +
                    "</div>"
                );

                $container.find(".select2-result-repository__title").text(repo.full_name);
                $container.find(".select2-result-repository__description").text(repo.telephone);

                return $container;
            }

            var formatRepoSelection = function (repo, elementSelected) {
                $("#" + elementSelected + "_val").val(repo.id);
                // console.log("formatRepoSelection", repo, elementSelected, $("#" + elementSelected + "_val").val());
                // reloadDatatable()
                // initTable1($("#client_id_val").val(), $("#commerce_id_val").val(),$("#start_date").val(), $("#end_date").val());
                return repo.id == '' ? repo.text : `${repo.full_name || ''} (${repo.telephone})`;
            }
            var exportTransactions = function (repo, elementSelected) {
                $.ajax({
                    type: 'POST',
                    url: "/liste-transactions/export",
                    data: {
                        "client": clientId == null ? '' : clientId,
                        "commerce": commerceId == null ? '' : commerceId,
                        "vendeur": commerceId == null ? '' : commerceId,
                        "start_date": startDate == null ? '' : startDate,
                        "end_date": endDate == null ? '' : endDate
                    },
                    dataType: "text",
                    order: [
                        [10, 'desc']
                    ],
                    success: function (resultData) {
                        alert("Save Complete")
                    }
                }).error(function (error) {
                    alert(error);
                });
            }
            // TODO: END Make by Romuald


            return {

                //main function to initiate the module
                init: function () {

                    initClientsSelect2();
                    initCommercesSelect2();
                    initTable1();

                    $("#start_date").on("change", function () {
                        reloadDatatable()
                        // initTable1($("#client_id_val").val(), $("#commerce_id_val").val(), $(this).val(), $("#end_date").val());
                    });
                    $("#end_date").on("change", function () {
                        reloadDatatable()
                        // initTable1($("#client_id_val").val(), $("#commerce_id_val").val(), $("#start_date").val(), $(this).val());
                    });
                    $("#export_transactions").on("click", function () {
                        // reloadDatatable()
                        // alert("hello");
                        //initTable1($("#client_id_val").val(),$("#commerce_id_val").val(),$("#start_date").val(),$(this).val());
                    });

                    $('#client_id, #commerce_id').on('select2:select', function (e) {
                        reloadDatatable()
                        // Do something
                    });
                },
            };
        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
