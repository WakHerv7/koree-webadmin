@extends('layouts.main')

@section('title', "Restauration de la transaction")
@php
    $wide = true;
@endphp

@section('content')
    <!--begin::Card-->
    @if(session()->has('info'))
        <div class="py-4 alert alert-info">{{ session()->get('info') }}</div>
    @endif
    @if(session()->has('error'))
        <div class="py-4 alert alert-danger">{{ session()->get('error') }}</div>
    @endif
    @if($errors->any())

        <div class="alert alert-danger">
            <ul class="px-4 pt-3">
                @foreach($errors->all() as $error)
                    <li><strong>{{ $error }}</strong></li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Confirmation d'restauration de transaction
                    <span
                        class="d-block text-muted pt-2 font-size-sm">Entrez le code OTP recu pour valider l'opération</span>
                </h3>
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('perform.transaction.restore') }}" method="POST">
                @csrf
{{--                <div class="row">--}}
{{--                    <div class="col-md-12 mb-5">--}}
{{--                        Vous etes sur le point de restaurer une transaction et vous devez recopier le code suivant <br>--}}
{{--                        <span class="mt-5 text-success" style="font-weight: bolder">{{ $transaction->id }}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <input type="hidden" name="transaction" value="{{ $transaction->id }}">
                <div class="row">
                    <div class="col col-12 col-md-12">
                        <input required maxlength="{{ \App\Helpers\AdminOtp::OTP_LENGTH }}" minlength="{{ \App\Helpers\AdminOtp::OTP_LENGTH }}" name="code" class="form-control"
                               placeholder="Entrez le code içi"/>
                    </div>
                    <div class="col col-12 col-md-12 mt-6">
                        <button class="btn btn-primary" type="submit">Valider</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
