@extends('layouts.main')

@section('title', "Historique des transactions supprimés")
@php
$wide = true;
@endphp
@section('content')
<!--begin::Card-->
@if(session()->has('info'))
<div class="py-4 alert alert-info">{{ session()->get('info') }}</div>
@endif
@if(session()->has('error'))
<div class="py-4 alert alert-danger">{{ session()->get('error') }}</div>
@endif
<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Historique de Transactions supprimées
                <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable Romuald -->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable_histo_transaction">
            <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Client</th>
                    <th>Type</th>
                    <th>Montant cde</th>
                    <th>Cashback</th>
                    <th>TopUp</th>
                    <th>Paiement</th>
                    <th>Bonus</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Date</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Commerce</th>
                    <th>Client</th>
                    <th>Type</th>
                    <th>Montant cde</th>
                    <th>Cashback</th>
                    <th>TopUp</th>
                    <th>Paiement</th>
                    <th>Bonus</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Date</th>
                    <th></th>
                </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>
        <!--end: Datatable Romuald -->
        <!--end::Card-->
    </div>
</div>

@endsection

@section('end_javascript')
<script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

        var initTable1 = function() {
            /*var table = $('#kt_datatable1');

            // begin first table
            // dom: 'Bfrtip',
            table.DataTable({
                scrollY: '50vh',
                destroy: true,
                searching: true,
                processing: true,
                order: [
                    [10, 'desc']
                ],
                lengthMenu: [
                    [5, 10, 25, 50, 100],
                    [5, 10, 25, 50, 100]
                ],
                scrollX: true,
                scrollCollapse: true,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });*/

            // TODO: START Make by Romuald
            $('#kt_datatable_histo_transaction').DataTable({
                scrollY: '50vh',
                destroy: true,
                processing: true,
                serverSide: true,
                order: [[10, 'desc']],
                lengthMenu: [
                    [5, 10, 25, 50, 100],
                    [5, 10, 25, 50, 100]
                ],
                scrollX: true,
                scrollCollapse: true,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                createdRow: function(row, data, dataIndex) {
                    // console.log("customersDataTable:createdRow row", data, dataIndex);
                    // $(row).attr('data-customerkey', data.customer_key);
                    // $(row).attr('data-customerid', data.customer_id);
                },
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (d) {
                        d.deleted = '1';
                        d.wallet_client_id = '{{request()->input('wallet_client_id')}}';
                        d.wallet_vendeur_id = '{{request()->input('wallet_vendeur_id')}}';
                    },
                    url: "/liste-transactions/ajaxDatable?deleted=1",
                    type: "POST",
                },
                columns: [{
                    data: "commerce"
                },
                    {
                        data: "client"
                    },
                    {
                        data: "type_transaction"
                    },
                    {
                        data: "montant_facture"
                    },
                    {
                        data: "montant_cashback"
                    },
                    {
                        data: "montant_topup"
                    },
                    {
                        data: "montant_paiement"
                    },
                    {
                        data: "montant_parrainage"
                    },
                    {
                        data: "pays"
                    },
                    {
                        data: "ville"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "actions",
                        render: function(datum, type, row) {
                            return $("<div/>").html(row.actions).text();
                        }
                    }
                ]
            });
        };
        // TODO: END Make by Romuald


        return {

            //main function to initiate the module
            init: function() {
                initTable1();
            },

        };

    }();

    jQuery(document).ready(function() {
        KTDatatablesBasicScrollable.init();
    });
</script>
@endsection
