@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Historique des Revenus
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            {{--            <div class="card-toolbar">--}}
            {{--                <!--begin::Button-->--}}
            {{--                @can('commerce-create')--}}
            {{--                <a href="{{ route('vendeurs.create') }}" class="btn btn-primary font-weight-bolder">--}}
            {{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau Propriétaire--}}
            {{--                </a>--}}
            {{--                @endcan--}}
            {{--                <!--end::Button-->--}}
            {{--            </div>--}}
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Type</th>
                    <th>Montant</th>
                    <th>Date</th>
                    <th>Pays</th>
                    <th>Ville</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    @php $wali = \App\Helpers\GeneralFunctions::getCommerceByWalletId($p->wallet_vendeur_id,$p->wallet_id);
                    @endphp
                    <tr>
                        <td>{{ $wali->programme != null ? ($wali->programme->commerce != null ? $wali->programme->commercenom : '(SUPPRIMÉ)'): '(SUPPRIMÉ)'   }}</td>
                        <td>{{ $p->type_transaction }}</td>
                        <td>{{ $p->gain }}</td>
                        <td>{{  \Illuminate\Support\Carbon::parse($p->created_at)->format('Y-m-d H:i') }}</td>
                        <td>{{ $wali->programme->commerce->ville->pays->name ??'' }}</td>
                        <td>{{ $wali->programme->commerce->ville->name ?? ''}}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    order: [
                        [3, 'desc']
                    ],
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
