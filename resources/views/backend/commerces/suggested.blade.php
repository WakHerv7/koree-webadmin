@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des commerces suggérés")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Commerces suggérés
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('commerces.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouveau Commerce
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Client</th>
                    <th>Commerce</th>
                    <th>Propriétaire</th>
                    <th>Téléphone propriétaire</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Adresse</th>
                    <th>Date suggestion</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->client?->getFullname() }}</td>
                        <td>{{ $item->shop_name }}</td>
                        <td>{{ $item->getFullname() }}</td>
                        <td>{{ $item->shop_owner_phone }}</td>
                        <td>{{ $item->city?->name }}</td>
                        <td>{{ $item->district?->name }}</td>
                        <td>{{ $item->shop_address }}</td>
                        <td data-sort="{{ $item->created_at->timestamp }}">{{ $item->created_at->format('Y-m-d H:i') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    order: [7, 'desc'],
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
