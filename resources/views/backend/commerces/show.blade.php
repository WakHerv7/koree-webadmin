@extends('layouts.main')

@section('title', "Details commerce")

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">informations Propriétaire + Commerce & Point(s) de vente</h3>
                    <div class="card-toolbar">
                        @can('role-delete')
                            @if($data->status==1)
                                {!! Form::open(['method' => 'PUT','route' => ['commerces.status', $data->id],'id'=>'form-desactivation'.$data->id,'style'=>'display:inline']) !!}
                                <input type="hidden" name="statut" value="0">
                                <button class="mx-4 btn btn-danger btn-sm font-weight-bold btn-icon- deactivate-form-row" type="submit"
                                        data-val="{{ $data->id }}"><i class='fas fa-user-slash icon-md'></i> Desactiver</button>
                                {!! Form::close() !!}
                            @elseif($data->status == 0)
                                {!! Form::open(['method' => 'PUT','route' => ['commerces.status', $data->id],'id'=>'form-activation'.$data->id,'style'=>'display:inline']) !!}
                                <input type="hidden" name="statut" value="1">
                                <button class="mx-4 btn btn-success btn-sm font-weight-bold btn-icon- activate-form-row" type="submit"
                                        data-val="{{ $data->id }}"><i class='fas fa-user-tag icon-md'></i> Activer</button>
                                {!! Form::close() !!}
                            @endif

                        @endcan
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('commerces.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
{{--                {!! Form::open(array('route' => 'commerces.store','method'=>'POST', 'files' => true)) !!}--}}
                <div class="card card-custom card-shadowless rounded-top-0">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first" data-wizard-clickable="true">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav">
                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-2">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>1.</span>Propriétaire</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>2.</span>Commerce</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->
                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>3.</span>Points de vente</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->

                                </div>
                            </div>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->
                            <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                                <div class="col-xl-12 col-xxl-10">
                                    <!--begin: Wizard Form-->
                                       <!--begin: Wizard Step 1-->
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <!--begin:: fields-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nom(s) <span class="text-danger">*</span></label>
                                                        {!! Form::text('nom', $data->vendeur->nom, array('placeholder' => 'Nom(s)','class' => 'form-control', 'readonly'=>'true')) !!}

                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Téléphone Principal <span class="text-danger">*</span></label>
                                                        {!! Form::text('telephone', $data->vendeur->user->telephone, array('placeholder' => 'Téléphone','class' => 'form-control','maxlength'=>'9', 'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <div class="form-group">
                                                        <label>Prénom </label>
                                                        {!! Form::text('code_transaction', $data->prenomn, array('placeholder' => 'Prénom','class' => 'form-control', 'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    {!! Form::text('code_transaction', $data->vendeur->user->email, array('placeholder' => 'Email','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-4">
                                            <div class="form-group">
                                                <label>Code transaction  <span class="text-danger">*</span></label>
                                                {!! Form::text('code_transaction', $data->vendeur->code_transaction, array('placeholder' => 'Code transaction ','class' => 'form-control', 'readonly'=>'true')) !!}
                                            </div>
                                        </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <div class="form-group">
                                                        <label>Téléphone mobile 1 </label>
                                                        {!! Form::text('telephone1', $data->vendeur->telephone1, array('placeholder' => 'Téléphone mobile 1 ','class' => 'form-control', 'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group">
                                                        <label>Téléphone mobile 2 </label>
                                                        {!! Form::text('telephone2', $data->vendeur->telephone2, array('placeholder' => 'Téléphone mobile 2 ','class' => 'form-control', 'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="form-group">
                                                        <label>Contact en cas d'urgence </label>
                                                        {!! Form::text('contact_compte', $data->vendeur->contact_compte, array('placeholder' => 'Contact en cas d\'urgence ','class' => 'form-control',  'maxlength'=>'9','oninput'=>'maxLengthCheck(this)')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RCCM  </label>
                                                        {!! Form::text('rccm', $data->vendeur->rccm, array('placeholder' => 'RCCM','class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Boîte postale </label>
                                                        {!! Form::text('code_postal', $data->vendeur->code_postal, array('placeholder' => 'Boîte postale','class' => 'form-control', 'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="form-group">
                                                        <label>Adresse</label>
                                                        {!! Form::textArea('adresse', $data->adresse, array('placeholder' => 'Adresse','rows'=>'2',  'readonly'=>'true','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="form-group">
                                                        <label>Website</label>
                                                        {!! Form::textArea('website', $data->website, array('placeholder' => 'Website','rows'=>'2',  'readonly'=>'true','class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end:: fields-->

                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Pays<span class="text-danger">*</span></label>
                                                        {!! Form::text('pays', $data->ville->region->pays->name, array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Région <span class="text-danger">*</span></label>
                                                        {!! Form::text('region', $data->ville->region->name, array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Ville<span class="text-danger">*</span></label>
                                                        {!! Form::text('ville', $data->ville->name, array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>Quartier<span class="text-danger">*</span></label>
                                                        {!! Form::text('quat', $data->quartier->name ?? '', array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                    <!--end::Select-->
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Wizard Step 1-->
                                        <!--begin: Wizard Step 2-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <!--begin::Input-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Catégorie  <span class="text-danger">*</span></label>
                                                        {!! Form::text('categorie', $data->categorie->libelle ?? '', array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Désignation  <span class="text-danger">*</span></label>
                                                        {!! Form::text('nomcom', $data->programme->libelle, array('class' => 'form-control',  'readonly'=>'true')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <label>Contact </label>
                                                    {!! Form::text('contact', $data->contact, array('placeholder' => 'Contact','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-6">
                                                    <label>Couleurs </label>
                                                    <select name='couleurs' class="form-control selectpicker" disabled="disabled" >
                                                        <option>Choisissez un dégradé</option>
                                                        <option value="#FA9D36-#121318" @if($data->couleurs=="#FA9D36-#121318") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, #FA9D36 -21.74%, #121318 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Marron</option>
                                                        <option value="#FFFFFF-#FA9D36" @if($data->couleurs=="#FFFFFF-#FA9D36") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FA9D36 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Orange</option>
                                                        <option value="#E55F25-#EB5429-#E42127" @if($data->couleurs=="#E55F25-#EB5429-#E42127") selected @endif data-content="&lt;span style='background: radial-gradient(29.84% 50% at 50% 50%, #E55F25 0%, #EB5429 44.27%, #E42127 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Rouge</option>
                                                        <option value="#AE15E3-#E088FF" @if($data->couleurs=="#AE15E3-#E088FF") selected @endif data-content="&lt;span style='background: linear-gradient(285.49deg, #AE15E3 -1.3%, rgba(224, 136, 255, 0.59) 111.94%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Violet</option>
                                                        <option value="#1CE500-#005C24"  @if($data->couleurs=="#1CE500-#005C24") selected @endif data-content="&lt;span style='background: linear-gradient(107.62deg, #1CE600 -24.23%, #005C24 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Vert</option>
                                                        <option value="#E3C215-##DD1A1A" @if($data->couleurs=="#E3C215-##DD1A1A") selected @endif data-content="&lt;span style='background: linear-gradient(108.03deg, #E3C215 -21.74%, #DD1A1A 107.13%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Orange fonce</option>
                                                        <option value="#FDF4E3-#000000" @if($data->couleurs=="#FDF4E3-#000000") selected @endif data-content="&lt;span style='background: linear-gradient(108.08deg, #FDF4E3 -21.74%, #000000 102.81%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient gris</option>
                                                        <option value="#00FDEE-#15B2E3" @if($data->couleurs=="#00FDEE-#15B2E3") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, rgba(0, 253, 238, 0) -21.74%, #15B2E3 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Cyan</option>
                                                        <option value="#056BC6-#000036" @if($data->couleurs=="#056BC6-#000036") selected @endif data-content="&lt;span style='background: linear-gradient(108.24deg, #056BC6 -21.74%, #000036 103.15%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Bleu</option>
                                                        <option value="#FFFFFF-#FFE4E4" @if($data->couleurs=="#FFFFFF-#FFE4E4") selected @endif data-content="&lt;span style='background: linear-gradient(107.02deg, #FFFFFF -7.81%, #FFE4E4 102.68%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient blanc casse</option>
                                                        <option value="#000000-#000000" @if($data->couleurs=="#000000-#000000") selected @endif data-content="&lt;span style='background: linear-gradient(108.08deg, #000000 -21.74%, #000000 102.81%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Noir</option>
                                                        <option value="#FFFFFF-#FF3299"  @if($data->couleurs=="#FFFFFF-#FF3299") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FF3299 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Rose</option>
                                                        <option value="#FFFFFF-#FEDC01" @if($data->couleurs=="#FFFFFF-#FEDC01") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FEDC01 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Jaune</option>
                                                        <option value="#FFFFFF-#A7E425" @if($data->couleurs=="#FFFFFF-#A7E425") selected @endif data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #A7E425 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Vert clair</option>
                                                        <option value="#F2DDB1-#C7A66E" @if($data->couleurs=="#F2DDB1-#C7A66E") selected @endif data-content="&lt;span style='background: radial-gradient(70.43% 217.7% at 43.72% 50.13%, #F2DDB1 0%, #C7A66E 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">Gradient Or</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row my-4">
                                                <div class="col-xl-4">
                                                    <label class="d-block">Logo Commerce <span class="text-danger">*</span> </label>
                                                    <img src="{{ $data->logo_url }}" style="height: 200px; width: 200px;">
                                                </div>
                                                <div class="col-xl-4">
                                                    <label class="d-block">Carte Commerce <span class="text-danger">*</span> </label>
                                                    <img src="{{ $data->card_url }}" style="height: 108px; width: 192px;">
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Arriere Plan Commerce <span class="text-danger">*</span> </label>
                                                    <img src="{{ $data->background_url  }}"
                                                         alt="{{ $data->nom }}" style="height: 108px; width: 192px;">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <label>Date de début  <span class="text-danger">*</span></label>
                                                    {!! Form::date('date_debut', $data->programme->date_debut, array('placeholder' => 'Nom','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Durée (Mois) <span class="text-danger">*</span></label>
                                                    {!! Form::select('duree',['1' => '1','3'=>'3','6'=>'6','9'=>'9','12'=>'12','24'=>'24','36'=>'36'],$data->programme->duree, ['class'=>'form-control', 'readonly'=>'true','placeholder'=>'Durée']) !!}

                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Montant abonnement mensuel <span class="text-danger">*</span></label>
                                                    {!! Form::number('montant_abonnement', $data->programme->montant_abonnement, array('placeholder' => 'Montant abonnement','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <label>Montant Max Facture  <span class="text-danger">*</span></label>
                                                    {!! Form::number('mnt_max_facture', $data->programme->mnt_max_facture, array('placeholder' => 'Montant Max Facture ','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Taux commission  <span class="text-danger">*</span></label>
                                                    {!! Form::number('taux_commission', $data->programme->taux_commission, array('placeholder' => 'Taux commission','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Taux Cashback  <span class="text-danger">*</span></label>
                                                    {!! Form::number('taux_cashback', $data->programme->taux_cashback, array('placeholder' => 'Taux Cashback ','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    <label>Montant Max Cashback  <span class="text-danger">*</span></label>
                                                    {!! Form::number('max_cashback', $data->programme->max_cashback, array('placeholder' => 'Montant Max Cashback ','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Taux Cashback VIP<span class="text-danger">*</span></label>
                                                    {!! Form::number('taux_cashback_vip', $data->programme->taux_cashback_vip, array('placeholder' => 'Taux Cashback VIP','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-4">
                                                    <label>Montant Max Cashback VIP <span class="text-danger">*</span></label>
                                                    {!! Form::number('max_cashback_vip', $data->programme->max_cashback_vip, array('placeholder' => 'Montant Max Cashback VIP','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <label>Localisation</label>
                                                    {!! Form::text('localisation', $data->localisation, array('placeholder' => 'Localisation','class' => 'form-control','readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-3">
                                                    <label>Bonus parrain</label>
                                                    {!! Form::text('referer_amount', $data->referer_amount, array('placeholder' => 'Bonus parrain','class' => 'form-control','readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-3">
                                                    <label>Bonus filleul</label>
                                                    {!! Form::text('referee_amount', $data->referee_amount, array('placeholder' => 'Bonus filleul','class' => 'form-control','readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <label>Adresse</label>
                                                    {!! Form::textArea('adresse_commerce', $data->adresse, array('placeholder' => 'Adresse','rows'=>'3','class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                                <div class="col-xl-6">
                                                    <label>Description </label>
                                                    {!! Form::textArea('description_commerce', $data->description, array('placeholder' => 'Description','rows'=>'3', 'class' => 'form-control', 'readonly'=>'true')) !!}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <label>Horaires d'ouverture </label>
                                                    {!! Form::textArea('horaires', $data->horaires, array('placeholder' => 'Horaires','rows'=>'3','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <!--end: Wizard Step 2-->
                                        <!--begin: Wizard Step 3-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            @foreach($data->pointVentes as $pv)
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Code <span class="text-danger">*</span>:</label>
                                                        <input type="text" class="form-control"  name="code_pv" placeholder="Code" value="{{ $pv->code ?? '' }}" readonly>
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Désignation  <span class="text-danger">*</span>:</label>
                                                        <input type="text" class="form-control" name="lbl_pv" value="{{ $pv->nom ?? '' }}" readonly>
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Nom du responsable <span class="text-danger">*</span>:</label>
                                                        <input type="text" class="form-control"  name="nom_respo_pv" placeholder="Nom du responsable" value="{{ $pv->nom_respo_pv ?? '' }}" readonly>
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>Tel. Responsable  <span class="text-danger">*</span>:</label>
                                                        <input type="text" class="form-control" name="lbl_pv" placeholder="Tel. du responsable"  value="{{ $pv->tel_respo_pv ?? '' }}" readonly>
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>Adresse:</label>
                                                        <textarea class="form-control" rows="2" name="adresse_pv" readonly>{{ $pv->adresse ?? '' }} </textarea>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                        <!--end: Wizard Step 3-->
                                        <!--begin: Wizard Actions-->
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="mr-2">
                                                <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Précédent</button>
                                            </div>
                                            <div><button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Suivant</button>
                                            </div>
                                        </div>
                                        <!--end: Wizard Actions-->
                                    <!--end: Wizard Form-->
                                </div>
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script src="{{ asset('assets/plugins/custom/wizard/wizard_commerce_show.js') }}"></script>

@endsection
