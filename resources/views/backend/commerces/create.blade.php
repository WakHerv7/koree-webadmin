@extends('layouts.main')

@section('title', "Creation commerce")

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouveau commerce</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('commerces.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {{--                {!! Form::open(array('route' => 'commerces.store','method'=>'POST', 'files' => true)) !!}--}}
                <div class="card card-custom card-shadowless rounded-top-0">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first"
                             data-wizard-clickable="true">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav">
                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-2">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>1.</span>Propriétaire</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>2.</span>Commerce</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->


                                </div>
                            </div>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->
                            <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                                <div class="col-xl-12 col-xxl-10">
                                    <!--begin: Wizard Form-->
                                    {!! Form::open(array('route' => 'commerces.store','method'=>'POST', 'files' => true, 'id'=>'kt_form')) !!}
                                    <!--begin: Wizard Step 1-->
                                    <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                        <!--begin:: fields-->

                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Nom(s) <span class="text-danger">*</span></label>
                                                    {!! Form::text('nom', null, array('placeholder' => 'Nom(s)','class' => 'form-control', 'required'=>'required')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Prénom </label>
                                                    {!! Form::text('prenom', null, array('placeholder' => 'Prénom','class' => 'form-control', 'required'=>'required')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Code transaction <span class="text-danger">*</span></label>
                                                    {!! Form::text('code_transaction', null, array('placeholder' => 'Code transaction ','class' => 'form-control', 'required'=>'required', 'min'=>'100000', 'maxlength'=>'6','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Code de sécurité<span class="text-danger">*</span></label>
                                                    {!! Form::text('password', null, array('placeholder' => 'Code de sécurité', 'class' => 'form-control', 'min'=>'100000', 'maxlength'=>'6','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Pays<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" id="kt_select2_pays"
                                                            name="country_id" required>
                                                        @foreach($pays as $p)
                                                            <option  data-phonelength="{{ $p->phonelength }}" value="{{ $p->id }}">{{ $p->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Téléphone Principal <span
                                                            class="text-danger">*</span></label>
                                                    {!! Form::text('telephone', null, array('placeholder' => 'Téléphone','class' => 'form-control','maxlength'=>'12', 'required'=>'required','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    {!! Form::text('email', null, array('placeholder' => 'Code de sécurité', 'class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <div class="form-group">
                                                    <label>Téléphone mobile 1 </label>
                                                    {!! Form::text('telephone1', null, array('placeholder' => 'Téléphone mobile 1 ','class' => 'form-control',  'maxlength'=>'9','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-4">
                                                <div class="form-group">
                                                    <label>Téléphone mobile 2 </label>
                                                    {!! Form::text('telephone2', null, array('placeholder' => 'Téléphone mobile 2 ','class' => 'form-control',  'maxlength'=>'9','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-4">
                                                <div class="form-group">
                                                    <label>Contact en cas d'urgence </label>
                                                    {!! Form::text('contact_compte', null, array('placeholder' => 'Contact en cas d\'urgence ','class' => 'form-control',  'maxlength'=>'9','oninput'=>'maxLengthCheck(this)')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>RCCM </label>
                                                    {!! Form::text('rccm', null, array('placeholder' => 'RCCM','class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Boîte postale </label>
                                                    {!! Form::text('code_postal', null, array('placeholder' => 'Boîte postale','class' => 'form-control', 'required'=>'required')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="form-group">
                                                    <label>Adresse</label>
                                                    {!! Form::textArea('adresse', null, array('placeholder' => 'Adresse','rows'=>'2', 'class' => 'form-control')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <!--end:: fields-->

                                        <div class="row">
                                            <div class="col-xl-4">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Région <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" id="kt_select2_state"
                                                            name="state_id" required>
                                                    </select>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin::Input-->
                                                <div class="form-group">
                                                    <label>Ville<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" id="kt_select2_ville"
                                                            name="city_id" required>
                                                    </select>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin::Select-->
                                                <div class="form-group">
                                                    <label>Quartier</label>
                                                    <select class="form-control select2" id="kt_select2_quartier"
                                                            name="quartier_id">
                                                    </select>
                                                </div>
                                                <!--end::Select-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Wizard Step 1-->
                                    <!--begin: Wizard Step 2-->
                                    <div class="pb-5" data-wizard-type="step-content">
                                        <!--begin::Input-->
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Catégorie <span class="text-danger">*</span></label>
                                                    <select class="form-control" id="category_id" name="category_id"
                                                            required>
                                                        @foreach($categories as $p)
                                                            <option value="{{ $p->id }}">{{ $p->libelle }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="form-group">
                                                    <label>Désignation <span class="text-danger">*</span></label>
                                                    {!! Form::text('nom_commerce', null, array('placeholder' => 'Désignation','class' => 'form-control', 'required'=>'required')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <label>Contact </label>
                                                {!! Form::text('contact', null, array('placeholder' => 'Contact','class' => 'form-control', 'required'=>'required')) !!}
                                            </div>
                                            <div class="col-xl-6">
                                                <label>Couleurs </label>
                                                <select name='couleurs' class="form-control selectpicker" required>
                                                    <option>Choisissez un dégradé</option>
                                                    <option value="#FA9D36-#121318"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, #FA9D36 -21.74%, #121318 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Marron
                                                    </option>
                                                    <option value="#FFFFFF-#FA9D36"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FA9D36 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Orange
                                                    </option>
                                                    <option value="#E55F25-#EB5429-#E42127"
                                                            data-content="&lt;span style='background: radial-gradient(29.84% 50% at 50% 50%, #E55F25 0%, #EB5429 44.27%, #E42127 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Rouge
                                                    </option>
                                                    <option value="#AE15E3-#E088FF"
                                                            data-content="&lt;span style='background: linear-gradient(285.49deg, #AE15E3 -1.3%, rgba(224, 136, 255, 0.59) 111.94%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Violet
                                                    </option>
                                                    <option value="#1CE500-#005C24"
                                                            data-content="&lt;span style='background: linear-gradient(107.62deg, #1CE600 -24.23%, #005C24 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Vert
                                                    </option>
                                                    <option value="#E3C215-#DD1A1A"
                                                            data-content="&lt;span style='background: linear-gradient(108.03deg, #E3C215 -21.74%, #DD1A1A 107.13%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Orange fonce
                                                    </option>
                                                    <option value="#FDF4E3-#000000"
                                                            data-content="&lt;span style='background: linear-gradient(108.08deg, #FDF4E3 -21.74%, #000000 102.81%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient gris
                                                    </option>
                                                    <option value="#00FDEE-#15B2E3"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, rgba(0, 253, 238, 0) -21.74%, #15B2E3 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Cyan
                                                    </option>
                                                    <option value="#056BC6-#000036"
                                                            data-content="&lt;span style='background: linear-gradient(108.24deg, #056BC6 -21.74%, #000036 103.15%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Bleu
                                                    </option>
                                                    <option value="#FFFFFF-#FFE4E4"
                                                            data-content="&lt;span style='background: linear-gradient(107.02deg, #FFFFFF -7.81%, #FFE4E4 102.68%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient blanc casse
                                                    </option>
                                                    <option value="#000000-#000000"
                                                            data-content="&lt;span style='background: linear-gradient(108.08deg, #000000 -21.74%, #000000 102.81%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Noir
                                                    </option>
                                                    <option value="#FFFFFF-#FF3299"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FF3299 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Rose
                                                    </option>
                                                    <option value="#FFFFFF-#FEDC01"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #FEDC01 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Jaune
                                                    </option>
                                                    <option value="#FFFFFF-#A7E425"
                                                            data-content="&lt;span style='background: linear-gradient(107.93deg, #FFFFFF -21.74%, #A7E425 112.31%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Vert clair
                                                    </option>
                                                    <option value="#F2DDB1-#C7A66E"
                                                            data-content="&lt;span style='background: radial-gradient(70.43% 217.7% at 43.72% 50.13%, #F2DDB1 0%, #C7A66E 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Or
                                                    </option>
                                                    <option value="#892D52-#762647"
                                                            data-content="&lt;span style='background: radial-gradient(70.43% 217.7% at 43.72% 50.13%, #892D52 0%, #762647 100%);width:85%;'         class='label  label-inline label-rounded'&gt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span&gt;">
                                                        Gradient Rouge sombre
                                                    </option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="row my-4">
                                            <div class="col-xl-4">
                                                <label>Logo Commerce </label>
                                                {!! Form::file('logo', null, array('placeholder' => 'logo','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Carte Commerce </label>
                                                {!! Form::file('card', null, array('placeholder' => 'logo','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Arriere Plan Commerce </label>
                                                {!! Form::file('background', null, array('placeholder' => 'logo','class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <label>Date de début <span class="text-danger">*</span></label>
                                                {!! Form::date('date_debut', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Durée (Mois) <span class="text-danger">*</span></label>
                                                {!! Form::select('duree',['1' => '1','3'=>'3','6'=>'6','9'=>'9','12'=>'12','24'=>'24','36'=>'36'],null, ['class'=>'form-control', 'required'=>'required','placeholder'=>'Durée']) !!}

                                            </div>
                                            <div class="col-xl-4">
                                                <label>Montant abonnement mensuel <span
                                                        class="text-danger">*</span></label>
                                                {!! Form::number('montant_abonnement', null, array('placeholder' => 'Montant abonnement','class' => 'form-control', 'required'=>'required')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <label>Taux commission <span class="text-danger">*</span></label>
                                                {!! Form::number('taux_commission', null, array('placeholder' => 'Taux commission','class' => 'form-control', 'required'=>'required','maxlength'=>'2','oninput'=>'maxLengthCheck(this)')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Taux Cashback <span class="text-danger">*</span></label>
                                                {!! Form::number('taux_cashback', null, array('placeholder' => 'Taux Cashback ','class' => 'form-control', 'required'=>'required','maxlength'=>'2','oninput'=>'maxLengthCheck(this)')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Montant Max Facture <span class="text-danger">*</span></label>
                                                {!! Form::number('mnt_max_facture', null, array('placeholder' => 'Montant Max Facture ','class' => 'form-control', 'required'=>'required')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <label>Montant Max Cashback <span class="text-danger">*</span></label>
                                                {!! Form::number('max_cashback', null, array('placeholder' => 'Montant Max Cashback ','class' => 'form-control', 'required'=>'required')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Taux Cashback VIP<span class="text-danger">*</span></label>
                                                {!! Form::number('taux_cashback_vip', null, array('placeholder' => 'Taux Cashback VIP','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-4">
                                                <label>Montant Max Cashback VIP <span
                                                        class="text-danger">*</span></label>
                                                {!! Form::number('max_cashback_vip', null, array('placeholder' => 'Montant Max Cashback VIP','class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <label>Localisation</label>
                                                {!! Form::text('localisation', null, array('placeholder' => 'Localisation','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-3">
                                                <label>Bonus parrain</label>
                                                {!! Form::text('referer_amount', null, array('placeholder' => 'Bonus parrain','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-3">
                                                <label>Bonus filleul</label>
                                                {!! Form::text('referee_amount', null, array('placeholder' => 'Bonus filleul','class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <label>Adresse</label>
                                                {!! Form::textArea('adresse_commerce', null, array('placeholder' => 'Adresse','rows'=>'3','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-xl-6">
                                                <label>Description </label>
                                                {!! Form::textArea('description_commerce', null, array('placeholder' => 'Description','rows'=>'3', 'class' => 'form-control')) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <label class="col-form-label">Horaires d'ouverture</label>
                                                {!! Form::textArea('horaires', null, array('placeholder' => 'Horaires du commerce','rows'=>'3', 'class' => 'form-control')) !!}

                                            </div>
                                        </div>
                                        <!--end::Input-->
                                    </div>
                                    <!--end: Wizard Step 2-->

                                    <!--begin: Wizard Actions-->
                                    <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                        <div class="mr-2">
                                            <button type="button"
                                                    class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4"
                                                    data-wizard-type="action-prev">Précédent
                                            </button>
                                        </div>
                                        <div>
                                            <input type="hidden" name="nbr_pv" id="nbr_pv" value="1">
                                            <button type="button"
                                                    class="btn btn-success font-weight-bolder text-uppercase px-9 py-4"
                                                    data-wizard-type="action-submit">Soumettre
                                            </button>
                                            <button type="button"
                                                    class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4"
                                                    data-wizard-type="action-next">Suivant
                                            </button>
                                        </div>
                                    </div>
                                    <!--end: Wizard Actions-->
                                    {!! Form::close() !!}
                                    <!--end: Wizard Form-->
                                </div>
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        const KTSelect2 = function () {
            // Private functions
            const demos = function () {
                // basic
                $('#kt_select2_pays').select2({
                    placeholder: "Selectionnez le pays"
                });

                $('#kt_select2_state').select2({
                    placeholder: "Selectionnez la province"
                });

                $('#kt_select2_ville').select2({
                    placeholder: "Selectionnez la ville"
                });

                $('#kt_select2_quartier').select2({
                    placeholder: "Selectionnez le quartier"
                });
            }

            const modalDemos = function () {
                $('#kt_select2_modal').on('shown.bs.modal', function () {
                    // basic
                    $('#kt_select2_pays_modal').select2({
                        placeholder: "Selectionnez le pays"
                    });
                    $('#kt_select2_state_modal').select2({
                        placeholder: "Selectionnez la province"
                    });
                    $('#kt_select2_ville_modal').select2({
                        placeholder: "Selectionnez la ville"
                    });

                    $('#kt_select2_quartier_modal').select2({
                        placeholder: "Selectionnez le quartier"
                    });
                });
            }

            return {
                init: function () {
                    demos();
                    modalDemos();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTSelect2.init();
        });

    </script>
    <script src="{{ asset('assets/plugins/custom/wizard/wizard_commerce.js') }}"></script>
    <script>
        $(document).ready(function () {

            $('#kt_select2_pays').on('change', function () {
                var PaysID = $(this).val();
                var phonelength = parseInt($(this).select2('data')[0].element.dataset['phonelength'] || 12);
                $('input[name=telephone]').attr('maxlength', phonelength).trigger('change');
                $('input[name=telephone1]').attr('maxlength', phonelength).trigger('change');

                if (PaysID) {
                    $.ajax({
                        url: '/getRegion/' + PaysID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#kt_select2_state').empty();
                                $('#kt_select2_ville').empty();
                                $('#kt_select2_quartier').empty();
                                // $('#kt_select2_state').append('<option hidden>Choisir une region</option>');
                                $.each(data, function (key, vale) {
                                    $('select[name="state_id"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#kt_select2_state').empty();
                                $('#kt_select2_ville').empty();
                                $('#kt_select2_quartier').empty();
                            }
                        }
                    });
                }
            });
            $('#kt_select2_state').on('change', function () {
                var RegionID = $(this).val();

                if (RegionID) {
                    $.ajax({
                        url: '/getVille/' + RegionID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#kt_select2_ville').empty();
                                $('#kt_select2_quartier').empty();
                                // $('#city_id').append('<option hidden>Choisir une ville</option>');
                                $.each(data, function (key, vale) {
                                    $('select[name="city_id"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#kt_select2_ville').empty();
                                $('#kt_select2_quartier').empty();
                            }
                        }
                    });
                }
            });
            $('#kt_select2_ville').on('change', function () {
                var CityID = $(this).val();
                if (CityID) {
                    $.ajax({
                        url: '/getQuartiers/' + CityID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#kt_select2_quartier').empty();
                                // $('#quartier_id').append('<option hidden>Choisir un Quartier</option>');
                                $.each(data, function (key, vale) {
                                    $('select[name="quartier_id"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#kt_select2_quartier').empty();
                            }
                        }
                    });
                }
            });
        }); // fin doc ready


        // $(document).ready(function(){
        function add_pointVente() {
            var nbrPv = $('#nbr_pv').val();
            var newN = eval(nbrPv) + 1;
            $('#nbr_pv').val(newN);
            $('div[id="kt_repeater_1"]').append("<div class='form-group row align-items-center' id='rep" + newN + "'><div class='col-md-3'><label>Code <span class='text-danger'>*</span>:</label><input type='text' class='form-control' maxlength='7' name='code_pv" + newN + "' placeholder='Code' required><div class='d-md-none mb-2'></div></div><div class='col-md-3'><label>Désignation  <span class='text-danger'>*</span>:</label><input type='text' class='form-control' name='lbl_pv" + newN + "' placeholder='Désignation' required><div class='d-md-none mb-2'></div></div><div class='col-md-6'><label>Adresse:</label><textarea class='form-control' rows='2' name='adresse_pv" + newN + "'> </textarea></div></div>");

        }

        function supprime_pv() {
            var nbrPv = $('#nbr_pv').val();
            if (nbrPv == 1) {
                alert("vous ne pouvez pas supprimer le 1er élément!");

            } else {
                $("div[id='rep" + nbrPv + "']").empty();
                lemoins = eval(nbrPv) - 1;
                $('#nbr_pv').val(eval(lemoins));
            }
        }

        // }); // fin doc ready
    </script>
@endsection
