@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Ajouter une catégorie de commerces")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Ajouter une catégorie de commerces
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">

            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('commerces.categories.save') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col col-12 col-md-12">
                        <div class="form-group">

                            <label for="libelle">Libéllé de la catégorie</label>
                            <input required name="libelle" value="{{ old('libelle') }}" id="libelle" placeholder="Libellé de la catégorie"
                                   class="form-control" type="text">
                        </div>
                    </div>
                    <div class="col col-12 col-md-12">
                        <div class="form-group">

                            <label for="libelle">(en)</label>
                            <input required name="libelle_en" value="{{ old('libelle_en') }}" id="libelle_en" placeholder="Libellé (en)"
                                   class="form-control" type="text">
                        </div>
                    </div>

                    <div class="col col-12 col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--end::Card-->

@endsection
