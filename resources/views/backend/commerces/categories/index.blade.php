@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des catégories de commerces")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Catégories de commerces
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('commerces.categories.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouvelle Catégorie
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Désignation</th>
                    <th>Anglais</th>
                    <th>Nbre de commerces</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $category)
                    <tr>
                        <td>
                            {{ $category->libelle }}
                        </td>
                        <td>
                            {{ $category->libelle_en }}
                        </td>
                        <td>{{ $category->commerces_count }}</td>
                        <td>
                            @can('commerce-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details"
                                   href="{{ route('commerces.categories.edit',$category->id) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
