@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des commerces")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                    @if($is_actif == false)
                            <h3 class="card-label">Liste des Commerces inactifs
                    @else
                            <h3 class="card-label">Liste des Commerces actifs
                    @endif
                             <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                <a href="{{ route('commerces.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouveau Commerce
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Propriétaire</th>
                    <th>Catégorie</th>
                    <th>Désignation</th>
                    {{-- <th>Contact</th> --}}
                    <th>Boosté</th>
                    <th>Points de vente</th>
                    <th>Nbre d'abonnés</th>
                    <th>Limite de scan</th>
                    <th>Ville</th>
                    <th>Pays</th>
                    <th>Créé le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->vendeur->nom ?? ''}}</td>
                        <td>{{ $p->categorie->libelle ?? '' }}</td>
                        <td>{{ $p->programme->libelle ?? '' }}</td>
                        {{-- <td>{{ $p->vendeur->user->telephone }}</td> --}}

                        <td>
                            @if($p->is_boost==1)
                                <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                            @else
                                <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                            @endif
                        </td>
                        <td>{{ count($p->pointVentes) ?? 0 }}</td>
                        <td>{{ $p->programme->nombreclients ?? 0 }}</td>
                        <td>
                            <span title="Limite par utilisateur">{{ $p->config?->ticket_scan_quota_per_user_daily }}</span> |
                            <span title="Limite pour tous les utilisateurs">{{ $p->config?->ticket_scan_quota_all_user_daily }}</span>
                        </td>

                        <td>{{ $p->ville->name ?? '' }}</td>
                        <td>{{ $p->ville->pays->name ?? ''}}</td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('commerces.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Cashbacks personnalisés" href="{{ route('custom-cashback.index',$p->id) }}">
                                <i class="fa fa-coins icon-md"></i>
                            </a>
                            @can('commerce-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('commerces.edit',$p->id) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            order: [[9, 'desc']],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    };

    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
