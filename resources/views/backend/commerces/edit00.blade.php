@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Mise à jour du commerce</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('commerces.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::model($data, ['route' => ['commerces.update', $data->id],'method' => 'PATCH', 'files' => true]) !!}
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="col-4 col-form-label">Propriétaire : </label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $data->vendeur->nom ?? '' }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label>Pays<span class="text-danger">*</span></label>
                            <select class="form-control" id="country_id"  name="country_id" required>
                                @foreach($pays as $p)
                                    @if($p->id == $pays_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Région <span class="text-danger">*</span></label>
                            <select class="form-control" id="state_id" name="state_id" required>
                                @foreach($regions as $p)
                                    @if($p->id == $state_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Ville<span class="text-danger">*</span></label>
                            <select class="form-control" id="city_id" name="city_id" required>
                                @foreach($villes as $p)
                                    @if($p->id == $data->city_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Quartier<span class="text-danger">*</span></label>
                            <select class="form-control" id="quartier_id" name="quartier_id" required>
                                @foreach($quartiers as $p)
                                    @if($p->id == $data->city_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Catégorie  <span class="text-danger">*</span></label>
                            <select class="form-control" id="category_id"  name="category_id" required>
                                @foreach($categories as $p)
                                    @if($p->id == $data->category_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->libelle }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->libelle }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label>Désignation  <span class="text-danger">*</span></label>
                            {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Contact  <span class="text-danger">*</span></label>
                            {!! Form::text('contact', null, array('placeholder' => 'Contact','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="col-lg-6">
                            <label>Description</label>
                            {!! Form::textArea('description', null, array('placeholder' => 'Nom','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Logo Commerce </label>
                            {!! Form::file('logo', null, array('placeholder' => 'logo','class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        $(document).ready(function(){
            $('#country_id').on('change', function(){
                var PaysID = $(this).val();
                if(PaysID){
                    $.ajax({
                        url:'/getRegion/'+PaysID,
                        type: "GET",
                        data: {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success: function(data){
                            if(data){
                                $('#state_id').empty(); $('#city_id').empty(); $('#quartier_id').empty();
                                $('#state_id').append('<option hidden>Choisir une region</option>');
                                $.each(data, function(key, vale){
                                    $('select[name="state_id"]').append('<option value="'+ vale.id+'">'+vale.name+'</option>');
                                });
                            }else{
                                $('#state_id').empty(); $('#city_id').empty();$('#quartier_id').empty();
                            }
                        }
                    });
                }
            });
            $('#state_id').on('change', function(){
                var RegionID = $(this).val();
                if(RegionID){
                    $.ajax({
                        url:'/getVille/'+RegionID,
                        type: "GET",
                        data: {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success: function(data){
                            if(data){
                                $('#city_id').empty(); $('#quartier_id').empty();
                                $('#city_id').append('<option hidden>Choisir une ville</option>');
                                $.each(data, function(key, vale){
                                    $('select[name="city_id"]').append('<option value="'+ vale.id+'">'+vale.name+'</option>');
                                });
                            }else{
                                $('#city_id').empty(); $('#quartier_id').empty();
                            }
                        }
                    });
                }
            });
            $('#city_id').on('change', function(){
                var CityID = $(this).val();
                if(CityID){
                    $.ajax({
                        url:'/getQuartiers/'+CityID,
                        type: "GET",
                        data: {"_token":"{{ csrf_token() }}"},
                        dataType: "json",
                        success: function(data){
                            if(data){
                                $('#quartier_id').empty();
                                $('#quartier_id').append('<option hidden>Choisir un Quartier</option>');
                                $.each(data, function(key, vale){
                                    $('select[name="quartier_id"]').append('<option value="'+ vale.id+'">'+vale.name+'</option>');
                                });
                            }else{
                                $('#quartier_id').empty();
                            }
                        }
                    });
                }
            });
        }); // fin doc ready

    </script>
@endsection
















































@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Mise à jour du package</h3>
                    </div>
                    <!--begin::Form-->
                {!! Form::model($data, ['route' => ['package.update', $data->id],'method' => 'PATCH']) !!}
                    <div class="card-body">
                        <div class="form-group">
                            <label>Désignation
                                <span class="text-danger">*</span></label>
                            {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Prix
                                <span class="text-danger">*</span></label>
                            {!! Form::number('prix', null, array('placeholder' => 'Prix','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Durée (Jours)
                                <span class="text-danger">*</span></label>
                            {!! Form::number('duree', null, array('placeholder' => 'Durée','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            {!! Form::textArea( 'description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <label>Pays<span class="text-danger">*</span></label>
                            <select class="form-control" name="country_id" required>
                                @foreach($pays as $p)
                                    @if($p->id == $data->country_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                {!! Form::close() !!}
                <!--end::Form-->
                </div>
        </div>
    </div>
@endsection
