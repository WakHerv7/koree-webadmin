@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des Configurations cashback")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Configurations cashback
                    <span class="d-block text-muted pt-2 font-size-sm">Pour le commerce {{ $commerce->nom }}</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('custom-cashback.create', ['id' => $commerce->id]) }}"
                       class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouvelle règle
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Propriétaire</th>
                    <th>Montant</th>
                    <th>Cashback(%)</th>
                    <th>Cashback VIP(%)</th>
                    <th>Status</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($commerce->customCashbacks as $cashback)
                    <tr>
                        <td>{{ $cashback->commerce->nom ?? ''}}</td>
                        <td>{{ $cashback->commerce->vendeur->nom ?? '' }}</td>
                        <td>{{ $cashback->max_amount }}</td>
                        <td>{{ $cashback->cashback_pct }}</td>
                        <td>{{ $cashback->vip_cashback_pct }}</td>
                        <td>
                            @if($cashback->active)
                                <span
                                    class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                            @else
                                <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                            @endif
                        </td>
                        <td nowrap="nowrap">
                            @if($cashback->active)
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Désactiver règle"
                                   href="{{ route('custom-cashback.lock',['c' => $commerce->id, 'r' => $cashback->id]) }}">
                                    <i class="fa fa-clipboard icon-md"></i>
                                </a>
                            @else
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Activer règle"
                                   href="{{ route('custom-cashback.unlock',['c' => $commerce->id, 'r' => $cashback->id]) }}">
                                    <i class="fa fa-clipboard-check icon-md"></i>
                                </a>
                            @endif
                            @can('commerce-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details"
                                   href="{{ route('custom-cashback.edit',['c' => $commerce->id, 'r' => $cashback->id]) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                            @can('commerce-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['custom-cashback.delete'],'id'=>'form-suppression'.$cashback->id,'style'=>'display:inline']) !!}
                                <input type="hidden" name="commerce" value="{{ $commerce->id }}"/>
                                <input type="hidden" name="rule" value="{{ $cashback->id }}"/>
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit"
                                        data-val="{{ $cashback->id }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
