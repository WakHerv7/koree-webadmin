@extends('layouts.main')

@section('title', 'Creation nouvelle règle pour '. $cashback->commerce->nom)

@section('content')
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Édition règle cashback
                    <span class="d-block text-muted pt-2 font-size-sm">Editer une règle pour configurer le cashback de {{ $cashback->commerce->nom }}</span>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('custom-cashback.index', ['id' => $cashback->commerce_id]) }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-file icon-md"></i>Afficher la liste
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('custom-cashback.update') }}" method="post" autocomplete="off">
                @csrf
                @method('PUT')
                <input type="hidden" name="commerce" value="{{ $cashback->commerce->id }}" />
                <input type="hidden" name="rule" value="{{ $cashback->id }}" />
                <div class="row">
                    <div class="col col-12 col-md-4">
                        <div class="form-group">
                            <label for="amount">Montant maximum commande</label>
                            <input type="text" value="{{ old('amount', $cashback->max_amount) }}" class="form-control" id="amount" name="amount" placeholder="Montant commande">
                        </div>
                    </div>

                    <div class="col col-12 col-md-4">
                        <div class="form-group">
                            <label for="commission">Cashback (%)</label>
                            <input type="text" value="{{ old('commission', $cashback->cashback_pct) }}" class="form-control" id="commission" name="commission" placeholder="Cashback (%)">
                        </div>
                    </div>

                    <div class="col col-12 col-md-4">
                        <div class="form-group">
                            <label for="vip_commission">Cashback VIP(%)</label>
                            <input type="text" value="{{ old('vip_commission', $cashback->vip_cashback_pct) }}" class="form-control" id="vip_commission" name="vip_commission" placeholder="Cashback VIP(%)">
                        </div>
                    </div>

                    @if(count($errors) > 0)
                        <div class="col col-12">
                            <div class="pb-0 alert alert-danger alert-dismissible fade show" role="alert">
                                <ul class="px-4 pt-1">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                    @endif
                    <div class="col col-12">
                        <button class="btn btn-primary">Mettre à jour</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
