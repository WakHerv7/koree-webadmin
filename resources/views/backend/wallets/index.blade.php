@extends('layouts.main')

@section('title', "Portefeuilles clients")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Portefeuilles client
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>

        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th style="width:1px; "></th>
                    <th>Abonnement</th>
                    <th>Client</th>
                    <th>Id Fidelite</th>
                    <th>Montant</th>
                    <th>Cashback</th>
                    <th>Topup</th>
                    <th>Bonus</th>
                    <th>Créé le</th>
                    <th width="80px">Actions</th>

                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td style="width:1px;color: white; ">{{  \Illuminate\Support\Carbon::parse($p->created_at)->timestamp * -1 }}</td>
                        <td>{{ $p->programme->libelle }}</td>
                        <td>{{ $p->client->nom." ".$p->client->prenom }}</td>
                        <td>{{ $p->id_fidelite }}</td>
                        <td>{{ number_format($p->montant,0,' ',' ') }}</td>
                        <td>{{ number_format($p->cashback,0,' ',' ') }}</td>
                        <td>{{ number_format($p->topup,0,' ',' ') }}</td>
                        <td>{{ number_format($p->bonus,0,' ',' ') }}</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($p->created_at)->format('Y-m-d') }}</td>

                        <td nowrap="nowrap">
                            <a href="{{ route('listing-transactions', ['wallet_client_id' => $p->id])}}" class="btn btn-sm btn-transparent-primary">Transactions</a>

                        {{--                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('wallets.show',$p->id) }}">--}}
{{--                                <i class="flaticon-eye icon-md"></i>--}}
{{--                            </a>--}}
{{--                            @can('programme-edit')--}}
{{--                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('wallets.edit',$p->id) }}">--}}
{{--                                    <i class='flaticon2-edit icon-md'></i>--}}
{{--                                </a>--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            order: [
                [8, 'desc']
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
