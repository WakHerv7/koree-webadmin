@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail du Programme
                            <span class="d-block text-muted pt-2 font-size-sm">infos complémentaires</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('programmes.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->commerce->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Désignation : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->libelle }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date de début : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->date_debut ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date expiration : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->date_fin ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Taux commission : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <span class="form-control-plaintext font-weight-bolder">{{ $data->taux_commission ?? '' }}</span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Cout wallet : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->bonus_wallet ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->commerce->ville->name ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Pays : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->commerce->ville->pays->name ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->created_at ?? '' }}</span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
