@php
    $headers = [
        'COMMERCE', 'CLIENT', 'TELEPHONE',
        'PARRAIN', 'TELEPHONE PARRAIN', 'AGENT',
        'TYPE', 'MONTANT', 'CASHBACK', 'TOPUP',
        'CASHBACK_OVER_TOPUP',
        'PAIEMENT', 'BONUS', 'PAYS', 'VILLE', 'DATE'
    ];
@endphp

<x-export-view
    :headers="$headers"
    caption="Koree | Historique Des Transactions"
>
    @foreach($transactions as $transaction)
        @php
            $localization = explode(',', $transaction->commerce_ville);
            $city = $localization[0] ?? '';
            $country = $localization[1] ?? '';
        @endphp

        <tr>
            <td>{{ $transaction->commerce }}</td>
            <td>{{ $transaction->client }}</td>
            <td>{{ $transaction->telephone }}</td>
            <td>{{ $transaction->referrer_fullName }}</td>
            <td>{{ $transaction->referrer_telephone }}</td>
            <td>{{ $transaction->agent }}</td>
            <td>{{ $transaction->type_transaction }}</td>
            <td>{{ $transaction->montant_facture }}</td>
            <td>{{ $transaction->cashback }}</td>
            <td>{{ $transaction->topup }}</td>
            <td>{{ $transaction->cashback_over_topup }}</td>
            <td>{{ $transaction->paiement }}</td>
            <td>{{ $transaction->bonus_parrainage }}</td>
            <td>{{ $country }}</td>
            <td>{{ $city }}</td>
            <td>{{ $transaction->created_at }}</td>
        </tr>
    @endforeach
</x-export-view>
