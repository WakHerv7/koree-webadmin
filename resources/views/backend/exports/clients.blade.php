@php
    $headers = [
        'NOM', 'PRENOM', 'TELEPHONE',
        'EMAIL', 'DATE DE NAISSANCE',
        'SEXE', 'STATUT', 'PAYS',
        'VILLE', 'QUARTIER', 'INSCRIT LE',
        'PREMIER COMMERCE', 'PREMIERE CARTE',
        'NOM PARRAIN INSCRIPTION',
        'CODE PARRAIN INSCRIPTION',
    ]
@endphp

<x-export-view
    :headers="$headers"
    caption="Koree | Liste Des Clients"
>
    @foreach($clients as $client)
        @php
            $referrer = $client->parrain?->parrain;

            $referrer_code = $referrer?->code_parrainage;
            $referrer_name = $referrer?->prenom . ' ' . $referrer?->nom;

            $first_wallet = $client->wallets()->orderBy('created_at', 'asc')->take(1)->first();
        @endphp
        <tr>
            <td>{{ $client->nom }}</td>
            <td>{{ $client->prenom }}</td>
            <td>{{ $client->user->telephone }}</td>
            <td>{{ $client->user->email }}</td>
            <td>{{ $client->date_naissance }}</td>
            <td>{{ $client->sexe }}</td>
            <td>{{ $client->status ? 'Actif' : 'Inactif' }}</td>
            <td>{{ $client->pays?->name }}</td>
            <td>{{ $client->quartier?->ville?->name }}</td>
            <td>{{ $client->quartier?->name }}</td>
            <td>{{ $client->created_at }}</td>
            <td>{{ $first_wallet?->programme?->commerce?->nom }}</td>
            <td>{{ $first_wallet?->id_fidelite }}</td>
            <td>{{ $referrer_name }}</td>
            <td>{{ $referrer_code }}</td>
        </tr>
    @endforeach
</x-export-view>
