@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Mise à jour du Programme</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('commerces.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::model($data, ['route' => ['programmes.update', $data->id],'method' => 'PATCH', 'files' => true]) !!}
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Commerce <span class="text-danger">*</span></label>
                            <select class="form-control" name="commerce_id" required>
                                @foreach($commerces as $p)
                                    @if($p->id == $data->commerce_id)
                                        <option value="{{ $p->id }}" selected>{{ $p->nom }}</option>
                                    @else
                                        <option value="{{ $p->id }}">{{ $p->nom }}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label>Désignation  <span class="text-danger">*</span></label>
                            {!! Form::text('libelle', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Date de début  <span class="text-danger">*</span></label>
                            {!! Form::date('date_debut', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="col-lg-6">
                            <label>Date d'expiration  <span class="text-danger">*</span></label>
                            {!! Form::date('date_fin', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Taux commission  <span class="text-danger">*</span></label>
                            {!! Form::number('taux_commission', null, array('placeholder' => 'Taux commission','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="col-lg-6">
                            <label>Cout wallet  <span class="text-danger">*</span></label>
                            {!! Form::number('bonus_wallet', null, array('placeholder' => 'Cout wallet ','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Description  <span class="text-danger">*</span></label>
                            {!! Form::textArea('description', null, array('placeholder' => 'Nom','class' => 'form-control','rows'=>'3', 'required'=>'required')) !!}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            {!! Form::close() !!}
            <!--end::Form-->
            </div>

        </div>
    </div>
@endsection
















































@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Mise à jour du package</h3>
                    </div>
                    <!--begin::Form-->
                {!! Form::model($data, ['route' => ['package.update', $data->id],'method' => 'PATCH']) !!}
                    <div class="card-body">
                        <div class="form-group">
                            <label>Désignation
                                <span class="text-danger">*</span></label>
                            {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Prix
                                <span class="text-danger">*</span></label>
                            {!! Form::number('prix', null, array('placeholder' => 'Prix','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Durée (Jours)
                                <span class="text-danger">*</span></label>
                            {!! Form::number('duree', null, array('placeholder' => 'Durée','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            {!! Form::textArea( 'description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                {!! Form::close() !!}
                <!--end::Form-->
                </div>
        </div>
    </div>
@endsection
