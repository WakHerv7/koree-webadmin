@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">User
                    @can('users-list')
                        <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('users-in') }}">Retour</a>
                    </span>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Name:</strong>
                        {{ $user->username }}
                    </div>
                    <div class="lead">
                        <strong>Email:</strong>
                        {{ $user->email }}
                    </div>
                    <div class="lead">
                        <strong>Telephone:</strong>
                        {{ $user->telephone }}
                    </div>
                    <div class="lead">
                        <strong>Role:</strong>
                        {{ $user->roles[0]->name }}
                    </div>
                    <div class="lead">
                        <strong>Password:</strong>
                        ********
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
