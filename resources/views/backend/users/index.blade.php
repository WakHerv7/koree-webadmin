@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Utilisateurs
                    <span class="d-block text-muted pt-2 font-size-sm">Utilisateurs internes (Administrateur , Agent)</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('users-create')
                    <a href="{{ route('users-in-create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouvel utilisateur
                    </a>
            @endcan
            <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Role</th>
                    <th>Nom</th>
                    <th>Email</th>
                    <th>Telephone</th>
                    <th>Date Creation</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $d)
                    <tr>
                        <td>{{ $d->roles[0]->name }}</td>
                        <td>{{ $d->username }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->telephone }}</td>
                        <td>{{ $d->created_at }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('users-in-show',$d->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('role-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('users-in-edit',$d->id) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                            @can('role-delete')
                                @if($d->roles[0]->name!= "Super-Admin")
                                    {!! Form::open(['method' => 'DELETE','route' => ['users-in-destroy', $d->id],'id'=>'form-suppression'.$d->id,'style'=>'display:inline']) !!}
                                    <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $d->id }}"><i class='flaticon-delete icon-md'></i></button>
                                    {!! Form::close() !!}
                                @endif
                            @endcan

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function() {

            var initTable1 = function() {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    order: [[4, 'desc']]
                });
            };


            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
