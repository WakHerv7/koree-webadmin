@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des roles
                    <span class="d-block text-muted pt-2 font-size-sm">roles des utilisateurs internes</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('role-create')
                <a href="{{ route('roles.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouveau Role
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('roles.show',$role->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('role-edit')
                                @if($role->name!= "Super-Admin")
                                    <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('roles.edit',$role->id) }}">
                                        <i class='flaticon2-edit icon-md'></i>
                                    </a>
                                @endif
                            @endcan
                            @can('role-delete')
                                @if($role->name!= "Super-Admin")
                                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'id'=>'form-suppression'.$role->id,'style'=>'display:inline']) !!}
                                    <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $role->id }}"><i class='flaticon-delete icon-md'></i></button>
                                    {!! Form::close() !!}
                                @endif
                            @endcan

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
