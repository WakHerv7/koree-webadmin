@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des propriétaires de commerce
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                <a href="{{ route('vendeurs.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouveau Propriétaire
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Telephone</th>
                    <th>Code de transaction</th>
                    <th>RCCM</th>
                    <th>Ville</th>
                    <th>Pays</th>
                    <th>Créé le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->nom }}</td>
                        <td>{{ wordwrap($p->adresse) }}</td>
                        <td>{{ $p->user->telephone }}</td>
                        <td>{{ $p->code_transaction ?? ''}}</td>
                        <td>{{ $p->rccm ?? '' }}</td>
                        <td>{{ $p->ville->name ?? ''}}</td>
                        <td>{{ $p->pays->name ?? ''}}</td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('vendeurs.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('commerce-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('vendeurs.edit',$p->id) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                            @can('commerce-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['vendeurs.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
