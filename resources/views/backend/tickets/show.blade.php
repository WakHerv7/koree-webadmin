@extends('layouts.main')

@section('title', 'Détails ticket')

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail de ticket
                            <span
                                class="d-block text-muted pt-2 font-size-sm">infos complémentaires {{$data->status}}</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('tickets.cashback.apply', [$data->id, 'charge']) }}"
                            @class(["btn btn-info btn-sm font-weight-bold", 'd-none' => $data->status !== 'pending'])>
                            <i class="flaticon2-accept"></i>Accorder le caskback</a>
                        <span class="ml-2"></span>
                        <a href="{{ route('tickets.cashback.apply', [$data->id, 'reject']) }}"
                           id="rejectCashbackOverTicketScan"
                            @class(["btn btn-warning btn-sm font-weight-bold", 'd-none' =>  $data->status !== 'pending'])>
                            <i class="flaticon2-delete"></i>Rejeter</a>
                        <span class="ml-2"></span>
                        <a href="{{ route('tickets.index', ['status' => 'pending']) }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commerce : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->wallet?->programme?->commerce->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Client : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->wallet?->client?->getFullname() ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <span class="form-control-plaintext font-weight-bolder">{{ $data->amount ?? '' }}</span>
                            </span>
                        </div>
                    </div>
                    @if($data->transaction)
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Cashback: </label>
                            <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <span
                                    class="form-control-plaintext font-weight-bolder">{{ $data->transaction?->montant ?? '' }}</span>
                            </span>
                            </div>
                        </div>
                    @endif
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Accorder : </label>
                        <div class="col-8">
                            <div>
                                @if($data->status === 'pending')
                                    <span
                                        class="label label-lg font-weight-bold label-light-dark label-inline">En attente</span>
                                @elseif($data->status === 'accepted')
                                    <span
                                        class="label label-lg font-weight-bold label-light-success label-inline">Accepter</span>
                                @elseif($data->status === 'cancelled')
                                    <span
                                        class="label label-lg font-weight-bold label-light-danger label-inline">Rejeter</span>
                                @else
                                    <span
                                        class="label label-lg font-weight-bold label-light-white label-inline">n/a</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->created_at ?? '' }}</span>
                        </div>
                    </div>

                    @if($data->status === 'accepted')

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Accepté le : </label>
                            <div class="col-8">
                                <span
                                    class="form-control-plaintext font-weight-bolder">{{ $data->charged_at ?? '' }}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Accepté par : </label>
                            <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->allower?->vendeur?->nom ?? "🕿 {$data->allower?->telephone}" ?? '' }}</span>
                            </div>
                        </div>
                    @endif

                    @if($data->status === 'cancelled')

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Rejeté le : </label>
                            <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->cancelled_at ?? '' }}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Rejeté par : </label>
                            <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->canceller?->vendeur?->nom ?? $data->canceller?->email ?? "🕿 {$data->canceller?->email}" ?? '' }}</span>
                            </div>
                        </div>

                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Motif : </label>
                            <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{Str::of($data->cancel_reason ?? '')->headline()}}</span>
                            </div>
                        </div>

                    @endif

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Image : </label>
                        <div class="col-8">
                            <a href="{{ $data->getMediaUrl() ?? '' }}" target="_blank">
                                <img class="img-thumbnail" src="{{ $data->getMediaUrl() ?? '' }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        const cancelReasons = {!! $cancel_reasons->toJson() !!};
        const promtReject = ($el) => bootbox.prompt({
            title: 'Choisir la raison du refus!',
            inputType: 'select',
            inputOptions: [
                {
                    text: 'Choisir une raison...',
                    value: ''
                }, ...cancelReasons],
            callback: function (result) {
                if (!!result) location.assign(`${$el.attr('href')}?cancel_reason=${result}`)

            }
        });
        $('#rejectCashbackOverTicketScan').on('click', function (e) {
            e.preventDefault()
            promtReject($(this))
        });
    </script>
@endpush
