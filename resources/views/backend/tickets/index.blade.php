@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste des tickets")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des tickets
                    <span class="d-block text-muted pt-2 font-size-sm">(cachback request: {{request('status')}})</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
{{--                <a href="{{ route('tickets.index') }}?status=accepted" class="btn btn-primary font-weight-bolder">--}}
{{--                    <i class="flaticon2-add-square icon-md"></i>voir les tickets cashbacks accordés--}}
{{--                </a>--}}
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Client</th>
                    <th>Montant</th>
                    <th>Cashback</th>
                    <th>Commerce</th>
                    <th>Accordé</th>
                    <th>Rejeté</th>
                    <th>Raison</th>
                    <th>Agent</th>
                    <th>Créé le</th>
                    <th>Décidé le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->wallet?->client?->getFullname() ?? ''}}</td>
                        <td>{{ $p->amount ?? ''}}</td>
                        <td>{{ $p->cashback ?? ''}}</td>
                        <td>{{ $p->wallet?->programme?->commerce?->nom }}</td>
                        <td>
                            @if($p->charged_at)
                                <span class="label label-lg font-weight-bold label-light-success label-inline"  title="Accordé le {{$p->charged_at}}">OUI</span>
                            @else
                                <span class="label label-lg font-weight-bold label-light-light text-black-50 label-inline">NON</span>
                            @endif
                        </td>
                        <td>
                            @if($p->cancelled_at)
                                <span class="label label-lg font-weight-bold label-light-danger label-inline"  title="Rejete le {{$p->cancelled_at}}">OUI</span>
                            @else
                                <span class="label label-lg font-weight-bold label-light-light text-black-50 label-inline">NON</span>
                            @endif
                        </td>
                        <td>{{Str::of($p->cancel_reason ?? '')->headline()}}</td>
                        <td>

                            @if($p->status === 'accepted')
                                <span>
                                    {{ $p->allower?->vendeur?->nom ?? $p->allower?->email ?? "🕿 {$p->allower?->telephone}" ?? '' }}
                                </span>
                            @elseif($p->status === 'cancelled')
                                <span>
                                    {{ $p->canceller?->vendeur?->nom ?? $p->canceller?->email ?? "🕿 {$p->canceller?->telephone}" ?? '' }}
                                </span>
                            @else
                                <span
                                    class="label label-lg font-weight-bold label-light-white label-inline">n/a</span>
                            @endif
                        </td>
                        <td>{{ $p->created_at ?? ''}}</td>
                        <td>
                            @if($p->cancelled_at)
                                <span> {{$p->cancelled_at}}</span>
                            @endif
                            @if($p->charged_at)
                                <span> {{$p->charged_at}}</span>
                            @endif
                        </td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('tickets.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            order: [[9, 'desc']],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    };

    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
