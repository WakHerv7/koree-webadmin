@extends('layouts.main')

@section('title', 'Liste des Abonnements')

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Abonnements
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('programmes.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouvel Abonnement
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Abonnement</th>
                    <th>Date debut</th>
                    <th>Date d'expiration</th>
                    <th>Montant Max Facture</th>
                    <th>Taux commission (%)</th>
                    <th>Cout Wallet</th>
                    <th>Statut</th>
                    <th>Pays</th>
                    <th>Créé le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->commerce->nom??'' }}</td>
                        <td>{{ $p->libelle ?? '' }}</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($p->date_debut)->format('Y-m-d') }}</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($p->date_fin)->format('Y-m-d') }}</td>
                        <td>{{ $p->mnt_max_facture  }}</td>
                        <td>{{ $p->taux_commission }}</td>
                        <td>{{ $p->bonus_wallet }}</td>
                        <td>
                            @if(!$p->expires)
                                <span
                                    class="label label-lg font-weight-bold label-light-success label-inline">ACTIF</span>
                            @else
                                <span
                                    class="label label-lg font-weight-bold label-light-danger label-inline">INACTIF</span>
                            @endif
                        </td>
                        <td>{{ $p->commerce->ville->pays->name ?? ''}}</td>
                        <td>{{ \Illuminate\Support\Carbon::parse($p->created_at)->format('Y-m-d')  }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details"
                               href="{{ route('programmes.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            {{--                            @can('programme-edit')--}}
                            {{--                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('programmes.edit',$p->id) }}">--}}
                            {{--                                    <i class='flaticon2-edit icon-md'></i>--}}
                            {{--                                </a>--}}
                            {{--                            @endcan--}}
                            {{--                            @can('role-delete')--}}
                            {{--                                {!! Form::open(['method' => 'DELETE','route' => ['programmes.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}--}}
                            {{--                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>--}}
                            {{--                                {!! Form::close() !!}--}}
                            {{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    order: [[9, 'desc']]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
