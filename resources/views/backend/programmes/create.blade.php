@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouvel abonnement</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('programmes.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::open(array('route' => 'programmes.store','method'=>'POST', 'files' => true)) !!}
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Commerce <span class="text-danger">*</span></label>
                                <select class="form-control" name="commerce_id" required>
                                    @foreach($commerces as $p)
                                        <option value="{{ $p->id }}">{{ $p->nom }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>Nom de l'Abonnement  <span class="text-danger">*</span></label>
                                {!! Form::text('libelle', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Date de début  <span class="text-danger">*</span></label>
                                {!! Form::date('date_debut', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                            <div class="col-lg-4">
                                <label>Durée (Jours) <span class="text-danger">*</span></label>
                                {!! Form::select('duree',['15' => '15','30'=>'30','90'=>'90','180'=>'180','360'=>'360'],null, ['class'=>'form-control', 'required'=>'required','placeholder'=>'Durée']) !!}

                            </div>
                            <div class="col-lg-4">
                                <label>Montant abonnement mensuel <span class="text-danger">*</span></label>
                                {!! Form::number('montant_abonnement', null, array('placeholder' => 'Montant abonnement','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-4">
                                <label>Taux commission  <span class="text-danger">*</span></label>
                                {!! Form::number('taux_commission', null, array('placeholder' => 'Taux commission','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                            <div class="col-lg-4">
                                <label>Cout wallet  <span class="text-danger">*</span></label>
                                {!! Form::number('bonus_wallet', null, array('placeholder' => 'Cout wallet ','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                            <div class="col-lg-4">
                                <label>Montant Max Facture  <span class="text-danger">*</span></label>
                                {!! Form::number('mnt_max_facture', null, array('placeholder' => 'Cout wallet ','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                <label>Description  <span class="text-danger">*</span></label>
                                {!! Form::textArea('description', null, array('placeholder' => 'Nom','class' => 'form-control','rows'=>'3', 'required'=>'required')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enrégistrer</button>
                    </div>
            {!! Form::close() !!}
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        $(document).ready(function(){

        }); // fin doc ready

    </script>
@endsection
