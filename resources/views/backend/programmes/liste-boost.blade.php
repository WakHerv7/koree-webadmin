@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Commerces boostés
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('role-create')
                <a href="#" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Booster un Commerce
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Ordre</th>
                    <th>Commerce</th>
                    <th>Pack</th>
                    <th>Prix</th>
                    <th>Durée</th>
                    <th>Date expiration</th>
                    <th>Statut</th>
                    <th>Pays</th>
                    <th width="80px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $loop->index +1 }}</td>
                        <td>{{ $p->commerce->nom }}</td>
                        <td>{{ $p->package->nom }}</td>
                        <td>{{ $p->package->prix }}</td>
                        <td>{{ $p->package->duree }}</td>
                        <td>{{ $p->commerce->programme->date_fin }}</td>
                        <td>
                            @if($p->status==1)
                                <span class="label label-lg font-weight-bold label-light-success label-inline">Payé</span>
                            @elseif($p->status == 0)
                                <span class="label label-lg font-weight-bold label-light-danger label-inline">Non payé</span>
                            @endif
                        </td>
                        <td>{{ $p->package->pays->name }}</td>
                        <td nowrap="nowrap">
                            @can('role-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="#">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollY: '50vh',
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
