@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Mise à jour d'un agent</h3>
                        <div class="card-toolbar">
                            <a href="{{ route('agents.index') }}" class="btn btn-success btn-sm font-weight-bold">
                                <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                        </div>
                    </div>
                    <!--begin::Form-->
                {!! Form::model($data, ['route' => ['agents.update', $data->id],'method' => 'PATCH']) !!}
                <div class="card-body">
                    <div class="form-group">
                        <label>Commerce<span class="text-danger">*</span></label>
                        <select class="form-control" id="commerce_id" name="commerce_id" required>
                            @foreach($commerces as $p)
                                @if($p->id == $commerce_id)
                                    <option value="{{ $p->id }}" selected>{{ $p->nom }}</option>
                                @else
                                    <option value="{{ $p->id }}">{{ $p->nom }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Point de vente<span class="text-danger">*</span></label>
                        <select class="form-control" name="point_vente_id" required>
                            @foreach($pointventes as $p)
                                @if($p->id == $point_vente_id)
                                    <option value="{{ $p->id }}" selected>{{ $p->nom }}</option>
                                @else
                                    <option value="{{ $p->id }}">{{ $p->nom }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Code
                            <span class="text-danger">*</span></label>
                        {!! Form::text('code', null, array('placeholder' => 'Code','class' => 'form-control','maxlength'=>'5', 'required'=>'required')) !!}
                    </div>
                    <div class="form-group">
                        <label>Nom(s) & prénom(s)
                            <span class="text-danger">*</span></label>
                        {!! Form::text('nom_prenom', null, array('placeholder' => 'Nom(s) & prénom(s)','class' => 'form-control', 'required'=>'required')) !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="is_admin">Administrateur</label>
                        <select name="is_admin" class="form-control" id="is_admin">
                            <option value="non" @if(!$data->is_admin) selected @endif>NON</option>
                            <option value="oui" @if($data->is_admin) selected @endif>OUI</option>
                        </select>
                    </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
                {!! Form::close() !!}
                <!--end::Form-->
                </div>
        </div>
    </div>
@endsection

 @section('end_javascript')
            <script>
                $(document).ready(function(){

                    $('#commerce_id').on('change', function(){
                        var PaysID = $(this).val();
                        if(PaysID){
                            $.ajax({
                                url:'/getPointsVentes/'+PaysID,
                                type: "GET",
                                data: {"_token":"{{ csrf_token() }}"},
                                dataType: "json",
                                success: function(data){
                                    if(data){
                                        $('#point_vente_id').empty();
                                        $('#point_vente_id').append('<option hidden>Choisir un point de vente</option>');
                                        $.each(data, function(key, vale){
                                            $('select[name="point_vente_id"]').append('<option value="'+ vale.id+'">'+vale.code+' '+vale.nom+'</option>');
                                        });
                                    }else{
                                        $('#point_vente_id').empty();
                                    }
                                }
                            });
                        }
                    });
                }); // fin doc ready

            </script>
@endsection


