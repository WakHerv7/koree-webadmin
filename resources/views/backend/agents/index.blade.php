@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Agents
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('role-create')
                <a href="{{ route('agents.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouvel Agent
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Commerce</th>
                    <th>Point de vente</th>
                    <th>Code Pt Vente</th>
                    <th>Code Agent</th>
                    <th>Nom(s) & Prénom(s)</th>
                    <th>Admin?</th>
                    <th>Date de création</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->pointVente?->commerce->nom ?? ''}}</td>
                        <td>{{ $p->pointVente?->nom ?? ''}}</td>
                        <td>{{ $p->pointVente?->code ?? '' }}</td>
                        <td>{{ $p->code }}</td>
                        <td>{{ $p->nom_prenom }}</td>
                        <td>
                            @if($p->is_admin)
                                <span class="badge badge-success">OUI</span>
                            @else
                                <span class="badge badge-danger">NON</span>
                            @endif
                        </td>
                        <td>{{ $p->created_at }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('agents.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('role-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('agents.edit',$p->id) }}">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                            @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['agents.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            order: [6, 'desc'],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
