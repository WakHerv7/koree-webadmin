@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails de l'entreprise</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('delivery.company.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">

                    <h3 class="card-title">Entreprise</h3>


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Téléphone fixe : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company_phone'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Reference : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['reference'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant par kilometre : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['amount_per_kilometer'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['city_company'] }}</span>
                        </div>
                    </div>

                    @if(isset($data['neighborhood']) && is_array($data['neighborhood']))
                    
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quartier : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['neighborhood']['name'] }}</span>
                        </div>
                    </div>
                    
                    @endif

                    

                    

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['location']['lat'] }}
                                <br/><br/>
                                Longitude : {{ $data['location']['long'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['location']['lat'],$data['location']['long'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Document RCCM : </label>
                        <div class="col-8">
                            <a class="btn btn-primary btn-sm font-weight-bold" href="{{ $data['rccm']}}">Télécharger le document</a>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Logo : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Image Background : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['bg_image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Actif : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by']['first_name'].' '.$data['created_by']['last_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by']['first_name'].' '.$data['updated_by']['last_name'] }}</span>
                        </div>
                    </div>
                    

                    <br/>
                    <br/>
                    <br/>
                    <h3 class="card-title">Responsable de l'entreprise</h3>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['manager_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Email : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['manager_email'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Téléphone : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['manager_phone'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Document d'identification : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Recto:
                                <img src="{{ $data['identity_document'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                            <span class="form-control-plaintext font-weight-bolder">
                                Verso:
                                <img src="{{ $data['identity_document_2'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
