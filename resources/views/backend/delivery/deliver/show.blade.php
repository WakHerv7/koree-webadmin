@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails du livreur</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('delivery.company.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['user']['first_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Prenom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['user']['last_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date de naissance : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['user']['date_of_birth'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Téléphone : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['user']['phone'] }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Entreprise: </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Photo : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Document d'identification : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Recto:
                                <img src="{{ $data['identity_document'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                            <span class="form-control-plaintext font-weight-bolder">
                                Verso:
                                <img src="{{ $data['identity_document_2'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Permis de conduire : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Recto:
                                <img src="{{ $data['driving_license'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                            <span class="form-control-plaintext font-weight-bolder">
                                Verso:
                                <img src="{{ $data['driving_license_2'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Actif : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div>
                    
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
