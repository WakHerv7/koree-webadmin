@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des commandes
                    <!-- <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3> -->
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                <a href="#" class="btn btn-primary font-weight-bolder">
                    <i class="flaticon2-add-square icon-md"></i>Nouvelle commande
                </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Reference</th>
                    <th>Entreprise</th>
                    <th>Vendeur</th>
                    <th>Client</th>
                    <th>Montant</th>
                    <th>Statut</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ strtoupper($p['reference']) }}</td>
                        <td>{{ $p['company'] }}</td>
                        <td>
                            <ul>
                                <li><strong>Adresse:</strong> <br/>{{ $p['seller_address'] }}</li>
                                <li><strong>localisation:</strong> <br/><a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$p['seller_location']['lat'],$p['seller_location']['long'].'&z=17'}}">Open in Google Maps</a></li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><strong>Adresse:</strong> <br/>{{ $p['customer_address'] }}</li>
                                <li><strong>Telephone:</strong> <br/>{{ $p['customer_phone'] }}</li>
                                <li><strong>Localisation:</strong> <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$p['customer_location']['lat'],$p['customer_location']['long'].'&z=17'}}">Open in Google Maps</a></li>
                            </ul>
                        </td>
                        <td>{{ $p['amount'] }}</td>
                        <td>
                            @if($p['status'] == 'FINISH')
                                <span class="badge badge-success">Acheve</span>
                            @elseif($p['status'] == 'PENDING')
                                <span class="badge badge-warning">En cours</span>
                            @elseif($p['status'] == 'WAIT')
                                <span class="badge badge-danger">En attente</span>
                            @endif
                        </td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="#">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            @can('commerce-edit')
                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="#">
                                    <i class='flaticon2-edit icon-md'></i>
                                </a>
                            @endcan
                            @can('commerce-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['vendeurs.destroy', $p['id'] ],'id'=>'form-suppression'.$p['id'],'style'=>'display:inline']) !!}
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p['id'] }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollY: '50vh',
            scrollCollapse: true,
            dom: 'Bfrtip',
            buttons: [
                'copy',
                'csv', 'pdf',
                'excel', 
                // 'print'
            ],
        });
    };


    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
