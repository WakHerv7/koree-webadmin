@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails de la livraison</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('delivery.company.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Jour : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ date('Y-m-d', strtotime($data['start_delivery'])) }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Debut de la livraison : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ date('H:i:s', strtotime($data['start_delivery'])) }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Fin de la livraison : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ date('H:i:s', strtotime($data['end_delivery'])) }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Reference de la commande : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order']['reference'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant de la commande : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order']['amount'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Livreur : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['deliver'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Adresse commerce : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order']['seller_address'] }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation commerce : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['order']['seller_location']['lat'] }}
                                <br/><br/>
                                Longitude : {{ $data['order']['seller_location']['long'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['order']['seller_location']['lat'],$data['order']['seller_location']['long'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Adresse client : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order']['customer_address'] }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Telephone client : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['order']['customer_phone'] }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation client : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['order']['customer_location']['lat'] }}
                                <br/><br/>
                                Longitude : {{ $data['order']['customer_location']['long'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['order']['customer_location']['lat'],$data['order']['customer_location']['long'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Statut : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                @if($data['status'] == 'FINISH')
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">Livré</span>
                                @elseif($data['status'] == 'RUN')
                                    <span class="label label-lg font-weight-bold label-light-warning label-inline">En cours</span>
                                @elseif($data['status'] == 'WAIT')
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">En attente</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Actif : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div> --}}
                    
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
