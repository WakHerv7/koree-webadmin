@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détails du contrat</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('delivery.company.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">


                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Debut du contrat : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ date('Y-m-d H:i:s', strtotime($data['start_date'])) }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Fin du contrat : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ date('Y-m-d H:i:s', strtotime($data['end_date'])) }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Commission : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['commission_rate'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Contrat actif ? : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                 @if($data['is_active'])
                                    <span class="label label-lg font-weight-bold label-light-success label-inline">OUI</span>
                                @else
                                    <span class="label label-lg font-weight-bold label-light-danger label-inline">NON</span>
                                @endif
                            </span>
                        </div>
                    </div>
                    
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Créé le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['created_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['created_by'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Modifié le : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ getFormattedDate($data['updated_at']) }}</span>
                            <span class="form-control-plaintext font-weight-bolder">Par: {{ $data['updated_by'] }}</span>
                        </div>
                    </div>
                    

                    <br/>
                    <br/>
                    <br/>
                    <h3 class="card-title">Entreprise</h3>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['company_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Telephone : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['company_phone'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Montant par kilometre : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['amount_per_kilometer'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['city_company'] }}</span>
                        </div>
                    </div>

                    @if(isset($data['company'][0]['neighborhood']) && is_array($data['company'][0]['neighborhood']))
                    
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quartier : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['neighborhood']['name'] }}</span>
                        </div>
                    </div>
                    
                    @endif

                    {{-- <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['city_company'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quartier : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['neighborhood_company'] }}</span>
                        </div>
                    </div> --}}

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Localisation : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Latitude : {{ $data['company'][0]['location']['lat'] }}
                                <br/><br/>
                                Longitude : {{ $data['company'][0]['location']['long'] }}
                                <br/><br/>
                                <a target="_blank" href="{{ 'https://www.google.com/maps/?ll='.$data['company'][0]['location']['lat'],$data['company'][0]['location']['long'].'&z=17'}}">Open in Google Maps</a>
                            </span>
                            <br/>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Document RCCM : </label>
                        <div class="col-8">
                            <a class="btn btn-primary btn-sm font-weight-bold" href="{{ $data['company'][0]['rccm']}}">Télécharger le document</a>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Logo : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['company'][0]['image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Image Background : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                <img src="{{ $data['company'][0]['bg_image'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>


                    <h3 class="card-title">Responsable de l'entreprise</h3>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['manager_name'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Email : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['manager_email'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Téléphone : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data['company'][0]['manager_phone'] }}</span>
                        </div>
                    </div>

                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Document d'identification : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">
                                Recto:
                                <img src="{{ $data['company'][0]['identity_document'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                            <span class="form-control-plaintext font-weight-bolder">
                                Verso:
                                <img src="{{ $data['company'][0]['identity_document_2'] }}" style="border-radius:15px;" width="300px"/>
                            </span>
                        </div>
                    </div>

                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
