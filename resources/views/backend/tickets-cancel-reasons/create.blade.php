@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouvel motif de refus de tickets de cashback</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('tickets.cancel-reasons.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                {!! Form::open(array('route' => 'tickets.cancel-reasons.store','method'=>'POST', 'files' => false)) !!}
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Texte<span class="text-danger">*</span></label>
                                {!! Form::text('text', null, array('placeholder' => 'Text','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>Valeur (de préférence sans espace, exemple: invalid_ticket)<span class="text-danger">*</span></label>
                                {!! Form::text('value', null, array('placeholder' => 'exemple: invalid_ticket','class' => 'form-control', 'required'=>'required')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enrégistrer</button>
                    </div>
            {!! Form::close() !!}
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        $(document).ready(function(){

        }); // fin doc ready

    </script>
@endsection
