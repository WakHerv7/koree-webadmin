@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', "Liste motifs de refus des tickets")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des motifs de refus de tickets de cashback</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('tickets-cancel-reasons-create')
                    <a href="{{ route('tickets.cancel-reasons.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouveau motif
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Text</th>
                    <th>Valeur</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->text ?? ''}}</td>
                        <td>{{ $p->value ?? ''}}</td>
                        <td nowrap="nowrap">
                            @can('tickets-cancel-reasons-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['tickets.cancel-reasons.destroy', $p->id],'id'=>'form-suppression'.$p->id,'style'=>'display:inline']) !!}
                                <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $p->id }}"><i class='flaticon-delete icon-md'></i></button>
                                {!! Form::close() !!}
                            @endcan

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
    "use strict";
    var KTDatatablesBasicScrollable = function() {

    var initTable1 = function() {
        var table = $('#kt_datatable1');

        // begin first table
        table.DataTable({
            scrollX: true,
            scrollCollapse: true,
            dom: 'Bfrtip',
            order: [[9, 'desc']],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    };

    return {

    //main function to initiate the module
    init: function() {
    initTable1();
    },

    };

    }();

    jQuery(document).ready(function() {
    KTDatatablesBasicScrollable.init();
    });
    </script>
@endsection
