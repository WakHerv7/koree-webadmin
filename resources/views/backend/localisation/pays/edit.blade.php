@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Mise a jour
                    <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('users-in') }}">Retour à la liste</a>
                </span>
                </div>

                <div class="card-body">
                    {!! Form::model($user, ['route' => ['users-in-update', $user->id], 'method'=>'PUT']) !!}
                    <div class="form-group">
                        <strong>Name:</strong>
                        {!! Form::text('username', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Telephone:</strong>
                        {!! Form::text('telephone', null, array('placeholder' => 'Telephone','class' => 'form-control')) !!}
                    </div>
                    <div class="form-group">
                        <strong>Email:</strong>
                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                    </div>

                    <div class="form-group">
                        <strong>Role:</strong>
                        {!! Form::select('roles[]', $roles, $userRole, array('class' => 'form-control','multiple')) !!}
                    </div>
                    <button type="submit" class="btn btn-primary">Valider</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
