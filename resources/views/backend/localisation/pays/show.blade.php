@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Pays
                    @can('pays-list')
                        <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('pays.index') }}">Retour</a>
                    </span>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Nom:</strong>
                        {{ $data->name }}
                    </div>
                    <div class="lead">
                        <strong>Indice:</strong>
                        {{ $data->phonecode }}
                    </div>
                    <div class="lead">
                        <strong>ISO3:</strong>
                        {{ $data->iso3 }}
                    </div>
                    <div class="lead">
                        <strong>Capitale:</strong>
                        {{ $data->capital }}
                    </div>
                    <div class="lead">
                        <strong>Devise :</strong>
                        {{ $data->currency }}
                    </div>
                    <div class="lead">
                        <strong>Latitude:</strong>
                        {{ $data->latitude }}
                    </div>
                    <div class="lead">
                        <strong>Longitude :</strong>
                        {{ $data->longitude }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
