@extends('layouts.main')
@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Pays
                    <span class="d-block text-muted pt-2 font-size-sm"> </span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @if(1 == 2)
                    @can('pays-create')
                        <a href="#" class="btn btn-primary font-weight-bolder">
                            <i class="flaticon2-add-square icon-md"></i>Nouveau Pays
                        </a>
                    @endcan
                @endif
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Indice</th>
                    <th>ISO3</th>
                    <th>Capitale</th>
                    <th>Devise</th>
                    <th>Latitude</th>
                    <th>longitude</th>
                    <th width="280px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $key => $d)
                    <tr>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->phonecode }}</td>
                        <td>{{ $d->iso3 }}</td>
                        <td>{{ $d->capital }}</td>
                        <td>{{ $d->currency }}</td>
                        <td>{{ $d->latitude }}</td>
                        <td>{{ $d->longitude }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="{{ route('pays.show',$d->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            <a class="btn btn-sm btn-clean mr-2" title="Voir les villes"
                                     href="{{ route('villes.index',['country_id'=>$d->id]) }}">
                                <small>Villes</small>
                            </a>
{{--                            @can('role-edit')--}}
{{--                                @if($d->name!= "Super-Admin")--}}
{{--                                    <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('pays.edit',$d->id) }}">--}}
{{--                                        <i class='flaticon2-edit icon-md'></i>--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            @endcan--}}
{{--                            @can('role-delete')--}}
{{--                                @if($d->name!= "Super-Admin")--}}
{{--                                    {!! Form::open(['method' => 'DELETE','route' => ['pays.destroy', $d->id],'id'=>'form-suppression'.$d->id,'style'=>'display:inline']) !!}--}}
{{--                                    <button class="btn btn-sm btn-clean btn-icon delete-form-row" type="submit" data-val="{{ $d->id }}"><i class='flaticon-delete icon-md'></i></button>--}}
{{--                                    {!! Form::close() !!}--}}
{{--                                @endif--}}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function() {

            var initTable1 = function() {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                });
            };


            return {

                //main function to initiate the module
                init: function() {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function() {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
