@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Something went wrong, please check below errors.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Mise a jour
                    <span class="float-right">
                    <a class="btn btn-primary" href="{{ route('quartiers.index') }}">Retour à la liste</a>
                </span>
                </div>

                <div class="card-body">
                    <form class="row" action="{{ route('quartiers.update', ['quartier' => $quartier->id]) }}"
                          method="post"
                          autocomplete="off">
                        @csrf
                        @method('put')
                        <div class="col col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="name">Nom du quartier</label>
                                <input type="text" id="name" name="name" value="{{ old('name') ?? $quartier->name }}" class="form-control">
                            </div>
                        </div>

                        <div class="col col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="kt_select2_pays">Selectionnez le pays</label>
                                <select class="form-control select2" id="kt_select2_pays" name="pays">
                                    @foreach($pays as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="kt_select2_ville">Selectionnez la ville</label>
                                <select class="form-control select2" name="ville" id="kt_select2_ville">
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="latitude">Latitude du quartier</label>
                                <input type="text" id="latitude" name="latitude" value="{{ old('latitude') ?? $quartier->latitude }}" class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="longitude">Longitude du quartier</label>
                                <input type="text" name="longitude" id="longitude" value="{{ old('longitude') ?? $quartier->longitude }}" class="form-control">
                            </div>
                        </div>
                        <div class="col col-12">
                            <button class="btn btn-primary" type="submit">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('end_javascript')
    <script>
        // Class definition
        const KTSelect2 = function () {
            // Private functions
            const demos = function () {
                // basic
                $('#kt_select2_pays').select2({
                    placeholder: "Selectionnez le pays"
                });

                $('#kt_select2_ville').select2({
                    placeholder: "Selectionnez la ville"
                });
            }

            const modalDemos = function () {
                $('#kt_select2_modal').on('shown.bs.modal', function () {
                    // basic
                    $('#kt_select2_pays_modal').select2({
                        placeholder: "Selectionnez le pays"
                    });
                    $('#kt_select2_ville_modal').select2({
                        placeholder: "Selectionnez la ville"
                    });
                });
            }

            return {
                init: function () {
                    demos();
                    modalDemos();
                }
            };
        }();

        // Initialization
        jQuery(document).ready(function () {
            KTSelect2.init();
        });

        window.onload = () => {
            const cities = (@json($villes))
            const country = (@json($pays->first()->id))

            const options = (cities.filter(it => {
                return it.country_id == country
            })).map(it => {
                return `<option value="${it.id}">${it.name}</option>`
            })

            $("#kt_select2_ville").html(options)
        }

        $("#kt_select2_pays").change(() => {
            const cities = (@json($villes))
            const country = (@json($pays)).filter(it => {
                return it.id == $("#kt_select2_pays").val()
            })[0].id

            const options = (cities.filter(it => {
                return it.country_id == country
            })).map(it => {
                return `<option value="${it.id}">${it.name}</option>`
            })

            $("#kt_select2_ville").html(options)
        })
    </script>
@endsection
