@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card">
                <div class="card-header">Villes
                    @can('pays-list')
                        <span class="float-right">
                        <a class="btn btn-primary" href="{{ route('villes.index') }}">Retour</a>
                    </span>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="lead">
                        <strong>Nom:</strong>
                        {{ $city->name }}
                    </div>
                    <div class="lead">
                        <strong>Pays:</strong>
                        {{ $city->pays->name }}
                    </div>
                    <div class="lead">
                        <strong>Province:</strong>
                        {{ $city->region->name }}
                    </div>
                    <div class="lead">
                        <strong>Latitude:</strong>
                        {{ $city->latitude }}
                    </div>
                    <div class="lead">
                        <strong>Longitude:</strong>
                        {{ $city->longitude }}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
