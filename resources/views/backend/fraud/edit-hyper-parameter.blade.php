@extends('layouts.main')

@section('title', 'Fraudes & Litiges | Hyper paramètres')

@section('content')
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Création d'hyper paramètres
                    <span class="d-block text-muted pt-2 font-size-sm">Ajouter des nouvelles règles pour configurer la fraud modération</span>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('fraud.hyper-parameters') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-file icon-md"></i>Afficher la liste
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('fraud.hyper-parameters.update') }}" method="post" autocomplete="off">
                @csrf
                @method('put')
                <input type="hidden" name="id" value="{{ $config->id }}"/>
                <div class="row">
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="limit">Limite</label>
                            <input type="text" value="{{ old('limit') ?? $config->value }}" class="form-control"
                                   id="limit" name="limit" placeholder="Limite">
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="interval_value">Intervalle</label>
                            <input type="text" value="{{ old('interval_value') ?? $config->interval_value }}"
                                   class="form-control" id="interval_value" name="interval_value"
                                   placeholder="Intervalle">
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="interval_unit">Unité</label>
                            <select class="form-control" name="interval_unit" id="interval_unit">
                                <option {{ $config->interval_unit == "minute" ? "selected" : "" }} value="minute">MINUTE
                                </option>
                                <option {{ $config->interval_unit == "hour" ? "selected" : "" }} value="hour">HEURE
                                </option>
                                <option {{ $config->interval_unit == "day" ? "selected" : "" }} value="day">JOUR
                                </option>
                                <option {{ $config->interval_unit == "week" ? "selected" : "" }} value="week">SEMAINE
                                </option>
                                <option {{ $config->interval_unit == "month" ? "selected" : "" }} value="month">MOIS
                                </option>
                                <option {{ $config->interval_unit == "year" ? "selected" : "" }} value="year">ANNÉE
                                </option>
                                <option {{ $config->interval_unit == "infinity" ? "selected" : "" }} value="infinity">
                                    INDÉFINI
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="transaction_type">Type de transaction</label>
                            <select class="form-control" name="transaction_type" id="transaction_type">
                                <option {{ $config->transaction_type == "all" ? "selected" : "" }} value="all">
                                    TOUT
                                </option>
                                <option {{ $config->transaction_type == "topup" ? "selected" : "" }} value="topup">
                                    TOPUP
                                </option>
                                <option  {{ $config->transaction_type == "paiement" ? "selected" : "" }} value="paiement">PAIEMENT</option>
                                <option
                                    {{ $config->transaction_type == "cashback" ? "selected" : "" }} value="cashback">
                                    CASHBACK
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="global">Global</label>
                            <select class="form-control" name="global" id="global">
                                <option {{ $config->global ? "selected" : "" }} value="true">OUI</option>
                                <option {{ !$config->global ? "selected" : "" }} value="false">NON</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="agent">Agent</label>
                            <select class="form-control" name="agent" id="agent">
                                <option {{ $config->look_agent ? "selected" : "" }} value="true">OUI</option>
                                <option {{ !$config->look_agent ? "selected" : "" }} value="false">NON</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="notify_shop_owner">Notifier vendeur</label>
                            <select class="form-control" name="notify_shop_owner" id="notify_shop_owner">
                                <option {{ $config->notify_shop_owner ? "selected" : "" }} value="true">OUI</option>
                                <option {{ !$config->notify_shop_owner ? "selected" : "" }} value="false">NON</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="notify_admin">Notifier Admin</label>
                            <select class="form-control" name="notify_admin" id="notify_admin">
                                <option {{ $config->notify_admin ? "selected" : "" }} value="true">OUI</option>
                                <option {{ !$config->notify_admin ? "selected" : "" }} value="false">NON</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="restrict">Bloquer le compte</label>
                            <select class="form-control" name="restrict" id="restrict">
                                <option {{ $config->restrict == 'yes' ? "selected" : "" }} value="yes">OUI</option>
                                <option {{ $config->restrict == 'no' ? "selected" : "" }} value="no">NON</option>
                                <option {{ $config->restrict == 'pending' ? "selected" : "" }} value="pending">LAISSER
                                    L'ADMIN DÉCIDER
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="flag_relatives">Inclure les proches</label>
                            <select class="form-control" name="flag_relatives" id="flag_relatives">
                                <option {{ !$config->flag_relatives ? "selected" : "" }} value="false">NON</option>
                                <option {{ $config->flag_relatives ? "selected" : "" }} value="true">OUI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="restrict_duration_value">Durée de la sanction</label>
                            <input value="{{ old('restrict_duration_value') ?? $config->restriction_duration_value }}" class="form-control" type="text"
                                   placeholder="Durée de la sanction"
                                   name="restrict_duration_value" id="restrict_duration_value"/>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="restrict_duration_unit">Unitée de la durée</label>
                            <select class="form-control" name="restrict_duration_unit" id="restrict_duration_unit">
                                <option {{ $config->restriction_duration_unit == "minute" ? "selected" : "" }} value="minute">MINUTE</option>
                                <option {{ $config->restriction_duration_unit == "hour" ? "selected" : "" }} value="hour">HEURE</option>
                                <option {{ $config->restriction_duration_unit == "day" ? "selected" : "" }} value="day">JOUR</option>
                                <option {{ $config->restriction_duration_unit == "week" ? "selected" : "" }} value="week">SEMAINE</option>
                                <option {{ $config->restriction_duration_unit == "month" ? "selected" : "" }} value="month">MOIS</option>
                                <option {{ $config->restriction_duration_unit == "year" ? "selected" : "" }} value="year">ANNÉE</option>
                                <option {{ $config->restriction_duration_unit == "infinity" ? "selected" : "" }} value="infinity">INDÉFINI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="score">Score</label>
                            <input value="{{ old('score') ?? $config->score }}" placeholder="Score" class="form-control" name="score"
                                   id="score"/>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="include_shop_limit">Inclure la limite du commerce</label>
                            <select class="form-control" name="include_shop_limit" id="include_shop_limit">
                                <option {{ !$config->compare_to_shop_limit ? "selected" : "" }} value="false">NON</option>
                                <option {{ $config->compare_to_shop_limit ? "selected" : "" }} value="true">OUI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="authorize">Autoriser la transaction</label>
                            <select class="form-control" name="authorize" id="authorize">
                                <option {{ $config->authorize ? "selected" : "" }} value="true">OUI</option>
                                <option {{ !$config->authorize ? "selected" : "" }} value="false">NON</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="form-group">
                            <label for="value_type">Comment calculer</label>
                            <select class="form-control" name="value_type" id="value_type">
                                <option {{ $config->value_type == 'amount' ? 'selected' : '' }} value="amount">Montant des transactions</option>
                                <option {{ $config->value_type == 'count' ? 'selected' : '' }} value="count">Nombre de transactions</option>
                            </select>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="vendor_message">Méssage vendeur</label>
                            <textarea name="vendor_message" class="form-control resize-none" id="vendor_message" rows="4" >{{ old('vendor_message') ?? $config->vendor_message }}</textarea>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="admin_message">Méssage admin</label>
                            <textarea name="admin_message" class="form-control resize-none" id="admin_message" rows="4" >{{ old('admin_message') ?? $config->admin_message }}</textarea>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="client_message">Méssage client</label>
                            <textarea name="client_message" class="form-control resize-none" id="client_message" rows="4" >{{ old('client_message') ?? $config->client_message }}</textarea>
                        </div>
                    </div>
                    @if(count($errors) > 0)
                        <div class="col col-12">
                            <div class="pb-0 alert alert-danger alert-dismissible fade show" role="alert">
                                <ul class="px-4 pt-1">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                    @endif
                    <div class="col col-12">
                        <button class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
