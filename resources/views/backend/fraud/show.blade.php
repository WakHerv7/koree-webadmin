@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', 'Detail de flag')


@section('content')
    <div class="card card-custom gutter-b border-2">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Details sur un flag
                    <span class="d-block text-muted pt-2 font-size-sm">Détails sur un transaction jugée suspecte par le système</span>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('fraud.index') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon-list icon-md"></i>Liste des flags
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <h2>Details sur le client</h2>
            <div class="row mb-12">
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Nom</label>
                    <h2 class="font-size-h3">{{ $trace->client->nom }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Prénom</label>
                    <h2 class="font-size-h3">{{ $trace->client->prenom }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Sexe</label>
                    <h2 class="font-size-h3">{{ $trace->client->sexe }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Date de naissance</label>
                    <h2 class="font-size-h3">{{ $trace->client->date_naissance?->format("D d M Y") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Pays</label>
                    <h2 class="font-size-h3">{{ $trace->client->pays->name }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Ville</label>
                    <h2 class="font-size-h3">{{ $trace->client->ville?->name }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Lien du profil</label>
                    <h2 class="font-size-h3"><a href="{{ route('clients.show', ['client' => $trace->client->id]) }}">{{ route('clients.show', ['client' => $trace->client->id]) }}</a>
                    </h2>
                </div>
            </div>

            <h2>Details sur le vendeur</h2>
            <div class="row mb-12">
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Nom</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->nom }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Fonction</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->fonction }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Adresse</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->adresse }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Tel. fixe</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->tel_fixe }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Pays</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->pays->name }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Ville</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->ville->name }}</h2>
                </div>
            </div>

            <h2>Details sur le commerce</h2>
            <div class="row mb-12">
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Nom</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->nom }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Contact</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->contact }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Catégorie</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->categorie?->libelle }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Pays</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->ville->pays->name }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Ville</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->ville->name }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Quartier</label>
                    <h2 class="font-size-h3">{{ $trace->vendeur->commerce->quartier->name }}</h2>
                </div>
            </div>

            @if($trace->transaction != null)
                <h2>Details sur la transaction</h2>
                <div class="row mb-12">
                    <div class="col col-12 col-md-6 col-lg-4 mt-6">
                        <label class="font-size-h4">Montant</label>
                        <h2 class="font-size-h3">{{ $trace->transaction->montant_achat }}</h2>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 mt-6">
                        <label class="font-size-h4">Gain</label>
                        <h2 class="font-size-h3">{{ $trace->transaction->gain }}</h2>
                    </div>
                    <div class="col col-12 col-md-6 col-lg-4 mt-6">
                        <label class="font-size-h4">Type de transaction</label>
                        <h2 class="font-size-h3">{{ strtoupper($trace->transaction->type_transaction) }}</h2>
                    </div>
                </div>
            @endif

            <h2>Details sur la règle enfreinte</h2>
            <div class="row mb-12">
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Limite</label>
                    <h2 class="font-size-h3">{{ $trace->fraud->value." ".strtoupper($trace->fraud->value_type == "count" ? "transactions" : "") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Intervalle</label>
                    <h2 class="font-size-h3">{{ $trace->fraud->restriction_duration_unit == "infinity" ? "GLOBAL" : $trace->fraud->interval_value." ".strtoupper($trace->fraud->interval_unit) }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Type de transaction</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->transaction_type) }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Score</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->score) }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Scope</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->global ? "tout commerce confondu" : "pour un commerce spécifique") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Autoriser ?</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->authorize ? "OUI" : "NON") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Bloquer le compte ?</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->restrict == "yes" ? "OUI" : "NON") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Inclure le parrain/filleuls ?</label>
                    <h2 class="font-size-h3">{{ strtoupper($trace->fraud->flag_relatives ? "OUI" : "NON") }}</h2>
                </div>
                <div class="col col-12 col-md-6 col-lg-4 mt-6">
                    <label class="font-size-h4">Durée sanction</label>
                    <h2 class="font-size-h3">{{ $trace->fraud->restriction_duration_unit == "infinity" ? "INDEFINI" : $trace->fraud->restriction_duration_value." ".strtoupper($trace->fraud->restriction_duration_unit) }}</h2>
                </div>
            </div>

            @if($trace->pending)
                <h2>Actions à faire</h2>
                <div>
                    <form method="post" action="{{ route('fraud.process.pending') }}" class="row">
                        @csrf
                        @method('put')
                        <input type="hidden" name="id" value="{{ $trace->id }}">
                        <div class="col col-12 col-md-6 col-lg-3 mt-6">
                            <div class="form-group">
                                <label for="restrict">Bloquer le compte</label>
                                <select class="form-control" name="restrict" id="restrict">
                                    <option {{ old('restrict') == 'yes' ? 'selected' : '' }} value="yes">OUI</option>
                                    <option {{ old('restrict') != 'yes' ? 'selected' : '' }} value="no">NON</option>
                                </select>
                            </div>
                        </div>

                        <div class="col col-12 col-md-6 col-lg-3 mt-6">
                            <div class="form-group">
                                <label for="flag_relatives">Inclure les proches</label>
                                <select class="form-control" name="flag_relatives" id="flag_relatives">
                                    <option {{ old('flag_relatives') == 'false' ? 'selected' : '' }} value="false">NON
                                    </option>
                                    <option {{ old('flag_relatives') == 'true' ? 'selected' : '' }} value="true">OUI
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col col-12 col-md-6 col-lg-3 mt-6">
                            <div class="form-group">
                                <label for="restrict_duration_value">Durée de la sanction</label>
                                <input value="{{ old('restrict_duration_value') }}" class="form-control" type="text"
                                       placeholder="Durée de la sanction"
                                       name="restrict_duration_value" id="restrict_duration_value"/>
                            </div>
                        </div>

                        <div class="col col-12 col-md-6 col-lg-3 mt-6">
                            <div class="form-group">
                                <label for="restrict_duration_unit">Unitée de la durée</label>
                                <select class="form-control" name="restrict_duration_unit" id="restrict_duration_unit">
                                    <option value="minute">MINUTE</option>
                                    <option value="hour">HEURE</option>
                                    <option value="day">JOUR</option>
                                    <option value="week">SEMAINE</option>
                                    <option value="month">MOIS</option>
                                    <option value="year">ANNÉE</option>
                                    <option value="infinity">INDÉFINI</option>
                                </select>
                            </div>
                        </div>
                        @if(count($errors) > 0)
                            <div class="col col-12">
                                <div class="pb-0 alert alert-danger alert-dismissible fade show" role="alert">
                                    <ul class="px-4 pt-1">
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        @endif
                        <div class="col col-12">
                            <button class="btn btn-primary">Enregistrer</button>
                        </div>
                    </form>
                </div>
            @endif

        </div>
    </div>
@endsection
