@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', 'Fraudes & Litiges')


@section('content')
    <div class="card card-custom gutter-b border-2">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Transaction suspectes
                    <span class="d-block text-muted pt-2 font-size-sm">Transaction ayant déclenché les seuils de normalité</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('fraud.pending-validation') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon-list icon-md"></i>En attente de validation
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>client</th>
                    <th>vendeur</th>
                    <th>Commerce</th>
                    <th>Autorisée?</th>
                    <th>Type de transaction</th>
                    <th>Montant</th>
                    <th>Score généré</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($frauds as $fraud)
                    <tr>
                        <td>{{ $fraud->client->nom }}</td>
                        <td>{{ $fraud->vendeur->nom }}</td>
                        <td>{{ $fraud->vendeur->commerce->nom }}</td>
                        <td>{{ $fraud->authorized ? 'OUI': 'NON' }}</td>
                        <td>{{ strtoupper($fraud->transaction_type) }}</td>
                        <td>{{ $fraud->amount }}</td>
                        <td>{{ $fraud->fraud->score }}</td>
                        <td>{{ $fraud->created_at->format('d M Y H:i') }}</td>
                        <td>
                            <a class="btn btn-sm btn-clean btn-icon" title="Éditer règle"
                               href="{{ route('fraud.show', ['id' => $fraud->id]) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    order: [
                        [7, 'desc']
                    ],
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
