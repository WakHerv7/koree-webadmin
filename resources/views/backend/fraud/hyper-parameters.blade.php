@php
    $wide = true;
@endphp
@extends('layouts.main')

@section('title', 'Fraudes & Litiges | Hyper paramètres')

@section('content')
    <div class="card card-custom gutter-b border-2">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Hyper paramètres
                    <span class="d-block text-muted pt-2 font-size-sm">pour configurer la fraud moderation</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('fraud.hyper-parameters.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouvelle règle
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Limite</th>
                    <th>Cumulé</th>
                    <th>Interval temporel</th>
                    <th>action à faire</th>
                    <th>Notifier</th>
                    <th>filleuls/parrain</th>
                    <th>Durée restriction</th>
                    <th>Score</th>
                    <th>autoriser</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($configs as $config)
                    <tr>
                        <td>{{ $config->displayLimit() }}</td>
                        <td>{{ $config->displayGlobal() }}</td>
                        <td>{{ $config->displayInterval() }}</td>
                        <td>{{ $config->displayWhatToDo() }}</td>
                        <td>{{ $config->displayWhoToNotify() }}</td>
                        <td>{{ $config->flag_relatives ? 'SANCTIONNER' : 'IGNORER' }}</td>
                        <td>{{ $config->displayInterval(true) }}</td>
                        <td>{{ $config->score }}</td>
                        <td>{{ $config->authorize ? 'OUI' : 'NON' }}</td>
                        <td>
                            <a class="btn btn-sm btn-clean btn-icon" title="Éditer règle"
                               href="{{ route('fraud.hyper-parameters.edit',  ['id' => $config->id]) }}">
                                <i class="flaticon2-pen icon-md"></i>
                            </a>
                            <form action="{{ route('fraud.hyper-parameters.delete') }}" method="post" class="d-inline">
                                @csrf
                                @method('delete')
                                <input type="hidden" name="id" value="{{ $config->id }}"/>
                                <button type="submit" class="btn btn-sm btn-clean btn-icon" title="Supprimer règle"
                                        href="">
                                    <i class="flaticon2-trash icon-md"></i>
                                </button>
                            </form>
                            @if($config->active)
                                <a class="btn btn-sm btn-clean btn-icon" title="Désactiver la règle"
                                   href="{{ route('fraud.toggle-status',  ['id' => $config->id]) }}">
                                    <i class="fas fa-lock icon-md"></i>
                                </a>
                            @else
                                <a class="btn btn-sm btn-clean btn-icon" title="Activer la règle"
                                   href="{{ route('fraud.toggle-status',  ['id' => $config->id]) }}">
                                    <i class="fas fa-lock-open icon-md"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollY: '50vh',
                    scrollX: true,
                    scrollCollapse: true,
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
