@extends('layouts.main')
@php
    $wide = true;
@endphp
@section('title', "Liste des clients")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des clients
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('role-create')
                    {{--                <a href="#" class="btn btn-primary font-weight-bolder">--}}
                    {{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau Client--}}
                    {{--                </a>--}}
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('clients.export') }}" method="GET" target="_blank">
                <div class="row mb-3">
                    <div class="col">
                        <input class="form-control" type="datetime-local" id="start_date" name="period_start"/>
                    </div>
                    <div class="col">
                        <input class="form-control" type="datetime-local" id="end_date" name="period_end"/>
                    </div>
                    <div class="col">
                        <input type="submit" class="btn btn-primary" value="Exporter" id="export_clients"/>
                    </div>
                </div>
            </form>
            <hr/>

            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Code parrain</th>
                    <th>Code parrainage</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Téléphone</th>
                    <th>Email</th>
                    <th>Date de naissance</th>
                    <th>Sexe</th>
                    <th>Statut</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Quartier</th>
                    <th>Créé le</th>
{{--                    <th>Modifié par</th>--}}
{{--                    <th>Modifié le</th>--}}
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {

                const documentTitle = "Liste des clients"
                // begin first table
                window.KtTable = $('#kt_datatable1').DataTable({

                    // paging: true,
                    scrollY: '60vh',
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    "pageLength": 50,
                    lengthMenu: [
                        [5, 10, 25, 50, 100],
                        [5, 10, 25, 50, 100]
                    ],
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtlip',
                    // buttons: true,
                    buttons: [
                        'copy',
                        'csv',
                        'excel',
                        // 'pdf',
                        // 'print'
                    ],
                    order: [
                        [12, 'desc']
                    ],
                    createdRow: function (row, data, dataIndex) {
                        // console.log("customersDataTable:createdRow row", data, dataIndex);
                        // $(row).attr('data-customerkey', data.customer_key);
                        // $(row).attr('data-customerid', data.customer_id);
                    },
                    ajax: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: function (d) {
                            {{--d.start_date = $("#start_date").val();--}}
                            {{--d.end_date = $("#end_date").val();--}}
                        },
                        url: "/clients/ajaxDatable?status={{request()->query('status', '1')}}",
                        type: "POST",
                    },
                    columns: [
                        {
                            data: "code_parrain"
                        },
                        {
                            data: "code_parrainage"
                        },
                        {
                            data: "nom"
                        },
                        {
                            data: "prenom"
                        },
                        {
                            data: "telephone"
                        },
                        {
                            data: "email"
                        },
                        {
                            data: "date_naissance"
                        },
                        {
                            data: "sexe"
                        },
                        {
                            data: "status",
                            render: function (datum, type, row) {
                                return $("<div/>").html(datum).text();
                            }
                        },
                        {
                            data: "country"
                        },
                        {
                            data: "city"
                        },
                        {
                            data: "district"
                        },
                        {
                            data: "created_at"
                        },
                        {
                            data: "actions",
                            render: function (datum, type, row) {
                                return $("<div/>").html(row.actions).text();
                            }
                        }
                    ]
                });
            };

            var reloadDatatable = function () {
                window.KtTable && window.KtTable.ajax.reload(null, false); // user paging is not reset on reload
            }
            setInterval(reloadDatatable, 300000); // refresh tous les 5 minutes


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
