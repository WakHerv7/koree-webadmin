@extends('layouts.main')
@php
    $wide = true;
@endphp
@section('title', "Liste des clients")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des clients
                    <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('role-create')
                    {{--                <a href="#" class="btn btn-primary font-weight-bolder">--}}
                    {{--                    <i class="flaticon2-add-square icon-md"></i>Nouveau Client--}}
                    {{--                </a>--}}
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Code parrain</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Code parrainage</th>
                    <th>Téléphone</th>
                    <th>Date de naissance</th>
                    <th>Sexe</th>
                    <th>Statut</th>
                    <th>Pays</th>
                    <th>Créé le</th>
                    <th>Modifié par</th>
                    <th>Modifié le</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Code parrain</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Code parrainage</th>
                    <th>Téléphone</th>
                    <th>Date de naissance</th>
                    <th>Sexe</th>
                    <th>Statut</th>
                    <th>Pays</th>
                    <th>Créé le</th>
                    <th>Modifié par</th>
                    <th>Modifié le</th>
                    <th width="180px">Actions</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($data as $key => $p)
                    <tr>
                        <td>{{ $p->parrain?->parrain?->code_parrainage }}</td>
                        <td>{{ $p->nom }}</td>
                        <td>{{ $p->prenom }}</td>
                        <td>{{ $p->code_parrainage }}</td>
                        <td>{{ $p->user?->telephone ?? '' }}</td>
                        <td data-sort="{{ $p->date_naissance?->timestamp }}">{{ $p->date_naissance?->format("Y-m-d")  }}</td>
                        <td>{{ $p->sexe }}</td>
                        <td>
                            @if($p->status==1)
                                <span
                                    class="label label-lg font-weight-bold label-light-success label-inline">Actif</span>
                            @elseif($p->status == 0)
                                <span
                                    class="label label-lg font-weight-bold label-light-danger label-inline">Inactif</span>
                            @endif
                        </td>
                        <td>{{ $p->pays?->name ?? '' }}</td>
                        <td data-sort="{{ $p->created_at->timestamp }}">{{ $p->created_at?->format("Y-m-d")  }}</td>
                        <td>{{ $p->updatedBy?->username ?? '' }}</td>
                        <td data-sort="{{ $p->updated_at->timestamp }}">{{ $p->updated_at?->format("Y-m-d")  }}</td>
                        <td nowrap="nowrap">
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details"
                               href="{{ route('clients.show',$p->id) }}">
                                <i class="flaticon-eye icon-md"></i>
                            </a>
                            <a class="btn btn-sm btn-clean btn-icon mr-2" title="Éditer details"
                               href="{{ route('clients.edit',$p->id) }}">
                                <i class="flaticon2-pen icon-md"></i>
                            </a>
                            @can('role-edit')

                                {{--                            <a href="#" onclick="launchModal('modal-edit-client-{{$p->id}}');">{{$client->id_interne}}</a>--}}
                                {{--                                <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('clients.edit',$p->id) }}">--}}
                                {{--                                    <i class='flaticon2-edit icon-md'></i>--}}
                                {{--                                </a>--}}
                            @endcan
                            @can('role-delete')
                                @if($p->status==1)
                                    {!! Form::open(['method' => 'PUT','route' => ['client-change', $p->id],'id'=>'form-desactivation'.$p->id,'style'=>'display:inline']) !!}
                                    <input type="hidden" name="statut" value="0">
                                    <button class="btn btn-sm btn-clean btn-icon deactivate-form-row" type="submit"
                                            data-val="{{ $p->id }}"><i class='fas fa-user-slash icon-md'></i></button>
                                    {!! Form::close() !!}
                                @elseif($p->status == 0)
                                    {!! Form::open(['method' => 'PUT','route' => ['client-change', $p->id],'id'=>'form-activation'.$p->id,'style'=>'display:inline']) !!}
                                    <input type="hidden" name="statut" value="1">
                                    <button class="btn btn-sm btn-clean btn-icon activate-form-row" type="submit"
                                            data-val="{{ $p->id }}"><i class='fas fa-user-tag icon-md'></i></button>
                                    {!! Form::close() !!}
                                @endif

                            @endcan
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');
                const documentTitle = "Liste des clients"
                // begin first table
                table.DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    order: [9, 'desc'],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            title: documentTitle
                        },
                        {
                            extend: 'excelHtml5',
                            title: documentTitle
                        },
                        {
                            extend: 'csvHtml5',
                            title: documentTitle
                        },
                        {
                            extend: 'pdfHtml5',
                            title: documentTitle
                        }
                    ],
                    // buttons: [
                    //     'copyHtml5',
                    //     'excelHtml5',
                    //     'csvHtml5',
                    //     'pdfHtml5'
                    // ],
                    paging: true
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
