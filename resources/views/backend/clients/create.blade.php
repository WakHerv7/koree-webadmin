@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Création d'un nouveau package</h3>
                </div>
                <!--begin::Form-->
                {!! Form::open(array('route' => 'package.store','method'=>'POST')) !!}
                    <div class="card-body">
                        <div class="form-group">
                            <label>Désignation
                                <span class="text-danger">*</span></label>
                            {!! Form::text('nom', null, array('placeholder' => 'Nom','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Prix
                                <span class="text-danger">*</span></label>
                            {!! Form::number('prix', null, array('placeholder' => 'Prix','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Durée (Jours)
                                <span class="text-danger">*</span></label>
                            {!! Form::number('duree', null, array('placeholder' => 'Durée','class' => 'form-control', 'required'=>'required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            {!! Form::textArea( 'description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <label>Pays<span class="text-danger">*</span></label>
                            <select class="form-control" name="country_id" required>
                                @foreach($pays as $p)
                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
            {!! Form::close() !!}
                <!--end::Form-->
            </div>

        </div>
    </div>
@endsection
