@extends('layouts.main')

@section('title', "Fermeture de compte")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Fermeture de compte
                    <span class="d-block text-muted pt-2 font-size-sm">Pour tous les pays</span></h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @can('commerce-create')
                    <a href="{{ route('commerces.create') }}" class="btn btn-primary font-weight-bolder">
                        <i class="flaticon2-add-square icon-md"></i>Nouveau Commerce
                    </a>
                @endcan
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Propriétaire</th>
                    <th>Tel</th>
                    <th>Date de demande</th>
                    <th>Agent</th>
                    <th>Status</th>
                    <th width="180px">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $request)
                    <tr>
                        <td>{{ $request->client->nom }}</td>
                        <td>{{ $request->client?->user?->telephone }}</td>
                        <td>{{ $request->created_at->format('Y-m-d H:i') }}</td>
                        <td>{{ $request->allower?->user?->telephone }}</td>
                        <td>
                            @if($request->processed)
                                <span class="badge badge-success">PROCESSED</span>
                            @else
                                <span class="badge badge-warning">PENDING</span>
                            @endif
                        </td>
                        <td class="d-inline-block">
                            @if(!$request->processed)
{{--                                <a href="{{ route('client-deletion-requests-accept', ['id' => $request->id]) }}">Valider</a>--}}
{{--                                <a href="{{ route('client-deletion-requests-cancel', ['id' => $request->id])  }}" class="ml-2">Refuser</a>--}}

                            @else
{{--                                <a href="{{ route('client-deletion-requests-reset', ['id' => $request->id])  }}">Reouvrir</a>--}}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollY: '50vh',
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    order: [3, 'asc'],
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
