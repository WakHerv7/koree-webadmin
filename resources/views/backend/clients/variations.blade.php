@extends('layouts.main')

@section('title', "Changements statut ". $client->getFullname())

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    Changements statut {{$client->getFullname()}}
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('clients.show', ['client' => $client->id]) }}"
                   class="btn btn-primary font-weight-bolder">
                    <i class="fa fa-eye icon-md"></i>Détails
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>De</th>
                    <th>Pour</th>
                    <th>Par</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($variations as $variation)
                    <tr>
                        <td>{{ $client->nom }}</td>
                        <td>{{ $client->prenom }}</td>
                        <td>{{ strtoupper($variation->from) }}</td>
                        <td>{{ strtoupper($variation->to) }}</td>
                        <td>{{ \App\Models\User::find($variation->admin_id)?->username }}</td>
                        <td>{{ \Illuminate\Support\Carbon::make($variation->date)->format('d, M Y H:i') }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

@endsection

@section('end_javascript')
    <script>
        "use strict";
        var KTDatatablesBasicScrollable = function () {

            var initTable1 = function () {
                var table = $('#kt_datatable1');

                // begin first table
                table.DataTable({
                    scrollY: '50vh',
                    scrollX: true,
                    scrollCollapse: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            };


            return {

                //main function to initiate the module
                init: function () {
                    initTable1();
                },

            };

        }();

        jQuery(document).ready(function () {
            KTDatatablesBasicScrollable.init();
        });
    </script>
@endsection
