@extends('layouts.main')

@section('title', "Client details")

@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Détail Client : {{ $data->nom ?? '' }}
                            &nbsp;&nbsp;{{ $data->prenom ?? '' }}
                            <span class="d-block text-muted pt-2 font-size-sm">infos Détaillées</span></h3>
                    </div>
                    <div class="card-toolbar">
                        @can('role-delete')
                            @if($data->status==1)
                                {!! Form::open(['method' => 'PUT','route' => ['client-change', $data->id],'id'=>'form-desactivation'.$data->id,'style'=>'display:inline']) !!}
                                <input type="hidden" name="statut" value="0">
                                <button class="mx-4 btn btn-danger btn-sm font-weight-bold btn-icon- deactivate-form-row" type="submit"
                                        data-val="{{ $data->id }}"><i class='fas fa-user-slash icon-md'></i> Desactiver</button>
                                {!! Form::close() !!}
                            @elseif($data->status == 0)
                                {!! Form::open(['method' => 'PUT','route' => ['client-change', $data->id],'id'=>'form-activation'.$data->id,'style'=>'display:inline']) !!}
                                <input type="hidden" name="statut" value="1">
                                <button class="mx-4 btn btn-success btn-sm font-weight-bold btn-icon- activate-form-row" type="submit"
                                        data-val="{{ $data->id }}"><i class='fas fa-user-tag icon-md'></i> Activer</button>
                                {!! Form::close() !!}
                            @endif

                        @endcan
                        <a href="{{ route('clients.mvmt', ['id' => $data->id]) }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="fa fa-history"></i>Variations</a>
                        <a href="{{ route('clients.index') }}" class="mx-4 btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                        <a href="{{ route('clients.edit', $data->id) }}"
                           class="btn btn-primary btn-sm font-weight-bold">
                            <i class="flaticon2-pen"></i>Éditer le client</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Nom(s) : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->nom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Prénom(s) : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->prenom ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Email : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->user->email ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Telephone : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->user->telephone ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Code de parrainage : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->code_parrainage ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Date de naissance : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->date_naissance?->format('d M Y') ?? ''}}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Sexe : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->sexe ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Statut : </label>
                        <div class="col-8">
                            @if($data->status ==1)
                                <a href="{{ route('toggle-status', $data->id) }}"
                                   class="btn btn-light-primary font-weight-bold mr-2">ACTIF</a>
                            @else
                                <a href="{{ route('toggle-status', $data->id) }}"
                                   class="btn btn-light-danger font-weight-bold mr-2">INACTIF</a>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Influenceur ? </label>
                        <div class="col-8">
                            @if($data->influencer)
                                <a href="{{ route('toggle-influencer', $data->id) }}"
                                   class="btn btn-light-primary font-weight-bold mr-2">OUI</a>
                            @else
                                <a href="{{ route('toggle-influencer', $data->id) }}"
                                   class="btn btn-light-danger font-weight-bold mr-2">NON</a>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Bonus filleul : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->referee_amount }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Bonus parrain : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->referrer_amount }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Quartier : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->quartier->name ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Ville : </label>
                        <div class="col-8">
                            <span
                                class="form-control-plaintext font-weight-bolder">{{ $data->ville->name ?? '' }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-4 col-form-label">Pays : </label>
                        <div class="col-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $data->pays->name ?? '' }}</span>
                        </div>
                    </div>
                    @if($data->photo != null && $data->photo != "")
                        <div class="form-group row my-2">
                            <label class="col-12 col-form-label">Photo : </label>
                            <div class="col-8">
                                <img class="w-120px" src="{{ $data->getUpdatedPhoto() }}"
                                     alt="{{ $data->nom ?? '' }} {{ $data->prenom ?? '' }}">
                            </div>
                        </div>
                    @endif

                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection
