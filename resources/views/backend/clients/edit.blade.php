@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header h-auto py-4">
                    <div class="card-title">
                        <h3 class="card-label">Édition profil Client : {{ $data->getFullname() }}
                            <span class="d-block text-muted pt-2 font-size-sm">Édition du profil</span></h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{ route('clients.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body py-4">
                    <form class="row" method="post" action="{{ route('clients.update', $data->id) }}">
                        @csrf
                        @method('put')
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="firstname">Prénom</label>
                                <input name="firstname" id="firstname" type="text"
                                       value="{{ old('firstname') ?? $data->prenom }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="lastname">Nom</label>
                                <input name="lastname" id="lastname" type="text"
                                       value="{{ old('lastname') ?? $data->nom }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="phone">Téléphone</label>
                                <input name="phone" type="text" id="phone"
                                       value="{{ old('phone') ?? $data->user->telephone}}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="username">Nom d'utilisateur</label>
                                <input name="username" id="username" type="text"
                                       value="{{ old('username') ?? $data->user->username }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input name="email" id="email" value="{{ old('email') ?? $data->user?->email }}"
                                       type="text"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="parrainage">Code parrainage</label>
                                <input name="parrainage" id="parrainage" type="text"
                                       value="{{ old('parrainage') ?? $data->code_parrainage }}" class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="referee_amount">Bonus filleul</label>
                                <input name="referee_amount" id="referee_amount" type="number"
                                       value="{{ old('referee_amount',  $data->referee_amount) }}" class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="referrer_amount">Bonus parrain</label>
                                <input name="referrer_amount" id="referrer_amount" type="number"
                                       value="{{ old('referrer_amount', $data->referrer_amount) }}"
                                       class="form-control">
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="sex">Sexe</label>
                                <select name="sex" id="sex" class="form-control">
                                    <option @if($data->sexe == 'M') selected @endif value="M">Masculin</option>
                                    <option @if($data->sexe == 'F') selected @endif value="F">Feminin</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="date">Date naissance</label>
                                <input name="date" type="text" id="date" class="form-control"
                                       value="{{ old('date') ?? $data->date_naissance?->format('m-d-Y') }}" readonly
                                       placeholder="Select date"/>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="country">Pays</label>
                                <select name="country" id="country" class="form-control select2">
                                    @foreach($countries as $country)
                                        <option @if($data->country_id == $country->id) selected
                                                @endif value="{{ $country->id }}">{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="state">Province</label>
                                <select name="state" id="state" class="form-control select2">
                                    @foreach($states as $state)
                                        <option @if($data->ville?->region->id == $state->id) selected
                                                @endif value="{{ $state->id }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="city">Ville</label>
                                <select name="city" id="city" class="form-control select2">
                                    @foreach($cities as $city)
                                        <option @if($data->ville?->id == $city->id) selected
                                                @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="district">Quartier</label>
                                <select name="district" id="district" class="form-control select2">
                                    @foreach($districts as $district)
                                        <option @if($data->quartier_id == $district->id) selected
                                                @endif value="{{ $district->id }}">{{ $district->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label for="influencer">Influenceur</label>
                                <select name="influencer" id="influencer" class="form-control">
                                    <option @if($data->influencer) selected @endif value="yes">Oui</option>
                                    <option @if(!$data->influencer) selected @endif value="no">Non</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-12">
                            <button type="submit" class="btn btn-primary">mettre à jour</button>
                        </div>
                    </form>
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
@endsection

@section('end_javascript')
    <script>
        const KTSelect2 = function () {
            // Private functions
            const demos = function () {
                // basic
                $('#country').select2({
                    placeholder: "Selectionnez le pays"
                });

                $('#state').select2({
                    placeholder: "Selectionnez la province"
                });

                $('#city').select2({
                    placeholder: "Selectionnez la ville"
                });

                $('#district').select2({
                    placeholder: "Selectionnez le quartier"
                });
            }

            const modalDemos = function () {
                $('#kt_select2_modal').on('shown.bs.modal', function () {
                    // basic
                    $('#pays_modal').select2({
                        placeholder: "Selectionnez le pays"
                    });
                    $('#state_modal').select2({
                        placeholder: "Selectionnez la province"
                    });
                    $('#city_modal').select2({
                        placeholder: "Selectionnez la ville"
                    });

                    $('#district_modal').select2({
                        placeholder: "Selectionnez le quartier"
                    });
                });
            }

            return {
                init: function () {
                    demos();
                    modalDemos();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTSelect2.init();
        });

    </script>
    <script>
        $(document).ready(function () {
            $('#country').on('change', function () {
                var PaysID = $(this).val();
                if (PaysID) {
                    $.ajax({
                        url: '/getRegion/' + PaysID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#state').empty();
                                $('#city').empty();
                                $('#district').empty();
                                $.each(data, function (key, vale) {
                                    $('select[name="state"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#state').empty();
                                $('#city').empty();
                                $('#district').empty();
                            }
                            $('#state').trigger('change');
                        }
                    });
                }
            });
            $('#state').on('change', function () {
                var RegionID = $(this).val();
                if (RegionID) {
                    $.ajax({
                        url: '/getVille/' + RegionID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#city').empty();
                                $('#district').empty();
                                $.each(data, function (key, vale) {
                                    $('select[name="city"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                                $('#city').trigger('change');

                            } else {
                                $('#city').empty();
                                $('#district').empty();
                            }
                        }
                    });
                }
            });
            $('#city').on('change', function () {
                var CityID = $(this).val();
                if (CityID) {
                    $.ajax({
                        url: '/getQuartiers/' + CityID,
                        type: "GET",
                        data: {"_token": "{{ csrf_token() }}"},
                        dataType: "json",
                        success: function (data) {
                            if (data) {
                                $('#district').empty();
                                $.each(data, function (key, vale) {
                                    $('select[name="district"]').append('<option value="' + vale.id + '">' + vale.name + '</option>');
                                });
                            } else {
                                $('#district').empty();
                            }
                            $('#district').trigger('change');

                        }
                    });
                }
            })

            $('#country').trigger('change');

        }); // fin doc ready
    </script>
    <script>
        // Class definition

        var KTBootstrapDatepicker = function () {

            var arrows;
            if (KTUtil.isRTL()) {
                arrows = {
                    leftArrow: '<i class="la la-angle-right"></i>',
                    rightArrow: '<i class="la la-angle-left"></i>'
                }
            } else {
                arrows = {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }

            // Private functions
            var demos = function () {
                // minimum setup
                $('#date').datepicker({
                    rtl: KTUtil.isRTL(),
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: arrows
                });
            }

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTBootstrapDatepicker.init();
        });
    </script>
@endsection
