@php use App\Helpers\SMS; @endphp
@extends('layouts.main')
@section('title', "Paramètres globaux")
@section('content')
    <div class="container">
        <div class="justify-content-center">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Opps!</strong> Erreurs signalées.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Configuration globale du système</h3>
                    <div class="card-toolbar">
                        <a href="{{ route('vendeurs.index') }}" class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Retour à la liste</a>
                    </div>
                </div>
                <!--begin::Form-->
                <form method="post" action="{{ route('parametrage.update') }}">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="nom_societe">Nom société <span class="text-danger">*</span></label>
                                <input name="nom_societe" id="nom_societe" type="text"
                                       value="{{ old('nom_societe',$data?->nom_societe) }}" class="form-control"
                                       placeholder="Nom société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="email">Email société <span class="text-danger">*</span></label>
                                <input type="email" name="email" id="email" value="{{ old('email',$data?->email) }}"
                                       class="form-control" placeholder="Email société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="email_support">Email support <span class="text-danger">*</span></label>
                                <input type="email" name="email_support" id="email_support"
                                       value="{{ old('email_support',$data?->email_support) }}" class="form-control"
                                       placeholder="Email support société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="telephone">Téléphone société <span class="text-danger">*</span></label>
                                <input name="telephone" id="telephone" type="text"
                                       value="{{ old('telephone',$data?->telephone) }}" class="form-control"
                                       placeholder="Téléphone société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="fax">Fax société </label>
                                <input type="text" name="fax" id="fax" value="{{ old('fax',$data?->fax) }}"
                                       class="form-control" placeholder="Fax société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="address">Adresse société </label>
                                <input type="text" name="address" id="address"
                                       value="{{ old('address',$data?->address) }}"
                                       class="form-control" placeholder="Adresse société"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="mnt_wallet_max">Montant Max Wallet <span
                                        class="text-danger">*</span></label>
                                <input name="mnt_wallet_max" id="mnt_wallet_max" type="text"
                                       value="{{ old('mnt_wallet_max',$data?->mnt_wallet_max) }}" class="form-control"
                                       placeholder="Montant Max Wallet"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="transaction_hebdo_max">Transaction hebdo max <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="transaction_hebdo_max" id="transaction_hebdo_max"
                                       value="{{ old('transaction_hebdo_max',$data?->transaction_hebdo_max) }}"
                                       class="form-control" placeholder="Transaction hebdo max"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="transaction_mensuelle_max">Transaction mensuelle max <span
                                        class="text-danger">*</span></label>
                                <input type="text" name="transaction_mensuelle_max" id="transaction_mensuelle_max"
                                       value="{{ old('transaction_mensuelle_max',$data?->transaction_mensuelle_max) }}"
                                       class="form-control" placeholder="Transaction mensuelle max"/>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="plafond_parrainage">Plafond parrainage <span
                                        class="text-danger">*</span></label>
                                <input name="plafond_parrainage" id="plafond_parrainage" type="text"
                                       value="{{ old('plafond_parrainage',$data?->plafond_parrainage) }}"
                                       class="form-control"
                                       placeholder="Plafond parrainage"/>
                            </div>


                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>

            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Configuration Applications Mobiles</h3>
                    <div class="card-toolbar">
                    </div>
                </div>
                <!--begin::Form-->
                <form method="post" action="{{ route('parametrage.update') }}">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">

                            <div class="col-12 col-md-6 col-lg-4 mb-6">
                                <label for="app_version">Version actuelle <span class="text-danger">*</span></label>
                                <input type="text" name="app_version" id="app_version"
                                       value="{{ old('app_version',$data?->app_version) }}"
                                       class="form-control" placeholder="Version actuelle de l'application"/>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Configuration Securite</h3>
                    <div class="card-toolbar">
                    </div>
                </div>
                <!--begin::Form-->
                <form method="post" action="{{ route('parametrage.update') }}">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">

                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12 col-md-6 ">
                                        <label for="otp_number">Numéro pour OTP <span
                                                class="text-danger">*</span></label>
                                        <input type="text" name="otp_number" id="otp_number"
                                               value="{{ old('otp_number',$data?->otp_number) }}"
                                               class="form-control" placeholder="Numéro pour OTP"/>
                                    </div>


                                    <div class="col-12 col-md-6">
                                        <label for="otp_email">Email pour OTP <span class="text-danger">*</span></label>
                                        <input type="email" name="otp_email" id="otp_email"
                                               value="{{ old('otp_email',$data?->otp_email) }}"
                                               class="form-control" placeholder="Email pour OTP"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Configuration SMS</h3>
                    <div class="card-toolbar">
                        <button id="getSmsBalance" onclick="getBalance()"
                                class="btn btn-success btn-sm font-weight-bold">
                            <i class="flaticon2-crisp-icons"></i>Voir les balances sms
                        </button>
                    </div>
                </div>
                <!--begin::Form-->
                <form method="post" action="{{ route('parametrage.update') }}">
                    <div class="card-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">

                            <div class="col-12 col-md-6  mb-6">
                                <label>Fournisseur SMS</label>
                                <div class="col-form-label">
                                    <div class="radio-inline">

                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TWILIO}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_TWILIO)? "checked" : "" }} />
                                            <span></span>
                                            TWILIO
                                        </label>


                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_SNS}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_SNS)? "checked" : "" }} />
                                            <span></span>
                                            AWS SNS SMS
                                        </label>


                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_MESAGOO}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_MESAGOO)? "checked" : "" }} />
                                            <span></span>
                                            MESAGOO
                                        </label>

                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_WHATSAPP}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_WHATSAPP)? "checked" : "" }} />
                                            <span></span>
                                            WHATSAPP
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TERMII}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_TERMII)? "checked" : "" }} />
                                            <span></span>
                                            Termii
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TECHSOFT}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_TECHSOFT)? "checked" : "" }} />
                                            <span></span>
                                            TECH Soft
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_NEXAH}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_NEXAH)? "checked" : "" }} />
                                            <span></span>
                                            Nexah
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_AUTOMATIC}}"
                                                   name="sms_provider" {{ ($data?->sms_provider==SMS::SMS_PROVIDER_AUTOMATIC)? "checked" : "" }} />
                                            <span></span>
                                            Routage Automatique&nbsp;<i class="fa fa-info-circle" rel="tooltip"
                                                                        data-toggle="tooltip"
                                                                        title="Le SMS provider est automatiquement selectionné en fonction du pays et du  numero de telephone"></i>
                                        </label>
                                    </div>
                                    <span class="form-text text-muted">Cochez le fournisseur SMS du sytème</span>
                                </div>
                            </div>
                            <div class="col-12 col-md-6  mb-6">
                                <label>Fournisseur SMS de Secour</label>
                                <div class="col-form-label">
                                    <div class="radio-inline">

                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TWILIO}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_TWILIO)? "checked" : "" }} />
                                            <span></span>
                                            TWILIO
                                        </label>

                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_SNS}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_SNS)? "checked" : "" }} />
                                            <span></span>
                                            AWS SNS SMS
                                        </label>


                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_MESAGOO}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_MESAGOO)? "checked" : "" }} />
                                            <span></span>
                                            MESAGOO
                                        </label>

                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_WHATSAPP}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_WHATSAPP)? "checked" : "" }} />
                                            <span></span>
                                            WHATSAPP
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TERMII}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_TERMII)? "checked" : "" }} />
                                            <span></span>
                                            Termii
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_TECHSOFT}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_TECHSOFT)? "checked" : "" }} />
                                            <span></span>
                                            TECH Soft
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_NEXAH}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_NEXAH)? "checked" : "" }} />
                                            <span></span>
                                            Nexah
                                        </label>
                                        <label class="radio radio-outline radio-outline-2x radio-primary">
                                            <input type="radio" value="{{SMS::SMS_PROVIDER_AUTOMATIC}}"
                                                   name="sms_provider_fallback" {{ ($data?->sms_provider_fallback==SMS::SMS_PROVIDER_AUTOMATIC)? "checked" : "" }} />
                                            <span></span>
                                            Routage Automatique&nbsp;<i class="fa fa-info-circle" rel="tooltip"
                                                                        data-toggle="tooltip"
                                                                        title="Le SMS provider est automatiquement selectionné en fonction du pays et du  numero de telephone"></i>
                                        </label>
                                    </div>
                                    <span class="form-text text-muted">Cochez le fournisseur SMS du sytème</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
                <!--end::Form-->
            </div>

        </div>
    </div>

    <script>
        var getBalance = function () {
            Swal.fire({
                title: '',
                confirmButtonText: 'Afficher la balance',
                text: '',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch('')
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)
                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(result.value)
                    let value = result.value.data
                    // let value = JSON.stringify(result.value.data)
                    Swal.fire({
                        title: `${result.value.message}`,
                        html: `<div>
                                    <p>Techsoft =>${value.techsoft}</p>
                                    <p>Termii => ${value.termii}</p>
                                    <p>Nexah => ${value.nexah}</p>
                                </div>`,
                    })
                }
            })
        };

    </script>
@endsection

@push('js')

@endpush




