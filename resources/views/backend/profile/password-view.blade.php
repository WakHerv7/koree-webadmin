@extends('layouts.main')

@section('title', "Profile")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Confirmez votre mot de passe
                    <span class="d-block text-muted pt-2 font-size-sm"> </span></h3>
            </div>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('password.confirm') }}">
                @csrf

                <div class="form-group row">

                    <div class="col-12 col-lg-6">
                        <label for="password" class="col-form-label">{{ __('Password') }}</label>

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    </div>
                </div>

                <div class="mb-0 form-group row">
                    <div class="col-12 col-lg-6">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Confirm Password') }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!--end::Card-->

@endsection
