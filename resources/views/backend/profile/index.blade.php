@extends('layouts.main')

@section('title', "Profile")

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Mon profile
                    <span class="d-block text-muted pt-2 font-size-sm"> </span></h3>
            </div>
        </div>
        <div class="card-body">
            @if(!auth()->user()->two_factor_secret)
                @if (session('status') == 'two-factor-authentication-disabled')
                    <div class="mb-4 font-medium text-sm">
                        L'authentification à multiples facteurs est déjà configuré sur votre compte
                    </div>
                @else
                    <div class="mb-4 font-medium text-sm">
                        L'authentification à multiples facteurs n'est pas configuré sur votre compte
                    </div>
                @endif
            @endif

            @if(auth()->user()->two_factor_secret)
                <form method="POST" action="{{ route('two-factor.disable') }}">
                    @csrf
                    @method('DElETE')

                    <div class="pb-5">
                        <h4>Scannez le QR Code pour commencer</h4>
                        {!! auth()->user()->twoFactorQrCodeSvg() !!}
                    </div>

                    <div>
                        <h3>Codes de récupération:</h3>

                        <ul>
                            @foreach (json_decode(decrypt(auth()->user()->two_factor_recovery_codes)) as $code)
                                <li>{{ $code }}</li>
                            @endforeach
                        </ul>
                    </div>

                    <button class="btn btn-danger">Désactiver</button>
                </form>
            @else
                <form method="POST" action="{{ route('two-factor.enable') }}">
                    @csrf
                    <button class="btn btn-primary">Activer</button>
                </form>
            @endif

        </div>
    </div>
    <!--end::Card-->

@endsection
