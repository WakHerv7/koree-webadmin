<!DOCTYPE html>
<html lang="fr">


@php
$title='Koree.africa l Vous êtes payés pour dépenser !';
$keywords='Cashback, Cameroun, koree, africa, top up, paiement, scanner QRcode, mobile, money, mobile-money, OM, MTN, camer, kmer, koree.africa, argent, remise, réduction, remises, réduction, momo, cash, nkap, remboursement, rembourser, ristourne, ristournes';
$desc = 'Grâce à l’application Koree, votre shopping est enfin récompensé ! Gagnez des cashback et récupérez votre monnaie chez tous vos commerçants préférés dès maintenant.';
@endphp
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ $title }}</title>
    <meta name='description' itemprop='description' content='{{ $desc }}' />
    <meta name='keywords' content='{{$keywords}}' />
    <meta property='article:published_time' content='{{ today()  }}' />
{{--    <meta property='article:section' content='news' />--}}

    <meta property="og:description"content="{{ $desc }}" />
    <meta property="og:title"content="{{ $title }}" />
    <meta property="og:url"content="https://www.koree.africa" />
    <meta property="og:type"content="website" />
    <meta property="og:locale"content="fr-fr" />
{{--    <meta property="og:locale:alternate"content="pt-pt" />--}}
{{--    <meta property="og:locale:alternate"content="en-us" />--}}
    <meta property="og:site_name"content="Koree" />
    <meta property="og:image"content="https://www.koree.africa/frontassets/img/cover.png" />
{{--    <meta property="og:image"content="http://image.url.com/img1.jpg" />--}}
{{--    <meta property="og:image"content="http://image.url.com/img2.jpg" />--}}
{{--    <meta property="og:image"content="http://image.url.com/img3.jpg" />--}}
{{--    <meta property="og:image:url"content="http://image.url.com/cover.jpg" />--}}
{{--    <meta property="og:image:size"content="300" />--}}

    <meta name="twitter:card"content="summary" />
    <meta name="twitter:title"content="{{ $title }}" />
    <meta name="twitter:site"content="@koree" />

    <!-- Favicons -->
    <link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="icon">
    <link href="{{ asset('assets/media/logos/favicon.ico') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,1 000,1 000i,600,600i,700,700i|Poppins:300,300i,400,400i,1 000,1 000i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('frontassets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('frontassets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontassets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('frontassets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontassets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontassets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <link href="{{ asset('frontassets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('frontassets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontassets/sass/main.css') }}" rel="stylesheet">
    @stack('css')
    @livewireStyles
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-lg-between">
        <h1 class="logo me-auto me-lg-0"><a href="#hero"><img src="{{ asset('frontassets/img/koree_Logo.png') }}" alt=""></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>

                <li><a class="nav-link scrollto" href="#how">Fonctionnalités</a></li>
                <li><a class="nav-link scrollto" href="#about">A propos</a></li>
                <li><a class="nav-link scrollto" href="#fa2">Aide</a></li>
                <li><a class="nav-link scrollto" href="#mycontact">Contact</a></li><br>
                <div class="btttt">
                    <a href="https://foy0wouxxwx.typeform.com/to/okMiyx5e" target="_blank" class="get-started-btn scrollto redirect_link1 ">Devenir Commerçant Koree</a>
                </div>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

        <a href="https://foy0wouxxwx.typeform.com/to/okMiyx5e" target="_blank" class="get-started-btn scrollto redirect_link">Devenir Commerçant Koree</a>

    </div>
</header>
<!-- End Header -->


@yield('content')

<!-- ======= Footer ======= -->
<footer id="footer" >
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6" >
                    <div class="footer-info" data-aos="fade-right">
                        <h1 class="logo me-auto me-lg-0"><a href="#"><img src="{{ asset('frontassets/img/koree_Logo.png') }}" alt="" height="74" ></a></h1>
                        <p class="details_koree">
                            Pour gagner de l’argent et <br> garder ton argent!<br><br>
                            <strong>Phone:</strong> +237 <br>
                            <strong>Email:</strong> wecare@koree.africa<br>
                        </p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-right">
                    <h4>Entreprise</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i><a href="#">Conditions d’utilisation</a> </li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('mention-legale') }}">Mentions légales</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('politique') }}">Politiques de confidentialité</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-right">
                    <h4>Aide & Contact</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#fa2">Centre d’aide</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#mycontact">Contact</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-newsletter" data-aos="fade-right">
                    <h4>Suivez-nous</h4>
                    <div class="social-links mt-3">
                        <a href="https://twitter.com/koreeafrica" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="https://www.instagram.com/koree.africa/" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="https://vm.tiktok.com/ZMLnqL3HG/" class="google-plus"><i class='bx bxl-tiktok' ></i></a>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy;2022 Copyright <strong><span>Koree</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
<!-- Vendor JS Files -->

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="{{ asset('frontassets/vendor/purecounter/purecounter.js') }}"></script>
<script src="{{ asset('frontassets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('frontassets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('frontassets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('frontassets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('frontassets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('frontassets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('frontassets/js/main.js') }}"></script>
<script src="{{ asset('frontassets/js/style.js') }}"></script>

@stack('js')
@livewireScripts
</body>

</html>

