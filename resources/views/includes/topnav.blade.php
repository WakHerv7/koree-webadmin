<div class="topbar">
    <!--begin::Notifications-->
    @include('includes.topnotification')
    <!--end::Notifications-->
     <!--begin::Languages-->
    <div class="dropdown">
        <!--begin::Toggle-->
        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
            <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1">
                <img class="h-20px w-20px rounded-sm" src="{{ asset('assets/media/svg/flags/195-france.svg')}}" alt="" />
            </div>
        </div>
        <!--end::Toggle-->
        @if(1 == 2)
            <!--begin::Dropdown-->
            <div class="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-right">
                <!--begin::Nav-->
                <ul class="navi navi-hover py-4">
                    <!--begin::Item-->
                    <li class="navi-item">
                        <a href="#" class="navi-link">
                        <span class="symbol symbol-20 mr-3">
                            <img src="{{ asset('assets/media/svg/flags/226-united-states.svg')}}" alt="" />
                        </span>
                            <span class="navi-text">English</span>
                        </a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="navi-item">
                        <a href="#" class="navi-link">
                        <span class="symbol symbol-20 mr-3">
                            <img src="{{ asset('assets/media/svg/flags/195-france.svg')}}" alt="" />
                        </span>
                            <span class="navi-text">Français</span>
                        </a>
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Nav-->
            </div>
            <!--end::Dropdown-->
        @endif
    </div>
    <!--end::Languages-->
    <!--begin::User-->
    <div class="topbar-item">
        <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
            <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Bienvenue en {{config('app.env')}},</span>
            <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{Auth::user()->name ?? '' }}</span>
            <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                <span class="symbol-label font-size-h5 font-weight-bold">{{ (isset(Auth::user()->name))? ucfirst(Auth::user()->name):''  }}</span>
            </span>
        </div>
    </div>
    <!--end::User-->
</div>
