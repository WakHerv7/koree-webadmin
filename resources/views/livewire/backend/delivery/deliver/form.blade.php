<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('delivery.deliver.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">
                
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Nom</label>
                        <input type="text" wire:model='first_name' class="form-control"> 
                        @error("first_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Prenom</label>
                        <input type="text" wire:model='last_name' class="form-control"> 
                        @error("last_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Telephone</label>
                        <input type="text" wire:model='phone' class="form-control"> 
                        @error("phone")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Photo</label>
                        <input type="file" wire:model='photo' class="form-control"> 
                        @error("photo")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Longitude</label>
                        <input type="text" wire:model='longitude' class="form-control"> 
                        @error("longitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Latitude</label>
                        <input type="text" wire:model='latitude' class="form-control"> 
                        @error("latitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>


                <h6>Document d'identification</h6>
                <div class="form-group row">
                    
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Recto:</label>
                        <input type="file" wire:model='identity_document' class="form-control"> 
                        @error("identity_document")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Verso:</label>
                        <input type="file" wire:model='identity_document_2' class="form-control"> 
                        @error("identity_document_2")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                

                <h6>Permis de conduire</h6>
                <div class="form-group row">
                    
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Recto:</label>
                        <input type="file" wire:model='driving_license' class="form-control"> 
                        @error("driving_license")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Verso:</label>
                        <input type="file" wire:model='driving_license_2' class="form-control"> 
                        @error("driving_license_2")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

            </div>
            </form><!-- row -->
    </div>
</div>
