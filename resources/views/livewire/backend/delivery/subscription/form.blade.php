<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('delivery.subscription.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Debut du contrat (Date)</label>
                        <input type="date" wire:model='start_date' class="form-control"> 
                        @error("start_date")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Fin du contrat (Date)</label>
                        <input type="date" wire:model='end_date' class="form-control"> 
                        @error("end_date")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Commission</label>
                        <input type="number" step="0.01" wire:model='commission_rate' class="form-control"> 
                        @error("commission_rate")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Entreprise</label>

                        <select wire:model="company_id" class="form-control">
                            <option value="null" label="Choisir l'entreprise" disabled selected>Choisir l'entreprise</option>
                            @foreach ($companies as $c)
                                <option value="{{ $c['id'] }}">{{ $c['company_name'] }}</option>
                            @endforeach
                        </select>

                        {{-- <input type="text" wire:model='company' class="form-control">  --}}
                        @error("company_id")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>
