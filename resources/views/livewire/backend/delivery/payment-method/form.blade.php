<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('delivery.payment_method.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">
                <h3 class="card-title">Entreprise</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Nom</label>
                        <input type="text" wire:model='payment_method_name' class="form-control"> 
                        @error("payment_method_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Telephone</label>
                        <input type="text" wire:model='payment_method_phone' class="form-control"> 
                        @error("payment_method_phone")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Ville</label>
                        <input type="text" wire:model='city_payment_method' class="form-control"> 
                        @error("city_payment_method")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Quartier</label>
                        <input type="text" wire:model='neighborhood_payment_method' class="form-control"> 
                        @error("neighborhood_payment_method")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Longitude</label>
                        <input type="text" wire:model='longitude' class="form-control"> 
                        @error("longitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Latitude</label>
                        <input type="text" wire:model='latitude' class="form-control"> 
                        @error("latitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Tarif au kilomètre</label>
                        <input type="text" wire:model='amount_per_kilometer' class="form-control"> 
                        @error("amount_per_kilometer")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                        <input type="file" wire:model='logo_image' class="form-control"> 
                        @error("logo_image")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Image Background</label>
                        <input type="file" wire:model='bg_image' class="form-control"> 
                        @error("bg_image")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Document RCCM</label>
                        <input type="file" wire:model='rccm' class="form-control"> 
                        @error("rccm")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <br/><br/><br/>
                <h3 class="card-title">Responsable de l'entreprise</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Nom</label>
                        <input type="text" wire:model.live='manager_name' class="form-control"> 
                        @error("manager_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Email</label>
                        <input type="email" wire:model.live='manager_email' class="form-control"> 
                        @error("manager_email")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Telephone</label>
                        <input type="text" wire:model.live='manager_phone' class="form-control"> 
                        @error("manager_phone")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Pièce d'identité:</label>
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Recto:</label>
                        <input type="file" wire:model='identity_document' class="form-control"> 
                        @error("identity_document")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Verso:</label>
                        <input type="file" wire:model='identity_document_2' class="form-control"> 
                        @error("identity_document_2")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>
