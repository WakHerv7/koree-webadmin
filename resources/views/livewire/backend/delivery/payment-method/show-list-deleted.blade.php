<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des entreprises de livraison supprimées
                <!-- <span class="d-block text-muted pt-2 font-size-sm">pour tous les pays</span></h3> -->
        </div>
        <div class="card-toolbar">
            <a href="{{ route('delivery.payment_method.index') }}" class="btn btn-success btn-sm font-weight-bold">
                <i class="flaticon2-crisp-icons"></i>Retour
            </a>
            <!--end::Button-->
        </div>
        
        
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
            <tr>   
                <th>Entreprise</th>
                <th>Manager</th>
                <th>Telephone</th>
                <th>Ville</th>
                <th>Quartier</th>
                <th width="180px">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $p)
                <tr>
                    <td>{{ $p['payment_method_name'] }}</td>
                    <td>{{ $p['manager_name'] }}</td>
                    <td>{{ $p['payment_method_phone'] }}</td>
                    <td>{{ $p['city_payment_method'] }}</td>
                    <td>{{ $p['neighborhood_payment_method'] }}</td>
                    <td nowrap="nowrap">

                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" wire:click="show({{ json_encode($p) }})" href="#">
                            <i class="flaticon-eye icon-md"></i>
                        </a>

                        @can('commerce-delete')
                            <button type="button" class="btn btn-sm btn-light" data-toggle="modal" data-target="#modaldelete-{{ $p['id'] }}">
                                Activer
                            </button>
                        @endcan

                    </td>
                </tr>

                <div class="modal"  id="modaldelete-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Réactivation ?<span class="text-danger"></h6>
                            {{-- <p class="text-center"><span class="text-danger mt-2">Cette action est irreversible</span></p> --}}
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleActivation({{ $p['id'] }})" class="btn btn-success">
                                <span wire:loading.remove>Oui, Réactiver</span>
                                <span wire:loading>En attente réactivation <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                            
                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
