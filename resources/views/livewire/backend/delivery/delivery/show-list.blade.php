<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des livraisons
               
        </div>
        <div class="card-toolbar">
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
            <tr>   
                <th>Jour</th>
                <th>Montant</th>
                <th>Adresse Commerce</th>
                <th>Adresse Client</th>
                <th>Livreur</th>
                <th>Statut</th>
                <th width="180px">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $p)
                <tr>
                    <td>
                        <p><strong>{{ date('Y-m-d', strtotime($p['start_delivery'])) }}</strong></p>
                        <ul style="padding-inline-start:0;">
                            <li><strong>Debut:</strong>&nbsp;&nbsp;{{ date('H:i:s', strtotime($p['start_delivery'])) }}</li>
                            <li><strong>Fin:</strong>&nbsp;&nbsp;{{ date('H:i:s', strtotime($p['end_delivery'])) }}</li>
                        </ul>
                    </td>

                    <td>{{ $p['order']['amount'] }}</td>

                    <td>{{ $p['order']['seller_address'] }}</td>

                    <td>{{ $p['order']['customer_address'] }}</td>

                    <td>{{ $p['deliver'] }}</td>

                    <td>
                        @if($p['status'] == 'FINISH')
                            <span class="badge badge-success">Livre</span>
                        @elseif($p['status'] == 'RUN')
                            <span class="badge badge-warning">En cours</span>
                        @elseif($p['status'] == 'WAIT')
                            <span class="badge badge-danger">En attente</span>
                        @endif
                    </td>

                    <td nowrap="nowrap">

                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" wire:click="show({{ json_encode($p) }})" href="#">
                            <i class="flaticon-eye icon-md"></i>
                        </a>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
