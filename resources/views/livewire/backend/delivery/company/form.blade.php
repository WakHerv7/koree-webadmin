<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('delivery.company.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">
                <h3 class="card-title">Entreprise</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Nom</label>
                        <input type="text" wire:model='company_name' class="form-control" id="company_name" name="company_name"> 
                        @error("company_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Telephone</label>
                        <input type="text" wire:model='company_phone' class="form-control" id="company_phone" name="company_phone"> 
                        @error("company_phone")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Tarif au kilomètre</label>
                        <input type="text" wire:model='amount_per_kilometer' class="form-control" id="amount_per_kilometer" name="amount_per_kilometer"> 
                        @error("amount_per_kilometer")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    {{-- <select wire:model.lazy="current_city" class="select-box select-box--primary-style" id="billing-state">
                        <option value="">{{__("admin/common.Choisissezvotreville")}}</option>
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select> --}}


                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Pays</label>
                        <select wire:model="country" class="form-control"name="country_id">
                            <option value="">Choisir le pays...</option>
                            @foreach($countries as $scountry)
                                <option value="{{ $scountry['id'] }}">{{ $scountry['name'] }}</option>
                            @endforeach
                        </select>
                        @error("country")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Ville</label>
                        <select wire:model="city" class="form-control"name="city_id" @if(is_null($cities)) disabled @endif>
                            <option value="">Choisir la ville...</option>
                            @if(!is_null($cities))
                            @foreach($cities as $scity)
                                <option value="{{ $scity['id'] }}">{{ $scity['name'] }}</option>
                            @endforeach
                            @endif
                        </select>
                        @error("city")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Quartier</label>
                        <select wire:model="neighborhood" class="form-control"name="neighborhood_id" @if(is_null($neighborhoods)) disabled @endif>
                            <option value="">Choisir le quartier...</option>
                            @if(!is_null($neighborhoods))
                            @foreach($neighborhoods as $sneighborhood)
                                <option value="{{ $sneighborhood['id'] }}">{{ $sneighborhood['name'] }}</option>
                            @endforeach
                            @endif
                        </select>
                        @error("neighborhood")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    {{-- <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Longitude</label>
                        <input type="text" wire:model='longitude' class="form-control"> 
                        @error("longitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Latitude</label>
                        <input type="text" wire:model='latitude' class="form-control"> 
                        @error("latitude")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div> --}}
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                                <input type="file" wire:model='logo_image' class="form-control"> 
                                @error("logo_image")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($logo_image)
                                    <img src="{{ $logo_image->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountlogo_image)
                                <img src="{{ $mountlogo_image }}" style="border-radius:15px;" width="70px"/>
                                @endif
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Background</label>
                                <input type="file" wire:model='bg_image' class="form-control"> 
                                @error("bg_image")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($bg_image)
                                    <img src="{{ $bg_image->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountbg_image)
                                <img src="{{ $mountbg_image }}" style="border-radius:15px;" width="70px"/>
                                @endif
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">RCCM</label>
                                <input type="file" wire:model='rccm' class="form-control"> 
                                @error("rccm")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($rccm)
                                    <img src="{{ $rccm->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountrccm)
                                <img src="{{ $mountrccm }}" style="border-radius:15px;" width="70px"/>
                                @endif
                                
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                        <input type="file" wire:model='logo_image' class="form-control"> 
                        @error("logo_image")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Image Background</label>
                        <input type="file" wire:model='bg_image' class="form-control"> 
                        @error("bg_image")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Document RCCM</label>
                        <input type="file" wire:model='rccm' class="form-control"> 
                        @error("rccm")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div> --}}
                </div>
                <div class="form-group row">
                    
                </div>

                <br/><br/><br/>
                <h3 class="card-title">Responsable de l'entreprise</h3>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Nom</label>
                        <input type="text" wire:model.live='manager_name' class="form-control"> 
                        @error("manager_name")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Email</label>
                        <input type="email" wire:model.live='manager_email' class="form-control"> 
                        @error("manager_email")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Telephone</label>
                        <input type="text" wire:model.live='manager_phone' class="form-control"> 
                        @error("manager_phone")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Pièce d'identité:</label>
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Recto:</label>                                
                                <input type="file" wire:model='identity_document' class="form-control"> 
                                    @error("identity_document")
                                        <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="ml-3">
                                    @if ($identity_document)
                                        <img src="{{ $identity_document->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                    @elseif($mountidentity_document)
                                    <img src="{{ $mountidentity_document }}" style="border-radius:15px;" width="70px"/>
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Pièce d'identité:</label>
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Verso:</label>                                
                                <input type="file" wire:model='identity_document_2' class="form-control"> 
                                    @error("identity_document_2")
                                        <small class="text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="ml-3">
                                    @if ($identity_document_2)
                                        <img src="{{ $identity_document_2->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                    @elseif($mountidentity_document_2)
                                    <img src="{{ $mountidentity_document_2 }}" style="border-radius:15px;" width="70px"/>
                                    @endif
                                </div>
                        </div>
                    </div>
                    {{-- <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Pièce d'identité:</label>
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Recto:</label>
                        <input type="file" wire:model='identity_document' class="form-control"> 
                        @error("identity_document")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Verso:</label>
                        <input type="file" wire:model='identity_document_2' class="form-control"> 
                        @error("identity_document_2")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div> --}}
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>
