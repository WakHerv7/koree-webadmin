<div class="container" id="service-modal-container-{{getRandomCode(8)}}">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                    <input type="text" wire:model='servicelibelle' class="form-control"> 
                    @error("servicelibelle")
                        <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Sens</label>
                    <input type="text" disabled wire:model='servicesens' class="form-control"> 
                    @error("servicesens")
                        <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group row">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Taux</label>
                    <input type="number" step="0.01" wire:model='servicerate' class="form-control"> 
                    @error("servicerate")
                        <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Type de service</label>

                        <select wire:model="servicetype" class="form-control">
                            <option value="null" label="Choisir le type" disabled selected></option>
                            @foreach ($servicetypes as $s)
                                <option value="{{ $s['id'] }}">{{ $s['name'] }}</option>
                            @endforeach
                        </select>

                        {{-- <input type="text" wire:model='company' class="form-control">  --}}
                        @error("servicetype")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="d-flex">
                        <div style="width:100%;">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
                            <input type="file" wire:model='serviceimage' class="form-control"> 
                            @error("serviceimage")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="ml-3">
                            @if ($serviceimage)
                                <img src="{{ $serviceimage->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                            @elseif($mountserviceimage)
                            <img src="{{ $mountserviceimage }}" style="border-radius:15px;" width="70px"/>
                            @endif
                            
                        </div>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary">{{$button_text}}</button>                    
            </div>
            </form><!-- row -->
    </div>
</div>


