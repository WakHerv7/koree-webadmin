<div class="container" id="provider-modal-container-{{$provider_id}}">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                    <input type="text" wire:model='providerlibelle' class="form-control"> 
                    @error("providerlibelle")
                        <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group row">
                    <div class="d-flex">
                        <div style="width:100%;">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
                            <input type="file" wire:model='providerimage' class="form-control"> 
                            @error("providerimage")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="ml-3">
                            @if ($providerimage)
                                <img src="{{ $providerimage->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                            @elseif($mountproviderimage)
                            <img src="{{ $mountproviderimage }}" style="border-radius:15px;" width="70px"/>
                            @endif
                            
                        </div>
                    </div>
                </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>


