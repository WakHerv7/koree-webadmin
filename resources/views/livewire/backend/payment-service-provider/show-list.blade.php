<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des fournisseurs de paiement</h3>
        </div>
        <div class="card-toolbar">
            {{-- <a href="{{ route('marketplace.shop.deleted') }}" class="btn btn-dark font-weight-bolder mr-5">
                Commerces désactivés
            </a> --}}
            <!--begin::Button-->
            @can('commerce-create')
            <a href="{{ route('marketplace.shop.create') }}" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#modalform">
                <i class="flaticon2-add-square icon-md"></i>Nouveau fournisseur
            </a>
            @endcan
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table style="overflow-y:auto;" class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
            <tr>   
                <th>Nom</th>
                <th>Depot</th>
                <th>Retrait</th>
                {{-- <th width="180px">Actions</th> --}}
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $p)
                <tr >
                    <td class="d-flex justify-content-between align-items-center payment_provider" style="height:150px; gap:15px;">
                        <div class="provider_libelle">
                            <img src="{{ $p['image'] }}" style="border-radius:15px;" width="70px"/>
                            <span>{{ $p['libelle'] }}</span>
                        </div>
                        <div class="provider_actions">
                            <button type="button" class="btn btn-sm btn-clean" title="Modifier" data-toggle="modal" data-target="#modaleditprovider-{{ $p['id'] }}">
                                <i class='flaticon2-edit icon-md'></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-clean" title="Supprimer" data-toggle="modal" data-target="#modaldeleteprovider-{{ $p['id'] }}">
                                <i class='flaticon-delete icon-md'></i>
                            </button>
                        </div>
                    </td>
                    <td style="width:350px;">
                        <ul>
                            @foreach ($p['services']['in'] as $s)
                            <li class="d-flex justify-content-between align-items-center payment-provider-service" style="gap:15px;">
                                <div class="service_libelle">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" wire:model="checkedServices.{{$p['id']}}.{{$s['id']}}" wire:click="toggleService({{$p['id']}}, {{$s['id']}})" class="custom-control-input status_switch" id="checkedServices.{{$p['id']}}.{{$s['id']}}">
                                        <label wire:loading.remove class="custom-control-label" for="checkedServices.{{$p['id']}}.{{$s['id']}}"></label>
                                        <span style="color:gray" wire:loading><i class="fas fa-spinner fa-pulse"></i></span>
                                    </div>
                                    {{ strtoupper($s['libelle']).' ('.$s['rate'].'%)' }}
                                </div>
                                <div class="service_actions">
                                    <button type="button" wire:click="toggleEditModal({{$p['id']}}, {{$s['id']}})" class="btn btn-sm btn-clean" title="Modifier" data-toggle="modal" data-target="#modaledite-{{$p['id']}}-{{$s['id']}}">
                                        <i class='flaticon2-edit icon-md'></i>
                                    </button>
                                    <button type="button" wire:click="toggleDeleteModal({{$p['id']}}, {{$s['id']}})" class="btn btn-sm btn-clean" data-toggle="modal" data-target="#modaldeleteserv-{{$p['id']}}.{{$s['id']}}">
                                        <i class='flaticon-delete icon-md'></i>
                                    </button>
                                </div>
                            </li>
                            @endforeach

                            <button type="button" wire:click="toggleAddServiceModal({{$p['id']}}, 'in')" class="mt-4 btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalservicein-{{$p['id']}}" style="border-radius: 35px" >
                                <i class="fa fa-plus"></i>
                                Ajouter
                            </button>
                        </ul>
                    </td>
                    <td style="width:350px;">
                        <ul class="payment-provider-list">
                            @foreach ($p['services']['out'] as $s)
                            <li class="d-flex justify-content-between align-items-center payment-provider-service" style="gap:15px;">
                                <div class="service_libelle">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" wire:model="checkedServices.{{$p['id']}}.{{$s['id']}}" wire:click="toggleService({{$p['id']}}, {{$s['id']}})" class="custom-control-input status_switch" id="checkedServices.{{$p['id']}}.{{$s['id']}}">
                                        <label wire:loading.remove class="custom-control-label" for="checkedServices.{{$p['id']}}.{{$s['id']}}"></label>
                                        <span style="color:gray" wire:loading><i class="fas fa-spinner fa-pulse"></i></span>
                                    </div>
                                    {{ strtoupper($s['libelle']).' ('.$s['rate'].'%)' }}
                                </div>
                                <div class="service_actions">
                                    <button type="button" wire:click="toggleEditModal({{$p['id']}}, {{$s['id']}})" class="btn btn-sm btn-clean" title="Modifier">
                                        <i class='flaticon2-edit icon-md'></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-clean" wire:click="toggleDeleteModal({{$p['id']}}, {{$s['id']}})" >
                                        <i class='flaticon-delete icon-md'></i>
                                    </button>
                                </div>
                            </li>
                            @endforeach

                            {{-- <button type="button" class="mt-4 btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalserviceout-{{$p['id']}}" style="border-radius: 35px" >
                                <i class="fa fa-plus"></i>
                                Ajouter
                            </button> --}}
                            <button type="button" wire:click="toggleAddServiceModal({{$p['id']}}, 'out')" class="mt-4 btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modalserviceout-{{$p['id']}}" style="border-radius: 35px" >
                                <i class="fa fa-plus"></i>
                                Ajouter
                            </button>
                        </ul>
                    </td>
                </tr>

                <div class="modal"  id="modaleditprovider-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                            @livewire('backend.payment-service-provider.provider-form', ['provider' => $p], key('editprovider-'.$p['id']))
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
                <div class="modal"  id="modaldeleteprovider-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Supprimer ce fournisseur ?</h6>  
                            <span class="text-danger">Attention! Cette action est irreversible</span>                         
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleProviderDeletion({{ $p['id'] }})" class="btn btn-danger">
                                <span wire:loading.remove>Oui, Supprimer</span>
                                <span wire:loading>En attente suppression <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                            
                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
                
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>

    

    <div class="modal"  id="modalform">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content ">
                @livewire('backend.payment-service-provider.provider-form')
                {{-- ['provider_id'=> $data[0]['shop'], 'shopsubcategories'=>$data] --}}
            </div>
        </div><!-- modal-dialog -->
        <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
    </div>

    @foreach ($data as $key => $p)

        <div id="modalservicein-{{$p['id']}}"
        class="modal {{$allProviders[$p['id']]['in'] ? 'show': ''}}"
        @if ($allProviders[$p['id']]['in']) aria-modal="true"
        role="dialog" style="display:block; padding-right:15px;" @endif
        >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content ">
                <button type="button" wire:click="toggleAddServiceModal({{$p['id']}}, 'in')" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top:15px;right:10px; z-index:1050;font-size:30px">
                    <span aria-hidden="true">&times;</span>
                </button>

                @livewire('backend.payment-service-provider.service-form', ['provider'=>$p, 'sens'=>strtoupper('in')], key('service-form-create-in'.$p['id']))
            </div>
        </div><!-- modal-dialog -->
        <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog --> 
    </div> 
    <div id="modalserviceout-{{$p['id']}}"
    class="modal {{$allProviders[$p['id']]['out'] ? 'show': ''}}"
    @if ($allProviders[$p['id']]['out']) aria-modal="true"
    role="dialog" style="display:block; padding-right:15px;" @endif
    >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content ">
                <button type="button" wire:click="toggleAddServiceModal({{$p['id']}}, 'out')" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top:15px;right:10px; z-index:1050;font-size:30px">
                    <span aria-hidden="true">&times;</span>
                </button>

                @livewire('backend.payment-service-provider.service-form', ['provider'=>$p, 'sens'=>strtoupper('out')], key('service-form-create-out'.$p['id']))
            </div>
        </div><!-- modal-dialog -->
        <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
    </div>


    @foreach ($p['services']['in'] as $s)
        <div 
        id="modaldeleteservice-{{$p['id']}}.{{$s['id']}}" 
        class="modal {{$allServices[$p['id']][$s['id']]['delete'] ? 'show': ''}}"
        @if ($allServices[$p['id']][$s['id']]['delete']) aria-modal="true"
        role="dialog" style="display:block; padding-right:15px;" @endif
        >
            <div class="modal-dialog modal-dialog-centered" role="document" >
                <div class="modal-content ">
                <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                    <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                        <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                        <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                    </p>
                    <h6 class="modal-title">Supprimer ce service ?</h6>  
                    <span class="text-danger">Attention! Cette action est irreversible</span>                         
                </div>
                <div class="mt-3 mb-5 d-flex justify-content-center ">
                    <button type="button" wire:click="handleServiceDeletion({{$s['id']}})" class="btn btn-danger">
                        <span wire:loading.remove>Oui, Supprimer</span>
                        <span wire:loading>En attente suppression <i class="fas fa-spinner fa-pulse"></i></span>
                    </button>
                    <button type="button" data-dismiss="modal" aria-label="Close" wire:click="toggleDeleteModal({{$p['id']}}, {{$s['id']}})" class="btn btn-outline-light ml-3">Annuler</button>
                    
                </div>
                </div>
            </div><!-- modal-dialog -->
            <div class="modal-dialog modal-dialog-centered" data-dismiss="modal" ></div><!-- modal-dialog -->
        </div>
        <div
        id="modaledit-{{$p['id']}}-{{$s['id']}}"
        class="modal {{$allServices[$p['id']][$s['id']]['edit'] ? 'show': ''}}"
        @if ($allServices[$p['id']][$s['id']]['edit']) aria-modal="true"
        role="dialog" style="display:block; padding-right:15px;" @endif
        >
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content ">
                    <button type="button" wire:click="toggleEditModal({{$p['id']}}, {{$s['id']}})" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top:15px;right:10px; z-index:1050;font-size:30px">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @livewire('backend.payment-service-provider.service-form', ['service' => $s, 'provider'=>$p, 'sens'=>$s['sens']], key('service-form-edit-'.$p['id'].'-'.$s['id']))
                </div>
            </div><!-- modal-dialog -->
            <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
        </div>
        @endforeach
        @foreach ($p['services']['out'] as $s)
        <div 
        id="modaldeleteservicein-{{$p['id']}}.{{$s['id']}}" 
        class="modal {{$allServices[$p['id']][$s['id']]['delete'] ? 'show': ''}}"
        @if ($allServices[$p['id']][$s['id']]['delete']) aria-modal="true"
        role="dialog" style="display:block; padding-right:15px;" @endif
        >
            <div class="modal-dialog modal-dialog-centered" role="document" >
                <div class="modal-content ">
                <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                    <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                        <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                        <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                    </p>
                    <h6 class="modal-title">Supprimer ce service ?</h6>  
                    <span class="text-danger">Attention! Cette action est irreversible</span>                         
                </div>
                <div class="mt-3 mb-5 d-flex justify-content-center ">
                    <button type="button" wire:click="handleServiceDeletion({{$s['id']}})" class="btn btn-danger">
                        <span wire:loading.remove>Oui, Supprimer</span>
                        <span wire:loading>En attente suppression <i class="fas fa-spinner fa-pulse"></i></span>
                    </button>
                    <button type="button" data-dismiss="modal" aria-label="Close" wire:click="toggleDeleteModal({{$p['id']}}, {{$s['id']}})" class="btn btn-outline-light ml-3">Annuler</button>
                    
                </div>
                </div>
            </div><!-- modal-dialog -->
            <div class="modal-dialog modal-dialog-centered" data-dismiss="modal" ></div><!-- modal-dialog -->
        </div>
        <div
        id="modaledit-{{$p['id']}}-{{$s['id']}}"
        class="modal {{$allServices[$p['id']][$s['id']]['edit'] ? 'show': ''}}"
        @if ($allServices[$p['id']][$s['id']]['edit']) aria-modal="true"
        role="dialog" style="display:block; padding-right:15px;" @endif
        >
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content ">
                    <button type="button" wire:click="toggleEditModal({{$p['id']}}, {{$s['id']}})" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; top:15px;right:10px; z-index:1050;font-size:30px">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @livewire('backend.payment-service-provider.service-form', ['service' => $s, 'provider'=>$p, 'sens'=>$s['sens']], key('service-form-edit-'.$p['id'].'-'.$s['id']))
                </div>
            </div><!-- modal-dialog -->
            <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
        </div>

        @endforeach
    @endforeach
   

    

</div>
