<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('marketplace.shop.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                        <input type="text" wire:model='libelle' class="form-control"> 
                        @error("libelle")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Contact</label>
                        <input type="text" wire:model='contact' class="form-control"> 
                        @error("contact")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Adresse</label>
                        <input type="text" wire:model='adresse' class="form-control"> 
                        @error("adresse")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">                    
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Categorie</label>
                        <select wire:model="category_id" class="form-control"name="category_id">
                            <option value="">Choisir la categorie...</option>
                            @foreach($categories as $scategory)
                                <option value="{{ $scategory['id'] }}">{{ $scategory['libelle'] }}</option>
                            @endforeach
                        </select>
                        @error("category_id")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                                <input type="file" wire:model='logo' class="form-control"> 
                                @error("logo")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($logo)
                                    <img src="{{ $logo->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountlogo)
                                <img src="{{ $mountlogo }}" style="border-radius:15px;" width="70px"/>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Image Background</label>
                                <input type="file" wire:model='background' class="form-control"> 
                                @error("background")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($background)
                                    <img src="{{ $background->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountbackground)
                                <img src="{{ $mountbackground }}" style="border-radius:15px;" width="70px"/>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>



                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Note</label>
                        <input type="number" step="0.01" wire:model='note' class="form-control"> 
                        @error("note")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Accepte les livraisons</label>
                        <div class="d-flex justify-content-center @error('accept_delivery') has-danger @enderror">
                            <label class="rdiobox">
                                <input wire:model="accept_delivery" value="true" type="radio" />
                                <span>Oui</span>
                            </label>
        
                            <label class="ml-4 rdiobox">
                                <input wire:model="accept_delivery" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        @error('accept_delivery')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Accepte les livraisons externes</label>
                        <div class="d-flex justify-content-center @error('accept_external_delivery') has-danger @enderror">
                            <label class="rdiobox">
                                <input wire:model="accept_external_delivery" value="true" type="radio" />
                                <span>Oui</span>
                            </label>
        
                            <label class="ml-4 rdiobox">
                                <input wire:model="accept_external_delivery" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        @error('accept_external_delivery')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
                        <textarea wire:model="description" class="form-control" placeholder="Description ..."></textarea>
                        @error("description")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>

