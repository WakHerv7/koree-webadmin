<div class="container" id="modal-container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Commerce</label>
                    <select wire:model="shop_id" class="form-control"name="shop_id">
                        <option value="">Choisir le commerce...</option>
                        @foreach($all_shops as $s)
                            <option value="{{ $s['id'] }}">{{ $s['libelle'] }}</option>
                        @endforeach
                    </select>
                    @error("shop_id")
                        <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="d-flex">
                        <div style="width:100%;">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                            <input type="file" wire:model='logo' class="form-control"> 
                            @error("logo")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="ml-3">
                            @if ($logo)
                                <img src="{{ $logo->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                            @elseif($mountlogo)
                            <img src="{{ $mountlogo }}" style="border-radius:15px;" width="70px"/>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="d-flex">
                        <div style="width:100%;">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image Background</label>
                            <input type="file" wire:model='background' class="form-control"> 
                            @error("background")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="ml-3">
                            @if ($background)
                                <img src="{{ $background->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                            @elseif($mountbackground)
                            <img src="{{ $mountbackground }}" style="border-radius:15px;" width="70px"/>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>


