<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('marketplace.article.index', ['shop_id' => $shop['id']]) }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">

                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                        <input type="text" wire:model='libelle' class="form-control">
                        @error("libelle")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-lg-4">
                        <label class="mb-4 az-content-label tx-11 tx-medium tx-gray-600">Categorie</label>

                        <select wire:model="product_category_id" class="form-control">
                            <option value="null" label="Choisir la categorie" disabled selected></option>
                            @foreach ($categories as $c)
                                <option value="{{ $c['id'] }}">{{ $c['libelle'] }}</option>
                            @endforeach
                        </select>
                        @error("product_category_id")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Unité de mesure</label>
                        <input type="text" wire:model='item_unit' class="form-control">
                        @error("item_unit")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Quantite stock</label>
                        <input type="number" wire:model='stock_quantity' class="form-control">
                        @error("stock_quantity")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Prix unitaire</label>
                        <input type="number" wire:model='unit_per_price' class="form-control">
                        @error("unit_per_price")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-lg-4">
                        <label class="d-flex az-content-label tx-11 tx-medium tx-gray-600">Date d'expiration
                        <div class="ml-4 d-flex">
                            <label class="rdiobox">
                                <input wire:model="has_exp_date" value="true" type="radio" />
                                <span>Oui</span>
                            </label>

                            <label class="ml-4 rdiobox">
                                <input wire:model="has_exp_date" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        </label>
                        <input type="date" wire:model='expiration_date' class="form-control" @if($has_exp_date=="false") disabled @endif>
                        @error("expiration_date")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Ticketing ?</label>
                        <div class="d-flex justify-content-center @error('is_ticket') has-danger @enderror">
                            <label class="rdiobox">
                                <input wire:model="is_ticket" value="true" type="radio" />
                                <span>Oui</span>
                            </label>

                            <label class="ml-4 rdiobox">
                                <input wire:model="is_ticket" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        @error('is_ticket')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Menu ?</label>
                        <div class="d-flex justify-content-center @error('is_menu') has-danger @enderror">
                            <label class="rdiobox">
                                <input wire:model="is_menu" value="true" type="radio" />
                                <span>Oui</span>
                            </label>

                            <label class="ml-4 rdiobox">
                                <input wire:model="is_menu" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        @error('is_menu')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4">
                        <div class="d-flex flex-column">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Image</label>
                                <input type="file" multiple accept="image/*" wire:model='logo' class="form-control">
                                @error("logo.*")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($logo)
                                    @foreach($logo as $image)
                                        <img src="{{ $image->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                    @endforeach
                                @elseif($mountlogo)
                                <img src="{{ $mountlogo }}" style="border-radius:15px;" width="70px"/>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Tags</label>
                        <br/>
                        <i class="tx-small tx-gray-600" style="font-size:12px;">Separer les tags par des point-virgules(;)</i>
                        <textarea wire:model="tags" class="form-control" placeholder="Tags ..."></textarea>
                        @error("tags")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
                        <textarea wire:model="description" class="form-control" placeholder="Description ..."></textarea>
                        @error("description")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                @if($is_menu == "true")

                <h6>Composants du menu</h6>
                <div style="width:100%; height:1px; background:rgb(170, 170, 170); margin:15px 0;"></div>


                @foreach ($components as $key => $comp)
                <div class="form-group row" style="position:relative; border:1px solid rgb(170, 170, 170); border-radius:15px; padding:15px; padding-top:35px;">
                    <h6 style="position: absolute; top:5px;left:10px; z-index:1050; font-size:20px;  color:rgb(208, 208, 208)">#{{$key+1}}</h6>

                    @if(is_null($comp['id']))
                        <button type="button" wire:click="removeComponent({{$key}})"class="close" title="Supprimer ce composant" style="position: absolute; top:5px;right:10px; z-index:1050;font-size:30px">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    @else
                        <button type="button" data-toggle="modal" data-target="#modaldeleteaddon-{{ $comp['id'] }}" class="close" title="Supprimer ce composant" style="position: absolute; top:5px;right:10px; z-index:1050;font-size:30px">
                            <i class='flaticon-delete icon-md' style="color:gray;"></i>
                        </button>
                    @endif

                    <div class="col-lg-4 mb-5">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                        <input type="text" wire:model='components.{{$key}}.libelle' class="form-control">
                        @error("components.*.libelle")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4 mb-5">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Obligatoire ?</label>
                        <div class="d-flex justify-content-center @error('components.*.is_required') has-danger @enderror">
                            <label class="rdiobox">
                                <input wire:model="components.{{$key}}.is_required" value="true" type="radio" />
                                <span>Oui</span>
                            </label>

                            <label class="ml-4 rdiobox">
                                <input wire:model="components.{{$key}}.is_required" value="false" type="radio" />
                                <span>Non</span>
                            </label>
                        </div>
                        @error("components.*.is_required")
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4 mb-5">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Quantite </label>
                        <input type="number" wire:model='components.{{$key}}.quantity' class="form-control">
                        @error("components.*.quantity")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-4 mb-5">
                        <div style="display:flex; gap:15px;">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Type</label>
                            {{-- <div style="display:flex; gap:5px; align-items:center;">
                                <span for="component-type-is-new" style="font-size:10px;">Nouveau</span>
                                <input type="checkbox" wire:model="components.{{$key}}.new_type" name="component-type-is-new" id="component-type-is-new">
                            </div> --}}
                        </div>
                        {{-- @if($components[$key]["new_type"])
                            <input type="text" placeholder="Entrer un type..." wire:model='components.{{$key}}.new_type' class="form-control">
                            @error("components.*.new_type")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        @else --}}
                            <select wire:model="components.{{$key}}.type" class="form-control">
                                <option value="null" label="Choisir le type" disabled selected></option>
                                @foreach ($component_types as $c)
                                    <option value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                @endforeach
                            </select>
                            @error("components.*.type")
                                <small class="text-danger">{{$message}}</small>
                            @enderror
                        {{-- @endif --}}
                    </div>
                    <div class="col-lg-4">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Description</label>
                        <textarea wire:model="components.{{$key}}.description" class="form-control" placeholder="Description ..."></textarea>
                        @error("components.*.description")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>

                <div class="modal"  id="modaldeleteaddon-{{ $comp['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Supprimer ce composant: {{$comp['libelle']}} ?</h6>
                            <span class="text-danger">Attention! Cette action est irreversible</span>
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleAddonDeletion({{ $comp['id'] }}, {{$key}})" class="btn btn-danger">
                                <span wire:loading.remove>Oui, Supprimer</span>
                                <span wire:loading>En attente suppression <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>

                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
                @endforeach

                <button type="button" wire:click="addComponent" class="btn btn-sm btn-outline-primary" style="border-radius: 35px" >
                    <i class="fa fa-plus"></i>
                    Ajouter un composant
                </button>

                @endif

                <div class="card-footer mt-5">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>


