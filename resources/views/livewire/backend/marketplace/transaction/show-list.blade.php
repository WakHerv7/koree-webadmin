<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des transactions</h3>
        </div>
        <div class="card-toolbar">
           
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
            <tr>   
                <th>Montant</th>
                <th>Commission</th>
                <th>Benefice</th>
                <th>Commerce</th>
                <th>Client</th>
                {{-- <th>Token de paiement</th>
                <th>Periode de retrait</th> --}}
                <th>Sens</th>
                <th>Statut</th>
                <th width="180px">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $p)
                <tr>
                    <td>{{ $p['amount']}}</td>
                    <td>{{ $p['commission_amount'] }}</td>
                    <td>{{ $p['income_amount'] }}</td>
                    <td>{{ $p['invoice']['shop'] }}</td>
                    <td>{{ $p['invoice']['customer'] }}</td>
                    {{-- <td>{{ $p['pay_token']}}</td>
                    <td>{{ $p['witdrawal_period'] }}</td> --}}
                    <td>
                        @if($p['sens'] == 'IN')
                            <span class="badge badge-primary">Pay In</span>
                        @else
                            <span class="badge badge-dark">Pay Out</span>
                        @endif
                    </td>
                    <td>
                        @if($p['status'] == 'FINISH')
                            <span class="badge badge-success">Acheve</span>
                        @elseif($p['status'] == 'PENDING')
                            <span class="badge badge-warning">En cours</span>
                        @elseif($p['status'] == 'WAIT')
                            <span class="badge badge-danger">En attente</span>
                        @endif
                    </td>
                    <td nowrap="nowrap">

                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" wire:click="show({{ json_encode($p) }})" href="#">
                            <i class="flaticon-eye icon-md"></i>
                        </a>

                        {{-- @can('commerce-edit')
                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" href="{{ route('marketplace.transaction.edit', ['id' => $p['id']]) }}">
                            <i class='flaticon2-edit icon-md'></i>
                        </a>
                        @endcan

                        @can('commerce-delete')

                            <button type="button" class="btn btn-sm btn-clean" data-toggle="modal" data-target="#modaldelete-{{ $p['id'] }}">
                                <i class='flaticon-delete icon-md'></i>
                            </button>
                        @endcan --}}

                    </td>
                </tr>

                <div class="modal"  id="modaldelete-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Désactiver ?<span class="text-danger"></h6>
                            {{-- <p class="text-center"><span class="text-danger mt-2">Cette action est irreversible</span></p> --}}
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleDeletion({{ $p['id'] }})" class="btn btn-danger">
                                <span wire:loading.remove>Oui, Désactiver</span>
                                <span wire:loading>En attente désactivation <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                            
                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
