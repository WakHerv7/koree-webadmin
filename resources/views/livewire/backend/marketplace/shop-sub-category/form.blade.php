<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                {{-- <div class="card-toolbar">
                    <a href="{{ route('marketplace.product_category.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div> --}}
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Categorie</label>

                        <select wire:model="category_id" class="form-control">
                            <option value="null" label="Choisir la categorie" disabled selected></option>
                            @foreach ($categories as $c)
                                <option value="{{ $c['id'] }}">{{ $c['libelle'] }}</option>
                            @endforeach
                        </select>

                        {{-- <input type="text" wire:model='company' class="form-control">  --}}
                        @error("company_id")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>


