<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des categories produits du commerce: {{$shop['libelle']}}</h3>
        </div>
        <div class="card-toolbar">
            {{-- <a href="{{ route('marketplace.shop-sub-category.deleted', ['shop_id' => $shop['id']]) }}" class="btn btn-dark font-weight-bolder mr-5">
                Categories produits désactivés
            </a> --}}
            <!--begin::Button-->
            @can('commerce-create')
            <button class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#modalform">
                <i class="flaticon2-add-square icon-md"></i>Ajouter une Categorie produit
            </button>
            @endcan
            <!--end::Button-->

            <a href="{{ route('marketplace.shop.show', ['id' => $shop['id']]) }}" class="btn btn-success font-weight-bold ml-4">
                <i class="flaticon2-crisp-icons"></i>Retour
            </a>
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table style="overflow-y:auto;" class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
                <tr>   
                    <th>Categorie</th>
                    <th>Nbre de produits</th>
                    <th width="180px">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $p)
                <tr>
                    <td>{{ $p['article_category'] }}</td>
                    <td>{{ $p['total_products'] }}</td>

                    <td nowrap="nowrap">
                        @can('commerce-delete')
                            <button type="button" class="btn btn-sm btn-clean" data-toggle="modal" data-target="#modaldelete-{{ $p['id'] }}">
                                <i class='flaticon-delete icon-md'></i>
                            </button>
                        @endcan
                    </td>
                </tr>

                <div class="modal"  id="modaldelete-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Désactiver ?<span class="text-danger"></h6>
                            {{-- <p class="text-center"><span class="text-danger mt-2">Cette action est irreversible</span></p> --}}
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleDeletion({{ $p['id'] }})" class="btn btn-danger">
                                <span wire:loading.remove>Oui, Désactiver</span>
                                <span wire:loading>En attente désactivation <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                            
                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>


    <div class="modal"  id="modalform">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content ">
                @livewire('backend.marketplace.shop-sub-category.form', ['shop_id'=> $data[0]['shop'], 'shopsubcategories'=>$data])
            </div>
        </div><!-- modal-dialog -->
        <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
    </div>

</div>

