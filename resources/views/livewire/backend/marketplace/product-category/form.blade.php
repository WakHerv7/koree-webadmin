<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('marketplace.product_category.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">

                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Libelle</label>
                        <input type="text" wire:model='libelle' class="form-control"> 
                        @error("libelle")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex">
                            <div style="width:100%;">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Logo</label>
                                <input type="file" wire:model='logo' class="form-control"> 
                                @error("logo")
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="ml-3">
                                @if ($logo)
                                    <img src="{{ $logo->temporaryUrl() }}" style="border-radius:15px;" width="70px">
                                @elseif($mountlogo)
                                <img src="{{ $mountlogo }}" style="border-radius:15px;" width="70px"/>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Couleur Logo:  {{$logo_color}}</label>
                        <input type="color" wire:model='logo_color' class="form-control" id='logo_color' name='logo_color'> 
                        @error("logo_color")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Couleur Background: {{$bg_color}}</label>
                        <input type="color" wire:model='bg_color' class="form-control" id='bg_color' name='bg_color'> 
                        @error("bg_color")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>


