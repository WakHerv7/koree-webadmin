<div class="container">
    <div class="justify-content-center">
        @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">{{ $form_title }}</h3>
                <div class="card-toolbar">
                    <a href="{{ route('marketplace.subscription.index') }}" class="btn btn-success btn-sm font-weight-bold">
                        <i class="flaticon2-crisp-icons"></i>Retour à la liste
                    </a>
                </div>
            </div>

            <form wire:submit.prevent='submitData' class="">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Debut du contrat (Date)</label>
                        <input type="date" wire:model='start_date' class="form-control"> 
                        @error("start_date")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Fin du contrat (Date)</label>
                        <input type="date" wire:model='end_date' class="form-control"> 
                        @error("end_date")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Frais operateur</label>
                        <input type="number" step="0.01" wire:model='operator_load' class="form-control"> 
                        @error("operator_load")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Frais plateforme</label>
                        <input type="number" step="0.01" wire:model='platform_load' class="form-control"> 
                        @error("platform_load")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Taxe</label>
                        <input type="number" step="0.01" wire:model='taxes' class="form-control"> 
                        @error("taxes")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Commerce</label>

                        <select wire:model="company_id" class="form-control">
                            <option value="null" label="Choisir l'entreprise" disabled selected></option>
                            @foreach ($companies as $c)
                                <option value="{{ $c['id'] }}">{{ $c['libelle'] }}</option>
                            @endforeach
                        </select>

                        {{-- <input type="text" wire:model='company' class="form-control">  --}}
                        @error("company_id")
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">{{$button_text}}</button>
                </div>
            </div>
            </form><!-- row -->
    </div>
</div>

