<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Liste des contrats</h3>
        </div>
        <div class="card-toolbar">
            <a href="{{ route('marketplace.subscription.deleted') }}" class="btn btn-dark font-weight-bolder mr-5">
                Contrats désactivés
            </a>
            <!--begin::Button-->
            @can('commerce-create')
            <a href="{{ route('marketplace.subscription.create') }}" class="btn btn-primary font-weight-bolder">
                <i class="flaticon2-add-square icon-md"></i>Nouveau contrat
            </a>
            @endcan
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable1">
            <thead>
            <tr>   
                <th>Debut</th>
                <th>Fin</th>    
                <th>Commerce</th>
                <th>Frais operateur</th>
                <th>Frais plateforme</th>
                <th>Taxe</th>
                <th width="180px">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $p)
                <tr>
                    <td>{{ date('Y-m-d H:i:s', strtotime($p['strat_date'])) }}</td>
                    <td>{{ date('Y-m-d H:i:s', strtotime($p['end_date'])) }}</td>                    
                    <td>{{ $p['shop']['name'] }}</td>
                    <td>{{ $p['operator_load'] }}</td>
                    <td>{{ $p['platform_load'] }}</td>
                    <td>{{ $p['taxes'] }}</td>
                    <td nowrap="nowrap">

                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" wire:click="show({{ json_encode($p) }})" href="#">
                            <i class="flaticon-eye icon-md"></i>
                        </a>

                        @can('commerce-edit')
                        <a class="btn btn-sm btn-clean btn-icon mr-2" title="Modifier details" wire:click="edit({{ json_encode($p) }})" href="#">
                            <i class='flaticon2-edit icon-md'></i>
                        </a>
                        @endcan

                        @can('commerce-delete')

                            <button type="button" class="btn btn-sm btn-clean" data-toggle="modal" data-target="#modaldelete-{{ $p['id'] }}">
                                <i class='flaticon-delete icon-md'></i>
                            </button>
                        @endcan

                    </td>
                </tr>

                <div class="modal"  id="modaldelete-{{ $p['id'] }}">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content ">
                        <div style="font-size: 15px" class="modal-body d-flex justify-content-center align-items-center flex-column">
                            <p class="mb-3 rounded-circle border border-warning d-flex justify-content-center align-items-center" style="height:70px; width:70px; border-width:2px">
                                <i wire:loading.remove class="fa fa-exclamation text-warning" style="font-size: 35px;"></i>
                                <i wire:loading class="fas fa-spinner fa-pulse text-warning" style="font-size: 35px"></i>
                            </p>
                            <h6 class="modal-title">Désactiver ?<span class="text-danger"></h6>
                        </div>
                        <div class="mt-3 mb-5 d-flex justify-content-center ">
                            <button type="button" wire:click="handleDeletion({{ json_encode($p) }})" class="btn btn-danger">
                                <span wire:loading.remove>Oui, Désactiver</span>
                                <span wire:loading>En attente désactivation <i class="fas fa-spinner fa-pulse"></i></span>
                            </button>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-outline-light ml-3">Annuler</button>
                            
                        </div>
                        </div>
                    </div><!-- modal-dialog -->
                    <div class="modal-dialog modal-dialog-centered" data-dismiss="modal"></div><!-- modal-dialog -->
                </div>
            @endforeach

            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
