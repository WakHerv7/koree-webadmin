<table>
    <thead>
    <tr>
        <th colspan="{{ count($headers) }}"
            style="font-weight: 700; text-align: center; font-size: 20px">{{ $caption }}
        </th>
    </tr>
    <tr>
        @foreach($headers as $header)
            <th style="font-weight: 700;">{{ $header }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        {{ $slot }}
    </tbody>
</table>
