<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1"
         data-menu-dropdown-timeout="500">
        <!--begin::Menu Nav-->
        <ul class="menu-nav">
            @foreach($navTree as $nav)

                @empty($nav['children'] )
                    <li class="menu-item" aria-haspopup="true">
                        <a href="{{ $nav['url'] == '' ? 'javascript:;' : $nav['url'] }}" class="menu-link">
                            <x-side-navigation-icon title="{{ $nav['title'] }}"/>&nbsp;
                            <span class="menu-text">{{$nav['title']}}</span>
                        </a>
                    </li>
                @else

                    <li class="menu-item " aria-haspopup="true" data-menu-toggle="hover">
                        <a href="{{ $nav['url'] == '' ? 'javascript:;' : $nav['url'] }}" class="menu-link menu-toggle">
                            <x-side-navigation-icon title="{{ $nav['title'] }}"/>&nbsp;
                            <span class="menu-text">{{ $nav['title'] }}</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="menu-submenu">
                            <i class="menu-arrow"></i>
                            <ul class="menu-subnav">
                                @foreach($nav['children'] as $navChild)

                                    <li class="menu-item" aria-haspopup="true">
                                        <a href="{{ $navChild['url'] == '' ? 'javascript:;' : $navChild['url']}}"
                                           class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">{{ $navChild['title'] }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endempty

            @endforeach


        </ul>
        <!--end::Menu Nav-->
    </div>
</div>
