@extends('layouts.guest')

@section('title', 'Politique de confidentialité - Koree')

@section('content')

    <style>
        #header {
            background: #000000;
        }
    </style>
    <main id="main">
        <br><br><br><br>

        <section class="row justify-content-center">
            <div class="col-md-6">
                <blockquote class="px-4 blockquote">
                    <h1>Politique de confidentialité</h1><br>


                    KOREE CAMEROUN SARL a conçu l'application KOREE comme une application gratuite pour les clients. Ce
                    service est fourni par KOREE CAMEROUN SARL et est destiné à être utilisé tel quel. Cette page est
                    utilisée pour informer les visiteurs et les utilisateurs de nos politiques en matière de collecte,
                    d'utilisation et de divulgation des données personnelles. En choisissant d'utiliser notre service,
                    vous acceptez la collecte, le stockage et l'utilisation des données en relation avec cette
                    politique. Les données personnelles que nous recueillons sont utilisées uniquement pour fournir et
                    améliorer le service. Nous n'utiliserons ni ne partagerons vos données avec quiconque, sauf dans les
                    cas décrits dans la présente politique de confidentialité.

                    <br>Quelles données personnelles sont traitées par KOREE? Pour une meilleure expérience, lors de
                    l'utilisation de notre Service, nous pouvons vous demander de nous fournir certaines données
                    personnelles telles que votre nom, numéro de téléphone, votre date de naissance, adresse postale,
                    horaires d’ouverture de boutique, adresse email et mot de passe. Sans s'y limiter, nous pouvons
                    également demander l'accès à votre caméra, à votre empreinte digitale, à votre répertoire et à votre
                    géolocalisation. Vous pouvez modifier ces données personnelles à tout moment ou ajouter des données
                    supplémentaires dans les paramètres du compte dans l’application mobile KOREE client.
                    <br>En choisissant d’utiliser nos applications vous pouvez choisir de nous donner accès aux données
                    suivantes:
                    <br>géolocalisation. Nous pouvons demander l'accès ou l'autorisation de recevoir les données de
                    géolocalisation de votre téléphone mobile, soit en continu ou soit uniquement pendant que vous
                    utilisez nos applications mobiles, pour vous fournir certains services basés sur la localisation .
                    Si vous souhaitez changer les autorisations que vous nous donnez, vous pouvez le faire directement
                    dans les paramètres de votre smartphone.

                    <br>Accès aux téléphones mobiles. Nous pouvons demander accès à certaines fonctionnalités de votre
                    téléphone mobile, comme les contacts, l’appareil photo, le microphone, l’empreinte digitale, le
                    service de messagerie SMS et d’autres fonctionnalités de votre téléphone mobile. Si vous souhaitez
                    changer les autorisations que vous nous octroyez, vous pouvez le faire directement dans les
                    paramètres de votre smartphone.
                    <br>Données des téléphones mobiles. Nous souhaitons vous informer que nous collectons des données et
                    des informations sur votre téléphone, appelées données de connexion. Ces données peuvent inclure des
                    informations telles que l'adresse du protocole Internet ("IP") de votre appareil, le nom de
                    l'appareil, la version du système d'exploitation, la configuration de l'application lors de
                    l'utilisation de notre service, l'heure et la date de votre utilisation du service et d'autres
                    statistiques.


                    <br><br>Notifications push. Nous pouvons vous demander de vous envoyer des notifications concernant
                    votre compte KOREE ou certaines fonctionnalités de l’application. Si vous souhaitez ne plus recevoir
                    ce type de communications, vous pouvez modifier les autorisations dans les paramètres de votre
                    téléphone.
                    <br>Les cookies. L'application peut utiliser du code et des bibliothèques de tiers qui utilisent des
                    "cookies" pour collecter des informations et améliorer leurs services. Vous avez la possibilité
                    d'accepter ou de refuser ces cookies et de savoir quand un cookie est envoyé à votre appareil. Si
                    vous choisissez de refuser nos cookies, il se peut que vous ne puissiez pas utiliser certaines
                    parties de ce service.
                    <br>Ces données sont principalement nécessaires pour maintenir la sécurité et pour le bon
                    fonctionnement de nos applications. Ces données sont également recueillies pour le dépannage et pour
                    nos analyses internes ou à des fins de rapports internes.

                    <br> Des données personnelles sensibles sont-elles traitées par KOREE? Nous ne traitons pas les
                    données personnelles sensibles .

                    <br>Des données de services tiers sont-elles reçues par KOREE? Nous ne recevons pas toute
                    information de services tiers.

                    <br><br>Comment KOREE traite-elle mes données personnelles? Nous traitons vos données personnelles
                    pour fournir, améliorer et administrer nos Services. Nous utilisons également vos données
                    personnelles pour communiquer avec vous, pour ajouter une couche de sécurité à nos services et pour
                    nous prémunir de toute fraude. Enfin nous utilisons vos données personnelles pour nous conformer à
                    la loi. Cela dit, nous pouvons également traiter vos données personnelles pour d'autres fins et ce
                    toujours avec votre consentement préalable. Nous traitons vos données uniquement lorsque nous avons
                    une raison légale de le faire. Nous apprécions la confiance que vous nous accordez en nous
                    fournissant vos données personnelles donc nous nous efforçons d'utiliser des moyens commercialement
                    acceptables pour protéger ces dernières.
                    <br>Nous traitons vos données personnelles pour diverses raisons, notamment:
                    <br>Pour faciliter la création de compte et l’authentification, pour gérer les comptes utilisateurs.
                    Nous pouvons traiter vos données afin que vous puissiez créer votre compte et vous y connecter.
                    <br> Pour livrer et faciliter la fourniture de services à l'utilisateur. Nous pouvons traiter vos
                    données pour vous fournir tous les services demandés.
                    <br>Pour sauvegarder ou protéger vos intérêts. Nous pouvons traiter vos données lorsque nécessaire
                    pour sauvegarder ou protéger vos intérêts.

                    <br>Dans quelles situations et avec quelles entités mes informations sont-elles partagées?
                    <br>Nous pouvons partager vos données personnelles dans les cas suivants:
                    <br>Transferts d'entreprise . Nous pourrions être amenés à partager ou transférer vos données
                    personnelles en cas de négociations, de fusion, de vente d'actifs de l'entreprise, de financement ou
                    d’acquisition de tout ou d’une partie de notre société par une autre société. Les données
                    personnelles de nos clients seraient l'un des actifs transférables ou acquérables par une société
                    tierce.
                    <br>Affiliés . Nous pouvons partager vos données avec nos affiliés, auquel cas nous exigerons de nos
                    affiliés qu’ils respectent la présente politique de confidentialité. Sont considérés comme
                    “Affiliés” notre société mère et toute filiale, coentreprise partenaire ou tout autre entreprise que
                    nous contrôlons ou dont nous partageons le contrôle.
                    <br>Partenaires commerciaux . Nous pouvons partager vos données personnelles à des sociétés ou des
                    personnes tierces pour les raisons suivantes : - Pour faciliter notre service ; - Pour fournir le
                    service en notre nom ; - Pour effectuer des services liés au Service ; ou - Pour nous aider à
                    analyser la façon dont notre service est utilisé. Nous souhaitons informer les utilisateurs de ce
                    service que ces partenaires commerciaux peuvent avoir accès à vos informations personnelles. La
                    raison en est l'exécution des tâches qui leur sont assignées en notre nom. Toutefois, ils sont tenus
                    de ne divulguer aucune information ni d’utiliser ces informations à d'autres fins que celles que
                    nous leur fournissons.

                    <br><br>Comment KOREE conserve mes données en toute sécurité ? Nous avons mis en place des
                    procédures organisationnelles et techniques pour protéger vos données personnelles. Cependant,
                    n'oubliez pas qu'aucune méthode de transmission sur Internet ou de stockage électronique n'est sûre
                    et fiable à 100% et que nous ne pouvons pas garantir une sécurité absolue.


                    <br><br>Des modifications de la présente politique de confidentialité sont-elles à prévoir? Nous
                    pouvons apporter des changements à notre politique de confidentialité aussi souvent que nécessaire
                    pour rester en conformité avec les lois applicables.
                    <br>KOREE encourage les utilisateurs à se renseigner fréquemment pour toutes les modifications
                    apportées à sa Politique de confidentialité. Si nous apportons un changement significatif à cette
                    politique, nous vous en informerons en publiant la nouvelle politique de confidentialité sur cette
                    page. Votre utilisation continue de nos sites et application après tout changement dans notre
                    politique de confidentialité constitue votre acceptation de tout changement. Cette politique est en
                    vigueur depuis le 2022-.

                    <br><br>Pendant combien de temps mes données personnelles sont-elles conservées?
                    <br>Nous ne conservons pas vos informations plus longtemps que nécessaire pour mener à bien cette
                    politique de confidentialité, sauf disposition contraire de la loi.
                    <br>Nous ne conserverons vos informations personnelles qu'aussi longtemps que nécessaire aux fins
                    énoncées dans la présente politique de confidentialité, à moins qu'une période de conservation plus
                    longue ne soit requise ou requise par la loi (telle que la fiscalité, la comptabilité ou d'autres
                    exigences légales). Aucun objectif de cet avis ne nous obligera à conserver vos informations
                    personnelles plus de soixante (60) mois après le début de la période d'inactivité du compte de
                    l'utilisateur.

                    <br><br>Lorsque nous n'avons aucun besoin commercial légitime de traiter vos informations
                    personnelles, nous supprimerons ou anonymiserons ces informations, ou, si cela n'est pas possible
                    (par exemple, parce que vos informations personnelles ont été stockées dans des archives de
                    sauvegarde), nous les stockerons et les isolerons de tout traitement ultérieur jusqu'à ce que la
                    suppression soit de nouveau possible.

                    <br><br>Nous contacter
                    <br>Si vous avez des questions ou des préoccupations, merci de nous contacter à l’adresse suivante :
                    contact@koree.africa.

                </blockquote>
            </div>
        </section>
    </main>


@endsection
