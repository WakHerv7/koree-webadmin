@extends('layouts.guest')

@section('title', 'MENTIONS LEGALES - Koree')

@section('content')

    <style>
        #header {
            background: #000000;
        }
    </style>
    <main id="main">
        <br><br><br><br>
        <section class="row justify-content-center">
            <div class="col-md-6">
                <blockquote class="blockquote px-4 text-justify">
                    <h1>MENTIONS LEGALES</h1><br>
                    <h2>EDITEUR</h2><br>
                    <p class="text-justify">

                        Les applications mobiles Koree Client & Koree Vendeur ainsi que le site internet https://www.koree.africa
                        sont édités par KOREE CAMEROUN SARL , société par actions simplifiées enregistrée au régistre de commerce et
                        du crédit mobilier N° : .<br>
                        Courriel : contact@koree.africa
                    </p>
                    <br>
                    <br>
                    <h2>HEBERGEUR</h2><br>
                    Le site KOREE est hébergé par la société NAMECHEAP, Inc dont le siège social se situe au 4600 East
                    Washington Street, Suite 305, Phoenix, Arizona 85034, USA.
                    <br>
                    <br>
                    <h2>PROPRIÉTÉ INTELLECTUELLE</h2><br>
                    La structure générale du site KOREE CAMEROUN SARL, ainsi que les textes, graphiques, images, sons et vidéos
                    la composant, sont la propriété de la société KOREE CAMEROUN SARL. Toute représentation et/ou reproduction
                    et/ou exploitation partielle ou totale de ce site, par quelque procédé que ce soit, sans l'autorisation
                    préalable et par écrit de la société KOREE CAMEROUN SARL est strictement interdite et serait susceptible de
                    constituer une contrefaçon au sens du code de la propriété intellectuelle.
                    <br> Date de dernière modification : 2022
                </blockquote>
            </div>
        </section>
    </main>


@endsection
