@extends('layouts.guest')

@section('title', 'Accueil Koree')

@section('content')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center justify-content-center">
        <div class="container" data-aos="fade-up">

            <div class="row row_hero">
                <div class="col-md-7 abso_content" data-aos="fade-right" data-aos-delay="100">
                    <div class="abso_img">
                        <img class="my_wave" src="{{ asset('frontassets/img/wave.png') }}" alt="">
                        <img class="my_star" src="{{ asset('frontassets/img/star.png') }}" alt="">
                    </div>
                    <h1>On vous rend votre monnaie partout avec Koree</h1>
                    <p>Koree l’appli shopping gratuite qui te fait gagner de l’argent et récupérer ta monnaie chez
                        n’importe quel commerçant!</p>

{{--                    <div class="box_gold">--}}
{{--                        <div class="store">--}}
{{--                            <a href="#">--}}
{{--                                <div class="my_btn">--}}
{{--                                    <div class="m_img">--}}
{{--                                        <img src="{{ asset('frontassets/img/icons/apple.png') }}" alt="">--}}
{{--                                    </div>--}}
{{--                                    <div class="text_block">--}}
{{--                                        <h5 class="tx1 tx">Télécharger sur</h5>--}}
{{--                                        <h3 class="tx">App Store</h3>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="play">--}}
{{--                            <a href="#">--}}
{{--                                <div class="my_btn">--}}
{{--                                    <div class="m_img">--}}
{{--                                        <img src="{{ asset('frontassets/img/icons/play.png') }}" alt="">--}}
{{--                                    </div>--}}
{{--                                    <div class="text_block">--}}
{{--                                        <h5 class="tx">Télécharger sur</h5>--}}
{{--                                        <h3 class="tx">Play Store</h3>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}


                    <div class="d-flex justify-content-start">
                        <h1 style="font-size: 20px">Demander une invitation</h1>
                    </div>
                    <div class="d-flex justify-content-start">

                        <form method="post" action="{{ url('/api/waitinglist') }}">
                            @csrf()
                            <div class="input-group mb-3">
                                <input id="phone" class="form-control" name="tel" type="tel">
                                <button type="submit" class="btn btn-outline-secondary btn-send">ENVOYER</button>
                                {{--                                    <button type="submit" class="btn btn-primary btn-sm btn-send">CHECK</button>--}}
                            </div>
                        </form>
                    </div>

                    <div class="box_prime">
                        <div class="p1">
                            <h2>+400 000F</h2>
                            <p>Gagnés par utilisateurs/an</p>
                        </div>
                        <div class="p2">
                            <h2>No.01</h2>
                            <p>Pour garder ton argent</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 rae_content" data-aos="fade-left" data-aos-delay="100">
                    <img class="myfirst" src="{{ asset('frontassets/img/first.png') }}" alt="">
                </div>
            </div>


        </div>
    </section><!-- End Hero -->

    <main id="main">

        <section id="why" class="why">
            <div class="container" data-aos="fade-up">
                <div class="text_title">
                    <h1>Pourquoi Koree ?</h1>
                    <p>Rejoins la révolution du paiement digital en Afrique !</p>
                </div>
                <div class="content_why">
                    <div class="service_card">
                        <div class="content" data-aos="zoom-in">
                            <div class="box_icon">
                                <img src="{{ asset('frontassets/img/icons/portefeuille.png') }}" alt="">
                            </div>
                            <div class="box_text">
                                <h3>Top up</h3>
                                <p>Montre ton QR code en caisse pour récupérer ta monnaie directement sur ton
                                    téléphone!</p>
                            </div>
                        </div>
                    </div>

                    <div class="service_card">
                        <div class="content" data-aos="zoom-in">
                            <div class="box_icon">
                                <img src="{{ asset('frontassets/img/icons/Union.png') }}" alt="">
                            </div>
                            <div class="box_text">
                                <h3>Cashback</h3>
                                <p>Montre ton QR code en caisse et découvre instantanément tes gains crédités sur ton
                                    wallet. Ta fidélité est enfin récompensée!</p>
                            </div>
                        </div>
                    </div>

                    <div class="service_card">
                        <div class="content" data-aos="zoom-in">
                            <div class="box_icon">
                                <img src="{{ asset('frontassets/img/icons/scan.png') }}" alt="">
                            </div>
                            <div class="box_text">
                                <h3>Paiement QR Code</h3>
                                <p>Scanne le QR code Koree de ton marchand préféré pour effectuer tes paiements. Grâce à
                                    la technologie sans contact, scanné c’est payé!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="how" class="how">
            <div class="container">
                <div class="text_title" data-aos="fade-up">
                    <h1>Comment ça marche ?</h1>
                    <p>Gagne de l’argent en faisant tes courses oklm!</p>
                </div>

                <div class="row my_row avant_cb">
                    <div class="col-md-6">
                        <div class="content_box active " data-aos="fade-right">
                            <div class="cercle">
                                <img src="{{ asset('frontassets/img/a1.png') }}" alt="">
                            </div>
                        </div>

                        <div class="content_box" data-aos="fade-right">
                            <div class="cercle">
                                <img src="{{ asset('frontassets/img/a2.png') }}" alt="">
                            </div>
                        </div>

                        <div class="content_box" data-aos="fade-right">
                            <div class="cercle">
                                <img src="{{ asset('frontassets/img/a3.png') }}" alt="">
                            </div>
                        </div>

                        <div class="content_box" data-aos="fade-right">
                            <div class="cercle">
                                <img src="{{ asset('frontassets/img/a4.png') }}" alt="">
                            </div>
                        </div>

                        <div class="content_box" data-aos="fade-right">
                            <div class="cercle">
                                <img src="{{ asset('frontassets/img/a5.png') }}" alt="">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 other_content">
                        <div class="my_boxx active" data-aos="fade-left">
                            <h5>1. Créé ton compte Koree</h5>
                        </div>
                        <div class="my_boxx" data-aos="fade-left">
                            <h5>2. Ajoute les cartes de tes boutiques préférées</h5>
                        </div>
                        <div class="my_boxx" data-aos="fade-left">
                            <h5>3. Montre ton QR code à chaque passage en caisse</h5>
                        </div>
                        <div class="my_boxx" data-aos="fade-left">
                            <h5>4. Garde ta monnaie et gagne tes cashback</h5>
                        </div>
                        <div class="my_boxx" data-aos="fade-left">
                            <h5>5. Paye en boutique</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="about" class="other">
            <div class="container" data-aos="fade-up">
                <div class="content_about">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-3">
                                <img src="{{ asset('frontassets/img/fav.png') }}" alt="">
                            </div>
                            <div class="col col-md-9">
                                <h1 class="title">Avec Koree, tu gagnes en dépensant!</h1>

                            </div>
                        </div>
                        <br>
                        <div class="">
                            <p>Mais, pourquoi “Koree” ? </p><br>
                            <p>Un peu d’histoire ! </p>
                            <p><strong>“Koree”</strong> est dérivée du mot <strong>“cauri”</strong>. Le cauri de son nom
                                “Monetaria moneta”
                                ou “Cypraea moneta” est une espèce de coquillage de la famille des Cypraeidae
                                (les “porcelaines”). Très connu, notamment en Afrique et dans l’océan Indien,
                                le cauri a longtemps été utilisé comme <strong>monnaie d’échange</strong> dans ces
                                régions. </p>
                            <img src="{{ asset('frontassets/img/ko.jpg') }}" class="img-fluid" alt=""><br><br>
                            <p>Au milieu du XIXème siècle le cauri pouvait même s’échanger contre de
                                l’<strong>or</strong> au Mali, à Tombouctou. C’est de là que vient la couleur <strong>dorée</strong>
                                que nous lui avons donnée.</p>
                            <p>Tu l’auras compris, <strong>le cauri est symbole de pouvoir et de richesse!</strong></p>
                            <p>Nous <strong>revenons à nos racines</strong> en payant nos transactions en
                                <strong>Koree</strong> au XXIème siècle.</p><br>
                            <p>L’application <strong>Koree </strong> est née d’une idée en <strong>2018</strong> , lors
                                d’un passage en caisse
                                dans une boulangerie… Face au manque de monnaie dans la caisse, à l’absence
                                de carte de fidélité physique ou dématérialisée et au désarroi de la
                                caissière, il fallait agir!</p> <br>
                            <p><strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Améliorer son
                                    shopping grâce à une application unique</strong></p><br>
                            <p>Nous construisons la première application de shopping d’Afrique Francophone
                                avec une solution simple, sécurisée, 100% numérique et hyper pratique afin
                                que tu puisses faire ton meilleur shopping. <br>
                                Sans chichi ni point de fidélité à cumuler avant d’être transformés, avec
                                Koree c’est en main! Tu dépenses: tu gagnes!
                                <strong> Il est temps que tes achats te rapportent de l’argent, rapidement!</strong></p>
                            <br>
                            <p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Adieu la
                                    frustration</strong></p><br>
                            <p>Nous sommes souvent confrontés à des <strong>problèmes de monnaie</strong> qui
                                rendent les paiements en boutique parfois difficiles. Chez Koree,
                                nous sommes convaincus que <strong>la technologie peut résoudre ce problème!</strong>
                            </p><br>
                            <p>Nous sommes contre les bons d’achat écrits au dos d’un ticket de caisse, et contre tous
                                les goodies donnés en caisse pour éviter de nous rendre notre monnaie! Nous sommes pour
                                les promos, pour les réductions, et pour le gain d’argent! <3</p>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ======= About Section ======= -->


        <section id="what_client" class="what_client">
            <div class="container">
                <div class="text_title" data-aos="fade-up">
                    <h1>Ce que pensent nos <br>
                        Clients</h1>
                </div>
                <div class="row">
                    <div class="box_card col-md-12 col-lg-6" data-aos="fade-right">
                        <div class="card_item ">
                            <img src="{{ asset('frontassets/img/team/team-1.jpg') }}" alt="">
                            <div class="box_text">
                                <h3>Milton Austin</h3>
                                <p>Directeur Commercial, Bady</p>
                            </div>
                        </div>

                        <div class="card_item">
                            <img src="{{ asset('frontassets/img/team/team-3.jpg') }}" alt="">
                            <div class="box_text">
                                <h3>Alzahed Oudini</h3>
                                <p>Directeur Général, Kira</p>
                            </div>
                        </div>

                        <div class="card_item">
                            <img src="{{ asset('frontassets/img/team/team-2.jpg') }}" alt="">
                            <div class="box_text">
                                <h3>Patricia Chou</h3>
                                <p>Directrice Commercial, DevOps</p>
                            </div>
                        </div>

                    </div>
                    <div class="box_comment col-md-12 col-lg-6" data-aos="fade-left">
                        <div class="what_content active">
                            <h2>C’était une belle expérience !</h2>
                            <br>
                            <div class="stars">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                            </div>
                            <br><br>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate <br><br><br>

                                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>

                        <div class="what_content">
                            <h2>Content d'avoir découvert Koree !</h2>
                            <br>
                            <div class="stars">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                            <br><br>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate <br><br><br>

                                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>

                        <div class="what_content">
                            <h2>Quelle appli 😱 C'est un truc de fou !</h2>
                            <br>
                            <div class="stars">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                            <br><br>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                reprehenderit in voluptate <br><br><br>

                                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="waitinglist" class="app">
            <div class="container" data-aos="fade-up">
                <div class="content_app">
                    <div class="container">
                        <h1 class="title">Demander une invitation</h1>
                        <br>
                        <p>Rejoignez plus de 50 millions d'utilisateurs potentiels en Afrique Francophone en
                            téléchargeant Koree gratuitement.</p>

                        <div class="d-flex justify-content-center">

                            <form method="post" action="{{ url('/api/waitinglist') }}">
                                @csrf()
                                <div class="input-group mb-3">
                                    <input id="phone2" class="form-control" name="tel" type="tel">
                                    <button type="submit" class="btn btn-outline-secondary btn-send2">ENVOYER</button>
                                    {{--                                    <button type="submit" class="btn btn-primary btn-sm btn-send">CHECK</button>--}}
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </section>

{{--        <section id="app" class="app">--}}
{{--            <div class="container" data-aos="fade-up">--}}
{{--                <div class="content_app">--}}
{{--                    <div class="container">--}}
{{--                        <h1 class="title">Découvrez notre app mobile</h1>--}}
{{--                        <br>--}}
{{--                        <p>Rejoignez plus de 50 millions d'utilisateurs potentiels en Afrique Francophone en--}}
{{--                            téléchargeant Koree gratuitement.</p>--}}

{{--                        <div class="box_gold">--}}
{{--                            <div class="store">--}}
{{--                                <a href="#">--}}
{{--                                    <div class=" my_btn">--}}
{{--                                        <div class="immg">--}}
{{--                                            <img src="{{ asset('frontassets/img/icons/apple.png') }}" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="text_block">--}}
{{--                                            <h5 class="tx">Télécharger sur</h5>--}}
{{--                                            <h3 class="tx">App Store</h3>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="play">--}}
{{--                                <a href="#">--}}
{{--                                    <div class="my_btn">--}}
{{--                                        <div class=" immg">--}}
{{--                                            <img src="{{ asset('frontassets/img/icons/play.png') }}" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="text_block">--}}
{{--                                            <h5 class="tx">Télécharger sur</h5>--}}
{{--                                            <h3 class="tx">Play Store</h3>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}


        <section id="fa2" class="fa2">
            <div class="container">
                <div class="text_title" data-aos="fade-up">
                    <h1>Foire Aux Questions</h1>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Pourquoi utiliser Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Tu es payé pour dépenser!
                            Avec Koree tu gagnes de l’argent à tous tes passages en caisse.
                            Avec Koree tu peux récupérer ta monnaie partout, adieu les problèmes de monnaie.
                            C’est simple et c’est 100% gratuit.
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Qu’est-ce qu’un cashback?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Le cashback c’est de l’argent que le commerçant te rembourse pour
                            récompenser ta fidélité! Ça correspond à une partie du montant
                            dépensé dans sa boutique. Cet argent peut être dépensé uniquement
                            dans la boutique du commerçant qui te l’a offert..</p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Comment toucher mon cashback?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>KOREE te verse ton cashback dès que tu fais scanner ton QR code chez
                            le commerçant en moins de 10 secondes. Tu toucheras ton argent automatiquement.
                            Aucune action n’est nécessaire de ta part.</p>
                    </div>
                </div>

                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Est-ce que mon cashback peut expirer?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Ton cashback est au chaud dans ton compte KOREE et ne va jamais expirer.</p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Qu’est-ce qu’un top up?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>C'est lorsque tu effectues un achat chez un commerçant et que ta monnaie est
                            reversée sur ton compte Koree</p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Où puis-je utiliser Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Tu peux utiliser ton solde Koree chez n’importe quel commerçant qui utilise l’application
                            Koree.
                            Partage ton QR code Koree à tes amis pour gagner des cashbacks à chacun de leurs achats!
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Comment utiliser mon solde Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Chez ton commerçant Koree, ouvre ton application, clique sur ta carte de fidélité et montre
                            lui ton QR Code. Le commerçant va scanner ton QR code pour enregistrer ton achat.

                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Quels sont les frais sur les transactions Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Ici c’est Koree, et c'est 100% gratuit!
                            Il n’y a aucun frais caché. Il n’y a aucun frais tout court.
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Qu’est ce que le bonus de parrainage Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Parraine tes amis et ta famille pour gagner du cash! Envoies-leur simplement
                            ton code d’invitation pour profiter d’un bonus de parrainage d’une valeur de
                            1 000F sur ton compte KOREE lorsqu’ils effectuent leur premier scan.
                            Ta famille/tes amis gagneront également un bonus de 1 000F s’ils utilisent
                            ton code parrainage pour ouvrir leur compte KOREE. Tu peux inviter
                            autant de personnes que tu le souhaites car il n'y a pas de limite à la
                            récompense de parrainage !</p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Comment recevoir mon bonus parrainage Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Connecte-toi à ton application Koree, clique sur Compte
                            Clique sur Inviter un.e ami.e
                            Tu verras ton code d’invitation apparaître. Clique sur le bouton Inviter un.e ami.e et
                            partage ton code à tous tes amis!
                            Tes amis doivent s’inscrire en utilisant ton code d’invitation pour que vous puissiez chacun
                            recevoir votre bonus.
                            Une fois inscrits, tes amis doivent faire scanner leur QR code dans le commerce de leur
                            choix pour activer le bonus de parrainage!
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Que signifie Bonus Potentiel?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Tu peux suivre tes bonus en cliquant sur Voir les détails de mes bonus
                            Bonus potentiel: <br> Tes amis ont bien utilisé ton code d’invitation pour ouvrir leur
                            compte Koree
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>Que signifie Bonus Activé?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Tu peux suivre tes bonus en cliquant sur Voir les détails de mes bonus
                            <br> Bonus activé: Tes amis ont fait scanner leur QR code après un achat.
                            Instantanément tu recevras ton bonus d’invitation 1 000F dans la dernière boutique dans
                            laquelle tu auras fait un achat! Ton ami.e recevra également son bonus de parrainage 1 000F
                            dans la première boutique dans laquelle il.elle aura utilisé son application!
                        </p>
                    </div>
                </div>
                <div class="wrapper" data-aos="fade-left">
                    <button class="toggle">
                        <h5>J’aimerais utiliser chez un commerçant qui n’accepte pas encore Koree?</h5>
                        <div class="icon_cercle">
                            <i class="fas fa-plus icon"></i>
                        </div>
                    </button>
                    <div class="myfaqcontent">
                        <p>Connecte-toi à l’application. Clique sur Compte Clique sur Suggérer un vendeur.
                            Renseigne tous les champs et Valider. Nous appellerons le.la commerçant.e et lui ferons une
                            offre qu’il.elle ne pourra pas refuser!

                        </p>
                    </div>
                </div>
            </div>
        </section>


        <section id="mycontact" class="mycontact" data-aos="fade-up">
            <div class="container">
                <div class="riba">
                    <div class="box1">
                        <img src="{{ asset('frontassets/img/icons/phone.png') }}" alt="">
                        <h5>Contactez votre service client</h5>
                        <p>Besoin d’assistance ? Notre service client est là pour vous aider du lundi au vendredi de 9 h
                            à 22 h.</p>
                    </div>

                    <div class="box1">
                        <img src="{{ asset('frontassets/img/icons/question.png') }}" alt="">
                        <h5><a href="#fa2">Koree FAQs</a></h5>
                        <p>Vous trouverez peut-être la réponse que vous cherchez dans notre foire aux questions.</p>
                    </div>

                    <div class="box1">
                        <img src="{{ asset('frontassets/img/icons/mail.png') }}" alt="">
                        <h5><a href="mailto:wecare@koree.africa">Adresse Mail</a></h5>
                        <p>Vous pouvez nous contacter à tout moment à l’adresse suivante: wecare@koree.africa. Notre
                            équipe vous répondra sous 24h.</p>
                    </div>
                </div>
            </div>
        </section>


    </main><!-- End #main -->




@endsection

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/css/intlTelInput.min.css">
    <link rel="stylesheet" href="https:////cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        /*.iti { width: 100%; }*/
        .iti__arrow {
            border: none;
        }
    </style>
@endpush

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            window.intlTelInputGlobals.loadUtils("https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/js/utils.js");

            // in
            var input = document.querySelector("#phone");
            var input2 = document.querySelector("#phone2");
            var opt = {
                // any initialisation options go
                initialCountry: "auto",
                separateDialCode: true,
                geoIpLookup: function (success, failure) {
                    $.get("https://ipinfo.io", function () {
                    }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "us";
                        success(countryCode);
                    });
                },
            }
            var iti = window.intlTelInput(input, opt)
            var iti2 = window.intlTelInput(input2, opt);

            $(".btn-send").click(function (e) {
                e.preventDefault();

                toastr.remove()
                toastr.options.newestOnTop = false;
                toastr.options.positionClass = 'toast-bottom-right';

                if (iti.isValidNumber()) {
                    // var number = iti.getNumber();
                    var number = iti.getNumber(intlTelInputUtils.numberFormat.E164);
                    // alert("It's a valid number, congrats!" + number);
                    $.post('{{ url('/api/waitinglist') }}', {
                        "_token": "{{ csrf_token() }}",
                        "tel": number
                    }, function (result) {
                        $('#phone').val('')
                        toastr.success("Démande enregistré", '');
                    });

                } else {
                    e.preventDefault();
                    toastr.error("Not a valid number", '');
                }

            });
            $(".btn-send2").click(function (e) {
                e.preventDefault();

                toastr.remove()
                toastr.options.newestOnTop = false;
                toastr.options.positionClass = 'toast-bottom-right';

                if (iti2.isValidNumber()) {
                    // var number = iti2.getNumber();
                    var number = iti2.getNumber(intlTelInputUtils.numberFormat.E164);
                    // alert("It's a valid number, congrats!" + number);
                    $.post('{{ url('/api/waitinglist') }}', {
                        "_token": "{{ csrf_token() }}",
                        "tel": number
                    }, function (result) {
                        $('#phone2').val('')
                        toastr.success("Démande enregistré", '');
                    });

                } else {
                    e.preventDefault();
                    toastr.error("Not a valid number", '');
                }

            });

        });

    </script>
@endpush
