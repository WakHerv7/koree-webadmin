<?php

namespace Database\Factories;

use App\Models\CategoryCommerce;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryCommerceFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryCommerce::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word,
            'parent_id' => null,
            'created_at' => now(),
            'updated_at' => now(),
            'created_by' => User::first()->id,
        ];
    }
}
