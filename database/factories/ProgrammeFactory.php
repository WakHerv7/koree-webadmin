<?php

namespace Database\Factories;

use App\Models\Commerce;
use App\Models\Programme;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProgrammeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Programme::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'commerce_id' => Commerce::factory(),
            'created_by' => User::first()->id,
            'updated_by' => User::first()->id,
            'status' => true,
            'bonus_wallet' => 0,
            'taux_commission' => 5.5,
            'is_cashback' => false,
            'description' => $this->faker->sentence,
            'libelle' => $this->faker->word,
            'date_debut' => $this->faker->dateTimeBetween('-1 year', '-2 months'),
            'date_fin' => $this->faker->dateTimeBetween('now', '5 months'),
        ];
    }
}
