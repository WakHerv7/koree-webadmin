<?php

namespace Database\Factories;

use App\Models\Quartier;
use App\Models\User;
use App\Models\Vendeur;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class VendeurFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vendeur::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->name,
            'created_by' => User::first()->id,
            'updated_by' => User::first()->id,
            'city_id' => Quartier::first()->ville->id,
            'country_id' => Quartier::first()->ville->pays->id,
            'status' => true,
            'contact_compte' => $this->faker->phoneNumber,
            'tel_fixe' => $this->faker->phoneNumber,
            'adresse' => $this->faker->address,
            'code_postal' => str_replace('-', '0', substr($this->faker->postcode, 0, 5)),
            'rccm' => Str::random(20),
            'fonction' => $this->faker->randomElement(['Vendeur', 'Commercial', 'Directeur']),
            'user_id' => User::factory()
        ];
    }
}
