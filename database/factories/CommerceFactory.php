<?php

namespace Database\Factories;

use App\Models\CategoryCommerce;
use App\Models\Commerce;
use App\Models\Quartier;
use App\Models\User;
use App\Models\Vendeur;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CommerceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Commerce::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $quartier = Quartier::all()->shuffle()->first();
        return [
            'nom' => $this->faker->company,
            'contact' => $this->faker->phoneNumber,
            'description' => $this->faker->sentence,
            'updated_by' => User::first()->id,
            'city_id' => $quartier->ville->id,
            'quartier_id' => $quartier->id,
            'categorie_id' => CategoryCommerce::all()->shuffle()->first()->id,
            'created_by' => User::first()->id,
            'logo' => $this->faker->imageUrl(300, 300),
            'vendeur_id' => Vendeur::factory()
        ];
    }
}
