<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Quartier;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->name,
            'prenom' => $this->faker->name,
            'code_parrainage' => $this->faker->unique()->randomNumber(6),
            'date_naissance' => $this->faker->dateTimeBetween('-60 years', '-18 years'),
            'created_by' => User::first()->id,
            'updated_by' => User::first()->id,
            'city_id' => Quartier::first()->ville->id,
            'country_id' => Quartier::first()->ville->pays->id,
            'sexe' => $this->faker->randomElement(['M', 'F']),
            'status' => true,
            'user_id' => User::factory()
        ];
    }
}
