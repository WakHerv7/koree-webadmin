<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_ventes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 10);
            $table->string('nom', 100);
            $table->text('adresse')->nullable();
            $table->unsignedBigInteger('commerce_id')->nullable();
            $table->timestamps();
            $table->tinyInteger('is_global')->nullable();
            $table->string('nom_responsable', 100)->nullable();
            $table->string('tel_responsable', 15)->nullable();
            $table->text('horaires')->nullable();
            $table->string('localisation')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_ventes');
    }
};
