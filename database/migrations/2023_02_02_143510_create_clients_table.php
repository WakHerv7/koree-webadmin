<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom', 100);
            $table->string('prenom', 100)->nullable();
            $table->string('code_parrainage', 10)->unique();
            $table->date('date_naissance')->nullable();
            $table->char('sexe', 2)->nullable()->default('M');
            $table->unsignedBigInteger('user_id')->nullable()->index('clients_user_id_foreign');
            $table->unsignedBigInteger('city_id')->nullable()->index('clients_city_id_foreign');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->boolean('status')->nullable()->default(true);
            $table->bigInteger('country_id')->nullable();
            $table->text('photo')->nullable();
            $table->integer('cumul_parrainage')->nullable()->default(0);
            $table->boolean('first_achat')->nullable()->default(false);
            $table->unsignedBigInteger('quartier_id')->nullable();
            $table->boolean('deleted')->default(false);
            $table->smallInteger('referrer_amount')->default(500);
            $table->smallInteger('referee_amount')->default(500);
            $table->boolean('influencer')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
