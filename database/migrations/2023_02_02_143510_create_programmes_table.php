<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('libelle', 150);
            $table->text('description');
            $table->date('date_debut');
            $table->date('date_fin')->nullable();
            $table->boolean('is_cashback')->nullable();
            $table->double('taux_commission', 4, 2);
            $table->boolean('status')->default(false);
            $table->unsignedBigInteger('commerce_id')->nullable()->index('programmes_commerce_id_foreign');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->integer('bonus_wallet')->nullable()->default(0);
            $table->integer('mnt_max_facture')->nullable();
            $table->integer('taux_cashback')->nullable();
            $table->integer('montant_abonnement')->nullable();
            $table->integer('duree')->nullable();
            $table->bigInteger('max_cashback')->default(5000);
            $table->double('taux_cashback_vip')->default(10);
            $table->bigInteger('max_cashback_vip')->default(5000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programmes');
    }
};
