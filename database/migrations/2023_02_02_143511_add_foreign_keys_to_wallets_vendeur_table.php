<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallets_vendeur', function (Blueprint $table) {
            $table->foreign(['programme_id'])->references(['id'])->on('programmes')->onUpdate('NO ACTION')->onDelete('SET NULL');
            $table->foreign(['vendeur_id'])->references(['id'])->on('vendeurs')->onUpdate('NO ACTION')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallets_vendeur', function (Blueprint $table) {
            $table->dropForeign('wallets_vendeur_programme_id_foreign');
            $table->dropForeign('wallets_vendeur_vendeur_id_foreign');
        });
    }
};
