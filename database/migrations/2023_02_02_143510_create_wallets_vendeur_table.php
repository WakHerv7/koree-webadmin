<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets_vendeur', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_fidelite', 50)->unique();
            $table->integer('cashback')->nullable()->default(0);
            $table->integer('topup')->nullable()->default(0);
            $table->integer('passages')->nullable()->default(0);
            $table->unsignedBigInteger('vendeur_id')->nullable()->index('wallets_vendeur_vendeur_id_foreign');
            $table->unsignedBigInteger('programme_id')->nullable()->index('wallets_vendeur_programme_id_foreign');
            $table->timestamps();
            $table->integer('payment')->default(0);
            $table->integer('bonus')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets_vendeur');
    }
};
