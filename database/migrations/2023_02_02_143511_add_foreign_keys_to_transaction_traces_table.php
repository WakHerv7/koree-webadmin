<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_traces', function (Blueprint $table) {
            $table->foreign(['transaction_id'])->references(['id'])->on('transactions')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign(['agent_id'])->references(['id'])->on('agents')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['fraud_id'])->references(['id'])->on('fraud_configs')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['vendeur_id'])->references(['id'])->on('vendeurs')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['client_id'])->references(['id'])->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_traces', function (Blueprint $table) {
            $table->dropForeign('transaction_traces_transaction_id_foreign');
            $table->dropForeign('transaction_traces_agent_id_foreign');
            $table->dropForeign('transaction_traces_fraud_id_foreign');
            $table->dropForeign('transaction_traces_vendeur_id_foreign');
            $table->dropForeign('transaction_traces_client_id_foreign');
        });
    }
};
