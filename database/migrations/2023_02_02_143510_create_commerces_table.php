<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commerces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom', 150);
            $table->string('contact', 150)->nullable();
            $table->text('description')->nullable();
            $table->text('logo')->nullable();
            $table->unsignedBigInteger('vendeur_id')->nullable()->index('commerces_vendeur_id_foreign');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->boolean('is_boost')->nullable();
            $table->integer('quartier_id')->nullable();
            $table->string('couleurs', 50)->nullable();
            $table->text('horaires')->nullable();
            $table->text('adresse')->nullable();
            $table->text('localisation')->nullable();
            $table->string('card')->nullable();
            $table->string('background')->nullable();
            $table->smallInteger('referee_amount')->default(500);
            $table->smallInteger('referer_amount')->default(500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commerces');
    }
};
