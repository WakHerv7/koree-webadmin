<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_cashbacks', function (Blueprint $table) {
            $table->foreign(['commerce_id'])->references(['id'])->on('commerces')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_cashbacks', function (Blueprint $table) {
            $table->dropForeign('custom_cashbacks_commerce_id_foreign');
        });
    }
};
