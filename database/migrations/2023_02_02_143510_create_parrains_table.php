<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parrains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parrain_id');
            $table->unsignedBigInteger('programme_id')->index('parrains_programme_id_foreign');
            $table->unsignedBigInteger('filleul_id')->index('parrains_filleul_id_foreign');
            $table->timestamps();

            $table->unique(['parrain_id', 'programme_id', 'filleul_id'], 'parrain-filleul-unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parrains');
    }
};
