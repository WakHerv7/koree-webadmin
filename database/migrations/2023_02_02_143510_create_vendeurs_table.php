<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendeurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom', 100);
            $table->text('adresse')->nullable();
            $table->string('code_postal', 50)->nullable();
            $table->string('telephone2', 35)->nullable();
            $table->string('rccm', 25)->nullable();
            $table->string('contact_compte', 25)->nullable();
            $table->string('telephone1', 75)->nullable();
            $table->boolean('status')->nullable()->default(false);
            $table->unsignedBigInteger('user_id')->nullable()->index('vendeurs_user_id_foreign');
            $table->unsignedBigInteger('city_id')->nullable()->index('vendeurs_city_id_foreign');
            $table->unsignedBigInteger('country_id')->nullable()->index('vendeurs_country_id_foreign');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->string('code_transaction', 7)->nullable();
            $table->string('prenom', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendeurs');
    }
};
