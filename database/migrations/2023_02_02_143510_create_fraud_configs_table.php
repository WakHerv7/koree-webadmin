<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fraud_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('value');
            $table->integer('interval_value')->default(1);
            $table->enum('interval_unit', ['minute', 'hour', 'day', 'week', 'month', 'year', 'infinity'])->default('hour');
            $table->enum('transaction_type', ['cashback', 'topup', 'all', 'paiement'])->default('all');
            $table->boolean('global')->default(true);
            $table->boolean('look_agent')->default(true);
            $table->boolean('flag_relatives')->default(false);
            $table->boolean('notify_shop_owner')->default(false);
            $table->boolean('notify_admin')->default(true);
            $table->enum('restrict', ['no', 'yes', 'pending'])->default('no');
            $table->integer('restriction_duration_value')->default(-1);
            $table->enum('restriction_duration_unit', ['minute', 'hour', 'day', 'week', 'month', 'year', 'infinity'])->default('infinity');
            $table->smallInteger('score')->default(100);
            $table->boolean('compare_to_shop_limit')->default(false);
            $table->boolean('authorize')->default(true);
            $table->enum('value_type', ['amount', 'count'])->default('amount');
            $table->timestamps();
            $table->string('vendor_message')->nullable();
            $table->string('admin_message')->nullable();
            $table->boolean('active')->default(true);
            $table->string('user_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fraud_configs');
    }
};
