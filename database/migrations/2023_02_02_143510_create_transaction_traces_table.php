<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_traces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->nullable()->index('transaction_traces_transaction_id_foreign');
            $table->unsignedBigInteger('client_id')->index();
            $table->unsignedBigInteger('vendeur_id')->index();
            $table->unsignedBigInteger('agent_id')->nullable()->index();
            $table->unsignedBigInteger('fraud_id')->index();
            $table->boolean('authorized')->default(true);
            $table->integer('amount')->default(0);
            $table->enum('transaction_type', ['cashback', 'topup', 'all', 'paiement'])->default('all');
            $table->boolean('pending')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_traces');
    }
};
