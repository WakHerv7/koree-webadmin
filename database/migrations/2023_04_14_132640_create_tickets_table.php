<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('ocr_data');
            $table->enum('ocr_driver', ['cloud_vision', 'aws_textract']);
            $table->decimal('amount');
            $table->string('bill_ref')->nullable();
            $table->string('content_hash')->comment('hash value of orc_data');
            $table->string('content_hash_algo')->unique();
            $table->unsignedBigInteger('wallet_id')->nullable()->index('tickets_wallet_id_foreign');
            $table->dateTime('charged_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
