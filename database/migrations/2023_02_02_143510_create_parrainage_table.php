<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parrainage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parrain_id');
            $table->unsignedBigInteger('filleul_id');
            $table->string('code_parrainage');
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parrainage');
    }
};
