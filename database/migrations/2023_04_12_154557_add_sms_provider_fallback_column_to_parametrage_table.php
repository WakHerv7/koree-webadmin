<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('parametrage', function (Blueprint $table) {
            if (! Schema::hasColumn($table->getTable(), 'sms_provider_fallback')) {
                $table->timestamp('sms_provider_fallback')->nullable()->after('sms_provider');
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('parametrage', function (Blueprint $table) {
            //
        });
    }
};
