<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_fidelite', 50)->unique();
            $table->integer('montant')->nullable()->default(0);
            $table->integer('bonus')->default(0)->comment('Total bonus');
            $table->integer('cashback')->nullable()->default(0);
            $table->integer('topup')->nullable()->default(0);
            $table->integer('passages')->nullable()->default(0);
            $table->unsignedBigInteger('programme_id')->nullable()->index('wallets_programme_id_foreign');
            $table->unsignedBigInteger('client_id')->nullable()->index('wallets_client_id_foreign');
            $table->timestamps();
            $table->boolean('deleted')->default(false);
            $table->boolean('vip')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
};
