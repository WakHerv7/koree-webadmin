<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * - ticket_scan_quota_per_user_daily
 *   Nombre de Scans par jour par commerce: chaque commerce détermine le nombre de ticket qu'il accepte par utilisateur par jour
 * - ticket_scan_quota_all_user_daily
 *   Identique a la regle precedent  mais pour tous les utilisateurs
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shop_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shop_id')->index('shop_configurations_shop_id_foreign');
            $table->smallInteger('ticket_scan_quota_per_user_daily')->default(1)->comment('');
            $table->smallInteger('ticket_scan_quota_all_user_daily')->default(100)->comment('');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shop_configurations');
    }
};
