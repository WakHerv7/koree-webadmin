<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * - ticket_scan_quota_per_user_daily
 *   Nombre de Scans par jour par commerce: chaque commerce détermine le nombre de ticket qu'il accepte par utilisateur par jour
 * - ticket_scan_quota_all_user_daily
 *   Identique a la regle precedent  mais pour tous les utilisateurs
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('shop_configurations', function (Blueprint $table) {
            // TODO to fix
//            $table->foreign(['shop_id'])->references(['id'])->on('commerces')->onUpdate('NO ACTION')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('shop_configurations', function (Blueprint $table) {
            $table->dropForeign('shop_configurations_shop_id_foreign');
        });
    }
};
