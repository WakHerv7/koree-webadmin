<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggest_vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->nullable();
            $table->string('shop_name');
            $table->string('shop_owner_lastname')->nullable();
            $table->string('shop_owner_firstname')->nullable();
            $table->string('shop_owner_phone')->nullable();
            $table->unsignedBigInteger('shop_city_id')->nullable();
            $table->unsignedBigInteger('shop_district_id')->nullable();
            $table->string('shop_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggest_vendors');
    }
};
