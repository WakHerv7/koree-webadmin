<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creation_vendeur', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom_societe', 150);
            $table->text('adresse')->nullable();
            $table->string('code_postal', 5);
            $table->string('ville', 35);
            $table->string('pays', 25);
            $table->string('tel_fixe', 35)->nullable();
            $table->string('rccm', 25)->nullable();
            $table->string('contact_compte', 25)->nullable();
            $table->string('telephone', 25);
            $table->string('fonction', 75)->nullable();
            $table->string('email', 25);
            $table->boolean('status')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('creation_vendeur');
    }
};
