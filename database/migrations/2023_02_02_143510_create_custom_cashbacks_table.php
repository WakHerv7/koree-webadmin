<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_cashbacks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('commerce_id')->index('custom_cashbacks_commerce_id_foreign');
            $table->integer('max_amount');
            $table->double('cashback_pct', 8, 2);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->double('vip_cashback_pct')->default(10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_cashbacks');
    }
};
