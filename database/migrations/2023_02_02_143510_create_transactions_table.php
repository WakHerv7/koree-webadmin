<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wallet_id')->nullable()->index('transactions_wallet_id_foreign');
            $table->unsignedBigInteger('wallet_vendeur_id')->nullable()->index('transactions_wallets_vendeur_id_foreign');
            $table->integer('achat_id')->nullable();
            $table->unsignedBigInteger('agent_id')->nullable();
            $table->char('uuid', 36)->nullable();
            $table->string('transaction_intent_id', 50)->nullable();
            $table->integer('montant');
            $table->integer('gain');
            $table->string('type_transaction', 25)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('solde_wallet')->nullable()->default(0);
            $table->tinyInteger('status_koree')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
