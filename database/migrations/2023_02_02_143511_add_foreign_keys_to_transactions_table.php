<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign(['wallet_id'])->references(['id'])->on('wallets')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['wallet_vendeur_id'], 'transactions_wallets_vendeur_id_foreign')->references(['id'])->on('wallets_vendeur')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('transactions_wallet_id_foreign');
            $table->dropForeign('transactions_wallets_vendeur_id_foreign');
        });
    }
};
