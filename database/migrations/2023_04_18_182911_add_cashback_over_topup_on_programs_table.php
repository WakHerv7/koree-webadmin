<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('programmes', function (Blueprint $table) {
            if (! Schema::hasColumn($table->getTable(), 'cashback_over_topup')) {
                $table->boolean('cashback_over_topup')->default(false);
            }
        });
    }

    public function down(): void
    {
        Schema::table('programmes', function (Blueprint $table) {
            $table->dropColumn('cashback_over_topup');
        });
    }
};
