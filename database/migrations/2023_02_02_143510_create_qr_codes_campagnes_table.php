<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_codes_campagnes', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('nom');
            $table->dateTime('date_from');
            $table->dateTime('date_until');
            $table->smallInteger('quantite');
            $table->bigInteger('commerce_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_codes_campagnes');
    }
};
