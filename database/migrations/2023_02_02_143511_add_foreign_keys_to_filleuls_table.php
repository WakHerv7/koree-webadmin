<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('filleuls', function (Blueprint $table) {
            $table->foreign(['filleul_id'])->references(['id'])->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['programme_id'])->references(['id'])->on('programmes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['parrain_id'])->references(['id'])->on('clients')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('filleuls', function (Blueprint $table) {
            $table->dropForeign('filleuls_filleul_id_foreign');
            $table->dropForeign('filleuls_programme_id_foreign');
            $table->dropForeign('filleuls_parrain_id_foreign');
        });
    }
};
