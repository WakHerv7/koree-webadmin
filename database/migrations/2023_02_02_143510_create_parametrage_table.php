<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametrage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom_societe', 100);
            $table->string('email', 100);
            $table->string('email_support', 100);
            $table->string('telephone', 15)->nullable();
            $table->string('fax', 35)->nullable();
            $table->text('address')->nullable();
            $table->integer('mnt_wallet_max')->nullable();
            $table->integer('transaction_hebdo_max')->nullable();
            $table->integer('transaction_mensuelle_max')->nullable();
            $table->integer('plafond_parrainage')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->integer('app_version')->default(1);
            $table->string('otp_number')->nullable();
            $table->string('otp_email')->nullable();
            $table->string('sms_provider', 50)->default('TECHSOFT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametrage');
    }
};
