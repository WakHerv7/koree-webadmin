<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id')->nullable()->index('influencer_changes_client_id_foreign');
            $table->unsignedBigInteger('admin_id')->nullable()->index('influencer_changes_admin_id_foreign');
            $table->enum('from', ['client', 'influencer'])->default('client');
            $table->enum('to', ['client', 'influencer'])->default('influencer');
            $table->dateTime('date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_changes');
    }
};
