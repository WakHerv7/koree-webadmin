<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('campagne_id', 36)->nullable()->index('qr_codes_campagne_id_foreign');
            $table->char('uuid', 36);
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_until')->nullable();
            $table->dateTime('date_used')->nullable();
            $table->string('type');
            $table->string('data', 4296);
            $table->smallInteger('number');
            $table->string('group_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_codes');
    }
};
