<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ParrainageTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('parrainage')->delete();
        
        \DB::table('parrainage')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parrain_id' => 1,
                'filleul_id' => 2,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'parrain_id' => 1,
                'filleul_id' => 4,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:32',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'parrain_id' => 1,
                'filleul_id' => 6,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:32',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'parrain_id' => 1,
                'filleul_id' => 8,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:32',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'parrain_id' => 1,
                'filleul_id' => 10,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:32',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'parrain_id' => 1,
                'filleul_id' => 12,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:32',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'parrain_id' => 1,
                'filleul_id' => 14,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'parrain_id' => 1,
                'filleul_id' => 16,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'parrain_id' => 1,
                'filleul_id' => 18,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'parrain_id' => 1,
                'filleul_id' => 20,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'parrain_id' => 1,
                'filleul_id' => 22,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'parrain_id' => 1,
                'filleul_id' => 24,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:33',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'parrain_id' => 1,
                'filleul_id' => 26,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:34',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'parrain_id' => 1,
                'filleul_id' => 28,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:34',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'parrain_id' => 1,
                'filleul_id' => 30,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:34',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'parrain_id' => 1,
                'filleul_id' => 32,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:34',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'parrain_id' => 1,
                'filleul_id' => 34,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:34',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'parrain_id' => 1,
                'filleul_id' => 36,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'parrain_id' => 1,
                'filleul_id' => 38,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'parrain_id' => 1,
                'filleul_id' => 40,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'parrain_id' => 1,
                'filleul_id' => 42,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'parrain_id' => 1,
                'filleul_id' => 44,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'parrain_id' => 1,
                'filleul_id' => 46,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:35',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'parrain_id' => 1,
                'filleul_id' => 48,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'parrain_id' => 1,
                'filleul_id' => 50,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'parrain_id' => 1,
                'filleul_id' => 52,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'parrain_id' => 1,
                'filleul_id' => 54,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'parrain_id' => 1,
                'filleul_id' => 56,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'parrain_id' => 1,
                'filleul_id' => 58,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:36',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'parrain_id' => 1,
                'filleul_id' => 60,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'parrain_id' => 1,
                'filleul_id' => 62,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'parrain_id' => 1,
                'filleul_id' => 64,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'parrain_id' => 1,
                'filleul_id' => 66,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'parrain_id' => 1,
                'filleul_id' => 68,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'parrain_id' => 1,
                'filleul_id' => 70,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:37',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'parrain_id' => 1,
                'filleul_id' => 72,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'parrain_id' => 1,
                'filleul_id' => 74,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'parrain_id' => 1,
                'filleul_id' => 76,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'parrain_id' => 1,
                'filleul_id' => 78,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'parrain_id' => 1,
                'filleul_id' => 80,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'parrain_id' => 1,
                'filleul_id' => 82,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:38',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'parrain_id' => 1,
                'filleul_id' => 84,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:39',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'parrain_id' => 1,
                'filleul_id' => 86,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:39',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'parrain_id' => 1,
                'filleul_id' => 88,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:39',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'parrain_id' => 1,
                'filleul_id' => 90,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:39',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'parrain_id' => 1,
                'filleul_id' => 92,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:39',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'parrain_id' => 1,
                'filleul_id' => 94,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'parrain_id' => 1,
                'filleul_id' => 96,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'parrain_id' => 1,
                'filleul_id' => 98,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'parrain_id' => 1,
                'filleul_id' => 100,
                'code_parrainage' => 'LEON16',
                'status' => 1,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => '2022-09-07 10:26:48',
            ),
            50 => 
            array (
                'id' => 51,
                'parrain_id' => 1,
                'filleul_id' => 102,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'parrain_id' => 1,
                'filleul_id' => 104,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:40',
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'parrain_id' => 1,
                'filleul_id' => 106,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'parrain_id' => 1,
                'filleul_id' => 108,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'parrain_id' => 1,
                'filleul_id' => 110,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'parrain_id' => 1,
                'filleul_id' => 112,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'parrain_id' => 1,
                'filleul_id' => 114,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'parrain_id' => 1,
                'filleul_id' => 116,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:41',
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'parrain_id' => 1,
                'filleul_id' => 118,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'parrain_id' => 1,
                'filleul_id' => 120,
                'code_parrainage' => 'LEON16',
                'status' => 0,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}