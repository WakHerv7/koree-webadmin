<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PersonalAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('personal_access_tokens')->delete();
        
        \DB::table('personal_access_tokens')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tokenable_type' => 'App\\Models\\User',
                'tokenable_id' => 2,
                'name' => '00237696852116',
                'token' => 'f82bc55ec9e27115fb0e8a636150ea1da811b69ace4c439243801b6e8a1f1c48',
                'abilities' => '["*"]',
                'last_used_at' => '2022-09-23 18:55:14',
                'created_at' => '2022-09-23 16:00:30',
                'updated_at' => '2022-09-23 18:55:14',
            ),
        ));
        
        
    }
}