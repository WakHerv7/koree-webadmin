<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShopReferralsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shop_referrals')->delete();
        
        \DB::table('shop_referrals')->insert(array (
            0 => 
            array (
                'id' => 1,
                'commerce_id' => 10,
                'parrain_id' => 1,
                'client_id' => 100,
                'created_at' => '2022-09-07 10:33:14',
                'updated_at' => '2022-09-07 10:33:14',
            ),
            1 => 
            array (
                'id' => 2,
                'commerce_id' => 9,
                'parrain_id' => 1,
                'client_id' => 121,
                'created_at' => '2022-09-07 17:21:53',
                'updated_at' => '2022-09-07 17:21:53',
            ),
            2 => 
            array (
                'id' => 3,
                'commerce_id' => 1,
                'parrain_id' => 1,
                'client_id' => 121,
                'created_at' => '2022-09-07 17:40:04',
                'updated_at' => '2022-09-07 17:40:04',
            ),
        ));
        
        
    }
}