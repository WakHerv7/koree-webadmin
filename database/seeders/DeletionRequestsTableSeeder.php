<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DeletionRequestsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('deletion_requests')->delete();
        
        
        
    }
}