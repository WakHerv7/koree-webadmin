<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'wallet_id' => 692,
                'wallet_vendeur_id' => 2,
                'montant' => 20000,
                'gain' => 800,
                'type_transaction' => 'topup',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:26:48',
                'updated_at' => '2022-09-07 10:26:48',
                'status' => 0,
                'achat_id' => 1,
                'solde_wallet' => 20000,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'wallet_id' => 692,
                'wallet_vendeur_id' => 2,
                'montant' => 2400,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:26:48',
                'updated_at' => '2022-09-07 10:26:48',
                'status' => 1,
                'achat_id' => 1,
                'solde_wallet' => 22400,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 500,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:26:48',
                'updated_at' => '2022-09-07 10:26:48',
                'status' => 1,
                'achat_id' => 1,
                'solde_wallet' => 500,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'wallet_id' => 692,
                'wallet_vendeur_id' => 2,
                'montant' => 500,
                'gain' => 0,
                'type_transaction' => 'first_buy',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:26:49',
                'updated_at' => '2022-09-07 10:26:49',
                'status' => 1,
                'achat_id' => 1,
                'solde_wallet' => 22900,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'wallet_id' => 690,
                'wallet_vendeur_id' => 10,
                'montant' => 20000,
                'gain' => 800,
                'type_transaction' => 'topup',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:35:25',
                'updated_at' => '2022-09-07 10:35:25',
                'status' => 0,
                'achat_id' => 2,
                'solde_wallet' => 20000,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'wallet_id' => 690,
                'wallet_vendeur_id' => 10,
                'montant' => 2400,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:35:25',
                'updated_at' => '2022-09-07 10:35:25',
                'status' => 1,
                'achat_id' => 2,
                'solde_wallet' => 22400,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'wallet_id' => 3,
                'wallet_vendeur_id' => 10,
                'montant' => 2823,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:35:25',
                'updated_at' => '2022-09-07 10:35:25',
                'status' => 1,
                'achat_id' => 2,
                'solde_wallet' => 2823,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'wallet_id' => 690,
                'wallet_vendeur_id' => 10,
                'montant' => 4945,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 101,
                'created_at' => '2022-09-07 10:35:25',
                'updated_at' => '2022-09-07 10:35:25',
                'status' => 1,
                'achat_id' => 2,
                'solde_wallet' => 27345,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 2000,
                'gain' => 80,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:23:38',
                'updated_at' => '2022-09-07 17:23:38',
                'status' => 0,
                'achat_id' => 3,
                'solde_wallet' => 2000,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 300,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:23:38',
                'updated_at' => '2022-09-07 17:23:38',
                'status' => 1,
                'achat_id' => 3,
                'solde_wallet' => 2300,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 2000,
                'gain' => 80,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:33:06',
                'updated_at' => '2022-09-07 17:33:06',
                'status' => 0,
                'achat_id' => 4,
                'solde_wallet' => 4300,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 300,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:33:06',
                'updated_at' => '2022-09-07 17:33:06',
                'status' => 1,
                'achat_id' => 4,
                'solde_wallet' => 4600,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 2000,
                'gain' => 80,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:36:27',
                'updated_at' => '2022-09-07 17:36:27',
                'status' => 0,
                'achat_id' => 5,
                'solde_wallet' => 6600,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 9,
                'montant' => 300,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:36:27',
                'updated_at' => '2022-09-07 17:36:27',
                'status' => 1,
                'achat_id' => 5,
                'solde_wallet' => 6900,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'wallet_id' => 843,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:40:29',
                'updated_at' => '2022-09-07 17:40:29',
                'status' => 0,
                'achat_id' => 6,
                'solde_wallet' => 2000,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'wallet_id' => 843,
                'wallet_vendeur_id' => 1,
                'montant' => 210,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:40:29',
                'updated_at' => '2022-09-07 17:40:29',
                'status' => 1,
                'achat_id' => 6,
                'solde_wallet' => 2210,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'wallet_id' => 14,
                'wallet_vendeur_id' => 1,
                'montant' => 4446,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:40:29',
                'updated_at' => '2022-09-07 17:40:29',
                'status' => 1,
                'achat_id' => 6,
                'solde_wallet' => 4446,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'wallet_id' => 843,
                'wallet_vendeur_id' => 1,
                'montant' => 2075,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 122,
                'created_at' => '2022-09-07 17:40:29',
                'updated_at' => '2022-09-07 17:40:29',
                'status' => 1,
                'achat_id' => 6,
                'solde_wallet' => 4285,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:23:33',
                'updated_at' => '2022-09-20 12:23:33',
                'status' => 0,
                'achat_id' => 7,
                'solde_wallet' => 8900,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:23:33',
                'updated_at' => '2022-09-20 12:23:33',
                'status' => 1,
                'achat_id' => 7,
                'solde_wallet' => 8960,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 4446,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:23:34',
                'updated_at' => '2022-09-20 12:23:34',
                'status' => 1,
                'achat_id' => 7,
                'solde_wallet' => 4946,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 2075,
                'gain' => 0,
                'type_transaction' => 'parrainage',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:23:34',
                'updated_at' => '2022-09-20 12:23:34',
                'status' => 1,
                'achat_id' => 7,
                'solde_wallet' => 11035,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:38:02',
                'updated_at' => '2022-09-20 12:38:02',
                'status' => 0,
                'achat_id' => 8,
                'solde_wallet' => 13035,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-20 12:38:02',
                'updated_at' => '2022-09-20 12:38:02',
                'status' => 1,
                'achat_id' => 8,
                'solde_wallet' => 13095,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-21 16:22:53',
                'updated_at' => '2022-09-21 16:22:53',
                'status' => 0,
                'achat_id' => 9,
                'solde_wallet' => 15095,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-21 16:22:53',
                'updated_at' => '2022-09-21 16:22:53',
                'status' => 1,
                'achat_id' => 9,
                'solde_wallet' => 15155,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 122,
                'created_at' => '2022-09-21 17:06:25',
                'updated_at' => '2022-09-21 17:06:25',
                'status' => 0,
                'achat_id' => 10,
                'solde_wallet' => 17155,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'wallet_id' => 845,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 122,
                'created_at' => '2022-09-21 17:06:25',
                'updated_at' => '2022-09-21 17:06:25',
                'status' => 1,
                'achat_id' => 10,
                'solde_wallet' => 17215,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:14',
                'updated_at' => '2022-09-22 12:41:14',
                'status' => 0,
                'achat_id' => 11,
                'solde_wallet' => 6946,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:14',
                'updated_at' => '2022-09-22 12:41:14',
                'status' => 1,
                'achat_id' => 11,
                'solde_wallet' => 7006,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:36',
                'updated_at' => '2022-09-22 12:41:36',
                'status' => 0,
                'achat_id' => 12,
                'solde_wallet' => 9006,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:36',
                'updated_at' => '2022-09-22 12:41:36',
                'status' => 1,
                'achat_id' => 12,
                'solde_wallet' => 9066,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:45',
                'updated_at' => '2022-09-22 12:41:45',
                'status' => 0,
                'achat_id' => 13,
                'solde_wallet' => 11066,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:45',
                'updated_at' => '2022-09-22 12:41:45',
                'status' => 1,
                'achat_id' => 13,
                'solde_wallet' => 11126,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-22 13:24:59',
                'updated_at' => '2022-09-22 13:24:59',
                'status' => 0,
                'achat_id' => 14,
                'solde_wallet' => 13126,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-22 13:24:59',
                'updated_at' => '2022-09-22 13:24:59',
                'status' => 1,
                'achat_id' => 14,
                'solde_wallet' => 13186,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-22 13:25:16',
                'updated_at' => '2022-09-22 13:25:16',
                'status' => 0,
                'achat_id' => 15,
                'solde_wallet' => 15186,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-22 13:25:16',
                'updated_at' => '2022-09-22 13:25:16',
                'status' => 1,
                'achat_id' => 15,
                'solde_wallet' => 15246,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-23 16:58:38',
                'updated_at' => '2022-09-23 16:58:38',
                'status' => 0,
                'achat_id' => 16,
                'solde_wallet' => 17246,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-23 16:58:38',
                'updated_at' => '2022-09-23 16:58:38',
                'status' => 1,
                'achat_id' => 16,
                'solde_wallet' => 17306,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 2000,
                'gain' => 60,
                'type_transaction' => 'topup',
                'created_by' => 2,
                'created_at' => '2022-09-23 18:55:14',
                'updated_at' => '2022-09-23 18:55:14',
                'status' => 0,
                'achat_id' => 17,
                'solde_wallet' => 19306,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'wallet_id' => 1,
                'wallet_vendeur_id' => 1,
                'montant' => 60,
                'gain' => 0,
                'type_transaction' => 'cashback',
                'created_by' => 2,
                'created_at' => '2022-09-23 18:55:14',
                'updated_at' => '2022-09-23 18:55:14',
                'status' => 1,
                'achat_id' => 17,
                'solde_wallet' => 19366,
                'status_koree' => NULL,
                'agent_id' => NULL,
            ),
        ));
        
        
    }
}