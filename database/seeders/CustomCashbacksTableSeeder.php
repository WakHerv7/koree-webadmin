<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CustomCashbacksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('custom_cashbacks')->delete();
        
        \DB::table('custom_cashbacks')->insert(array (
            0 => 
            array (
                'id' => 1,
                'commerce_id' => 1,
                'max_amount' => 15000,
                'cashback_pct' => 10.0,
                'active' => 1,
                'created_at' => '2022-09-09 15:09:45',
                'updated_at' => '2022-09-09 15:09:45',
            ),
            1 => 
            array (
                'id' => 2,
                'commerce_id' => 1,
                'max_amount' => 50000,
                'cashback_pct' => 15.0,
                'active' => 1,
                'created_at' => '2022-09-09 15:09:53',
                'updated_at' => '2022-09-09 15:09:53',
            ),
            2 => 
            array (
                'id' => 3,
                'commerce_id' => 1,
                'max_amount' => 5000,
                'cashback_pct' => 5.0,
                'active' => 1,
                'created_at' => '2022-09-09 15:10:03',
                'updated_at' => '2022-09-09 15:10:03',
            ),
            3 => 
            array (
                'id' => 4,
                'commerce_id' => 1,
                'max_amount' => 2000,
                'cashback_pct' => 2.0,
                'active' => 1,
                'created_at' => '2022-09-09 15:14:07',
                'updated_at' => '2022-09-09 15:14:07',
            ),
        ));
        
        
    }
}