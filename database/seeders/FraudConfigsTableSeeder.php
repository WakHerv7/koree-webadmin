<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FraudConfigsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fraud_configs')->delete();
        
        \DB::table('fraud_configs')->insert(array (
            0 => 
            array (
                'id' => 2,
                'value' => 4,
                'interval_value' => 3,
                'interval_unit' => 'month',
                'transaction_type' => 'topup',
                'global' => 1,
                'look_agent' => 0,
                'flag_relatives' => 0,
                'notify_shop_owner' => 0,
                'notify_admin' => 0,
                'restrict' => 'no',
                'restriction_duration_value' => 0,
                'restriction_duration_unit' => 'minute',
                'score' => 25,
                'compare_to_shop_limit' => 0,
                'authorize' => 0,
                'value_type' => 'count',
                'created_at' => '2022-09-20 12:53:52',
                'updated_at' => '2022-09-23 16:58:15',
                'vendor_message' => NULL,
                'admin_message' => NULL,
                'active' => 0,
            ),
            1 => 
            array (
                'id' => 3,
                'value' => 10,
                'interval_value' => 1,
                'interval_unit' => 'day',
                'transaction_type' => 'all',
                'global' => 1,
                'look_agent' => 0,
                'flag_relatives' => 1,
                'notify_shop_owner' => 0,
                'notify_admin' => 0,
                'restrict' => 'yes',
                'restriction_duration_value' => 2,
                'restriction_duration_unit' => 'minute',
                'score' => 20,
                'compare_to_shop_limit' => 0,
                'authorize' => 0,
                'value_type' => 'count',
                'created_at' => '2022-09-20 13:17:13',
                'updated_at' => '2022-09-23 16:58:13',
                'vendor_message' => NULL,
                'admin_message' => NULL,
                'active' => 0,
            ),
        ));
        
        
    }
}