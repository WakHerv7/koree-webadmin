<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RestrictionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('restrictions')->delete();
        
        \DB::table('restrictions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'client_id' => 121,
                'batch' => '5d1d15736d95289186a2e8c176c5cad0',
                'end_date' => '2022-09-22 12:44:30',
                'processed' => 1,
                'created_at' => '2022-09-22 12:39:30',
                'updated_at' => '2022-09-22 13:04:01',
            ),
            1 => 
            array (
                'id' => 2,
                'client_id' => 1,
                'batch' => '371fba01f6ed4976761913638e3b2fb0',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            2 => 
            array (
                'id' => 3,
                'client_id' => 2,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            3 => 
            array (
                'id' => 4,
                'client_id' => 4,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            4 => 
            array (
                'id' => 5,
                'client_id' => 6,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            5 => 
            array (
                'id' => 6,
                'client_id' => 8,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            6 => 
            array (
                'id' => 7,
                'client_id' => 10,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            7 => 
            array (
                'id' => 8,
                'client_id' => 12,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            8 => 
            array (
                'id' => 9,
                'client_id' => 14,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            9 => 
            array (
                'id' => 10,
                'client_id' => 16,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            10 => 
            array (
                'id' => 11,
                'client_id' => 18,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            11 => 
            array (
                'id' => 12,
                'client_id' => 20,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            12 => 
            array (
                'id' => 13,
                'client_id' => 22,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            13 => 
            array (
                'id' => 14,
                'client_id' => 24,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            14 => 
            array (
                'id' => 15,
                'client_id' => 26,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            15 => 
            array (
                'id' => 16,
                'client_id' => 28,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            16 => 
            array (
                'id' => 17,
                'client_id' => 30,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            17 => 
            array (
                'id' => 18,
                'client_id' => 32,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            18 => 
            array (
                'id' => 19,
                'client_id' => 34,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            19 => 
            array (
                'id' => 20,
                'client_id' => 36,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            20 => 
            array (
                'id' => 21,
                'client_id' => 38,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            21 => 
            array (
                'id' => 22,
                'client_id' => 40,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            22 => 
            array (
                'id' => 23,
                'client_id' => 42,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:00',
            ),
            23 => 
            array (
                'id' => 24,
                'client_id' => 44,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            24 => 
            array (
                'id' => 25,
                'client_id' => 46,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            25 => 
            array (
                'id' => 26,
                'client_id' => 48,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            26 => 
            array (
                'id' => 27,
                'client_id' => 50,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            27 => 
            array (
                'id' => 28,
                'client_id' => 52,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            28 => 
            array (
                'id' => 29,
                'client_id' => 54,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            29 => 
            array (
                'id' => 30,
                'client_id' => 56,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            30 => 
            array (
                'id' => 31,
                'client_id' => 58,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            31 => 
            array (
                'id' => 32,
                'client_id' => 60,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            32 => 
            array (
                'id' => 33,
                'client_id' => 62,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            33 => 
            array (
                'id' => 34,
                'client_id' => 64,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            34 => 
            array (
                'id' => 35,
                'client_id' => 66,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            35 => 
            array (
                'id' => 36,
                'client_id' => 68,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            36 => 
            array (
                'id' => 37,
                'client_id' => 70,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            37 => 
            array (
                'id' => 38,
                'client_id' => 72,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            38 => 
            array (
                'id' => 39,
                'client_id' => 74,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            39 => 
            array (
                'id' => 40,
                'client_id' => 76,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            40 => 
            array (
                'id' => 41,
                'client_id' => 78,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            41 => 
            array (
                'id' => 42,
                'client_id' => 80,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            42 => 
            array (
                'id' => 43,
                'client_id' => 82,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            43 => 
            array (
                'id' => 44,
                'client_id' => 84,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            44 => 
            array (
                'id' => 45,
                'client_id' => 86,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            45 => 
            array (
                'id' => 46,
                'client_id' => 88,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            46 => 
            array (
                'id' => 47,
                'client_id' => 90,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            47 => 
            array (
                'id' => 48,
                'client_id' => 92,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            48 => 
            array (
                'id' => 49,
                'client_id' => 94,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            49 => 
            array (
                'id' => 50,
                'client_id' => 96,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            50 => 
            array (
                'id' => 51,
                'client_id' => 98,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            51 => 
            array (
                'id' => 52,
                'client_id' => 100,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            52 => 
            array (
                'id' => 53,
                'client_id' => 102,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            53 => 
            array (
                'id' => 54,
                'client_id' => 104,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            54 => 
            array (
                'id' => 55,
                'client_id' => 106,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            55 => 
            array (
                'id' => 56,
                'client_id' => 108,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            56 => 
            array (
                'id' => 57,
                'client_id' => 110,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            57 => 
            array (
                'id' => 58,
                'client_id' => 112,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            58 => 
            array (
                'id' => 59,
                'client_id' => 114,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            59 => 
            array (
                'id' => 60,
                'client_id' => 116,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:52',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            60 => 
            array (
                'id' => 61,
                'client_id' => 118,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:52',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            61 => 
            array (
                'id' => 62,
                'client_id' => 120,
                'batch' => '69f5eca2f460ca864c15f7dcb2c30a53',
                'end_date' => '2022-09-22 13:11:51',
                'processed' => 1,
                'created_at' => '2022-09-22 13:09:52',
                'updated_at' => '2022-09-22 13:12:01',
            ),
            62 => 
            array (
                'id' => 63,
                'client_id' => 1,
                'batch' => '95214cf0debd026cbc81888486f3c089',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            63 => 
            array (
                'id' => 64,
                'client_id' => 2,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            64 => 
            array (
                'id' => 65,
                'client_id' => 4,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            65 => 
            array (
                'id' => 66,
                'client_id' => 6,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            66 => 
            array (
                'id' => 67,
                'client_id' => 8,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            67 => 
            array (
                'id' => 68,
                'client_id' => 10,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            68 => 
            array (
                'id' => 69,
                'client_id' => 12,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            69 => 
            array (
                'id' => 70,
                'client_id' => 14,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            70 => 
            array (
                'id' => 71,
                'client_id' => 16,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            71 => 
            array (
                'id' => 72,
                'client_id' => 18,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            72 => 
            array (
                'id' => 73,
                'client_id' => 20,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            73 => 
            array (
                'id' => 74,
                'client_id' => 22,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            74 => 
            array (
                'id' => 75,
                'client_id' => 24,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            75 => 
            array (
                'id' => 76,
                'client_id' => 26,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            76 => 
            array (
                'id' => 77,
                'client_id' => 28,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            77 => 
            array (
                'id' => 78,
                'client_id' => 30,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            78 => 
            array (
                'id' => 79,
                'client_id' => 32,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            79 => 
            array (
                'id' => 80,
                'client_id' => 34,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            80 => 
            array (
                'id' => 81,
                'client_id' => 36,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            81 => 
            array (
                'id' => 82,
                'client_id' => 38,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            82 => 
            array (
                'id' => 83,
                'client_id' => 40,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            83 => 
            array (
                'id' => 84,
                'client_id' => 42,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            84 => 
            array (
                'id' => 85,
                'client_id' => 44,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            85 => 
            array (
                'id' => 86,
                'client_id' => 46,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            86 => 
            array (
                'id' => 87,
                'client_id' => 48,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            87 => 
            array (
                'id' => 88,
                'client_id' => 50,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            88 => 
            array (
                'id' => 89,
                'client_id' => 52,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            89 => 
            array (
                'id' => 90,
                'client_id' => 54,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            90 => 
            array (
                'id' => 91,
                'client_id' => 56,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            91 => 
            array (
                'id' => 92,
                'client_id' => 58,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            92 => 
            array (
                'id' => 93,
                'client_id' => 60,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            93 => 
            array (
                'id' => 94,
                'client_id' => 62,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            94 => 
            array (
                'id' => 95,
                'client_id' => 64,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            95 => 
            array (
                'id' => 96,
                'client_id' => 66,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            96 => 
            array (
                'id' => 97,
                'client_id' => 68,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            97 => 
            array (
                'id' => 98,
                'client_id' => 70,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            98 => 
            array (
                'id' => 99,
                'client_id' => 72,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            99 => 
            array (
                'id' => 100,
                'client_id' => 74,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            100 => 
            array (
                'id' => 101,
                'client_id' => 76,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            101 => 
            array (
                'id' => 102,
                'client_id' => 78,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            102 => 
            array (
                'id' => 103,
                'client_id' => 80,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            103 => 
            array (
                'id' => 104,
                'client_id' => 82,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            104 => 
            array (
                'id' => 105,
                'client_id' => 84,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            105 => 
            array (
                'id' => 106,
                'client_id' => 86,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            106 => 
            array (
                'id' => 107,
                'client_id' => 88,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            107 => 
            array (
                'id' => 108,
                'client_id' => 90,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            108 => 
            array (
                'id' => 109,
                'client_id' => 92,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:02',
            ),
            109 => 
            array (
                'id' => 110,
                'client_id' => 94,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            110 => 
            array (
                'id' => 111,
                'client_id' => 96,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            111 => 
            array (
                'id' => 112,
                'client_id' => 98,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            112 => 
            array (
                'id' => 113,
                'client_id' => 100,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            113 => 
            array (
                'id' => 114,
                'client_id' => 102,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            114 => 
            array (
                'id' => 115,
                'client_id' => 104,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            115 => 
            array (
                'id' => 116,
                'client_id' => 106,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            116 => 
            array (
                'id' => 117,
                'client_id' => 108,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            117 => 
            array (
                'id' => 118,
                'client_id' => 110,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            118 => 
            array (
                'id' => 119,
                'client_id' => 112,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            119 => 
            array (
                'id' => 120,
                'client_id' => 114,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            120 => 
            array (
                'id' => 121,
                'client_id' => 116,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            121 => 
            array (
                'id' => 122,
                'client_id' => 118,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            122 => 
            array (
                'id' => 123,
                'client_id' => 120,
                'batch' => '4b53dbc6c9c7b9bbcb01c79028bcf4c2',
                'end_date' => '2022-09-22 13:22:38',
                'processed' => 1,
                'created_at' => '2022-09-22 13:20:39',
                'updated_at' => '2022-09-22 13:23:03',
            ),
            123 => 
            array (
                'id' => 124,
                'client_id' => 1,
                'batch' => 'a202e1f97841207bcef84ce67e1a160f',
                'end_date' => '2022-09-22 13:57:09',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:09',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            124 => 
            array (
                'id' => 125,
                'client_id' => 2,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            125 => 
            array (
                'id' => 126,
                'client_id' => 4,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            126 => 
            array (
                'id' => 127,
                'client_id' => 6,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            127 => 
            array (
                'id' => 128,
                'client_id' => 8,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            128 => 
            array (
                'id' => 129,
                'client_id' => 10,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            129 => 
            array (
                'id' => 130,
                'client_id' => 12,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            130 => 
            array (
                'id' => 131,
                'client_id' => 14,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            131 => 
            array (
                'id' => 132,
                'client_id' => 16,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            132 => 
            array (
                'id' => 133,
                'client_id' => 18,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            133 => 
            array (
                'id' => 134,
                'client_id' => 20,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            134 => 
            array (
                'id' => 135,
                'client_id' => 22,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            135 => 
            array (
                'id' => 136,
                'client_id' => 24,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            136 => 
            array (
                'id' => 137,
                'client_id' => 26,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            137 => 
            array (
                'id' => 138,
                'client_id' => 28,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            138 => 
            array (
                'id' => 139,
                'client_id' => 30,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            139 => 
            array (
                'id' => 140,
                'client_id' => 32,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            140 => 
            array (
                'id' => 141,
                'client_id' => 34,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            141 => 
            array (
                'id' => 142,
                'client_id' => 36,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            142 => 
            array (
                'id' => 143,
                'client_id' => 38,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            143 => 
            array (
                'id' => 144,
                'client_id' => 40,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            144 => 
            array (
                'id' => 145,
                'client_id' => 42,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            145 => 
            array (
                'id' => 146,
                'client_id' => 44,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            146 => 
            array (
                'id' => 147,
                'client_id' => 46,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            147 => 
            array (
                'id' => 148,
                'client_id' => 48,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            148 => 
            array (
                'id' => 149,
                'client_id' => 50,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            149 => 
            array (
                'id' => 150,
                'client_id' => 52,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            150 => 
            array (
                'id' => 151,
                'client_id' => 54,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            151 => 
            array (
                'id' => 152,
                'client_id' => 56,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            152 => 
            array (
                'id' => 153,
                'client_id' => 58,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            153 => 
            array (
                'id' => 154,
                'client_id' => 60,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            154 => 
            array (
                'id' => 155,
                'client_id' => 62,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            155 => 
            array (
                'id' => 156,
                'client_id' => 64,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            156 => 
            array (
                'id' => 157,
                'client_id' => 66,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            157 => 
            array (
                'id' => 158,
                'client_id' => 68,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:03',
            ),
            158 => 
            array (
                'id' => 159,
                'client_id' => 70,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            159 => 
            array (
                'id' => 160,
                'client_id' => 72,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            160 => 
            array (
                'id' => 161,
                'client_id' => 74,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            161 => 
            array (
                'id' => 162,
                'client_id' => 76,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            162 => 
            array (
                'id' => 163,
                'client_id' => 78,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            163 => 
            array (
                'id' => 164,
                'client_id' => 80,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            164 => 
            array (
                'id' => 165,
                'client_id' => 82,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            165 => 
            array (
                'id' => 166,
                'client_id' => 84,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            166 => 
            array (
                'id' => 167,
                'client_id' => 86,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            167 => 
            array (
                'id' => 168,
                'client_id' => 88,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            168 => 
            array (
                'id' => 169,
                'client_id' => 90,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            169 => 
            array (
                'id' => 170,
                'client_id' => 92,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            170 => 
            array (
                'id' => 171,
                'client_id' => 94,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            171 => 
            array (
                'id' => 172,
                'client_id' => 96,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            172 => 
            array (
                'id' => 173,
                'client_id' => 98,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            173 => 
            array (
                'id' => 174,
                'client_id' => 100,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            174 => 
            array (
                'id' => 175,
                'client_id' => 102,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            175 => 
            array (
                'id' => 176,
                'client_id' => 104,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            176 => 
            array (
                'id' => 177,
                'client_id' => 106,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            177 => 
            array (
                'id' => 178,
                'client_id' => 108,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            178 => 
            array (
                'id' => 179,
                'client_id' => 110,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            179 => 
            array (
                'id' => 180,
                'client_id' => 112,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            180 => 
            array (
                'id' => 181,
                'client_id' => 114,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            181 => 
            array (
                'id' => 182,
                'client_id' => 116,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            182 => 
            array (
                'id' => 183,
                'client_id' => 118,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
            183 => 
            array (
                'id' => 184,
                'client_id' => 120,
                'batch' => '61a8c062f0018f7031c9119fb204b45c',
                'end_date' => '2022-09-22 13:57:10',
                'processed' => 1,
                'created_at' => '2022-09-22 13:55:10',
                'updated_at' => '2022-09-22 13:58:04',
            ),
        ));
        
        
    }
}