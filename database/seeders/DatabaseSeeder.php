<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(SuperAdminSeeder::class);
//        $this->call(PermissionSeeder::class);
//        $this->call(LocalisationSeeder::class);
//        $this->call(ClientSampleSeeder::class);
//        $this->call(CommercePointAgSeeder::class);
        $this->call(AbonnementTableSeeder::class);
        $this->call(AchatTableSeeder::class);
        $this->call(AgentsTableSeeder::class);
        $this->call(BoostsTableSeeder::class);
        $this->call(CategoriesCommercesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(VendeursTableSeeder::class);
        $this->call(CommercesTableSeeder::class);
        $this->call(CreationVendeurTableSeeder::class);
        $this->call(CustomCashbacksTableSeeder::class);
        $this->call(DeletionRequestsTableSeeder::class);
        $this->call(FailedJobsTableSeeder::class);
        $this->call(FilleulsTableSeeder::class);
        $this->call(FraudesTableSeeder::class);
        $this->call(FraudConfigsTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(ModelHasPermissionsTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(NotesTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(OtpTokensTableSeeder::class);
        $this->call(PackageBoostTableSeeder::class);
        $this->call(ParametrageTableSeeder::class);
        $this->call(ParrainageTableSeeder::class);
        $this->call(ParrainsTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PersonalAccessTokensTableSeeder::class);
        $this->call(PointsVentesTableSeeder::class);
        $this->call(ProgrammesTableSeeder::class);
        $this->call(ProvidersTableSeeder::class);
        $this->call(QuartiersTableSeeder::class);
        $this->call(RestrictionsTableSeeder::class);
        $this->call(ShopReferralsTableSeeder::class);
        $this->call(SupportsTableSeeder::class);
        $this->call(TracabiliteTableSeeder::class);
        $this->call(WalletsTableSeeder::class);
        $this->call(WalletsVendeurTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(TransactionTracesTableSeeder::class);
        $this->call(WaitingListsTableSeeder::class);
    }
}
