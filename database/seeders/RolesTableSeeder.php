<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insertOrIgnore(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Super-Admin',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Administrateur',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Client',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Vendeur',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Analyst',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Developer',
                'guard_name' => 'web',
                'created_at' => '2022-08-13 12:54:21',
                'updated_at' => '2022-08-13 12:54:21',
            ),
        ));


    }
}
