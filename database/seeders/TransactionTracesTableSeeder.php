<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TransactionTracesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transaction_traces')->delete();
        
        \DB::table('transaction_traces')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 1,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:04:34',
                'updated_at' => '2022-09-21 17:04:34',
            ),
            1 => 
            array (
                'id' => 2,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 1,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:05:23',
                'updated_at' => '2022-09-21 17:05:23',
            ),
            2 => 
            array (
                'id' => 3,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 1,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:06:25',
                'updated_at' => '2022-09-21 17:06:25',
            ),
            3 => 
            array (
                'id' => 4,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:06:58',
                'updated_at' => '2022-09-21 17:06:58',
            ),
            4 => 
            array (
                'id' => 5,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:08:23',
                'updated_at' => '2022-09-21 17:08:23',
            ),
            5 => 
            array (
                'id' => 6,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-21 17:09:20',
                'updated_at' => '2022-09-21 17:09:20',
            ),
            6 => 
            array (
                'id' => 7,
                'transaction_id' => NULL,
                'client_id' => 121,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-22 12:39:30',
                'updated_at' => '2022-09-22 12:39:30',
            ),
            7 => 
            array (
                'id' => 8,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 5000,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:09:51',
            ),
            8 => 
            array (
                'id' => 9,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 1,
                'amount' => 5000,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-22 13:09:51',
                'updated_at' => '2022-09-22 13:09:51',
            ),
            9 => 
            array (
                'id' => 10,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-22 13:20:38',
                'updated_at' => '2022-09-22 13:20:38',
            ),
            10 => 
            array (
                'id' => 11,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 3,
                'authorized' => 0,
                'amount' => 10,
                'transaction_type' => 'all',
                'pending' => 0,
                'created_at' => '2022-09-22 13:55:09',
                'updated_at' => '2022-09-22 13:55:09',
            ),
            11 => 
            array (
                'id' => 12,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-22 13:55:09',
                'updated_at' => '2022-09-22 13:55:09',
            ),
            12 => 
            array (
                'id' => 13,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-23 16:48:30',
                'updated_at' => '2022-09-23 16:48:30',
            ),
            13 => 
            array (
                'id' => 14,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-23 16:48:46',
                'updated_at' => '2022-09-23 16:48:46',
            ),
            14 => 
            array (
                'id' => 15,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-23 16:55:17',
                'updated_at' => '2022-09-23 16:55:17',
            ),
            15 => 
            array (
                'id' => 16,
                'transaction_id' => NULL,
                'client_id' => 1,
                'vendeur_id' => 1,
                'agent_id' => NULL,
                'fraud_id' => 2,
                'authorized' => 0,
                'amount' => 4,
                'transaction_type' => 'topup',
                'pending' => 0,
                'created_at' => '2022-09-23 16:57:32',
                'updated_at' => '2022-09-23 16:57:32',
            ),
        ));
        
        
    }
}