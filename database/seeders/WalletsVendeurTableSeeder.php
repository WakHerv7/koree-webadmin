<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WalletsVendeurTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wallets_vendeur')->delete();
        
        \DB::table('wallets_vendeur')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_fidelite' => '2OHTVW',
                'cashback' => -870,
                'topup' => -24000,
                'passages' => 0,
                'vendeur_id' => 1,
                'programme_id' => 1,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-23 18:55:14',
            ),
            1 => 
            array (
                'id' => 2,
                'id_fidelite' => 'BWXAVK',
                'cashback' => -2400,
                'topup' => -20000,
                'passages' => 0,
                'vendeur_id' => 2,
                'programme_id' => 2,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-07 10:26:48',
            ),
            2 => 
            array (
                'id' => 3,
                'id_fidelite' => 'N5GLDT',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 3,
                'programme_id' => 3,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
            ),
            3 => 
            array (
                'id' => 4,
                'id_fidelite' => '3AUMPY',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 4,
                'programme_id' => 4,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
            ),
            4 => 
            array (
                'id' => 5,
                'id_fidelite' => 'D8LC0S',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 5,
                'programme_id' => 5,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
            ),
            5 => 
            array (
                'id' => 6,
                'id_fidelite' => 'WXQHVZ',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 6,
                'programme_id' => 6,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            6 => 
            array (
                'id' => 7,
                'id_fidelite' => 'EWPEQZ',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 7,
                'programme_id' => 7,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            7 => 
            array (
                'id' => 8,
                'id_fidelite' => '0VFEQI',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 8,
                'programme_id' => 8,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            8 => 
            array (
                'id' => 9,
                'id_fidelite' => 'IOL5TT',
                'cashback' => -900,
                'topup' => -6000,
                'passages' => 0,
                'vendeur_id' => 9,
                'programme_id' => 9,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-07 17:36:27',
            ),
            9 => 
            array (
                'id' => 10,
                'id_fidelite' => 'IWPYII',
                'cashback' => -2400,
                'topup' => -20000,
                'passages' => 0,
                'vendeur_id' => 10,
                'programme_id' => 10,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-07 10:35:25',
            ),
            10 => 
            array (
                'id' => 11,
                'id_fidelite' => 'YQRDSR',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 11,
                'programme_id' => 11,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            11 => 
            array (
                'id' => 12,
                'id_fidelite' => 'DI2CPW',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 12,
                'programme_id' => 12,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            12 => 
            array (
                'id' => 13,
                'id_fidelite' => 'KNB7FF',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 13,
                'programme_id' => 13,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            13 => 
            array (
                'id' => 14,
                'id_fidelite' => 'SCWJ71',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 14,
                'programme_id' => 14,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            14 => 
            array (
                'id' => 15,
                'id_fidelite' => 'XFAFGI',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 15,
                'programme_id' => 15,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
            ),
            15 => 
            array (
                'id' => 16,
                'id_fidelite' => 'BXNXAWL2ZN',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 1,
                'programme_id' => 1,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            16 => 
            array (
                'id' => 17,
                'id_fidelite' => 'DGS5ZRMPA8',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 2,
                'programme_id' => 2,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            17 => 
            array (
                'id' => 18,
                'id_fidelite' => 'JPIPJ5V57R',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 3,
                'programme_id' => 3,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            18 => 
            array (
                'id' => 19,
                'id_fidelite' => 'UPARZKQMMH',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 4,
                'programme_id' => 4,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            19 => 
            array (
                'id' => 20,
                'id_fidelite' => 'DN0DRPHPSV',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 5,
                'programme_id' => 5,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            20 => 
            array (
                'id' => 21,
                'id_fidelite' => 'PUZZTRHTUZ',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 6,
                'programme_id' => 6,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            21 => 
            array (
                'id' => 22,
                'id_fidelite' => 'HJ0H0O0DNH',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 7,
                'programme_id' => 7,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            22 => 
            array (
                'id' => 23,
                'id_fidelite' => 'RJNMS8DO0A',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 8,
                'programme_id' => 8,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            23 => 
            array (
                'id' => 24,
                'id_fidelite' => '1RTVNRBPLD',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 9,
                'programme_id' => 9,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            24 => 
            array (
                'id' => 25,
                'id_fidelite' => 'DINONDIUBG',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 10,
                'programme_id' => 10,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            25 => 
            array (
                'id' => 26,
                'id_fidelite' => 'REUOURIGJV',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 11,
                'programme_id' => 11,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            26 => 
            array (
                'id' => 27,
                'id_fidelite' => 'GSEPDKAXGQ',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 12,
                'programme_id' => 12,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            27 => 
            array (
                'id' => 28,
                'id_fidelite' => '4D1RFGVIDC',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 13,
                'programme_id' => 13,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            28 => 
            array (
                'id' => 29,
                'id_fidelite' => '1GM7AHOV2K',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 14,
                'programme_id' => 14,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
            29 => 
            array (
                'id' => 30,
                'id_fidelite' => 'A8JJLSJU1S',
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => 15,
                'programme_id' => 15,
                'created_at' => '2022-09-06 21:30:49',
                'updated_at' => '2022-09-06 21:30:49',
            ),
        ));
        
        
    }
}