<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesCommercesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories_commerces')->delete();
        
        \DB::table('categories_commerces')->insert(array (
            0 => 
            array (
                'id' => 1,
                'libelle' => 'Hôtel',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'libelle' => 'Restauration',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'libelle' => 'Snack - Bar - Lounge',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'libelle' => 'Mode',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'libelle' => 'Beauté',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'libelle' => 'Supermarché',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'libelle' => 'Electronique',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'libelle' => 'Sport',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'libelle' => 'Pharmacie',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'libelle' => 'Boulangerie - Patisserie',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'libelle' => 'Transport',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'libelle' => 'Station service',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'libelle' => 'Quincaillerie',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'libelle' => 'Opticien',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'libelle' => 'Pressing',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:31',
                'updated_at' => '2022-09-06 21:30:31',
                'parent_id' => NULL,
            ),
        ));
        
        
    }
}