<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProgrammesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('programmes')->delete();
        
        \DB::table('programmes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'libelle' => 'Hickle, Feil and Schinner',
                'description' => 'Consequatur eum saepe recusandae voluptas cumque veritatis suscipit.',
                'date_debut' => '2022-08-13',
                'date_fin' => '2023-08-13',
                'is_cashback' => 1,
                'taux_commission' => 3.0,
                'status' => 0,
                'commerce_id' => 1,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 103145,
                'taux_cashback' => 7,
                'montant_abonnement' => 60000,
                'duree' => 12,
            ),
            1 => 
            array (
                'id' => 2,
                'libelle' => 'Bins, Cormier and Schimmel',
                'description' => 'Voluptatem est earum minima necessitatibus perferendis soluta corporis.',
                'date_debut' => '2022-08-10',
                'date_fin' => '2024-08-10',
                'is_cashback' => 1,
                'taux_commission' => 4.0,
                'status' => 0,
                'commerce_id' => 2,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 79905,
                'taux_cashback' => 8,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            2 => 
            array (
                'id' => 3,
                'libelle' => 'DuBuque, Ferry and Heaney',
                'description' => 'Enim dicta rerum tempora possimus distinctio.',
                'date_debut' => '2022-07-12',
                'date_fin' => '2023-01-12',
                'is_cashback' => 1,
                'taux_commission' => 7.0,
                'status' => 0,
                'commerce_id' => 3,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 138024,
                'taux_cashback' => 3,
                'montant_abonnement' => 30000,
                'duree' => 6,
            ),
            3 => 
            array (
                'id' => 4,
                'libelle' => 'Jakubowski Ltd',
                'description' => 'Perferendis ut quia pariatur nesciunt quisquam qui.',
                'date_debut' => '2022-07-02',
                'date_fin' => '2024-07-02',
                'is_cashback' => 1,
                'taux_commission' => 1.0,
                'status' => 0,
                'commerce_id' => 4,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 135776,
                'taux_cashback' => 4,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            4 => 
            array (
                'id' => 5,
                'libelle' => 'Fadel, Brekke and Howell',
                'description' => 'In inventore voluptatibus sunt nulla.',
                'date_debut' => '2022-06-15',
                'date_fin' => '2022-12-15',
                'is_cashback' => 1,
                'taux_commission' => 7.0,
                'status' => 0,
                'commerce_id' => 5,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:42',
                'updated_at' => '2022-09-06 21:30:42',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 234594,
                'taux_cashback' => 6,
                'montant_abonnement' => 30000,
                'duree' => 6,
            ),
            5 => 
            array (
                'id' => 6,
                'libelle' => 'Parker, Heathcote and Klein',
                'description' => 'Repellendus error qui dignissimos saepe.',
                'date_debut' => '2022-08-26',
                'date_fin' => '2023-02-26',
                'is_cashback' => 1,
                'taux_commission' => 2.0,
                'status' => 0,
                'commerce_id' => 6,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 91137,
                'taux_cashback' => 5,
                'montant_abonnement' => 30000,
                'duree' => 6,
            ),
            6 => 
            array (
                'id' => 7,
                'libelle' => 'Hegmann-Will',
                'description' => 'Molestias aut quia aut deserunt aut est consectetur reprehenderit.',
                'date_debut' => '2022-08-16',
                'date_fin' => '2024-08-16',
                'is_cashback' => 1,
                'taux_commission' => 10.0,
                'status' => 0,
                'commerce_id' => 7,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 141128,
                'taux_cashback' => 2,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            7 => 
            array (
                'id' => 8,
                'libelle' => 'Hodkiewicz-Borer',
                'description' => 'Non delectus recusandae sunt distinctio quidem quia.',
                'date_debut' => '2022-08-06',
                'date_fin' => '2024-08-06',
                'is_cashback' => 1,
                'taux_commission' => 2.0,
                'status' => 0,
                'commerce_id' => 8,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 170362,
                'taux_cashback' => 10,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            8 => 
            array (
                'id' => 9,
                'libelle' => 'Wiza, Pfannerstill and Jast',
                'description' => 'Iure reiciendis voluptatem eos repellat voluptas eveniet.',
                'date_debut' => '2022-07-14',
                'date_fin' => '2024-07-14',
                'is_cashback' => 1,
                'taux_commission' => 4.0,
                'status' => 0,
                'commerce_id' => 9,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 86386,
                'taux_cashback' => 10,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            9 => 
            array (
                'id' => 10,
                'libelle' => 'Metz-Herman',
                'description' => 'Quia soluta ut qui optio exercitationem.',
                'date_debut' => '2022-08-31',
                'date_fin' => '2024-08-31',
                'is_cashback' => 1,
                'taux_commission' => 4.0,
                'status' => 0,
                'commerce_id' => 10,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 147790,
                'taux_cashback' => 8,
                'montant_abonnement' => 120000,
                'duree' => 24,
            ),
            10 => 
            array (
                'id' => 11,
                'libelle' => 'Hegmann, Watsica and Dare',
                'description' => 'Quia fugit iusto ducimus eos expedita.',
                'date_debut' => '2022-06-10',
                'date_fin' => '2023-06-10',
                'is_cashback' => 1,
                'taux_commission' => 2.0,
                'status' => 0,
                'commerce_id' => 11,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 131638,
                'taux_cashback' => 4,
                'montant_abonnement' => 60000,
                'duree' => 12,
            ),
            11 => 
            array (
                'id' => 12,
                'libelle' => 'Smitham, Boyle and Stoltenberg',
                'description' => 'Porro quis sint magni dolores similique.',
                'date_debut' => '2022-07-12',
                'date_fin' => '2023-04-12',
                'is_cashback' => 1,
                'taux_commission' => 10.0,
                'status' => 0,
                'commerce_id' => 12,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 123975,
                'taux_cashback' => 2,
                'montant_abonnement' => 45000,
                'duree' => 9,
            ),
            12 => 
            array (
                'id' => 13,
                'libelle' => 'Zemlak Group',
                'description' => 'Beatae deleniti quidem et debitis.',
                'date_debut' => '2022-08-09',
                'date_fin' => '2023-05-09',
                'is_cashback' => 1,
                'taux_commission' => 8.0,
                'status' => 0,
                'commerce_id' => 13,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 40758,
                'taux_cashback' => 1,
                'montant_abonnement' => 45000,
                'duree' => 9,
            ),
            13 => 
            array (
                'id' => 14,
                'libelle' => 'Romaguera-Blanda',
                'description' => 'Dicta et sed dolores debitis ut consequatur vero.',
                'date_debut' => '2022-07-10',
                'date_fin' => '2023-01-10',
                'is_cashback' => 1,
                'taux_commission' => 4.0,
                'status' => 0,
                'commerce_id' => 14,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 63982,
                'taux_cashback' => 7,
                'montant_abonnement' => 30000,
                'duree' => 6,
            ),
            14 => 
            array (
                'id' => 15,
                'libelle' => 'Bode and Sons',
                'description' => 'Sit maxime quasi neque suscipit.',
                'date_debut' => '2022-08-06',
                'date_fin' => '2023-05-06',
                'is_cashback' => 1,
                'taux_commission' => 5.0,
                'status' => 0,
                'commerce_id' => 15,
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2022-09-06 21:30:43',
                'updated_at' => '2022-09-06 21:30:43',
                'bonus_wallet' => 500,
                'mnt_max_facture' => 193852,
                'taux_cashback' => 9,
                'montant_abonnement' => 45000,
                'duree' => 9,
            ),
        ));
        
        
    }
}