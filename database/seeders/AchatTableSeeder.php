<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AchatTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('achat')->delete();
        
        \DB::table('achat')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_fidelite_client' => 'EPZG77S4EK',
                'montant_facture' => 30000,
                'montant_paye' => 50000,
                'commerce_id' => 2,
                'created_by' => 101,
                'created_at' => '2022-09-07 10:26:48',
                'updated_at' => '2022-09-07 10:26:48',
                'agent_id' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'id_fidelite_client' => '1H7MBTKFCL',
                'montant_facture' => 30000,
                'montant_paye' => 50000,
                'commerce_id' => 10,
                'created_by' => 101,
                'created_at' => '2022-09-07 10:35:25',
                'updated_at' => '2022-09-07 10:35:25',
                'agent_id' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 9,
                'created_by' => 122,
                'created_at' => '2022-09-07 17:23:38',
                'updated_at' => '2022-09-07 17:23:38',
                'agent_id' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 9,
                'created_by' => 122,
                'created_at' => '2022-09-07 17:33:06',
                'updated_at' => '2022-09-07 17:33:06',
                'agent_id' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 9,
                'created_by' => 122,
                'created_at' => '2022-09-07 17:36:27',
                'updated_at' => '2022-09-07 17:36:27',
                'agent_id' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'id_fidelite_client' => 'IHJQ3VSASW',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 122,
                'created_at' => '2022-09-07 17:40:29',
                'updated_at' => '2022-09-07 17:40:29',
                'agent_id' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 122,
                'created_at' => '2022-09-20 12:23:33',
                'updated_at' => '2022-09-20 12:23:33',
                'agent_id' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 122,
                'created_at' => '2022-09-20 12:38:02',
                'updated_at' => '2022-09-20 12:38:02',
                'agent_id' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 122,
                'created_at' => '2022-09-21 16:22:53',
                'updated_at' => '2022-09-21 16:22:53',
                'agent_id' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'id_fidelite_client' => 'SBTKRGHFVQ',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 122,
                'created_at' => '2022-09-21 17:06:25',
                'updated_at' => '2022-09-21 17:06:25',
                'agent_id' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:14',
                'updated_at' => '2022-09-22 12:41:14',
                'agent_id' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:36',
                'updated_at' => '2022-09-22 12:41:36',
                'agent_id' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-22 12:41:45',
                'updated_at' => '2022-09-22 12:41:45',
                'agent_id' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-22 13:24:59',
                'updated_at' => '2022-09-22 13:24:59',
                'agent_id' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-22 13:25:16',
                'updated_at' => '2022-09-22 13:25:16',
                'agent_id' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-23 16:58:38',
                'updated_at' => '2022-09-23 16:58:38',
                'agent_id' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'id_fidelite_client' => 'X1ZZUL713Z',
                'montant_facture' => 3000,
                'montant_paye' => 5000,
                'commerce_id' => 1,
                'created_by' => 2,
                'created_at' => '2022-09-23 18:55:14',
                'updated_at' => '2022-09-23 18:55:14',
                'agent_id' => NULL,
            ),
        ));
        
        
    }
}