SELECT T.id                                            as id,
       T.montant,
       T.gain,
       T.type_transaction,
       T.solde_wallet,
       (select ach.montant_facture
        from achat ach
        where ach.id = T.achat_id)                     AS achat_montant_facture,
       CONCAT(cli.nom, ' ', cli.prenom, ',', cli.sexe) AS client_nom_prenom_sexe,
       CONCAT(cme.nom, ',', ctr.name, ',', cty.name)   AS commerce,
       (select CONCAT(agt.nom_prenom, ',', ptv.nom, ',', ptv.adresse) agent
        from agents agt
                 left join points_ventes ptv on agt.point_vente_id = ptv.id
        where agt.id = T.agent_id)                     AS agent_nom_prenom,
       ctr.name                                        AS pays,
       cty.name                                        AS ville

FROM transactions T
         LEFT join wallets_vendeur wvd ON T.wallet_vendeur_id = wvd.id
         LEFT join vendeurs vend ON wvd.vendeur_id = vend.id
         LEFT join wallets wvc ON T.wallet_id = wvc.id
         LEFT join clients cli ON wvc.client_id = cli.id
         left join commerces cme on cme.vendeur_id = wvd.vendeur_id
         left join cities cty on cty.id = cme.city_id
         LEFT join countries ctr on ctr.id = cty.country_id
