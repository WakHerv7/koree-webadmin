.DEFAULT_GOAL = help

.PHONY: help
help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: helpers
helpers: ## Generate dev helpers for IDE autocomplete
	php artisan ide-helper:generate
	php artisan ide-helper:models -F helpers/ModelHelper.php -M
	php artisan ide-helper:meta

.PHONY: analyse
analyse: ## Run static code analysis
	./vendor/bin/phpstan analyse

.PHONY: format
format: ## Format the code using standard Laravel conventions
	./vendor/bin/pint


.PHONY: format
preview: ## Preview code formatting to be applied to follow standard Laravel conventions
	./vendor/bin/pint --test
