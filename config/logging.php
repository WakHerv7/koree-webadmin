<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Deprecations Log Channel
    |--------------------------------------------------------------------------
    |
    | This option controls the log channel that should be used to log warnings
    | regarding deprecated PHP and library features. This allows you to get
    | your application ready for upcoming major versions of dependencies.
    |
    */

    'deprecations' => [
        'channel' => env('LOG_DEPRECATIONS_CHANNEL', 'null'),
        'trace' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily', 'rollbar', 'slack', 'papertrail'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel-single.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'tickets' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel-ticket.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel-daily.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
            'permission' => 0664,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL', 'https://hooks.slack.com/services/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'),
            'username' => 'EXCEPTION LOG',
            'emoji' => ':boom:',
            'level' => env('SLACK_LOG_LEVEL', 'critical'),
        ],

        'httpRequestLogStack' => [
            'driver' => 'stack',
            'channels' => ['httpRequestLogDaily',/* 'httpRequestLogSlack', 'rollbar'*/],
            'ignore_exceptions' => false,
        ],

        'httpRequestLogSlack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL', 'https://hooks.slack.com/services/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'),
            'username' => 'HTTP LOG',
            'emoji' => ':flechette:',
            'level' => 'info',
        ],

        'httpRequestLogDaily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel-http.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL', 'logs5.papertrailapp.com'),
                'port' => env('PAPERTRAIL_PORT', '13272'),
            ],
        ],

        'rollbar' => [
            'driver' => 'monolog',
            'handler' => \Rollbar\Laravel\MonologHandler::class,
            'access_token' => env('ROLLBAR_TOKEN'),
            'level' => 'debug',
        ],

        'stderr' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],
    ],

];
