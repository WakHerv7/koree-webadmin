<?php

return [


    'providers' => [

        'termii' => [
            'label' => "TECHSOFT",
            'code' => 'termii',
        ],

        'nexah' => [
            'label' => "NEXAH",
            'code' => 'nexah',
        ],

        'techsoft' => [
            'label' => "TECHSOFT",
            'code' => 'TECHSOFT',
        ],
    ],


    'del' => [

        'termii' => [
            'label' => "TECHSOFT",
            'code' => 'termii',
        ],

        'nexah' => [
            'label' => "NEXAH",
            'code' => 'nexah',
        ],

        'techsoft' => [
            'label' => "TECHSOFT",
            'code' => 'techsoft',
        ],
    ],


];

