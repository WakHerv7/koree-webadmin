<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'eu-central-1'),
    ],

    'sns' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_SNS_DEFAULT_REGION', 'eu-central-1'),
    ],

    'whatsapp' => [
        'from-phone-number-id' => env('WHATSAPP_FROM_PHONE_NUMBER_ID'),
        'token' => env('WHATSAPP_TOKEN' ),
    ],

    'github' => [
        'client_id' => env('GITHUB_CLIENT_ID'),
        'client_secret' => env('GITHUB_CLIENT_SECRET'),
        'redirect' => env('GITHUB_REDIRECT_URI'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => 'FACEBOOK_REDIRECT_URI',
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => 'GOOGLE_REDIRECT_URI',
    ],

    'smsapi' => [
        'url' => env('TECHSOFT_SMS_URL'),
        'key' => env('TECHSOFT_SMS_KEY'),
        'sender' => env('TECHSOFT_SMS_SENDER')
    ],

    'techsoft' => [
        'url' => env('TECHSOFT_SMS_URL'),
        'key' => env('TECHSOFT_SMS_KEY'),
        'sender' => env('TECHSOFT_SMS_SENDER')
    ],

    'nexah' => [
        'url' => env('NEXAH_SMS_URL'),
        'username' => env('NEXAH_SMS_USER'),
        'password' => env('NEXAH_SMS_PASSWORD'),
        'sender' => env('NEXAH_SMS_SENDER'),
    ],


    'onesignal' => [
        'app_id' => env('ONESIGNAL_APP_ID'),
        'rest_api_key' => env('ONESIGNAL_REST_API_KEY')
    ],

    'termii' => [
        'sender_id' => env('TERMII_SMS_SENDER_ID', "Koree"),
        'api_key' => env('TERMII_SMS_API_KEY'),
        'secret_key' => env('TERMII_SMS_SECRET'),
        'url' => env('TERMII_SMS_ENDPOINT', 'https://api.ng.termii.com/api'),
    ],

    'appsettings' => [
        'sms_provider' => env('APPSETTINGS_SMS_PROVIDER')
    ],

    'mesagoo' => [
        'token' => env('MESAGOO_TOKEN'),
        'url' => env('MESAGOO_URL', "https://sms.mesagoo.com/api/v1/"),
        'sender' => env('MESAGOO_SENDER'),
    ],

    'koree' => [
        'api' => [
            'url' => env('API_URL', "https://api-dev.koree.africa"),
        ],
    ]


];
