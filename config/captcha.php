<?php

return [
    'default'   => [
        'length'    => 5,
        'width'     => 200,
        'height'    => 36,
        'quality'   => 95,
        'math'      => false, //Enable Math Captcha
        'expire'    => 60,   //Stateless/API captcha expiration
    ],
];
