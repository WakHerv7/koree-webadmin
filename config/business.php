<?php

return [

    'cashback' => [
      'custom_cashback_amount_ceil' => 5000
    ],

//    'default_referrer_amount' => 100000,
    'default_referrer_amount' => 500,
    'country__enabled' => ['cm', 'ci']

];
