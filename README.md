
# Koree-backend

Backend of the customer reward app made with [Laravel](https://laravel.com/docs)

## Installation

Getting started with Laravel is well documented on the official documentation

Perhaps below are steps to follow and how to configure our computer

- Install [php](https://www.php.net)
- Install [composer](https://getcomposer.org)
- Install [mysql](https://www.mysql.com)

## Project Setup

- Create a mysql database
- Clone the repository
- cd into the project directory
- make `composer install`
- make `cp .env.example .env`
- make `php artisan key:generate`
- change `APP_URL` in the `.env` file and set it maybe to `http://localhost:8000` where `8000` represents the
  listening **PORT** of our app
- Configure database access by providing correct values to the following keys
    - `DB_CONNECTION`
    - `DB_HOST`
    - `DB_PORT`
    - `DB_DATABASE`
    - `DB_USERNAME`
    - `DB_PASSWORD`
- import the database dump provided separately

## Project startup

To start the project, just run the command `php artisan serve` and our app will be listening on the port specified
in `APP_URL`


## Run Project Unit Tests
Please look at the file .env.testing and make sure to enter the right database credentials. After that, ensure your
database has already been created and run command `php artisan migrate --database testing_sqlite` to load project dump
database data into your test database.
At this point, simply run command `composer test`
