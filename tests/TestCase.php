<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Helpers\FixtureDataObject;

/** @property FixtureDataObject $fixtures */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected const BASE_PATH = 'http://127.0.0.1:8000/api/v2';

    protected $defaultHeaders = [
        'Accept' => 'application/json'
    ];
}
