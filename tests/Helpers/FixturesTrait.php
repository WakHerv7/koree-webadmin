<?php

namespace Tests\Helpers;

use App\Models\Parrainage;
use App\Models\ShopReferral;
use Tests\Fixtures\AddressFixture;
use Tests\Fixtures\CustomerFixture;
use Tests\Fixtures\ReferrerFixture;
use Tests\Fixtures\SellerExtraFixture;
use Tests\Fixtures\SellerFixture;

trait FixturesTrait
{
    protected function loadFixtures(): FixtureDataObject
    {
        $address = AddressFixture::generate();

        /** @var SellerFixture $seller */
        $seller = SellerFixture::generate(['address' => $address]);

        /** @var SellerExtraFixture $sellerExtra */
        $sellerExtra = SellerExtraFixture::generate(['sellerID' => $seller->profile->id, 'address' => $address]);

        $customer = CustomerFixture::generate(['programID' => $sellerExtra->program->id]);
        $referrer = ReferrerFixture::generate(['programID' => $sellerExtra->program->id]);

        return FixtureDataObject::make(
            compact('seller', 'sellerExtra', 'customer', 'referrer')
        );
    }

    protected function assignRefererToClient(): Parrainage
    {
        return Parrainage::create([
            'parrain_id' => $this->referrerProfile->id,
            'filleul_id' => $this->clientProfile->id,
            'code_parrainage' => $this->referrerProfile->code_parrainage
        ]);
    }

    protected function assignReferrerToShop(): ShopReferral
    {
        $shopReferral = new ShopReferral();
        $shopReferral->client_id = $this->clientProfile->id;
        $shopReferral->parrain_id = $this->referrerProfile->id;
        $shopReferral->commerce_id = $this->commerce->id;
        $shopReferral->save();

        return $shopReferral;
    }
}
