<?php

namespace Tests\Helpers;

interface FixtureContract
{

    public static function generate(array $deps = []): self;

}
