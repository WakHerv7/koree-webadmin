<?php

namespace Tests\Helpers;

use App\Models\Client;
use App\Models\Commerce;
use App\Models\Programme;
use App\Models\User;
use App\Models\Vendeur;
use App\Models\WalletClient;
use App\Models\WalletVendeur;
use Tests\Fixtures\CustomerFixture;
use Tests\Fixtures\ReferrerFixture;
use Tests\Fixtures\SellerExtraFixture;
use Tests\Fixtures\SellerFixture;

class FixtureDataObject
{

    public function __construct(
        public readonly Vendeur       $sellerProfile,
        public readonly Client        $clientProfile,
        public readonly Client        $referrerProfile,
        public readonly User          $sellerUser,
        public readonly User          $clientUser,
        public readonly User          $referrerUser,
        public readonly WalletClient  $clientWallet,
        public readonly WalletClient  $referrerWallet,
        public readonly WalletVendeur $programWallet,
        public readonly Programme     $program,
        public readonly Commerce      $commerce,
    )
    {
    }

    public static function make(array $data): self
    {
        /**
         * @var $seller SellerFixture
         * @var $sellerExtra SellerExtraFixture
         * @var $customer CustomerFixture
         * @var $referrer ReferrerFixture
         */
        [
            'seller' => $seller, 'sellerExtra' => $sellerExtra,
            'customer' => $customer, 'referrer' => $referrer
        ] = $data;

        return new static(
            sellerProfile: $seller->profile,
            clientProfile: $customer->profile,
            referrerProfile: $referrer->profile,

            sellerUser: $seller->user,
            clientUser: $customer->user,
            referrerUser: $referrer->user,

            clientWallet: $customer->wallet,
            referrerWallet: $referrer->wallet,
            programWallet: $sellerExtra->wallet,

            program: $sellerExtra->program,
            commerce: $sellerExtra->commerce
        );
    }

}
