<?php

namespace Tests\Fixtures;

use App\Models\Commerce;
use App\Models\Programme;
use App\Models\User;
use App\Models\WalletVendeur;
use Carbon\Carbon;
use Tests\Helpers\FixtureContract;

class SellerExtraFixture implements FixtureContract
{

    public function __construct(
        public readonly Commerce      $commerce,
        public readonly Programme     $program,
        public readonly WalletVendeur $wallet,
    )
    {
    }

    public static function generate(array $deps = []): FixtureContract
    {
        /** @var AddressFixture $address */
        ['sellerID' => $sellerID, 'address' => $address] = $deps;

        $commerce = Commerce::create([
            'nom' => 'Seller Commerce',
            'vendeur_id' => $sellerID,
            'referer_amount' => 3500,
            'referee_amount' => 750,
            'created_by' => User::first()->id,
            'quartier_id' => $address->street->id
        ]);

        $program = Programme::create([
            'taux_commission' => 10,
            'taux_cashback' => 10,
            'taux_cashback_vip' => 25,
            'libelle' => 'Program Unit',
            'commerce_id' => $commerce->id,
            'description' => 'Program Unit',
            'created_by' => User::first()->id,
            'date_debut' => Carbon::create(2022, 06, 06)
        ]);

        $wallet = WalletVendeur::create([
            'vendeur_id' => $sellerID,
            'programme_id' => $program->id,
            'id_fidelite' => \Str::random('8'),
        ]);

        return new static(
            commerce: $commerce,
            program: $program,
            wallet: $wallet
        );
    }
}
