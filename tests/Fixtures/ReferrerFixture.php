<?php

namespace Tests\Fixtures;

use App\Models\Client;
use App\Models\User;
use App\Models\WalletClient;
use Tests\Helpers\FixtureContract;

class ReferrerFixture implements FixtureContract
{

    public function __construct(
        public readonly User         $user,
        public readonly Client       $profile,
        public readonly WalletClient $wallet,
    )
    {
    }

    public static function generate(array $deps = []): FixtureContract
    {
        $programID = $deps['programID'] ?? null;

        $user = User::create([
            'username' => 'unit.referrer',
            'email' => 'unit.referrer@local.dev',
            'telephone' => '00237652585456',
            'type_user' => 'client',
            'password' => bcrypt('000000')
        ]);

        $profile = Client::create([
            'nom' => 'Unit',
            'prenom' => 'Referral',
            'code_parrainage' => 'Ref47',
            'user_id' => $user->id,
            'created_by' => User::first()->id,
        ]);

        $wallet = WalletClient::create([
            'programme_id' => $programID,
            'client_id' => $profile->id,
            'id_fidelite' => \Str::random('8'),
        ]);

        return new static(
            user: $user,
            profile: $profile,
            wallet: $wallet
        );
    }
}
