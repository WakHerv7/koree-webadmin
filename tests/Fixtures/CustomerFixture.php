<?php

namespace Tests\Fixtures;

use App\Models\Client;
use App\Models\User;
use App\Models\WalletClient;
use Tests\Helpers\FixtureContract;

class CustomerFixture implements FixtureContract
{
    public function __construct(
        public readonly User         $user,
        public readonly Client       $profile,
        public readonly WalletClient $wallet
    )
    {
    }

    public static function generate(array $deps = []): FixtureContract
    {
        $programID = $deps['programID'];

        $user = User::create([
            'username' => 'unit.client',
            'email' => 'unit.client@local.dev',
            'telephone' => '00237611111111',
            'type_user' => 'client',
            'password' => bcrypt('000000')
        ]);

        $profile = Client::create([
            'nom' => 'Unit',
            'prenom' => 'Client',
            'code_parrainage' => 'Cli47',
            'user_id' => $user->id,
            'created_by' => User::first()->id,
        ]);

        $wallet = WalletClient::create([
            'client_id' => $profile->id,
            'programme_id' => $programID,
            'id_fidelite' => \Str::random('8')
        ]);

        return new static(
            user: $user,
            profile: $profile,
            wallet: $wallet
        );
    }
}
