<?php

namespace Tests\Fixtures;

use App\Models\User;
use App\Models\Vendeur;
use Tests\Helpers\FixtureContract;

class SellerFixture implements FixtureContract
{

    public function __construct(
        public readonly User    $user,
        public readonly Vendeur $profile
    )
    {
    }

    public static function generate(array $deps = []): FixtureContract
    {
        /** @var AddressFixture $address */
        $address = $deps['address'];

        $user = User::create([
            'username' => 'unit.seller',
            'email' => 'unit.seller@local.dev',
            'telephone' => '00237699999999',
            'type_user' => 'vendeur',
            'password' => bcrypt('000000')
        ]);

        $profile = Vendeur::create([
            'status' => 1,
            'nom' => 'Unit Seller',
            'user_id' => $user->id,
            'city_id' => $address->city->id,
            'country_id' => $address->country->id,
            'created_by' => User::first()->id
        ]);

        return new static(
            user: $user,
            profile: $profile
        );
    }
}
