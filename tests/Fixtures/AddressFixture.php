<?php

namespace Tests\Fixtures;

use App\Models\Pays;
use App\Models\Quartier;
use App\Models\Region;
use App\Models\Ville;
use Tests\Helpers\FixtureContract;

class AddressFixture implements FixtureContract
{

    public function __construct(
        public readonly Pays     $country,
        public readonly Ville    $city,
        public readonly Region   $state,
        public readonly Quartier $street,
    )
    {
    }

    public static function generate(array $deps = []): self
    {
        $country = Pays::create([
            'name' => 'Pest Country',
            'iso3' => 'PTC',
            'iso2' => 'PT',
            'timezones' => '[]',
            'translations' => '[]',
            'numeric_code' => '237'
        ]);

        $state = Region::create([
            'name' => 'Pest State',
            'country_id' => $country->id,
            'country_code' => $country->numeric_code
        ]);

        $city = Ville::create([
            'latitude' => 'lat',
            'longitude' => 'lon',
            'name' => 'Pest City',
            'state_code' => '237',
            'state_id' => $state->id,
            'country_id' => $country->id,
            'country_code' => $country->numeric_code
        ]);

        $street = Quartier::create([
            'name' => 'Pest Neighborhood',
            'city_id' => $city->id,
            'country_id' => $country->id
        ]);

        return new static(
            country: $country,
            city: $city,
            state: $state,
            street: $street
        );
    }
}
