<?php

uses(
    Tests\TestCase::class,
    \Tests\Helpers\FixturesTrait::class,
    Illuminate\Foundation\Testing\DatabaseTransactions::class,
)->in('Feature');

function endpoint(string $path): string
{
    return "http://127.0.0.1:8000/api/v2{$path}";
}
