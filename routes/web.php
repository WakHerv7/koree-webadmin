<?php

use App\Exports\ClientsExport;
use App\Exports\TransactionsExport;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\Backend\AgentController;
use App\Http\Controllers\Backend\CampagneController;
use App\Http\Controllers\Backend\ClientController;
use App\Http\Controllers\Backend\CommerceController;
use App\Http\Controllers\Backend\CustomCashbackController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\Localisation\PaysController;
use App\Http\Controllers\Backend\Localisation\QuartiersController;
use App\Http\Controllers\Backend\Localisation\VillesController;
use App\Http\Controllers\Backend\PackageController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\PointVenteController;
use App\Http\Controllers\Backend\ProgrammeController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\TransactionController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\VendeurController;
use App\Http\Controllers\Backend\WalletController;
use App\Http\Controllers\CaptchaValidationController;
use App\Http\Controllers\FraudController;
use App\Http\Controllers\ParametrageController;
use App\Http\Controllers\QRCodeController;
use App\Http\Controllers\TicketController;

// Delivery
use App\Http\Controllers\Backend\Delivery\CompanyController;
use App\Http\Controllers\Backend\Delivery\DeliverController;
use App\Http\Controllers\Backend\Delivery\DeliveryController;
use App\Http\Controllers\Backend\Delivery\OrderController;
use App\Http\Controllers\Backend\Delivery\PaymentMethodController;
use App\Http\Controllers\Backend\Delivery\SubscriptionController;
use App\Http\Controllers\Backend\Delivery\DeliveryTransactionController;

// Marketplace
use App\Http\Controllers\Backend\Marketplace\MarketplaceCategoryController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceProductCategoryController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceShopController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceShopSubcategoryController;

use App\Http\Controllers\Backend\Marketplace\MarketplaceArticleController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceOrderController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceCustomerController;
use App\Http\Controllers\Backend\Marketplace\MarketplacePaymentController;
use App\Http\Controllers\Backend\Marketplace\MarketplacePeriodicityWithdrawalController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceAddonController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceNoteController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceSubscriptionController;
use App\Http\Controllers\Backend\Marketplace\MarketplaceTransactionController;
use App\Http\Controllers\Backend\PaymentServiceProvider\PaymentServiceProviderController;


use Illuminate\Support\Facades\Route;


Route::domain(config('app.env') != 'production' ? 'download-dev.koree.africa' : 'download.koree.africa')->group(function () {
    Route::get('/', [QRCodeController::class, 'frontend']);
});

Route::domain(config('app.env') != 'production' ? 'store-dev.koree.africa' : 'store.koree.africa')->group(function () {
    Route::get('/', [QRCodeController::class, 'frontend']);
});

Route::get('/qrcodes',[QRCodeController::class, 'frontend']);



Route::get('/', function () {
    return view('frontend/index');
});

Route::get('/politique-confidentialite', function () {
    return view('frontend/politique');
})->name('politique');


Route::get('/mentions-legales', function () {
    return view('frontend/mentions_legales');
})->name('mention-legale');

Route::get('logout', [AuthController::class, 'logout']);
Route::get('contact-form-captcha', [CaptchaValidationController::class, 'index']);
Route::post('captcha-validation', [CaptchaValidationController::class, 'capthcaFormValidate']);
Route::get('reload-captcha', [CaptchaValidationController::class, 'reloadCaptcha']);


//Route::get('/confirm-password', function () {
//    return view('auth.confirm-password');
//})->middleware('auth')->name('password.confirm');

//Route::post('/confirm-password', function (Request $request) {
//    if (! Hash::check($request->password, $request->user()->password)) {
//        return back()->withErrors([
//            'password' => ['The provided password does not match our records.']
//        ]);
//    }
//
//    $request->session()->passwordConfirmed();
//
//    return redirect()->intended();
//})->middleware(['auth', 'throttle:6,1']);

Route::group(['middleware' => ['auth']], function () {

    // route for intern users
    Route::get('users/waitinglist', [UserController::class, 'listUsersOnWaitinglist'])->name('users-on-waitinglist');
    Route::get('users/users-internes', [UserController::class, 'listUsersInternes'])->name('users-in');
    Route::get('users/users-internes/create', [UserController::class, 'createUsersInternes'])->name('users-in-create');
    Route::post('users/users-internes', [UserController::class, 'storeUsersInternes'])->name('users-in-store');
    Route::get('users/users-internes/{id}/edit', [UserController::class, 'editUsersInternes'])->name('users-in-edit');
    Route::put('users/users-internes/{id}', [UserController::class, 'updateUsersInternes'])->name('users-in-update');
    Route::get('users/users-internes/{id}', [UserController::class, 'showUsersInternes'])->name('users-in-show');
    Route::delete('users/users-internes/{id}', [UserController::class, 'destroyUsersInternes'])->name('users-in-destroy');

    // route for vendeurs
    Route::get('users/influenceurs', [UserController::class, 'listInfluenceurs'])->name('users.influenceurs.index');
    Route::get('users/vendeurs', [UserController::class, 'listVendeurs'])->name('vendeurs-in');
    Route::get('users/vendeurss/create', [UserController::class, 'createVendeurs'])->name('vendeurs-in-create');
    Route::post('users/vendeurs', [UserController::class, 'storeVendeurs'])->name('vendeurs-in-store');
    Route::get('users/vendeurs/{id}/edit', [UserController::class, 'editVendeurs'])->name('vendeurs-in-edit');
    Route::put('users/vendeurs/{id}', [UserController::class, 'updateVendeurs'])->name('vendeurs-in-update');
    Route::get('users/vendeurs/{id}', [UserController::class, 'showVendeurs'])->name('vendeurs-in-show');
    Route::delete('users/vendeurs/{id}', [UserController::class, 'destroyVendeurs'])->name('vendeurs-in-destroy');

    Route::resource('vendeurs', VendeurController::class);

    Route::get('/clients/export', ClientsExport::class)->name('clients.export');
    Route::put('clients/change-statut/{id}', [ClientController::class, 'changeStatut'])->name('client-change');
    Route::get('clients/toggle-statut/{id}', [ClientController::class, 'toggleStatut'])->name('toggle-status');
    Route::get('clients/toggle-influencer/{id}', [ClientController::class, 'toggleInfluencer'])->name('toggle-influencer');
    Route::get('/clients/mvmt/{id}', [ClientController::class, 'mvmt'])->name('clients.mvmt');
    Route::match(['GET', 'POST'],'clients/ajaxDatable', [ClientController::class, 'datatable'])->name('listing-clients-datable');
    Route::get('clients/deletion-requests', [ClientController::class, 'deletionRequests'])->name('client-deletion-requests');
    Route::get('clients/deletion-requests/accept/{id}', [ClientController::class, 'acceptDeletionRequests'])->name('client-deletion-requests-accept');
    Route::get('clients/deletion-requests/cancel/{id}', [ClientController::class, 'cancelDeletionRequests'])->name('client-deletion-requests-cancel');
    Route::get('commerces/deletion-requests/reset/{id}', [ClientController::class, 'resetDeletionRequests'])->name('client-deletion-requests-reset');
    Route::resource('clients', ClientController::class);

    Route::get('liste-parrainage', [UserController::class, 'listParrainage'])->name('listing-parrainage');
    Route::get('liste-parrainage-commerce', [UserController::class, 'listParrainageCommerce'])->name('listing-parrainage-commerce');
    Route::get('liste-transactions', [TransactionController::class, 'listeTransactions'])->name('listing-transactions');
    Route::get('liste-transactions-supprimer', [TransactionController::class, 'listeTransactionsSupprimes'])->name('listing-transactions-supprimes');
    // TODO: Start By Romuald
    Route::post('liste-transactions/ajaxDatable', [TransactionController::class, 'listeTransactionsDatatable'])->name('listing-transactions-datable');
//    Route::get('liste-transactions/export', [TransactionController::class, 'listeTransactionsExport'])->name('listing-transactions-export');
    Route::get('liste-transactions/export', TransactionsExport::class)->name('transactions.export');
    Route::get('ajaxSelect2/clients', [ClientController::class, 'listeClientsSelect2'])->name('listing-clients-select2');
    Route::get('ajaxSelect2/commerces', [CommerceController::class, 'listeCommercesSelect2'])->name('listing-commerces-select2');
    // TODO: end By Romuald
    Route::get('liste-revenu', [TransactionController::class, 'listeRevenus'])->name('listing-revenu');

    Route::get('revert_transaction_init/{id}', [TransactionController::class, 'revertInit'])->middleware(['password.confirm'])->name('transaction.revert.init');
    Route::get('revert_transaction/{id}', [TransactionController::class, 'revert'])->middleware(['password.confirm'])->name('transaction.revert');
    Route::get('restore_transaction/{id}', [TransactionController::class, 'restore'])->middleware(['password.confirm'])->name('transaction.restore');
    Route::get('restore_transaction_init/{id}', [TransactionController::class, 'restoreInit'])->middleware(['password.confirm'])->name('transaction.restore.init');
    Route::post('revert_transaction', [TransactionController::class, 'performRevert'])->name('perform.transaction.revert');
    Route::post('restore_transaction', [TransactionController::class, 'performRestore'])->name('perform.transaction.restore');

    Route::get('getRegion/{id}', function ($id) {
        $regions = App\Models\Region::where('country_id', $id)->get();
        return response()->json($regions);
    });
    Route::get('getPointsVentes/{id}', function ($id) {
        $pointventes = App\Models\PointVente::where('commerce_id', $id)->get();
        return response()->json($pointventes);
    });
    Route::get('getVille/{id}', function ($id) {
        $villes = App\Models\Ville::where('state_id', $id)->orderBy('name')->get();
        return response()->json($villes);
    });
    Route::get('getVilleParPays/{id}', function ($id) {
        $villes = App\Models\Ville::where('country_id', $id)->orderBy('name')->get();
        return response()->json($villes);
    });
    Route::get('getQuartiers/{id}', function ($id) {
        $quartiers = App\Models\Quartier::where('city_id', $id)->orderBy('name')->get();
        return response()->json($quartiers);
    });


    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('localisation/pays', PaysController::class)->only(['index', 'show']);
    Route::resource('localisation/villes', VillesController::class);
    Route::resource('localisation/quartiers', QuartiersController::class)->except(['store']);
    Route::controller(ParametrageController::class)->prefix('parametrage')->group(function () {
        Route::get('/', "index")->name('parametrage.index');
        Route::put('/', "update")->name('parametrage.update');
    });
    Route::resource('permissions', PermissionController::class);

    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

    Route::get('package/liste-commerces', [PackageController::class, 'listCommerceBoostes'])->name('commerces-boostes');
    Route::get('package/historique-boost', [PackageController::class, 'historiqueCommerceBoostes'])->name('historique-commerces-boost');
    Route::resource('package', PackageController::class);

    Route::get('tickets-cancel-reasons/', [TicketController::class, 'indexCancellationReasons'])->name('tickets.cancel-reasons.index');
    Route::get('tickets-cancel-reasons/create', [TicketController::class, 'createCancellationReasons'])->name('tickets.cancel-reasons.create');
    Route::post('tickets-cancel-reasons/', [TicketController::class, 'storeCancellationReasons'])->name('tickets.cancel-reasons.store');
    Route::delete('tickets-cancel-reasons/{model}', [TicketController::class, 'deleteCancellationReasons'])->name('tickets.cancel-reasons.destroy');
    Route::get('tickets/', [TicketController::class, 'index'])->name('tickets.index');
    Route::get('tickets/{ticket}', [TicketController::class, 'show'])->name('tickets.show');
    Route::get('tickets/{ticket}/apply/{apply}', [TicketController::class, 'apply_cashback'])->whereIn('apply', ['charge',  'reject'])->name('tickets.cashback.apply');


    Route::get('commerces/notation-clients', [CommerceController::class, 'notationCommerce'])->name('notation-commerces');
    Route::get('commerces/suggestions', [CommerceController::class, 'suggested'])->name('commerces.suggested');
    Route::get('commerces/categories', [CommerceController::class, 'categories'])->name('commerces.categories');
    Route::get('commerces/categories/create', [CommerceController::class, 'createCategory'])->name('commerces.categories.create');
    Route::post('commerces/categories/save', [CommerceController::class, 'saveCategory'])->name('commerces.categories.save');
    Route::get('commerces/categories/edit/{id}', [CommerceController::class, 'editCategory'])->name('commerces.categories.edit');
    Route::put('commerces/categories/update', [CommerceController::class, 'updateCategory'])->name('commerces.categories.update');
    Route::get('commerces/deletion-requests', [CommerceController::class, 'deletionRequests'])->name('deletion-requests');
    Route::get('commerces/deletion-requests/accept/{deletionRequest}', [CommerceController::class, 'acceptDeletionRequests'])->name('deletion-requests-accept');
    Route::get('commerces/deletion-requests/cancel/{deletionRequest}', [CommerceController::class, 'cancelDeletionRequests'])->name('deletion-requests-cancel');
    Route::get('commerces/inactifs', [CommerceController::class, 'getInactif'])->name('commerces.inactifs');
    Route::put('commerces/update/status/{id}', [CommerceController::class, 'updateStatus'])->name('commerces.status');


    Route::get('campagnes/', [CampagneController::class, 'index'])->name('campagnes.index');
    Route::get('campagnes/nouveau', [CampagneController::class, 'create'])->name('campagnes.create');
    Route::post('campagnes', [CampagneController::class, 'store'])->name('campagnes.store');
    Route::get('campagnes/{id}', [CampagneController::class, 'detail'])->name('campagnes.detail');
    Route::get('campagnes/{id}/qrcodes/pdf', [CampagneController::class, 'download'])->name('campagnes.pdf');
    Route::delete('qrcodes/{id}', [QRCodeController::class, 'destroy'])->name('qrcodes.destroy');
    Route::resource('commerces', CommerceController::class);
    Route::get('commerces/deletion-requests/reset/{id}', [CommerceController::class, 'resetDeletionRequests'])->name('deletion-requests-reset');
    Route::resource('programmes', ProgrammeController::class);
    Route::controller(CustomCashbackController::class)->prefix('commerces/custom-cashbacks')->group(function () {
        Route::get('/create/{id}', 'create')->name('custom-cashback.create');
        Route::get('/edit/{c}/{r}', 'edit')->name('custom-cashback.edit');
        Route::get('/lock/{c}/{r}', 'lock')->name('custom-cashback.lock');
        Route::get('/unlock/{c}/{r}', 'unlock')->name('custom-cashback.unlock');
        Route::get('/{id}', 'index')->name('custom-cashback.index');
        Route::post('/save', 'save')->name('custom-cashback.save');
        Route::put('/update', 'update')->name('custom-cashback.update');
        Route::delete('/delete', 'delete')->name('custom-cashback.delete');
    });

    //Route::resource('wallets', WalletController::class);
    Route::resource('points-ventes', PointVenteController::class);
    Route::resource('agents', AgentController::class);

    Route::get('portefeuilles/liste-clients', [WalletController::class, 'listeWalletClients'])->name('wallet-clients');
    Route::get('portefeuilles/liste-commerces', [WalletController::class, 'listeWalletVendeurs'])->name('wallet-vendeurs');

    Route::prefix('fraud')->group(function () {
        Route::get('/', [FraudController::class, 'index'])->name('fraud.index');
        Route::get('/pending-validation', [FraudController::class, 'pending'])->name('fraud.pending-validation');
        Route::get('/hyper-parameters', [FraudController::class, 'hyperParameters'])->name('fraud.hyper-parameters');
        Route::get('/hyper-parameters/create', [FraudController::class, 'createHyperParameter'])->name('fraud.hyper-parameters.create');
        Route::get('/hyper-parameters/edit/{id}', [FraudController::class, 'editHyperParameter'])->name('fraud.hyper-parameters.edit');
        Route::put('/hyper-parameters/edit', [FraudController::class, 'updateHyperParameter'])->name('fraud.hyper-parameters.update');
        Route::post('/hyper-parameters', [FraudController::class, 'saveHyperParameter'])->name('fraud.hyper-parameters.save');
        Route::delete('/hyper-parameters', [FraudController::class, 'deleteHyperParameter'])->name('fraud.hyper-parameters.delete');
        Route::get('/toggle-status/{id}', [FraudController::class, 'toggleStatus'])->name('fraud.toggle-status');
        Route::get('/toggle-status/{id}', [FraudController::class, 'toggleStatus'])->name('fraud.toggle-status');
        Route::get('/{id}', [FraudController::class, 'show'])->name('fraud.show');
        Route::put('/process-pending', [FraudController::class, 'processPending'])->name('fraud.process.pending');
    });

    Route::controller(ProfileController::class)->prefix('profile')->group(function () {
        Route::get('/', 'index')->name('profile');
    });

    /**
     * DELIVERY START
     */
    Route::controller(CompanyController::class)->prefix('delivery')->group(function () {
        Route::get('/company', "index")->name('delivery.company.index');
        Route::get('/company/deleted', "indexDeleted")->name('delivery.company.deleted');
        Route::get('/company/show', "show")->name('delivery.company.show');
        Route::get('/company/create', "create")->name('delivery.company.create');
        Route::get('/company/edit/{id}', "edit")->name('delivery.company.edit');
    });
    Route::controller(DeliverController::class)->prefix('delivery')->group(function () {
        Route::get('/deliver', "index")->name('delivery.deliver.index');
        Route::get('/deliver/deleted', "indexDeleted")->name('delivery.deliver.deleted');
        Route::get('/deliver/show', "show")->name('delivery.deliver.show');
        Route::get('/deliver/create', "create")->name('delivery.deliver.create');
        Route::get('/deliver/edit/{id}', "edit")->name('delivery.deliver.edit');
    });
    Route::controller(DeliveryController::class)->prefix('delivery')->group(function () {
        Route::get('/delivery', "index")->name('delivery.delivery.index');
        Route::get('/delivery/show', "show")->name('delivery.delivery.show');
    });
    Route::controller(OrderController::class)->prefix('delivery')->group(function () {
        Route::get('/order', "index")->name('delivery.order.index');
    });
    Route::controller(PaymentMethodController::class)->prefix('delivery')->group(function () {
        Route::get('/payment-method', "index")->name('delivery.payment_method.index');
        Route::get('/payment-method/deleted', "indexDeleted")->name('delivery.payment_method.deleted');
        Route::get('/payment-method/show', "show")->name('delivery.payment_method.show');
        Route::get('/payment-method/create', "create")->name('delivery.payment_method.create');
        Route::get('/payment-method/edit/{id}', "edit")->name('delivery.payment_method.edit');
    });
    Route::controller(SubscriptionController::class)->prefix('delivery')->group(function () {
        Route::get('/subscription', "index")->name('delivery.subscription.index');
        Route::get('/subscription/deleted', "indexDeleted")->name('delivery.subscription.deleted');
        Route::get('/subscription/show', "show")->name('delivery.subscription.show');
        Route::get('/subscription/create', "create")->name('delivery.subscription.create');
        Route::get('/subscription/edit/{id}', "edit")->name('delivery.subscription.edit');
    });
    Route::controller(DeliveryTransactionController::class)->prefix('delivery')->group(function () {
        Route::get('/transaction', "index")->name('delivery.transaction.index');
        Route::get('/transaction/show', "show")->name('delivery.transaction.show');
    });

    /**
     * DELIVERY ENDS
     */



    /**
     * MARKETPLACE START
     */
    Route::controller(MarketplaceCategoryController::class)->prefix('marketplace')->group(function () {
        Route::get('/category', "index")->name('marketplace.category.index');
        Route::get('/category/deleted', "indexDeleted")->name('marketplace.category.deleted');
        Route::get('/category/show', "show")->name('marketplace.category.show');
        Route::get('/category/create', "create")->name('marketplace.category.create');
        Route::get('/category/edit/{id}', "edit")->name('marketplace.category.edit');
    });
    Route::controller(MarketplaceProductCategoryController::class)->prefix('marketplace')->group(function () {
        Route::get('/product-category', "index")->name('marketplace.product_category.index');
        Route::get('/product-category/deleted', "indexDeleted")->name('marketplace.product_category.deleted');
        Route::get('/product-category/show', "show")->name('marketplace.product_category.show');
        Route::get('/product-category/create', "create")->name('marketplace.product_category.create');
        Route::get('/product-category/edit/{id}', "edit")->name('marketplace.product_category.edit');
    });
    Route::controller(MarketplaceShopController::class)->prefix('marketplace')->group(function () {
        Route::get('/shop', "index")->name('marketplace.shop.index');
        Route::get('/shop/deleted', "indexDeleted")->name('marketplace.shop.deleted');
        Route::get('/shop/show', "show")->name('marketplace.shop.show');
        Route::get('/shop/create', "create")->name('marketplace.shop.create');
        Route::get('/shop/edit/{id}', "edit")->name('marketplace.shop.edit');
    });
    Route::controller(MarketplaceShopSubcategoryController::class)->prefix('marketplace')->group(function () {
        Route::get('/shop-sub-category/{shop_id}', "index")->name('marketplace.shop-sub-category.index');
        Route::get('/shop-sub-category/deleted/{shop_id}', "indexDeleted")->name('marketplace.shop-sub-category.deleted');
    });
    Route::controller(MarketplaceArticleController::class)->prefix('marketplace')->group(function () {
        Route::get('/article/shop/{shop_id}', "index")->name('marketplace.article.index');
        Route::get('/article/deleted/shop/{shop_id}', "indexDeleted")->name('marketplace.article.deleted');
        Route::get('/article/show/{id}', "show")->name('marketplace.article.show');
        Route::get('/article/create', "create")->name('marketplace.article.create');
        Route::get('/article/edit/{id}', "edit")->name('marketplace.article.edit');
    });
    Route::controller(MarketplaceOrderController::class)->prefix('marketplace')->group(function () {
        Route::get('/order', "index")->name('marketplace.order.index');
        Route::get('/order/shop/{shop_id}', "index")->name('marketplace.order.shop.index');
        Route::get('/order/show', "show")->name('marketplace.order.show');
    });
    // Route::controller(MarketplaceCustomerController::class)->prefix('marketplace')->group(function () {
    //     Route::get('/customer', "index")->name('marketplace.customer.index');
    // });
    // Route::controller(MarketplacePaymentController::class)->prefix('marketplace')->group(function () {
    //     Route::get('/payment', "index")->name('marketplace.payment.index');
    // });
    // Route::controller(MarketplacePeriodicityWithdrawalController::class)->prefix('marketplace')->group(function () {
    //     Route::get('/periodicity-withdrawal', "index")->name('marketplace.periodicity-withdrawal.index');
    // });
    // Route::controller(MarketplaceAddonController::class)->prefix('marketplace')->group(function () {
    //     Route::get('/addon', "index")->name('marketplace.addon.index');
    // });
    // Route::controller(NoteController::class)->prefix('marketplace')->group(function () {
    //     Route::get('/note', "index")->name('marketplace.note.index');
    // });

    Route::controller(MarketplaceSubscriptionController::class)->prefix('marketplace')->group(function () {
        Route::get('/subscription', "index")->name('marketplace.subscription.index');
        Route::get('/subscription/deleted', "indexDeleted")->name('marketplace.subscription.deleted');
        Route::get('/subscription/show', "show")->name('marketplace.subscription.show');
        Route::get('/subscription/create', "create")->name('marketplace.subscription.create');
        Route::get('/subscription/edit/{id}', "edit")->name('marketplace.subscription.edit');
    });
    Route::controller(MarketplaceTransactionController::class)->prefix('marketplace')->group(function () {
        Route::get('/transaction', "index")->name('marketplace.transaction.index');
        Route::get('/transaction/show', "show")->name('marketplace.transaction.show');
    });

    /**
     * MARKETPLACE ENDS
     */

     /**
      * PAYMENT SERVICE PROVIDER
      */
      Route::controller(PaymentServiceProviderController::class)->prefix('payment-service-provider')->group(function () {
        Route::get('/', "index")->name('payment-service-provider.index');
        Route::get('/show', "show")->name('payment-service-provider.show');
    });
      

});
