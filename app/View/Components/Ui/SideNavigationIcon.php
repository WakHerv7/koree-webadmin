<?php

namespace App\View\Components\Ui;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class SideNavigationIcon extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public string $title,
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.side-navigation-icon', [
            'data' => Str::lower($this->title)
        ]);
    }
}
