<?php

namespace App\View\Components\Ui;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;
use Spatie\Navigation\Navigation;
use Spatie\Navigation\Section;

class SideNavigation extends Component
{
    private bool $userHasAnalystRole;
    private bool $userHasSomeBackendRoles;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userHasAnalystRole = Auth::user()->hasRole(User::ROLE_ANALYST);
        $this->userHasSomeBackendRoles = Auth::user()->hasBackendUserRole();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
//        $userHasAnalystRole = Auth::user()->hasRole(User::ROLE_ANALYST);
//        $userHasSomeBackendRoles = Auth::user()->hasBackendUserRole();

        Navigation::make()
            ->addIf(!$this->userHasAnalystRole,'Dashboard', route('dashboard'))
            ->addIf($this->userHasSomeBackendRoles || $this->userHasAnalystRole, 'Transactions', '', function (Section $section) {
                $section
                    ->addIf(!$this->userHasAnalystRole, 'Historique des transactions', route('listing-transactions'))
                    ->addIf(!$this->userHasAnalystRole, 'Historique des transactions supprimées', route('listing-transactions-supprimes'))
                    ->add('Tickets de bonus cashback', route('tickets.index'))
                    ->add('Tickets de bonus cashback en attente', route('tickets.index', ['status' => 'pending']))
                    ->add('Tickets de bonus cashback acceptés', route('tickets.index', ['status' => 'accepted']))
                    ->add('Tickets  de bonus cashback réjétés', route('tickets.index', ['status' => 'cancelled']));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Commerces', '', function (Section $section) {
                $section
                    ->add('Propriétaires', route('vendeurs.index'))
                    ->add('Commerces actifs', route('commerces.index'))
                    ->add('Commerces inactifs', route('commerces.inactifs'))
                    ->add('Campagne Qrcode', route('campagnes.index'))
                    ->add('Points de ventes', route('points-ventes.index'))
                    ->add('Agents', route('agents.index'))
                    ->add('Historique de souscriptions', route('historique-commerces-boost'))
                    ->add('Historique notation des commerces', route('notation-commerces'))
                    ->add('Suggestions', route('commerces.suggested'))
                    ->add('Catégories', route('commerces.categories'))
                    ->add('Demandes de suppression', route('deletion-requests'));
             })
             ->addIf($this->userHasSomeBackendRoles, 'Delivery', '', function (Section $section) {
                $section
                    ->add('Entreprises de livraison', route('delivery.company.index'))
                    ->add('Livreurs', route('delivery.deliver.index'))
                    ->add('Livraisons', route('delivery.delivery.index'))
                    // ->add('Commandes', route('delivery.order.index'))
                    ->add('Contrats', route('delivery.subscription.index'))
                    ->add('Transactions', route('delivery.transaction.index'));
             })
             ->addIf($this->userHasSomeBackendRoles, 'Marketplace', '', function (Section $section) {
                $section                    
                    ->add('Catégories de commerces', route('marketplace.category.index'))
                    ->add('Catégories de produits', route('marketplace.product_category.index'))
                    ->add('Commerces', route('marketplace.shop.index'))
                    // ->add('Entreprises de livraison', route('marketplace.addon.index'))
                    // ->add('Articles', route('marketplace.article.index'))
                    
                    // ->add('Clients', route('marketplace.customer.index'))
                    // ->add('Notes', route('marketplace.note.index'))
                    ->add('Commandes', route('marketplace.order.index'))
                    // ->add('Paiements', route('marketplace.payment.index'))
                    // ->add('Commerces', route('marketplace.shop.index'))
                    ->add('Contrats', route('marketplace.subscription.index'))
                    ->add('Transactions', route('marketplace.transaction.index'));
             })
            ->addIf($this->userHasSomeBackendRoles, 'Abonnements', route('programmes.index'))
            ->addIf($this->userHasSomeBackendRoles, 'Utilisateurs', '', function (Section $section) {
                $section
                    ->addIf($this->userHasSomeBackendRoles, 'Profils internes', route('users-in'))
//                    ->add('Liste d\'attente', route('users-on-waitinglist'))
                    ->addIf($this->userHasSomeBackendRoles || Auth::user()->hasRole(User::ROLE_SUPER_ADMIN), 'Clients actifs', route('clients.index'))
                    ->addIf($this->userHasSomeBackendRoles || Auth::user()->hasRole(User::ROLE_SUPER_ADMIN), 'Clients inactifs', route('clients.index', ['status' => '0']))
                    ->addIf($this->userHasSomeBackendRoles, 'Influenceurs', route('users.influenceurs.index'))
                    ->addIf($this->userHasSomeBackendRoles,'Fermeture de compte', route('client-deletion-requests'));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Portefeuilles', '', function (Section $section) {
                $section
                    ->add('Portefeuilles commerces', route('wallet-vendeurs'))
                    ->add('Portefeuilles clients', route('wallet-clients'));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Parrainages', '', function (Section $section) {
                $section
                    ->add('Historique de parrainage', route('listing-parrainage'))
                    ->add('Parrainage par commerce', route('listing-parrainage-commerce'));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Revenus', route('listing-revenu'))
            ->addIf($this->userHasSomeBackendRoles, 'Boost de commerce', '', function (Section $section) {
                $section
                    ->add('Package de boost', route('package.index'))
                    ->add('Commerces boostés', route('commerces-boostes'))
                    ->add('Historique de souscriptions', route('historique-commerces-boost'));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Fraudes et Litiges', '', function (Section $section) {
                $section
                    ->add('Historique ', route('fraud.index'))
                    ->add('En attente de validation', route('fraud.pending-validation'))
                    ->add('Hyper-paramètres', route('fraud.hyper-parameters'));
            })
            ->addIf($this->userHasSomeBackendRoles, 'Paramétrages', '', function (Section $section) {
                $section
                    ->add('Paramétrage global', route('parametrage.index'))
                    ->add('Payment Service Providers', route('payment-service-provider.index'))
                    ->add('Motis de refus de tickets', route('tickets.cancel-reasons.index'))
                    ->add('Gestion des Pays', route('pays.index'))
                    ->add('Gestion des Villes', route('villes.index'))
                    ->add('Gestion des Quartiers', route('quartiers.index'))
                    ->add('Gestion des Roles', route('roles.index'))
                    ->add('Consulter les logs', url('log-viewer'));
            });

        $navTree = Navigation::make()->tree();

        return view('components.ui.side-navigation', compact('navTree'));
    }
}
