<?php

namespace App\Services;

use App\AccountRestriction;
use App\Events\Pusher\ClientLock;
use App\Helpers\TransactionHelper;
use App\Mail\FraudMail;
use App\Models\Client;
use App\Models\FraudConfig;
use App\Models\Parrainage;
use App\Models\Transaction as TransactionModel;
use App\Models\TransactionTrace;
use App\Models\User;
use App\Models\WalletClient;
use App\Models\WalletVendeur;
use App\Notifications\FraudModerationNotification;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class FraudModeration
{
    private TransactionHelper $transaction;
    private TransactionModel|Builder|Collection $transactions;
    private WalletVendeur $walletVendeur;
    private WalletClient $wallet;
    private array $notify;
    private bool $block;
    private array $restrictions;
    private int $score;
    private Collection $flaggedRules;

    /**
     * @param TransactionHelper $transaction
     */
    public function __construct(TransactionHelper $transaction)
    {
        $this->transaction = $transaction;
        $this->restrictions = [];
        $this->block = false;
        $this->score = 0;
        $this->notify = [
            'vendor' => [],
            'admin' => [],
        ];
        $this->flaggedRules = collect([]);
        $this->transactions = new TransactionModel();
        $this->wallet = new WalletClient();
        $this->walletVendeur = new WalletVendeur();
    }

    /**
     * @return array
     */
    public function getNotify(): array
    {
        return $this->notify;
    }

    /**
     * @return bool
     */
    public function isBlock(): bool
    {
        return $this->block;
    }

    /**
     * @return array
     */
    public function getRestrictions(): array
    {
        return $this->restrictions;
    }


    /**
     * @return TransactionHelper
     */
    public function getTransaction(): TransactionHelper
    {
        return $this->transaction;
    }

    /**
     * @return array
     */
    public function getFlaggedRules(): Collection
    {
        return $this->flaggedRules;
    }

    /**
     * @return Collection
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * @return WalletVendeur
     */
    public function getWalletVendeur(): WalletVendeur
    {
        return $this->walletVendeur;
    }

    /**
     * @return WalletClient
     */
    public function getWallet(): WalletClient
    {
        return $this->wallet;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    // -------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------

    /**
     * Initialize the wallet
     *
     * @return void
     */
    public function initializeWallet(): void
    {
        $this->wallet = WalletClient::with('client')->where('id_fidelite', $this->getTransaction()->getIdFidelity())
            ->first();
        $this->walletVendeur = WalletVendeur::with('vendeur.user')
            ->where('programme_id', $this->wallet->programme_id)
            ->first();
    }

    /**
     * Process the rules, check if any rule is broken
     *
     * @return void
     */
    public function processFlaggedRules()
    {
        $flaggedRules = collect([]);

        // No rule enabled, so don't proceeed
        $rules = FraudConfig::where('active', true)->get();
        if ($rules->isEmpty()) {
            return null;
        }

        // There is at least 1 rule, so we can now initialize the wallet.
        $this->initializeWallet();

        // Loop through the rules and build up queries to pull the transaction that matches the rule
        foreach ($rules as $rule) {
            $weight = 0;
            // Duration filter if not infinity
            if ($rule->restrict_duration_unit != 'infinity') {
                $min_date = now()->sub($rule->interval_value, $rule->interval_unit);
                $this->transactions = $this->transactions->where('created_at', '>=', $min_date);
            }

            // Transaction type filter if not all
            if ($rule->transaction_type != 'all') {
                $this->transactions = $this->transactions->where('type_transaction', $rule->transaction_type);
            }

            // Per agent filter if not global
            if (!$rule->global) {
                $this->transactions = $this->transactions->where('wallet_vendeur_id', $this->getWalletVendeur()->id);
                if ($this->getTransaction()->getAgentId() != null) {
                    $this->transactions = $this->transactions->where('agent_id', $this->getTransaction()->getAgentId());
                }
            }

            // Sum the amount || count the total transactions
            if ($rule->value_type == 'count') {
                $weight = $this->transactions->count();
            } else if ($rule->value_type == 'amount') {
                $weight = $this->transactions->sum('montant');
                // $achat_ids = $this->transactions->get()->pluck('achat_id');
                // $weight = Achat::whereIn('id', $achat_ids)->sum('montant_facture');
            }

            // If the sum/count is equal or more than the min value, then transaction is fraudulent. Decide what to do with the transaction.
            if ($weight >= $rule->value) {

                // Add the flagged rule to list of rules
                $flaggedRules->add($rule);

                // Check if need to notify admin, then add to the bag
                if ($rule->notify_admin) {
                    $this->notify['admin'][] = $rule->admin_message ?? 'Illegal transaction found';
                }

                // Check if need to notify owner, then add message to the bag
                if ($rule->notify_shop_owner) {
                    $this->notify['vendor'][] = $rule->vendor_message ?? 'Blocked transaction';
                }

                // If not authorized, then set blocked to true.
                if (!$rule->authorize) {
                    $this->block = true;
                }

                if ($rule->restrict != "no") {
                    $this->restrictions[] = [
                        'restrict' => $rule->restrict == "yes",
                        'data' => [
                            'message' => $rule->user_message,
                            'infinity' => $rule->restriction_duration_unit == 'infinity',
                            'flag_relatives' => $rule->flag_relatives,
                            'duration' => $this->convertToMinutes($rule->restriction_duration_unit, $rule->restriction_duration_value),
                        ]
                    ];
                }

                // Add the score of this to the total score.
                $this->score += $rule->score;
            }
        }

        $this->flaggedRules = $flaggedRules;
    }

    public function process(): array
    {
        $response = false;
        $this->processFlaggedRules();
        $message = "";

        // No rule is broken, return false;
        if ($this->flaggedRules->isEmpty()) {
            return [false, ""];
        }

        // If blocked or the score(total of all scores) is more than the min of 100,
        if ($this->block || $this->getScore() >= 100) {
            $response = true;
        }

        // Record new transaction trace
        foreach ($this->flaggedRules as $item) {
            // if ($item instanceof FraudConfig) {
            $flag = new TransactionTrace;
            $flag->transaction_id = null;
            $flag->client_id = $this->getWallet()->client_id;
            $flag->vendeur_id = $this->getTransaction()->getVendorId();
            $flag->agent_id = $this->getTransaction()->getAgentId();
            $flag->fraud_id = $item->id;
            $flag->authorized = $item->authorize;
            $flag->amount = $item->value;
            $flag->transaction_type = $item->transaction_type;
            $flag->pending = $item->restrict == 'pending';
            $flag->save();

            if ($item->notify_admin) {
                $this->sendMessageToSuperAdmins(new FraudMail($item, $flag));
            }
            // }
        }

        foreach ($this->notify['vendor'] as $message) {
            try {
                Notification::locale('fr')
                    ->sendNow(
                        $this->getWalletVendeur()->vendeur->user,
                        new FraudModerationNotification($message)
                    );
            } catch (\Throwable $e) {
                Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
            }
        }

        $this->restrictions = collect($this->restrictions)->sortByDesc("data.duration")->toArray();
        $activeRestriction = collect($this->restrictions)->filter(function ($item) {
            return $item['restrict'] == true;
        })->first();
        $relativeRestriction = collect($this->restrictions)->filter(function ($item) {
            return $item['data']['flag_relatives'] == true && $item['restrict'] == true;
        })->first();

        $batch1 = md5(time());
        $batch2 = md5(time() + 0.5);

        if ($activeRestriction != null) {
            $client = Client::with('user')->find($this->wallet->client_id);
            $client->status = false;
            $client->save();
            event(new ClientLock($client));
            $client->user->tokens()->delete();
            $message = "Désole transaction non autorisée";
            if (!$activeRestriction['data']['infinity']) {
                $date = now()->addMinutes($activeRestriction['data']['duration']);
                AccountRestriction::schedule(collect([$client]), $date, $batch1);
                $message = $activeRestriction['data']['message'];
            }
        }

        if ($relativeRestriction != null) {
            $parrain = Parrainage::with('parrain')->where('filleul_id', $this->wallet->client_id)->first();
            $filleuls = Parrainage::with('filleul')->where('parrain_id', $this->wallet->client_id)->get();
            $toRestrict = collect([]);
            if ($parrain) {
                $toRestrict->add($parrain->parrain);
            }
            foreach ($filleuls as $filleul) {
                $toRestrict->add($filleul->filleul);
            }

            foreach ($toRestrict as $item) {
                $item->status = false;
                $item->save();
                event(new ClientLock($item));
                User::find($item->user_id)->tokens()->delete();
            }

            if (!$relativeRestriction['data']['infinity']) {
                $date = now()->addMinutes($relativeRestriction['data']['duration']);
                AccountRestriction::schedule($toRestrict, $date, $batch2);
            }
        }

        return [$response, $message];
    }

    private function sendMessageToSuperAdmins(Mailable $mail): void
    {
        foreach (User::role('Super-Admin')->get() as $superAdmin) {
            if ($superAdmin instanceof User && $superAdmin->email != null && $superAdmin->email != "") {
                Mail::to($superAdmin->email)->send($mail);
            }
        }
    }

    private function convertToMinutes(string $unit, int $value): int
    {
        return match ($unit) {
            'minute' => $value,
            'hour' => $value * 60,
            'day' => $value * 60 * 24,
            'week' => $value * 60 * 24 * 7,
            'month' => $value * 60 * 24 * 30,
            'year' => $value * 60 * 24 * 365,
            default => INF,
        };
    }

    private function revertMinutes(int $minutes): string
    {
        try {
            return CarbonInterval::minutes($minutes)->cascade()->forHumans();
        } catch (\Exception $exception) {
            return $minutes > 1 ? "$minutes minutes" : "1 minute";
        }
    }
}
