<?php

namespace App\Services;

use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class SnsService
{

    const TYPE_PROMOTIONAL = 'Promotional ';
    const TYPE_TRANSACTIONAL = 'Transactional ';
    /**
     * @var SnsClient
     */
    private SnsClient $snsClient;

    public function __construct()
    {
        $this->snsClient = App::make('aws')->createClient('sns');
    }

    /**
     * @param string $sender
     * @param string $type
     */
    private function setSMSAttributes(string $sender, string $type = 'Transactional')
    {
        $this->snsClient->SetSMSAttributes(
            [
                'attributes' => [
                    'DefaultSMSType' => $type,
                    'DefaultSenderID' => $sender
                ],
            ]
        );
    }


    /**
     * @param string $sender
     * @param string $phone
     * @param string $message
     * @return string|null
     */
    public function sendSMSSns(string $sender, string $phone, string $message, $type = 'Promotional'): ?string
    {
        try {
            if (strlen($sender) > 11) {
                $sender = str_replace(' ', '', ucfirst($sender));
                $sender = substr($sender, 0, 11);
            }
            $this->setSMSAttributes($sender, $type);
            $result = $this->snsClient->publish([
                'Message' => trim($message),
                'PhoneNumber' => $phone
            ]);
            return $result->hasKey('MessageId') ? $result->get('MessageId') : null;
        } catch (AwsException $e) {
            Log::alert('SEND SMS EXCEPTION : ' . $e->getMessage());
            return $e;
        }
    }

    /**
     * @return SnsClient
     */
    public function getSnsClient(): SnsClient
    {
        return $this->snsClient;
    }
}
