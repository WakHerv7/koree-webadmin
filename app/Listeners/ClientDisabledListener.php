<?php

namespace App\Listeners;

use App\Events\Pusher\ClientDisabled;
use App\Events\SendSMSEvent;
use App\Helpers\SmsApi;
use App\Helpers\SmsPartner;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientDisabledListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param SendSMSEvent $event
     * @return void
     */
    public function handle(ClientDisabled $event)
    {
        $event->getUser()->tokens()->delete();
    }
}
