<?php

use Illuminate\Support\Facades\Log;


if (!function_exists('sendSmsWithTarget')) {
    function sendSmsWithTarget($data){
        $endpoint = "https://api-public-2.mtarget.fr/messages";
        $client = new \GuzzleHttp\Client();
        $theQuery = [
            'username' => $data['username'],
            'password' => $data['password'], 'sender'=>env('MT_SENDER'),
            'msisdn' => $data['msisdn'], 'msg' => $data['message']];

//            Log::info($theQuery);

        try {
            $response = $client->request('POST', $endpoint, ['form_params' => $theQuery]);
            $statusCode = $response->getStatusCode();
            $content = $response->getBody();

            Log::info($content);

            return $content;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            Log::error("Message erreur SMS: ". $e->getMessage());
            return false;
        }


    }
}

if (!function_exists('getParrain')) {
    function getParrain($filleulId){

        try {
           $parrainage= \App\Models\Parrainage::where('filleul_id','=',$filleulId)->with('parrain')->first();

            if($parrainage){
                return $parrainage->parrain;
            }else{
                return false;
            }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            Log::error("Erreur recuperation du parrain : ". $e->getMessage());
            return false;
        }
    }
}

