<?php

namespace App\Http\Middleware;

use Kreait\Firebase\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\FailedToVerifyToken;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;

class FirebaseAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $idToken      = $request->bearerToken();

        if ( !$idToken ) {
            return response()->json(['message' => 'The token is not provide' ], 401);
        }

        $serviceAccount = [
            'type' => 'service_account',
            'project_id' => env('FIREBASE_PROJECT_ID'),
            'private_key_id' => env('FIREBASE_PRIVATE_KEY_ID'),
            'private_key' => Str::replace('\n', "\n", env('FIREBASE_PRIVATE_KEY')),
            'client_email' => env('FIREBASE_CLIENT_EMAIL'),
            'client_id' => env('FIREBASE_CLIENT_ID'),
            'auth_uri' => env('FIREBASE_AUTH_URI'),
            'token_uri' => env('FIREBASE_TOKEN_URI'),
            'auth_provider_x509_cert_url' => env('FIREBASE_AUTH_PROVIDER_CERT_URL'),
            'client_x509_cert_url' => env('FIREBASE_CLIENT_CERT_URL'),
        ];

        $factory      = (new Factory)->withServiceAccount($serviceAccount);
        $firebaseAuth = $factory->createAuth();
        try {
            $verifiedIdToken = $firebaseAuth->verifyIdToken($idToken);
            $claims          = $verifiedIdToken->claims();
            $uid             = $claims->get('sub');

            $guest = User::query()->where('firebase_uid', $uid)->first();

            if ( !$guest ) {
                return response()->json(['message' => 'The user is empty'], 401);
            }

            $request->attributes->set('user', $guest);

        } catch (FailedToVerifyToken $e) {
            return response()->json(['message' => 'The token is invalid: ' . $e->getMessage()], 401);
        } catch (FirebaseException $e) {
            return response()->json(['message' => 'An error occurred while verifying the token: ' . $e->getMessage()], 500);
        }

        return $next($request);

    }
}
