<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AppToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
//         if (!$request->header('app-token') || config('app.token') != $request->header('app-token')) {
//             $message = "Token d'accès invalide";
//             if ($request->header('locale') == 'en')
//                 $message = "Invalid app token";
//             return response(['message' => $message, 'success' => false, 'data' => null], Response::HTTP_BAD_REQUEST);
//         }

        return $next($request);

    }
}
