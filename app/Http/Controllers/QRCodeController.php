<?php

namespace App\Http\Controllers;

use App\Models\QrCode;
use Illuminate\Http\Request;

class QRCodeController extends Controller
{
    public function frontend(Request $request)
    {
        dd($request);
        return view("frontend.qrcode");
    }

    public function destroy($id)
    {
        QrCode::query()->findOrFail($id)->delete();
        return redirect()->back();
    }
}
