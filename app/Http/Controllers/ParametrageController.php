<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ParametrageController extends Controller
{
    public function index()
    {
        $data = DB::table('parametrage')->first();
        return view("backend.parametrage.index", compact('data'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom_societe' => 'nullable',
            'email' => 'nullable|email',
            'email_support' => 'nullable|email',
            'telephone' => 'nullable',
            'mnt_wallet_max' => 'nullable',
            'transaction_hebdo_max' => 'nullable',
            'transaction_mensuelle_max' => 'nullable',
            'plafond_parrainage' => 'nullable',
            'app_version' => 'nullable',
            'otp_number' => 'nullable',
            'otp_email' => 'nullable|email',
            'sms_provider' => 'nullable',
        ], [
            'email' => "Le champ :attribute doit être un email valide",
            'required' => "Le champ :attribute est obligatoire",
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput($request->input());
        }
//        dd($request->all());

        $first = DB::table('parametrage')->first();


        $data = $request->except(['_method', '_token']);

        if ($first) {
            DB::table('parametrage')->update($data);
        } else {
            DB::table('parametrage')->truncate();

            DB::table('parametrage')->insert([
                'nom_societe' => $request->get('nom_societe'),
                'email' => $request->get('email'),
                'email_support' => $request->get('email_support'),
                'telephone' => $request->get('telephone'),
                'mnt_wallet_max' => $request->get('mnt_wallet_max'),
                'transaction_hebdo_max' => $request->get('transaction_hebdo_max'),
                'transaction_mensuelle_max' => $request->get('transaction_mensuelle_max'),
                'plafond_parrainage' => $request->get('plafond_parrainage'),
                'app_version' => $request->get('app_version'),
                'address' => $request->get('address'),
                'fax' => $request->get('fax'),
                'otp_number' => $request->get('otp_number'),
                'otp_email' => $request->get('otp_email'),
                'sms_provider' => $request->get('sms_provider'),
                'updated_at' => now(),
                'created_at' => now(),
                'created_by' => auth()->id(),
                'updated_by' => auth()->id(),
            ]);
        }



        return back()->with('success', "Paramètres globaux mis à jour !");
    }
}
