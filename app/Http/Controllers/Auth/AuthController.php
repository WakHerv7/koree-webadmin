<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Session;
use App\Models\User;
use Hash;

class AuthController extends Controller
{
   // use AuthenticatesUsers;
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        return view('backend.auth.login');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function registration()
    {
        return view('auth.registration');
    }

    /**
     * Write code on Method
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(Request $request)
    {
       /* $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
       */
        $validator = Validator::make($request->all(), [
            'username' => 'required|email',
            'password' => 'required|min:6',
            //'seats' => 'integer|size:10';
        ]);

        if($validator->fails()){
            return redirect("login")->withErrors($validator)->withInput();
        }

        $credentials = ['email'=>$request->get('username'),'password'=>$request->get('password')];
        //dd($credentials);
        if (Auth::attempt($credentials)) {
            return redirect("dashboard");
            //return view('backend.dashboard.super_admin');
        }else{
            return redirect("login")->with('error',"Email ou mot de passe incorrect")->withInput();
        }
        //dd($validator->errors());
        return redirect("login");
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function postRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();
        $check = $this->create($data);

        return redirect("dashboard")->withSuccess('Great! You have Successfully loggedin');
    }

    /**
     * Retourne lutilisateur ici apres connexion on recuperera eventuellement des donnees
     *
     * @return response()
     */
    public function dashboard()
    {
        if(Auth::check()){
            return view('backend.dashboard.super_admin');
        }

        return redirect("login")->withSuccess('Opps! You do not have access');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }

    /**
     * Write code on Method
     *
     * @return \Illuminate\Http\RedirectResponse()
     */
    public function logout(Request $request) {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return to_route('login');
    }
}
