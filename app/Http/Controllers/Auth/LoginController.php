<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        return view('backend.auth.login');
    }

    /**
     * Write code on Method
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(Request $request)
    {
        /* $request->validate([
             'email' => 'required',
             'password' => 'required',
         ]);
        */
        $validator = Validator::make($request->all(), [
            'username' => 'required|email',
            'password' => 'required|min:6',
            //'seats' => 'integer|size:10';
        ]);

        if($validator->fails()){
            return redirect("login")->withErrors($validator)->withInput();
        }

        $credentials = ['email'=>$request->get('username'),'password'=>$request->get('password')];
        //dd($credentials);
        if (Auth::attempt($credentials)) {
            return redirect("dashboard");
            //return view('backend.dashboard.super_admin');
        }else{
            return redirect("login")->with('error',"Email ou mot de passe incorrect")->withInput();
        }
        //dd($validator->errors());
        return redirect("login");
    }

    /**
     * Retourne lutilisateur ici apres connexion on recuperera eventuellement des donnees
     *
     * @return response()
     */
    public function dashboard()
    {
        if(Auth::check()){
          //  return view('backend.dashboard.super_admin');
            return redirect('dashboard');
        }

        return redirect("login")->withSuccess('Opps! You do not have access');
    }
}
