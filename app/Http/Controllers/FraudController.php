<?php

namespace App\Http\Controllers;


use App\AccountRestriction;
use App\Models\Client;
use App\Models\FraudConfig;
use App\Models\Parrainage;
use App\Models\TransactionTrace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function Aws\boolean_value;

class FraudController extends Controller
{
    public function index()
    {
        $frauds = TransactionTrace::with(['fraud', 'client', 'vendeur'])->where("pending", false)->orderByDesc('created_at')->get();
        return view("backend.fraud.index", compact("frauds"));
    }

    public function pending()
    {
        $frauds = TransactionTrace::with(['fraud', 'client', 'vendeur'])->where('pending', true)->orderByDesc('created_at')->get();
        return view("backend.fraud.pending-validation", compact('frauds'));
    }

    public function show(int $id)
    {
        $trace = TransactionTrace::with(['client', 'fraud', 'vendeur.commerce.categorie', 'transaction', 'agent'])->find($id);
        return view("backend.fraud.show", compact('trace'));
    }

    public function processPending(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric|exists:transaction_traces',
            'restrict' => 'required|in:yes,no',
            'flag_relatives' => 'required|in:true,false',
            'restrict_duration_value' => 'required|numeric',
            'restrict_duration_unit' => 'required|in:minute,hour,day,week,month,year,infinity',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->input());
        }
        $trace = TransactionTrace::with('client')->find($request->id);
        if ($request->restrict == 'yes') {
            $date = now();
            if ($request->restriction_duration_unit == 'minute') {
                $date = now()->addMinutes($request->restrict_duration_value);
            } elseif ($request->restriction_duration_unit == 'hour') {
                $date = now()->addHours($request->restrict_duration_value);
            } elseif ($request->restriction_duration_unit == 'day') {
                $date = now()->addDays($request->restriction_duration_value);
            } elseif ($request->restriction_duration_unit == 'week') {
                $date = now()->addWeeks($request->restriction_duration_value);
            } elseif ($request->restriction_duration_unit == 'month') {
                $date = now()->addMonths($request->restriction_duration_value);
            } elseif ($request->restriction_duration_unit == 'year') {
                $date = now()->addYears($request->restriction_duration_value);
            }
            $toRestrict = collect([]);
            $toRestrict->add($trace->client);
            if ($request->flag_relatives == 'true') {
                $parrain = Parrainage::where('filleul_id', $trace->client_id)->first();
                $filleuls = Parrainage::where('parrain_id', $trace->client_id)->get();
                foreach ($filleuls as $filleul) {
                    $toRestrict->add($filleul->filleul);
                }
                if ($parrain) {
                    $toRestrict->add($parrain->parrain);
                }
            }
            foreach ($toRestrict as $item) {
                if ($item instanceof Client) {
                    $item->status = true;
                    $item->save();
                }
            }
            AccountRestriction::schedule($toRestrict, $date, md5(time()));
        }

        $trace->pending = false;
        $trace->save();

        return redirect()->route("fraud.show", ['id' => $trace->id])->with('success', 'Flag traité avec succès');
    }

    public function hyperParameters()
    {
        $configs = FraudConfig::all();
        return view("backend.fraud.hyper-parameters", compact('configs'));
    }

    public function createHyperParameter()
    {
        return view("backend.fraud.create-hyper-parameter");
    }

    public function editHyperParameter(int $id)
    {
        $config = FraudConfig::find($id);
        if ($config) {
            return view("backend.fraud.edit-hyper-parameter", compact('config'));
        }

        return redirect()->route('fraud.hyper-parameters');

    }

    public function saveHyperParameter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required|numeric',
            'interval_value' => 'required|numeric',
            'interval_unit' => 'required|in:minute,hour,day,week,month,year,infinity',
            'transaction_type' => 'required|in:cashback,topup,all,paiement',
            'global' => 'required|in:true,false',
            'agent' => 'required|in:true,false',
            'notify_shop_owner' => 'required|in:true,false',
            'notify_admin' => 'required|in:true,false',
            'admin_message' => 'nullable|string',
            'client_message' => 'nullable|string',
            'vendor_message' => 'nullable|string',
            'restrict' => 'required|in:yes,no,pending',
            'flag_relatives' => 'required|in:true,false',
            'authorize' => 'required|in:true,false',
            'restrict_duration_value' => 'required|numeric',
            'restrict_duration_unit' => 'required|in:minute,hour,day,week,month,year,infinity',
            'score' => 'required|numeric',
            'include_shop_limit' => 'required|in:true,false',
            'value_type' => 'required|in:amount,count',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->input());
        }
        $config = new FraudConfig;
        $config->value = $request->limit;
        $config->interval_value = $request->interval_value;
        $config->interval_unit = $request->interval_unit;
        $config->transaction_type = $request->transaction_type;
        $config->global = boolean_value($request->global);
        $config->look_agent = boolean_value($request->agent);
        $config->flag_relatives = boolean_value($request->flag_relatives);
        $config->notify_shop_owner = boolean_value($request->notify_shop_owner);
        $config->notify_admin = boolean_value($request->notify_admin);
        $config->restrict = $request->restrict;
        $config->restriction_duration_unit = $request->restrict_duration_unit;
        $config->restriction_duration_value = $request->restrict_duration_value;
        $config->score = $request->score;
        $config->authorize = boolean_value($request->authorize);
        $config->compare_to_shop_limit = boolean_value($request->include_shop_limit);
        $config->value_type = $request->value_type;
        $config->admin_message = $request->admin_message;
        $config->vendor_message = $request->vendor_message;
        $config->user_message = $request->client_message;

        $config->save();
        return redirect()->route('fraud.hyper-parameters')->with('success', 'Hyper paramètre créé avec succès');
    }

    public function updateHyperParameter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:fraud_configs,id',
            'limit' => 'required|numeric',
            'interval_value' => 'required|numeric',
            'interval_unit' => 'required|in:minute,hour,day,week,month,year,infinity',
            'transaction_type' => 'required|in:cashback,topup,all,paiement',
            'global' => 'required|in:true,false',
            'agent' => 'required|in:true,false',
            'notify_shop_owner' => 'required|in:true,false',
            'notify_admin' => 'required|in:true,false',
            'admin_message' => 'nullable|string',
            'client_message' => 'nullable|string',
            'vendor_message' => 'nullable|string',
            'restrict' => 'required|in:yes,no,pending',
            'flag_relatives' => 'required|in:true,false',
            'include_shop_limit' => 'required|in:true,false',
            'authorize' => 'required|in:true,false',
            'restrict_duration_value' => 'required|numeric',
            'restrict_duration_unit' => 'required|in:minute,hour,day,week,month,year,infinity',
            'score' => 'required|numeric',
            'value_type' => 'required|in:amount,count',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->input());
        }
        $config = FraudConfig::find($request->id);
        $config->value = $request->limit;
        $config->interval_value = $request->interval_value;
        $config->interval_unit = $request->interval_unit;
        $config->transaction_type = $request->transaction_type;
        $config->global = boolean_value($request->global);
        $config->look_agent = boolean_value($request->agent);
        $config->flag_relatives = boolean_value($request->flag_relatives);
        $config->notify_shop_owner = boolean_value($request->notify_shop_owner);
        $config->notify_admin = boolean_value($request->notify_admin);
        $config->restrict = $request->restrict;
        $config->restriction_duration_unit = $request->restrict_duration_unit;
        $config->restriction_duration_value = $request->restrict_duration_value;
        $config->score = $request->score;
        $config->authorize = boolean_value($request->authorize);
        $config->compare_to_shop_limit = boolean_value($request->include_shop_limit);
        $config->value_type = $request->value_type;
        $config->admin_message = $request->admin_message;
        $config->vendor_message = $request->vendor_message;
        $config->user_message = $request->client_message;
        $config->save();
        return redirect()->route('fraud.hyper-parameters')->with('success', 'Hyper paramètre modifié avec succès');
    }

    public function deleteHyperParameter(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:fraud_configs,id'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->input());
        }

        $config = FraudConfig::find($request->id);
        $config->delete();
        return redirect()->route('fraud.hyper-parameters')->with('success', 'Hyper paramètre supprimé avec succès');
    }

    public function toggleStatus(int $id)
    {
        $config = FraudConfig::find($id);
        if ($config) {
            $message = $config->active ? "désactivé" : "activé";
            $config->active = !$config->active;
            $config->save();
            return redirect()->back()->with('success', "Hyper paramètre $message avec succès");
        }

        return redirect()->back()->with('error', "Erruer pendant le traitement de la requête !");
    }
}
