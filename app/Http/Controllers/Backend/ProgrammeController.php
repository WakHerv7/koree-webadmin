<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commerce;
use App\Models\Programme;
use App\Models\WalletVendeur;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\ProgrammeRepository;
use App\Repositories\Frontend\V1\VendeurRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProgrammeController extends Controller
{
    protected $rpCommerce;
    protected $rpVendeur;
    protected $rpProgramme;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository $rpcom, VendeurRepository $rpv, ProgrammeRepository $rpProg)
    {
        $this->middleware('permission:programme-list|programme-create|programme-edit|programme-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:programme-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:programme-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:programme-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpcom;
        $this->rpVendeur = $rpv;
        $this->rpProgramme = $rpProg;
    }

    /**
     * Display a listing commerce actif.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->rpProgramme->getAll();
        return view('backend.programmes.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commerces = $this->rpCommerce->getAllFree();
        //dd($commerces);

        return view('backend.programmes.create', compact('commerces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'date_debut' => 'required',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $inputs = $request->all();
        $desc = (isset($request['description'])) ? $request['description'] : '';
        $date_debut = Carbon::createFromDate($inputs['date_debut']);
        $date_fin = $date_debut->addDays($inputs['duree']);

        $donnees = ['libelle' => $inputs['libelle'], 'date_debut' => $date_debut, 'status' => 1, 'is_cashback' => 1,
            'date_fin' => $date_fin, 'description' => $desc, 'commerce_id' => $inputs['commerce_id'], 'montant_abonnement' => $inputs['montant_abonnement'],
            'taux_commission' => $inputs['taux_commission'], 'mnt_max_facture' => $inputs['mnt_max_facture'], 'bonus_wallet' => $inputs['bonus_wallet'], 'created_by' => Auth::user()->id];

        $prog = Programme::create($donnees);

        //on recupere le vendeur
        $commerce = Commerce::find($inputs['commerce_id']);

        $datar = ['id_fidelite' => $this->generateUniqueNumber(), 'cashback' => 0, 'topup' => 0, 'passages' => 0,
            'vendeur_id' => $commerce->vendeur_id, 'programme_id' => $prog->id];

        // ici on cree le wallet du vendeur
        $wali = WalletVendeur::create($datar);

        return redirect()->route('programmes.index')
            ->with('success', 'programme créé avec succès.');
    }

    // genere un unique code de parrainage et recherche sil nya pas un suer qui la deja
    public function generateUniqueNumber()
    {
        $code = "";
        do {
            $code = strtoupper(Str::random(6));
        } while (WalletVendeur::where("id_fidelite", "=", $code)->first());

        return $code;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $data = $this->rpProgramme->getById($id);
        return view('backend.programmes.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Programme::find($id);
        $commerces = Commerce::get();

        return view('backend.programmes.edit', compact('data', 'commerces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'commerce_id' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->withInput($request->all())->with('error', $mes);
        }

        $data = Programme::find($id);
        $data->libelle = $request->input('libelle');
        $data->date_debut = $request->input('date_debut');
        $data->description = $request->input('description');
        $date_debut = Carbon::createFromDate($request->input('date_debut'));
        $date_fin = $date_debut->addDays($request->input('duree'));

        $data->date_fin = $date_fin;
        $data->commerce_id = $request->input('commerce_id');
        $data->taux_commission = $request->input('taux_commission');
        $data->bonus_wallet = $request->input('bonus_wallet');
        $data->mnt_max_facture = $request->input('mnt_max_facture');
        $data->status = 1;

        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect()->route('programmes.index')
            ->with('success', 'Abonnement mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Programme::find($id)->delete();

        return redirect()->route('programmes.index')
            ->with('success', 'programme supprimé');
    }
}
