<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Commerce;
use App\Models\PointVente;
use App\Repositories\Backend\V1\AgentRepository;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\PointVenteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class AgentController extends Controller
{
    protected $rpCommerce;
    protected $rpPointvente;
    protected $rpAgent;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository $rpCom, PointVenteRepository $pv, AgentRepository $ag)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpCom;
        $this->rpPointvente = $pv;
        $this->rpAgent = $ag;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->rpAgent->getAll();
        return view('backend.agents.index', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commerces = Commerce::get();

        return view('backend.agents.create', compact('commerces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom_prenom' => 'required',
            'point_vente_id' => 'required|numeric',
            'code' => 'required',
            'is_admin' => 'required|in:non,oui',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
            'is_admin.in' => 'Le champ :attribute doit avoir soit la valeur oui soit la valeur non',
        ]);
        $inputs = $request->all();
        //dd(strlen($request['code']));
        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->withInput($request->input())->with('error', $mes);
        } else {
            if (strlen($inputs['code']) != 5) {
                return redirect()->back()->withInput($request->input())->with('error', 'Le code doit être de 5 caractères');
            }
        }
        try{
            if($this->rpAgent->checkExistCode($inputs['code'])) {
                $mes = "Impossible d'utiliser le même code pour plusieurs agents";
                return redirect()->back()->withInput($request->input())->with('error', $mes);
            }
        }catch(\Exception $e){
            $mes = "Une erreur est survenue lors de la vérificaion du code de l'agent";
            Log::info("Une erreur c'est produite ".$e);
            return redirect()->back()->withInput($request->input())->with('error', $mes);
        }

        $desc = (isset($request['code'])) ? $request['code'] : '';
        try {
            Agent::create([
                'code' => $desc,
                'nom_prenom' => $inputs['nom_prenom'],
                'point_vente_id' => $inputs['point_vente_id'],
                'is_admin' => $inputs['is_admin'] == 'oui'
            ]);
    
            return redirect()->route('agents.index')
                ->with('success', 'Agent créé avec succès.');
        }catch(\Exception $e){
            $mes = "Une erreur est survenue lors de la créaion de l'agent";
            Log::info("Une erreur c'est produite ".$e);
            return redirect()->back()->withInput($request->input())->with('error', $mes);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->rpAgent->getById($id);
        return view('backend.agents.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->rpAgent->getById($id);
        $le_point_vente = PointVente::where('id', '=', $data->point_vente_id)->with('commerce')->first();
        $point_vente_id = $le_point_vente->id;
        $commerce_id = (isset($le_point_vente->commerce)) ? $le_point_vente->commerce->id : null;
        $pointventes = PointVente::where('commerce_id', '=', $commerce_id)->get();
        $commerces = Commerce::get();

        return view('backend.agents.edit', compact('data', 'commerce_id', 'point_vente_id', 'commerces', 'pointventes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nom_prenom' => 'required',
            'point_vente_id' => 'required|numeric',
            'code' => 'required',
            'is_admin' => 'required|in:non,oui',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
            'is_admin.in' => 'Le champ :attribute doit avoir soit la valeur oui soit la valeur non',
        ]);

        $inputs = $request->all();

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        } else {
            if (strlen($inputs['code']) != 5) {
                return redirect()->back()->withInput($request->input())->with('error', 'Le code doit être de 5 caractères');
            }
        }

        $data = Agent::find($id);
        $data->nom_prenom = $request->input('nom_prenom');
        $data->code = $request->input('code');
        $data->point_vente_id = $request->input('point_vente_id');
        $data->is_admin = $request->get('is_admin') == "oui";
        $data->save();

        return redirect()->route('agents.index')
            ->with('success', 'Agent mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agent::find($id)->delete();
        return redirect()->route('agents.index')
            ->with('success', 'Agent supprimé');
    }
}
