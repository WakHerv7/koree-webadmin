<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Parrainage;
use App\Models\ShopReferral;
use App\Models\User;
use App\Models\WaitingList;
use App\Repositories\Frontend\V1\ClientRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    private ClientRepository $clientRepository;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(ClientRepository $clientRepository)
    {
        $this->middleware('permission:users-list|users-create|users-edit|users-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:users-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
        $this->clientRepository = $clientRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::with('')->orderBy('id', 'desc')->paginate(25);

        return view('users.index', compact('data'));
    }

    /**
     * Display a listing of the internal users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listUsersInternes()
    {
        $data = User::where('type_user', '=', 'user')->with('roles')->orderBy('id', 'desc')->paginate(100);
        //dd($data[0]->roles[0]);

        return view('backend.users.index', compact('data'));
    }

    /**
     * Display a listing of the users on waiting list .
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listUsersOnWaitinglist()
    {
        abort(Response::HTTP_NOT_FOUND);

        $data = WaitingList::query()->paginate(100);

        return view('backend.users.waitinglist', compact('data'));
    }

    public function listParrainage()
    {
        $data = Parrainage::with('parrain.user', 'filleul.user')->orderBy('id', 'desc')->paginate(10000);
        return view('backend.parrainage.index', compact('data'));
    }

    public function listParrainageCommerce()
    {
        $data = ShopReferral::with(['commerce', 'parrain.user', 'filleul.user', 'transactions.wallet_vendeur.programme'])->get();
        //dd($data[0]->roles[0]);
        return view('backend.parrainage.index-2', compact('data'));
    }

    /**
     * Display a listing of the internal users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listVendeurs()
    {
        $data = User::where('type_user', '=', 'vendeur')->with('roles')->orderBy('id', 'desc')->paginate(100);
        //dd($data[0]->roles[0]);

        return view('backend.vendeurs.index', compact('data'));
    }

    /**
     * Display a listing of the internal users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function listInfluenceurs()
    {

        $data = $this->clientRepository->paginateAllInfluencers();

        return view('backend.influenceurs.index', compact('data'));
    }

    /**
     * Show the form for creating a new internal user.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUsersInternes()
    {
        $roles = Role::pluck('name', 'name')->all();

        return view('backend.users.create', compact('roles'));
    }

    /**
     * Show the form for creating a new internal user.
     *
     * @return \Illuminate\Http\Response
     */
    public function createVendeurs()
    {
        $roles = Role::pluck('name', 'name')->all();

        return view('backend.vendeurs.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeUsersInternes(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'telephone' => 'required|numeric|unique:users,telephone',
            'password' => 'required|confirmed',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $user->uuid = Str::uuid()->toString();
        return redirect()->route('users-in')
            ->with('success', 'User created successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeVendeurs(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'email' => 'required|email|unique:users,email',
            'telephone' => 'required|numeric|unique:users,telephone',
            'password' => 'required|confirmed'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $user->uuid = Str::uuid()->toString();

        return redirect()->route('vendeurs-in')
            ->with('success', 'Seller created successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $user->uuid = Str::uuid()->toString();

        return redirect()->route('users.index')
            ->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showUsersInternes($id)
    {
        $user = User::find($id);

        return view('backend.users.show', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showVendeurs($id)
    {
        $user = User::find($id);

        return view('backend.vendeurs.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editUsersInternes($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('backend.users.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function editVendeurs($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('backend.vendeurs.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateUsersInternes(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required',
            'telephone' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required'
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')
            ->where('model_id', $id)
            ->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users-in')
            ->with('success', 'User updated successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateVendeurs(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'telephone' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required'
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')
            ->where('model_id', $id)
            ->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('vendeurs-in')
            ->with('success', 'User updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUsersInternes($id)
    {
        User::find($id)->delete();

        return redirect()->route('backend.users.index')
            ->with('success', 'User deleted successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyVendeurs($id)
    {
        User::find($id)->delete();

        return redirect()->route('backend.vendeurs.index')
            ->with('success', 'User deleted successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('users.edit', compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'confirmed',
            'roles' => 'required'
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')
            ->where('model_id', $id)
            ->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully.');
    }
}
