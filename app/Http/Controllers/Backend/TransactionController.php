<?php


namespace App\Http\Controllers\Backend;


use App\Events\Pusher\NewClientTransaction;
use App\Events\Pusher\NewVendorTransaction;
use App\Exports\TransactionsExport;
use App\Helpers\AdminOtp;
use App\Models\Transaction;
use App\Models\WalletClient;
use App\Models\WalletVendeur;
use App\Repositories\Backend\V1\AchatRepository;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\ParrainageRepository;
use App\Repositories\Backend\V1\TransactionRepository;
use App\Repositories\Backend\V1\WalletRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class TransactionController
{
    protected $rpCommerce;
    protected $rpTrans;
    protected $rpParrainage;
    protected $rpAchat;
    protected $rpWallet;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository $rpcom, TransactionRepository $ts, ParrainageRepository $pr, AchatRepository $ac, WalletRepository $rpw)
    {
        $this->rpCommerce = $rpcom;
        $this->rpTrans = $ts;
        $this->rpParrainage = $pr;
        $this->rpAchat = $ac;
        $this->rpWallet = $rpw;
    }

    public function listeTransactions(Request $request)
    {
        $data = [];
        return view('backend.transactions.histo_transaction', compact('data'));
    }


    public function listeTransactionsSupprimes()
    {
        $data = [];

        return view('backend.transactions.histo_transaction_supprimes', compact('data'));
    }

    public function listeTransactionsDatatable(Request $request)
    {
        /** @var JsonResponse $dataArr */
        $dataArr = $this->rpTrans->getTransactionsDatatable($request, $request->filled('deleted'));

        $dataArr = $dataArr->getOriginalContent();

        return response()->json($dataArr, Response::HTTP_OK);
    }

    public function listeTransactionsExport(Request $request)
    {
        try {
            $now = now()->toDateTimeLocalString();
            ini_set('memory_limit', '1G');
            return Excel::download(TransactionsExport::make($request), "koree_transactions_history_{$now}.xlsx");
        } catch (Exception $e) {
            Log::error("An error encountered when trying to download transaction history. Exception: {$e->getMessage()}");
            return redirect()->back();
        }
    }

    public function listeRevenus()
    {
//        $data = $this->rpTrans->getRevenuForLastYearLimits(-1);
        $data = Transaction::active()->where('gain', '<>', 0)->orderByDesc('created_at')->get();
        return view('backend.transactions.histo_revenu', compact('data'));
    }

    public function restoreInit($id)
    {
        $transaction = Transaction::query()->where('status', '0')->findOrFail($id);

        $parametrage = DB::table('parametrage')->first();

        if ($parametrage != null) {
            $otpTo = $parametrage->otp_email;

            if ($otpTo == null) {
                session()->flash('info', "Impossible de continuer, pas de destinataire définit pour le OTP");
                return redirect()->route('listing-transactions');
            }

            (new AdminOtp)->toEmail($otpTo);

//        (new SMS($otpNumber, "Code de confirmation de la transaction est $code"))->send();

            session()->flash('info', "Code OTP envoyé au {$otpTo} veuillez l'utiliser pour continuer");
            return redirect()->route('transaction.restore', ['id' => $transaction->id]);
        };
    }

    public function revertInit($id)
    {
        $transaction = Transaction::query()->where('status', '1')->findOrFail($id);

        $parametrage = DB::table('parametrage')->first();

        if ($parametrage != null) {
            $otpTo = $parametrage->otp_email;

            if ($otpTo == null) {
                session()->flash('info', "Impossible de continuer, pas de destinataire définit pour le OTP");
                return redirect()->route('listing-transactions');
            }

            (new AdminOtp)->toEmail($otpTo);

            session()->flash('info', "Code OTP envoyé au $otpTo veuillez l'utiliser pour continuer");
            return redirect()->route('transaction.revert', ['id' => $transaction->id]);
        };
    }

    public function revert($id)
    {
        session()->reflash();
        $transaction = Transaction::query()->where('status', '1')->findOrFail($id);
        return view("backend.transactions.validate-revert", compact('transaction'));

    }

    public function restore($id)
    {
        session()->reflash();
        $transaction = Transaction::query()->where('status', '0')->findOrFail($id);
        return view("backend.transactions.validate-restore", compact('transaction'));
    }


    public function performRestore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction' => 'required|exists:transactions,id',
            'code' => 'required'
        ], [
            'required' => 'Le champ :attribute est obligatoire',
            'exists' => 'Le champ :attribute est incorrect',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput($request->input());
        }


        $code = $request->get('code');

        $rows = DB::table('admin_otps')->where('code', Str::upper($code))->where('used', 0)->first();

        if ($rows == null)
            return back()->withErrors(['Code de sécurité incorrect'])->withInput($request->input());

        DB::table('admin_otps')->where('id', $rows->id)->update([
            'used' => 1
        ]);

        /** @var Transaction $transaction */
        $transaction = Transaction::with(['wallet.client.user', 'wallet_vendeur.vendeur.user'])->where('status', '0')->find($request->get('transaction'));

//        if ($transaction->id != $code)
//            return back()->withErrors(['Code de sécurité incorrect'])->withInput($request->input());

        return $this->makeRestore($transaction);

    }

    public function performRevert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction' => 'required|exists:transactions,id',
            'code' => 'required'
        ], [
            'required' => 'Le champ :attribute est obligatoire',
            'exists' => 'Le champ :attribute est incorrect',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput($request->input());
        }

        $code = $request->get('code');

        $rows = DB::table('admin_otps')->where('code', Str::upper($code))->where('used', 0)->first();
        if ($rows == null)
            return back()->withErrors(['Code de sécurité incorrect'])->withInput($request->input());

        DB::table('admin_otps')->where('id', $rows->id)->update([
            'used' => 1
        ]);

        /** @var Transaction $transaction */
        $transaction = Transaction::with(['wallet.client.user', 'wallet_vendeur.vendeur.user'])->where('status', '1')->find($request->get('transaction'));

        return $this->makeRevert($transaction);

    }

    public function makeRestore(Transaction $transaction)
    {
        if ($transaction->type_transaction == Transaction::TYPE_CREDIT_FIRSTBUY) {
            $type = Transaction::TYPE_CREDIT_BONUS_PARRAINAGE;
        } else {
            $type = $transaction->type_transaction;
        }
        /** @var WalletClient $walletClient */
        $walletClient = $transaction->wallet;

        /** @var WalletVendeur $walletVendor */
        $walletVendor = $transaction->wallet_vendeur;

        try {

            if ($transaction->status == 1) {
                throw new Exception('La transaction est déja restaurée');
            }

            DB::beginTransaction();
            switch ($type) {
                case Transaction::TYPE_CREDIT_TOPUP:
                    $walletVendor->decrement('topup', $transaction->montant);
                    if ($walletClient->topup >= $transaction->montant) {
                        $walletClient->increment('topup', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->topup;
                        $walletClient->topup = 0;
                        $walletClient->cashback = max(0, $walletClient->cashback - $removed);
                        $walletClient->save();
                    }
                    $walletClient->increment('montant', max(0, $transaction->montant));
                    break;
                case Transaction::TYPE_CREDIT_BONUS_PARRAINAGE:
                    $walletVendor->decrement('bonus', $transaction->montant);
                    if ($walletClient->topup >= $transaction->montant) {
                        $walletClient->increment('bonus', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->topup;
                        $walletClient->topup = 0;
                        $walletClient->cashback = max(0, $walletClient->cashback - $removed);
                        $walletClient->save();
                    }
                    $walletClient->increment('montant', max(0, $transaction->montant));
                    break;
                case Transaction::TYPE_DEBIT_PAIEMENT:
                    $walletVendor->increment('payment', $transaction->montant);
                    $walletClient->decrement('topup', $transaction->montant);
                    $walletClient->decrement('montant', $transaction->montant);
                    break;
                case Transaction::TYPE_CREDIT_CASHBACK:
                    $walletVendor->decrement('cashback', $transaction->montant);
                    if ($walletClient->cashback >= $transaction->montant) {
                        $walletClient->increment('cashback', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->cashback;
                        $walletClient->cashback = 0;
                        $walletClient->topup = max(0, $walletClient->topup - $removed);
                        $walletClient->save();
                    }
                    $walletClient->increment('montant', max(0, $transaction->montant));
                default:
                    break;
            }

            $transaction->status = 1;
            $transaction->save();

            DB::commit();
            event(new NewClientTransaction($transaction->wallet->client, $transaction->wallet));
            event(new NewVendorTransaction($transaction->wallet_vendeur->vendeur, $transaction->wallet_vendeur));
            session()->flash('info', "Transaction restaurée avec succès !");
            return redirect()->route('listing-transactions');
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
            session()->flash('error', "Erreur pendant la restauration de la transaction");
            return redirect()->route('listing-transactions-supprimes');
        }
    }

    public function makeRevert(Transaction $transaction)
    {
        if ($transaction->type_transaction == Transaction::TYPE_CREDIT_FIRSTBUY) {
            $type = Transaction::TYPE_CREDIT_BONUS_PARRAINAGE;
        } else {
            $type = $transaction->type_transaction;
        }
        $walletClient = $transaction->wallet;
        $walletVendor = $transaction->wallet_vendeur;

        try {

            if ($transaction->status == 0) {
                throw new Exception('La transaction est déja annulée');
            }

            DB::beginTransaction();
            switch ($type) {
                case Transaction::TYPE_CREDIT_TOPUP:
                    $walletVendor->increment('topup', $transaction->montant);
                    if ($walletClient->topup >= $transaction->montant) {
                        $walletClient->decrement('topup', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->topup;
                        $walletClient->topup = 0;
                        $walletClient->cashback = max(0, $walletClient->cashback - $removed);
                        $walletClient->save();
                    }
                    $walletClient->decrement('montant', max(0, $transaction->montant));
                    break;
                case Transaction::TYPE_CREDIT_BONUS_PARRAINAGE:
                    $walletVendor->increment('bonus', $transaction->montant);
                    if ($walletClient->topup >= $transaction->montant) {
                        $walletClient->decrement('topup', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->topup;
                        $walletClient->topup = 0;
                        $walletClient->cashback = max(0, $walletClient->cashback - $removed);
                        $walletClient->save();
                    }
                    $walletClient->decrement('montant', max(0, $transaction->montant));
                    break;
                case Transaction::TYPE_DEBIT_PAIEMENT:
                    $walletVendor->decrement('payment', $transaction->montant);
                    $walletClient->increment('topup', $transaction->montant);
                    $walletClient->increment('montant', $transaction->montant);
                    break;
                default:
                    $walletVendor->increment('cashback', $transaction->montant);
                    if ($walletClient->cashback >= $transaction->montant) {
                        $walletClient->decrement('cashback', $transaction->montant);
                    } else {
                        $removed = $transaction->montant - $walletClient->cashback;
                        $walletClient->cashback = 0;
                        $walletClient->topup = max(0, $walletClient->topup - $removed);
                        $walletClient->save();
                    }
                    $walletClient->decrement('montant', max(0, $transaction->montant));
                    break;
            }


            $transaction->status = 0;
            $transaction->save();

            DB::commit();
            event(new NewClientTransaction($transaction->wallet->client, $transaction->wallet));
            event(new NewVendorTransaction($transaction->wallet_vendeur->vendeur, $transaction->wallet_vendeur));
            session()->flash('info', "Transaction annulée avec succès !");
            return redirect()->route('listing-transactions');
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
            session()->flash('error', "Erreur pendant l'annulation de la transaction");
            return redirect()->route('listing-transactions');
        }
    }

    //Historique transaction Decroissant
    //Portefeuille Decroissant
    //Desactivation de compte
    //
}
