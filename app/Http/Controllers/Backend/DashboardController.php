<?php


namespace App\Http\Controllers\Backend;


use App\Helpers\TechSoftSMS;
use App\Models\Achat;
use App\Models\Parrainage;
use App\Models\Transaction;
use App\Models\User;
use App\Repositories\Backend\V1\AchatRepository;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\ParrainageRepository;
use App\Repositories\Backend\V1\TransactionRepository;
use App\Repositories\Backend\V1\WalletRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;



class DashboardController
{
    protected $rpCommerce;
    protected $rpTrans;
    protected $rpParrainage;
    protected $rpAchat;
    protected $rpWallet;
    private bool $userHasAnalystRole;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository $rpcom, TransactionRepository $ts, ParrainageRepository $pr, AchatRepository $ac, WalletRepository $rpw)
    {
//        $this->middleware('permission:commerce-list|commerce-create|commerce-edit|commerce-delete', ['only' => ['index','store']]);
//        $this->middleware('permission:commerce-create', ['only' => ['create','store']]);
//        $this->middleware('permission:commerce-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:commerce-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpcom;
        $this->rpTrans = $ts;
        $this->rpParrainage = $pr;
        $this->rpAchat = $ac;
        $this->rpWallet = $rpw;

        $this->userHasAnalystRole = Auth::user()->hasRole(User::ROLE_ANALYST);
    }


    /**
     * Retourne lutilisateur ici apres connexion on recuperera eventuellement des donnees
     *
     *
     */
    public function dashboard()
    {

        getToken();

        if (!\auth()->check()) {
            return redirect()->route('login');
        }

        if ($this->userHasAnalystRole) {
            return redirect()->route('tickets.index');
        }

        Carbon::setLocale('fr');

        $techsoftBalance = 0;

        $dayScans = Achat::whereBetween('created_at', [now()->subHours(24), now()])->get()->toArray();
        $weekScans = Achat::whereBetween('created_at', [now()->subDays(7), now()])->get()->toArray();
        $monthScans = Achat::whereBetween('created_at', [now()->subMonth(), now()])->get()->toArray();
        $yearScans = Achat::whereBetween('created_at', [now()->subYear(), now()])->get()->toArray();


        $dayScansAvg = $weekScansAvg = $monthScansAvg = $yearScansAvg = 0;

        for ($i = sizeof($yearScans) - 1; $i > 0; $i--) {
            $yearScansAvg += ($yearScans[$i]['created_at'] - $yearScans[$i - 1]['created_at']);
        }
        for ($i = sizeof($monthScans) - 1; $i > 0; $i--) {
            $monthScansAvg += ($monthScans[$i]['created_at'] - $monthScans[$i - 1]['created_at']);
        }
        for ($i = sizeof($weekScans) - 1; $i > 0; $i--) {
            $weekScansAvg += ($weekScans[$i]['created_at'] - $weekScans[$i - 1]['created_at']);
        }
        for ($i = sizeof($dayScans) - 1; $i > 0; $i--) {
            $dayScansAvg += ($dayScans[$i]['created_at'] - $dayScans[$i - 1]['created_at']);
        }
        if (count($dayScans) != 0)
            $dayScansAvg = intval(ceil(($dayScansAvg / count($dayScans)) / 60));
        if (count($weekScans) != 0)
            $weekScansAvg = intval(ceil(($weekScansAvg / count($weekScans)) / 60));
        if (count($monthScans) != 0)
            $monthScansAvg = intval(ceil(($monthScansAvg / count($monthScans)) / 60));
        if (count($yearScans) != 0)
            $yearScansAvg = intval(ceil(($yearScansAvg / count($yearScans)) / 60));

        // get les donnees stats
        // get info parrainage
        $lastMonthParrainages = Parrainage::where('status', true)->whereBetween('created_at', [now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth()])->count();
        $currentMonthParrainages = Parrainage::where('status', true)->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])->count();
        if ($lastMonthParrainages == 0) {
            $ratio = 100;
        } elseif ($currentMonthParrainages == 0) {
            $ratio = -100;
        } elseif ($currentMonthParrainages == $lastMonthParrainages) {
            $ratio = 0;
        } else {
            $ratio = round((($currentMonthParrainages / $lastMonthParrainages) - 1) * 100, 2);
        }
        $lastMonth = ucfirst(Carbon::parse(now()->subMonth())->translatedFormat('F Y'));
        $thisMonth = ucfirst(Carbon::parse(now())->translatedFormat('F Y'));
        $dtParrainage = [
            'last_month' => 'Mois dernier', 'taux' => $ratio, 'last_month_val' => $lastMonthParrainages,
            'this_month' => ucfirst(Carbon::parse(now())->translatedFormat('F Y')), 'this_month_val' => $currentMonthParrainages];
        //recup transactions de lannee transactions
        $lestransactions = $this->rpTrans->getAll();
        $allTransactions = collect($lestransactions);
        $lastTrans = $allTransactions->where('montant', '>', 0)->take(5);
        $dtTrans = [];
        foreach ($lastTrans as $lt) {
            $type = ($lt->type_transaction == 'first_buy') ? 'parrainage' : $lt->type_transaction;
            // if($type=='cashback'){}
            $dtTrans[] = ['date' => Carbon::parse($lt->created_at)->format('d-m-Y H:i:s'), 'type' => $lt->type_transaction, 'montant' => $lt->montant,
                'commission' => $lt->gain];

        }

        // on get les derniers abonnments

        $abons = $this->rpWallet->getCurrentMonthAbonnement();
        $lastAbonnements = count($abons);
        $lesAbonnements = collect($abons);
        $filtered_collectionAbon = $lesAbonnements->filter(function ($item) {
            return Carbon::parse($item->created_at)->format('d-m-Y') == date('d-m-Y');
        })->values();

        $abonJour = count($filtered_collectionAbon);

        // on get le valeur des achats du mois
        $totalAchat = Achat::whereBetween('created_at', [now()->startOfMonth(), now()])->sum('montant_facture');
        $totalCashback = Transaction::active()->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])->where('type_transaction', 'cashback')->sum('montant');
        $totalTopup = Transaction::active()->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])->where('type_transaction', 'topup')->sum('montant');
        $totalPayment = Transaction::active()->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])->where('type_transaction', 'paiement')->sum('montant');


        // On recupere nos commissions
        $totCommissions = $allTransactions->sum('gain');
        $jourComissions = $allTransactions->filter(function ($item) {
            return Carbon::parse($item->created_at)->format('d-m-Y') == date('d-m-Y');
        })->sum('gain');

        $moisComissions = $allTransactions->filter(function ($item) {
            return Carbon::parse($item->created_at)->format('m') == date('m');
        })->sum('gain');

        // on get le topup et cashback des 30 derniers jours
        $last30TransTop = $allTransactions->where('type_transaction', 'topup')->where('created_at', '>', now()->subDays(30)->endOfDay())->all();
        $last30TransCash = $allTransactions->where('type_transaction', 'cashback')->where('created_at', '>', now()->subDays(30)->endOfDay())->all();
        $last30TransPaymt = $allTransactions->where('type_transaction', 'paiement')->where('created_at', '>', now()->subDays(30)->endOfDay())->all();


        $tabTopLbl = [];
        $tabTopVal = [];
        $tabPaymtVal = [];
        $tabPaymtLbl = [];
        $tabCashLbl = [];
        $tabCashVal = [];
        $indice = 0;
        $dtPrec = $dtnext = '';
        $val = 0;
        foreach (collect($last30TransTop)->reverse() as $e) {
            if ($indice == 0) {
                $dtPrec = $dtnext = Carbon::parse($e->created_at)->format('d-m-Y');
                $val = $e->montant;
            } else {
                $dtnext = Carbon::parse($e->created_at)->format('d-m-Y');
                if ($dtPrec == $dtnext) { // meme jour on somme
                    $val += $e->montant;
                } else {
                    $tabTopLbl[] = "'" . $dtPrec . "'";
                    $tabTopVal[] = $val;
                    $dtPrec = $dtnext;
                    $val = $e->montant;
                }
            }
            $indice++;
        }
        // on insere la derniere ligne
        $tabTopLbl[] = "'" . $dtPrec . "'";
        $tabTopVal[] = $val;

        // les derniers cashback
        $indice = 0;
        $dtPrec = $dtnext = '';
        $val = 0;
        foreach (collect($last30TransCash)->reverse() as $e) {
            if ($indice == 0) {
                $dtPrec = $dtnext = Carbon::parse($e->created_at)->format('d-m-Y');
                $val = $e->montant;
            } else {
                $dtnext = Carbon::parse($e->created_at)->format('d-m-Y');
                if ($dtPrec == $dtnext) { // meme jour on somme
                    $val += $e->montant;
                } else {
                    $tabCashLbl[] = "'" . $dtPrec . "'";
                    $tabCashVal[] = $val;
                    $dtPrec = $dtnext;
                    $val = $e->montant;
                }
            }
            $indice++;
        }
        // on insere la derniere ligne
        $tabCashLbl[] = "'" . $dtPrec . "'";
        $tabCashVal[] = $val;

        // les derniers paiements
        $indice = 0;
        $dtPrec = $dtnext = '';
        $val = 0;
        foreach (collect($last30TransPaymt)->reverse() as $p) {
            if ($indice == 0) {
                $dtPrec = $dtnext = Carbon::parse($p->created_at)->format('d-m-Y');
                $val = $p->montant;
            } else {
                $dtnext = Carbon::parse($p->created_at)->format('d-m-Y');
                if ($dtPrec == $dtnext) { // meme jour on somme
                    $val += $p->montant;
                } else {
                    $tabPaymtLbl[] = "'" . $dtPrec . "'";
                    $tabPaymtVal[] = $val;
                    $dtPrec = $dtnext;
                    $val = $p->montant;
                }
            }
            $indice++;
        }
        // on insere la derniere ligne
        $tabPaymtLbl[] = "'" . $dtPrec . "'";
        $tabPaymtVal[] = $val;

        $tabTopLbl = implode(',', $tabTopLbl);
        $tabCashLbl = implode(',', $tabCashLbl);
        $tabPaymtLbl = implode(',', $tabPaymtLbl);

        // pour les commissions transactions
        $totComTrans1 = $totComTrans2 = 0;
        $totComTrans2 = $allTransactions->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])->sum('gain');
        $totComTrans1 = $allTransactions->whereBetween('created_at', [now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth()])->sum('gain');
        if ($totComTrans1 == 0) {
            $variance = 100;
        } elseif ($totComTrans2 == 0) {
            $variance = -100;
        } elseif ($totComTrans1 == $totComTrans2) {
            $variance = 0;
        } else {
            $variance = round((($totComTrans2 / $totComTrans1) - 1) * 100, 2);
        }

        $dtCommissionsTransactions = ['last_month' => $lastMonth, 'taux' => $variance, 'last_month_val' => $totComTrans1,
            'this_month' => $thisMonth, 'this_month_val' => $totComTrans2];

        // pour les nouveaux clients le type_transactions first_buy est notre repere
        $totComTrans1 = $totComTrans2 = 0;
        $totComTrans2 = $allTransactions->filter(function ($item) {
            return Carbon::parse($item->created_at)->format('m') == date('m') && $item->type_transaction == 'first_buy';
        })->count();
        $totComTrans1 = $allTransactions->filter(function ($item) {
            return (Carbon::parse($item->created_at)->format('m') == Carbon::parse(now()->subMonth())->format('m')) && $item->type_transaction == 'first_buy';
        })->count();

        if ($totComTrans1 > 0) {
            $variance = ($totComTrans1 != 0) ? round((($totComTrans2 / $totComTrans1) - 1) * 100, 2) : ($totComTrans2 == 0 ? 0 : 100);
        } else {
            $variance = 0;
        }

        $dtNewClient = ['last_month' => $lastMonth, 'taux' => round($variance, 2, 1), 'last_month_val' => $totComTrans1,
            'this_month' => $thisMonth, 'this_month_val' => $totComTrans2];

        // pour les cleints mensuels: ceux qui ont achete ce mois et le mois precedent
        $totComTrans1 = $totComTrans2 = 0;
        $totComTrans2 = $allTransactions->unique('wallet_id')->count();
        $totComTrans1 = $allTransactions->filter(function ($item) {
            // dd($item);
            return $item->created_at && $item->created_at->isBefore(now()->startOfMonth());
            
        })->unique('wallet_id')->count();

        if ($totComTrans1 > 0) {
            $variance = ($totComTrans1 != 0) ? round((($totComTrans2 / $totComTrans1) - 1) * 100, 2) : ($totComTrans2 == 0 ? 0 : 100);
        } else {
            $variance = 0;
        }

        $dtAllClient = [
            'last_month' => $lastMonth,
            'taux' => round($variance, 2, 1),
            'last_month_val' => $totComTrans1,
            'this_month' => $thisMonth,
            'this_month_val' => $totComTrans2
        ];

        // pour les cleints mensuels: ceux qui ont achete ce mois et le mois precedent
        $totComTrans1 = $totComTrans2 = 0;
        $totComTrans2 = $allTransactions->filter(function ($item) {
            return $item->created_at && $item->created_at->between(now()->startOfMonth(), now());
        })->unique('wallet_id')->count();
        $totComTrans1 = $allTransactions->filter(function ($item) {
            return $item->created_at && $item->created_at->between(now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth());
        })->unique('wallet_id')->count();

        if ($totComTrans1 > 0) {
            $variance = ($totComTrans1 != 0) ? round((($totComTrans2 / $totComTrans1) - 1) * 100, 2) : ($totComTrans2 == 0 ? 0 : 100);
        } else {
            $variance = 0;
        }

        $dtMensClient = [
            'last_month' => $lastMonth,
            'taux' => round($variance, 2, 1),
            'last_month_val' => $totComTrans1,
            'this_month' => $thisMonth,
            'this_month_val' => $totComTrans2
        ];

        // pour les topup moyens par client
        $totComTrans1 = $totComTrans2 = 0;
        $topupCurrentMonth = $allTransactions
            ->whereBetween('created_at', [now()->startOfMonth(), now()->endOfMonth()])
            ->where('type_transaction', 'topup');
        $topupLastMonth = $allTransactions
            ->whereBetween('created_at', [now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth()])
            ->where('type_transaction', 'topup');


        $topupCurrentMonthAvg = $topupCurrentMonth->countBy('wallet_id')->count() == 0 ? 0 : $topupCurrentMonth->sum('montant') / $topupCurrentMonth->countBy('wallet_id')->count();
        $topupLastMonthAvg = $topupLastMonth->countBy('wallet_id')->count() == 0 ? 0 : ($topupLastMonth->sum('montant') / $topupLastMonth->countBy('wallet_id')->count());

        if ($topupLastMonth->count() == 0) {
            $variance = 100;
        } elseif ($topupCurrentMonth->count() == 0) {
            $variance = -100;
        } elseif ($topupCurrentMonthAvg == $topupLastMonthAvg) {
            $variance = 0;
        } else {
            $variance = round((($topupCurrentMonthAvg / $topupLastMonthAvg) - 1) * 100, 2);

        }

        if ($topupLastMonth->count() == 0 && $topupCurrentMonth->count() == 0) {
            $variance = 0;
        }

        $dtAvgTopup = [
            'last_month' => $lastMonth,
            'taux' => $variance,
            'last_month_val' => $topupLastMonthAvg,
            'this_month' => $thisMonth,
            'this_month_val' => $topupCurrentMonthAvg
        ];

        $transactionsCurrentMonth = Achat::whereBetween('created_at', [now()->startOfMonth(), now()]);
        $transactionsLastMonth = Achat::whereBetween('created_at', [now()->subMonth()->startOfMonth(), now()->subMonth()->endOfMonth()]);

        $transactionsCurrentMonthAvg = $transactionsCurrentMonth->count() == 0 ? 0 : $transactionsCurrentMonth->sum('montant_facture') / $transactionsCurrentMonth->count();
        $transactionsLastMonthAvg = $transactionsLastMonth->count() == 0 ? 0 : $transactionsLastMonth->sum('montant_facture') / $transactionsLastMonth->count();

        if ($transactionsLastMonth->count() == 0) {
            $variance = 100;
        } elseif ($transactionsCurrentMonth->count() == 0) {
            $variance = -100;
        } elseif ($transactionsCurrentMonthAvg == $transactionsLastMonthAvg) {
            $variance = 0;
        } else {
            $variance = round((($transactionsCurrentMonthAvg / $transactionsLastMonthAvg) - 1) * 100, 2);
        }
        if ($transactionsLastMonth->count() == 0 && $transactionsCurrentMonth->count() == 0) {
            $variance = 0;
        }

        $dtAov = [
            'last_month' => $lastMonth,
            'taux' => $variance,
            'last_month_val' => $transactionsLastMonthAvg,
            'this_month' => $thisMonth,
            'this_month_val' => $transactionsCurrentMonthAvg
        ];

        $avgScans = [
            'dayScansAvg' => $dayScansAvg,
            'weekScansAvg' => $weekScansAvg,
            'monthScansAvg' => $monthScansAvg,
            'yearScansAvg' => $yearScansAvg
        ];

        return view(
            'backend.dashboard.super_admin',
            compact(
                'dtParrainage',
                'dtCommissionsTransactions',
                'dtMensClient',
                'dtAllClient',
                'dtNewClient',
                'dtTrans',
                'totalAchat',
                'totalTopup',
                'totalCashback',
                'totalPayment',
                'lastAbonnements',
                'abonJour',
                'totCommissions',
                'dtAvgTopup',
                'dtAov',
                'moisComissions',
                'jourComissions',
                'tabTopLbl',
                'tabTopVal',
                'tabCashLbl',
                'tabCashVal',
                'tabPaymtLbl',
                'tabPaymtVal',
                'avgScans',
                'techsoftBalance'
            )
        );
    }

}
