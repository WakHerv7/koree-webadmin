<?php

namespace App\Http\Controllers\Backend\PaymentServiceProvider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class PaymentServiceProviderController extends Controller
{
    /**
     * @var string $url
     */
    protected $url = '/api/payment/provider';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];

            $data = array_map(function($item) {
                $services = $item['id']['services'];
                $item_id = $item['id']['provider_id'];
                $item['id'] = $item_id;
                $inServices = array_filter($services, function($service) {
                    return $service['sens'] === 'IN';
                });
                $outServices = array_filter($services, function($service) {
                    return $service['sens'] === 'OUT';
                });
                $item['services'] = [
                    'in' => $inServices,
                    'out' => $outServices,
                ];
                return $item;
            }, $data);

            // dd($data);

            return view('backend.payment-service-provider.index', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->withError('Failed to fetch payment-service-provider data from the API');
        }
    }

    public function indexDeleted()
    {

        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });

            return view('backend.payment-service-provider.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = session('currentValue');
        // dd($data);
        return view('backend.payment-service-provider.show', compact('data'));
    }
}
