<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commerce;
use App\Models\Programme;
use App\Models\WalletClient;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\ProgrammeRepository;
use App\Repositories\Backend\V1\WalletRepository;
use App\Repositories\Backend\V1\WalletVendeurRepository;
use App\Repositories\Frontend\V1\VendeurRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class WalletController extends Controller
{
    /**
     * @var CommerceRepository
     */
    protected $rpCommerce;

    /**
     * @var VendeurRepository
     */
    protected $rpVendeur;

    /**
     * @var ProgrammeRepository
     */
    protected $rpProgramme;

    /**
     * @var WalletRepository
     */
    protected $rpWallet;

    /**
     * @var WalletVendeurRepository
     */
    protected $rpWalVendeur;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository      $rpcom, VendeurRepository $rpv, ProgrammeRepository $rpProg, WalletRepository $rpwa,
                         WalletVendeurRepository $walVe)
    {
        $this->middleware('permission:programme-list|programme-create|programme-edit|programme-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:programme-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:programme-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:programme-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpcom;
        $this->rpVendeur = $rpv;
        $this->rpProgramme = $rpProg;
        $this->rpWallet = $rpwa;
        $this->rpWalVendeur = $walVe;
    }

    /**
     * Display a listing commerce actif.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->rpWallet->getAll();
        Log::info($data);
        return view('backend.wallets.index', compact('data'));
    }

    /**
     * Display a listing commerce actif.
     *
     * @return \Illuminate\Http\Response
     */
    public function listeWalletClients(Request $request)
    {
        $data = $this->rpWallet->getAll();
        return view('backend.wallets.index', compact('data'));
    }

    /**
     * Display a listing commerce actif.
     *
     * @return \Illuminate\Http\Response
     */
    public function listeWalletVendeurs()
    {
        $data = $this->rpWalVendeur->getAll();
        return view('backend.wallets-vendeurs.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commerces = $this->rpCommerce->getAllFree();
        //dd($commerces);

        return view('backend.programmes.create', compact('commerces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'date_fin' => 'required',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $inputs = $request->all();
        $desc = (isset($request['description'])) ? $request['description'] : '';

        $donnees = ['libelle' => $inputs['libelle'], 'date_debut' => Carbon::createFromDate($inputs['date_debut']), 'status' => 1, 'is_cashback' => 1,
            'date_fin' => Carbon::createFromDate($inputs['date_fin']), 'description' => $desc, 'commerce_id' => $inputs['commerce_id'],
            'taux_commission' => $inputs['taux_commission'], 'bonus_wallet' => $inputs['bonus_wallet'], 'created_by' => Auth::user()->id];


        $com = Programme::create($donnees);

        return redirect()->route('programmes.index')
            ->with('success', 'programme créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->rpWallet->getById($id);
        return view('backend.wallets.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Programme::find($id);
        $commerces = Commerce::get();

        //    dd($data,$pays,$pays_id,$regions,$state_id,'villes','categories');
        return view('backend.programmes.edit', compact('data', 'commerces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'commerce_id' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $data = Programme::find($id);
        $data->libelle = $request->input('libelle');
        $data->date_debut = $request->input('date_debut');
        $data->description = $request->input('description');
        $data->date_fin = $request->input('date_fin');
        $data->commerce_id = $request->input('commerce_id');
        $data->taux_commission = $request->input('taux_commission');
        $data->bonus_wallet = $request->input('bonus_wallet');
        $data->status = 1;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect()->route('programmes.index')
            ->with('success', 'programme mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WalletClient::find($id)->delete();

        return redirect()->route('programmes.index')
            ->with('success', 'programme supprimé');
    }
}
