<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commerce;
use App\Models\Pays;
use App\Models\PointVente;
use App\Models\Quartier;
use App\Models\Ville;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\PointVenteRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class PointVenteController extends Controller
{
    protected $rpCommerce;
    protected $rpPointvente;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CommerceRepository $rpCom, PointVenteRepository $pv)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpCom;
        $this->rpPointvente = $pv;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->rpPointvente->getAll();
        return view('backend.pointventes.index', compact('data'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $commerces = Commerce::get();
        $countries = Pays::all();
        return view('backend.pointventes.create', compact('commerces', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'nom_responsable' => 'required',
            'tel_responsable' => 'required',
            'commerce_id' => 'required|numeric',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $inputs = $request->all();
        $desc = (isset($request['adresse'])) ? $request['adresse'] : '';
        PointVente::create([
            'code' => $inputs['code'],
            'nom' => $inputs['nom'],
            'adresse' => $desc,
            'is_global' => $inputs['is_global'],
            'commerce_id' => $inputs['commerce_id'],
            'district_id' => $inputs['district'],
            'city_id' => $inputs['city'],
            'nom_responsable' => $inputs['nom_responsable'],
            'tel_responsable' => $inputs['tel_responsable'],
            'localisation' => $inputs['localisation'],
            'horaires' => $inputs['horaires'],
        ]);

        return redirect()->route('points-ventes.index')
            ->with('success', 'point de vente créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $data = $this->rpPointvente->getById($id);
        return view('backend.pointventes.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $commerces = Commerce::get();
        $data = $this->rpPointvente->getById($id);
        $countries = Pays::all();
        $cities = Ville::all();
        $districts = Quartier::all();

        return view('backend.pointventes.edit', compact('data', 'commerces', 'countries', 'cities', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'commerce_id' => 'required|numeric',
            'tel_responsable' => 'required',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $data = PointVente::find($id);
        $data->nom = $request->input('nom');
        $data->code = $request->input('code');
        $data->is_global = $request->input('is_global');
        $data->adresse = $request->input('adresse');
        $data->commerce_id = $request->input('commerce_id');
        $data->nom_responsable = $request->input('nom_responsable');
        $data->tel_responsable = $request->input('tel_responsable');
        $data->city_id = $request->input('city');
        $data->district_id = $request->input('district');
        $data->localisation = $request->input('localisation');
        $data->horaires = $request->input('horaires');

        $data->save();

        return redirect()->route('points-ventes.index')
            ->with('success', 'Point de vente mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        PointVente::find($id)->delete();

        return redirect()->route('points-ventes.index')
            ->with('success', 'Point de vente supprimé');
    }
}
