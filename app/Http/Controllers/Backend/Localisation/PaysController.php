<?php

namespace App\Http\Controllers\Backend\Localisation;

use App\Http\Controllers\Controller;
use App\Models\Pays;

class PaysController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('permission:pays-list|pays-create|pays-edit|pays-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:pays-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pays-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pays-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pays::orderBy('id', 'DESC')->get();

        return view('backend.localisation.pays.index', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pays::find($id);

        return view('backend.localisation.pays.show', compact('data'));
    }
}
