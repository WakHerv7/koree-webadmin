<?php

namespace App\Http\Controllers\Backend\Localisation;

use App\Http\Controllers\Controller;
use App\Models\Pays;
use App\Models\Region;
use App\Models\Ville;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VillesController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('permission:pays-list|pays-create|pays-edit|pays-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:pays-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pays-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pays-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data = Ville::withTrashed()
            ->when($request->country_id, function ($q, $country_id) {
                return $q->where('country_id', $country_id);
            })
            ->with('pays')->with('region')->orderBy('name', 'DESC')->get();

        return view('backend.localisation.ville.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $pays = Pays::all();
        $states = Region::all();

        return view('backend.localisation.ville.create', compact('pays', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'pays' => 'required',
            'state' => 'required',
            'latitude' => 'sometimes|nullable|numeric',
            'longitude' => 'sometimes|nullable|numeric',
        ]);

        try {
            $ville = new Ville;
            $state = Region::where('id', $request->state)->first();
            $pays = Pays::where('id', $request->pays)->first();
            $ville->name = $request->name;
            $ville->state_code = $state->fips_code;
            $ville->state_id = $state->id;
            $ville->country_id = $pays->id;
            $ville->country_code = $pays->iso2;
            $ville->latitude = $request->latitude;
            $ville->longitude = $request->longitude;

            $ville->save();
            return redirect()->route('villes.show', $ville->id)
                ->with('success', 'Ville créée avec succès');
        } catch (\Exception $ex) {
            Log::error("Erreur survenue: " . $ex->getMessage());
            return redirect()->back()
                ->withInput($request->input())
                ->with('error', 'une erreur est survenue');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $city = Ville::with('pays')->with('region')->find($id);

        return view('backend.localisation.ville.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $pays = Pays::all();
        $states = Region::all();
        $ville = Ville::find($id);

        return view('backend.localisation.ville.edit', compact('ville', 'pays', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'pays' => 'required',
            'state' => 'required',
            'latitude' => 'sometimes|nullable|numeric',
            'longitude' => 'sometimes|nullable|numeric',
        ]);

        $ville = Ville::find($id);

        if ($ville) {

            try {
                $ville->name = $request->name;
                $state = Region::where('id', $request->state)->first();
                $pays = Pays::where('id', $request->pays)->first();
                $ville->state_code = $state->fips_code;
                $ville->state_id = $state->id;
                $ville->country_id = $pays->id;
                $ville->country_code = $pays->iso2;
                $ville->latitude = $request->latitude;
                $ville->longitude = $request->longitude;

                $ville->save();
                return redirect()->route('villes.show', $id)
                    ->with('success', 'Quartier mis à jour avec succès.');
            } catch (\Exception $ex) {
                Log::error("Erreur survenue: " . $ex->getMessage());
                return redirect()->back()
                    ->withInput($request->input())
                    ->with('error', 'une erreur est survenue');
            }
        }

        return redirect()->route('villes.index')
            ->with('error', 'une erreur est survenue');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Ville::find($id)->delete();

        return redirect()->route('villes.index')
            ->with('success', 'Ville supprimée');
    }
}
