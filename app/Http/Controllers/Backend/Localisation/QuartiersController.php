<?php

namespace App\Http\Controllers\Backend\Localisation;

use App\Http\Controllers\Controller;
use App\Models\Pays;
use App\Models\Quartier;
use App\Models\Ville;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class QuartiersController extends Controller
{
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct()
    {
        $this->middleware('permission:pays-list|pays-create|pays-edit|pays-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:pays-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pays-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pays-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data = Quartier::withTrashed()
            ->when($request->ville_id, function ($q, $ville_id) {
                return $q->where('city_id', $ville_id);
            })->with('ville')->with('pays')->orderBy('name', 'DESC')->get();

        return view('backend.localisation.quartier.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permission = Permission::get();

        return view('backend.roles.create', compact('permission'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $quartier = Quartier::find($id);

        return view('backend.localisation.quartier.show', compact('quartier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $quartier = Quartier::with('pays')->with('ville')->find($id);
        $villes = Ville::all();
        $pays = Pays::all();
        return view('backend.localisation.quartier.edit', compact('quartier', 'villes', 'pays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'latitude' => 'sometimes|nullable|numeric',
            'longitude' => 'sometimes|nullable|numeric',
            'ville' => 'required',
            'pays' => 'required',
        ]);

        $quartier = Quartier::find($id);

        if ($quartier) {

            try {
                $quartier->name = $request->name;
                $quartier->latitude = $request->latitude;
                $quartier->longitude = $request->longitude;
                $quartier->country_id = $request->pays;
                $quartier->city_id = $request->ville;

                $quartier->save();
                return redirect()->route('quartiers.show', $id)
                    ->with('success', 'Quartier mis à jour avec succès.');
            } catch (\Exception $ex) {
                Log::error("Erreur survenue: " . $ex->getMessage());
                return redirect()->back()
                    ->withInput($request->input())
                    ->with('error', 'une erreur est survenue');
            }
        }

        return redirect()->route('quartiers.index')
            ->with('error', 'une erreur est survenue');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Quartier::find($id)->delete();

        return redirect()->route('quartiers.index')
            ->with('success', 'Quartier supprimé');
    }
}
