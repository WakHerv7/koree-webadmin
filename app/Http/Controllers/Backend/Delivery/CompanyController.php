<?php

namespace App\Http\Controllers\Backend\Delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
// use Tymon\JWTAuth\Facades\JWTAuth;

class CompanyController extends Controller
{
    protected $url = '/api/delivery/company';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->middleware('checkStatus');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'items' => 100
                ]
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return $object['is_active'];
            });
            return view('backend.delivery.company.index', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    public function indexDeleted()
    {
        
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });
            
            return view('backend.delivery.company.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }

    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = session('currentValue');

        // dd($data);

        return view('backend.delivery.company.show', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.delivery.company.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'company_id' => $id,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'][0];
            return view('backend.delivery.company.edit', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('DELETE', env('API_URL').$this->url.'/'.$id, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            
            return redirect()->route('delivery.company.index')
            ->with('success', 'Suppression réussie.');

            // return view('backend.delivery.company.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->withError('Failed to fetch company data from the API');
        }
    }



}
