<?php

namespace App\Http\Controllers\Backend\Delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class PaymentMethodController extends Controller
{
    protected $url = '/api/delivery/payment-method';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->middleware('checkStatus');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return $object['is_active'];
            });
            return view('backend.delivery.payment_method.index', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    public function indexDeleted()
    {
        
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });
            
            return view('backend.delivery.payment_method.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = session('currentValue');
        return view('backend.delivery.payment_method.show', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.delivery.payment_method.create');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'payment_method_id' => $id,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'][0];
            return view('backend.delivery.payment_method.edit', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }
}
