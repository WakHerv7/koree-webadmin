<?php

namespace App\Http\Controllers\Backend\Delivery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class OrderController extends Controller
{
    protected $url = '/api/delivery/order';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->middleware('checkStatus');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = new Client();
        $token = session('accessToken');
        $response = $client->request('GET', env('API_URL').$this->url, [
            'headers' => [
                'Authorization' => 'Token '.$token,
                'Accept' => 'application/json',
            ],
        ]);

        if ($response->getStatusCode() == 200) {
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];
            return view('backend.delivery.order.index', compact('data'));
        } else {
            return back()->withError('Failed to fetch user data from the API');
        }
    }
}
