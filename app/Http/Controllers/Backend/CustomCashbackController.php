<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commerce;
use App\Models\CustomCashback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomCashbackController extends Controller
{
    public function index(Request $request, int $id)
    {
        $commerce = Commerce::with(['customCashbacks', 'vendeur.user'])->find($id);
        if ($commerce == null) {
            session()->flash('error', "Commerce non trouvé");
            return back();
        }
        return view("backend.commerces.cashbacks.index", compact('commerce'));
    }

    public function create(Request $request, int $id)
    {
        $commerce = Commerce::with('customCashbacks')->find($id);
        if ($commerce == null) {
            session()->flash('error', "Commerce non trouvé");
            return back();
        }
        return view("backend.commerces.cashbacks.create", compact('commerce'));

    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'commission' => 'required',
            'vip_commission' => 'required',
            'commerce' => 'required|exists:commerces,id',
        ], [
            'required' => "Le champ :attribute est obligatoire"
        ]);

        if ($validator->fails()) {
            return back()->withInput($request->input())->withErrors($validator->errors());
        }

        $cashback = CustomCashback::where('commerce_id', $request->get('commerce'))->where('max_amount', $request->get('amount'))->exists();

        if ($cashback) {
            return back()->withErrors(["Il ya deja une règle avec ce montant, veuillez changer de montant"])->withInput($request->input());
        }

        $nCashback = new CustomCashback;
        $nCashback->max_amount = $request->get('amount');
        $nCashback->cashback_pct = $request->get('commission');
        $nCashback->vip_cashback_pct = $request->get('vip_commission');
        $nCashback->active = true;
        $nCashback->commerce_id = $request->get('commerce');
        $nCashback->save();

        session()->flash("success", "Règle ajoutée");
        return redirect()->route("custom-cashback.index", ['id' => $request->get('commerce')]);

    }

    public function edit(Request $request, int $c, int $r)
    {
        $cashback = CustomCashback::with('commerce')->where('commerce_id', $c)->where('id', $r)->first();
        if ($cashback->exists()) {
            return view("backend.commerces.cashbacks.edit", compact('cashback'));
        }
        session()->flash('error', "Commerce et/ou règle non trouvé");
        return back();
    }

    public function lock(Request $request, int $c, int $r)
    {
        $cashback = CustomCashback::where('commerce_id', $c)->where('id', $r)->first();
        if ($cashback) {
            $cashback->active = false;
            $cashback->save();
            session()->flash('success', "Règle désactivée");
            return back();
        }

        session()->flash('error', "Commerce et/ou règle non trouvé");
        return back();

    }

    public function unlock(Request $request, int $c, int $r)
    {
        $cashback = CustomCashback::where('commerce_id', $c)->where('id', $r)->first();
        if ($cashback) {
            $cashback->active = true;
            $cashback->save();
            session()->flash('success', "Règle activée");
            return back();
        }

        session()->flash('error', "Commerce et/ou règle non trouvé");
        return back();
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'commission' => 'required',
            'vip_commission' => 'required',
            'commerce' => 'required|exists:commerces,id',
            'rule' => 'required|exists:custom_cashbacks,id',
        ], [
            'required' => "Le champ :attribute est obligatoire"
        ]);

        if ($validator->fails()) {
            return back()->withInput($request->input())->withErrors($validator->errors());
        }

        $cashback = CustomCashback::where('commerce_id', $request->get('commerce'))->where('id', '<>', $request->get('rule'))->where('max_amount', $request->get('amount'))->exists();

        if ($cashback) {
            return back()->withErrors(["Il ya deja une règle avec ce montant, veuillez changer de montant"])->withInput($request->input());
        }

        $rule = CustomCashback::find($request->get('rule'));
        $rule->max_amount = $request->get('amount');
        $rule->cashback_pct = $request->get('commission');
        $rule->vip_cashback_pct = $request->get('vip_commission');
        $rule->save();

        session()->flash("success", "Règle modifiée");
        return redirect()->route("custom-cashback.index", ['id' => $request->get('commerce')]);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rule' => 'required|exists:custom_cashbacks,id',
            'commerce' => 'required|exists:commerces,id',
        ], [
            'required' => "Le champ :attribute est requis",
            'exists' => "Le champ :attribute est incorrect",
        ]);
        if ($validator->fails()) {
            session()->flash('error', $validator->errors()->first());
            return back();
        }
        $cashback = CustomCashback::where('commerce_id', $request->get('commerce'))->where('id', $request->get('rule'))->first();
        if ($cashback) {
            $cashback->delete();
            session()->flash('success', "Règle supprimée avec succès");
            return back();
        }

        session()->flash('error', "Commerce et/ou règle non trouvé");
        return back();
    }
}
