<?php

namespace App\Http\Controllers\Backend;

use App\Events\Pusher\ClientDisabled;
use App\Events\Pusher\ClientLock;
use App\Events\Pusher\ClientUpdated;
use App\Helpers\PhoneNumberFormatter;
use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\DeletionRequest;
use App\Models\PackageBoost;
use App\Models\Pays;
use App\Models\Quartier;
use App\Models\Region;
use App\Models\Ville;
use App\Models\User;
use App\Notifications\ClientUnlockedNotification;
use App\Repositories\Frontend\V1\ClientRepository;
use App\Repositories\UsersRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    protected $rpClient;
    protected $rpUser;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(UsersRepository $rpU, ClientRepository $rpCli)
    {
//        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
//        $this->middleware('permission:role-create', ['only' => ['create','store']]);
//        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->rpClient = $rpCli;
        $this->rpUser = $rpU;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data = [];

        return view('backend.clients.index', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function datatable(Request $request)
    {
        return $dataArr = $this->rpClient->getClientsDatatable($request, $request->filled('deleted'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $pays = Pays::get();

        return view('backend.clients.create', compact('pays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'prix' => 'required|numeric',
            'duree' => 'required|numeric',
            'country_id' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->route('package.index')
                ->with('error', $mes);
        }

        $inputs = $request->all();
        $desc = (isset($request['description'])) ? $request['description'] : '';

        $role = PackageBoost::create(['nom' => $inputs['nom'], 'prix' => $inputs['prix'],
            'duree' => $inputs['duree'], 'description' => $desc, 'country_id' => $inputs['country_id']]);

        return redirect()->route('package.index')
            ->with('success', 'package boost créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $data = $this->rpClient->getClient($id);

        return view('backend.clients.show', compact('data'));
    }

    public function mvmt($id)
    {
        $client = Client::findOrFail($id);
        $variations = $this->rpClient->getInfluencerVariations($client->id);
        return view("backend.clients.variations", compact('variations', 'client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $data = Client::with(['pays', 'ville', 'quartier', 'user'])->find($id);
        $states = Region::where('country_id', $data->country_id)->orderBy('name')->get();
        $cities = Ville::orderBy('name')->where('country_id', $data->country_id)->get();
        $districts = Quartier::orderBy('name');
        if ($data->city_id) {
            $districts = $districts->where('city_id', $data->city_id);
        } else {
            $districts = $districts->where('country_id', $data->country_id);
        }
        $districts = $districts->get();
        $countries = Pays::orderBy('name')->get();
        return view('backend.clients.edit', compact('data', 'countries', 'states', 'cities', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $country = Pays::where('id', '=', $request->input('country'))->firstOrFail();
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('error', 'Impossible de créer le commerce : Pays introuvable !');
        }

        $validator = Validator::make($request->all(), [
            'lastname' => 'required',
            'firstname' => 'required',
            'parrainage' => 'required',
            'sex' => 'required',
            'date' => 'nullable',
            'email' => 'nullable|email',
            'phone' => 'required|numeric',
            'username' => 'required',
            'influencer' => 'required|in:yes,no',
            'country' => 'required|numeric',
            'city' => 'required|numeric',
            'district' => 'nullable|numeric',
            'referee_amount' => 'required|numeric',
            'referrer_amount' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->withInput($request->input())->with('error', $mes);
        }

        $client = Client::with('user')->find($id);
        $user = $client->user;

        if ($client && $user) {
            try {
                $old = $client->influencer;
                $client->nom = $request->lastname;
                $client->prenom = $request->firstname;
                $client->code_parrainage = $request->parrainage;
                $client->sexe = $request->sex;
                if ($request->date) {
                    $client->date_naissance = Carbon::createFromFormat('m-d-Y', $request->date);
                }
                $client->country_id = $request->country;
                $client->city_id = $request->city;
                $client->quartier_id = $request->district;
                $client->referrer_amount = $request->referrer_amount;
                $client->referee_amount = $request->referee_amount;
                $client->influencer = $request->influencer == 'yes';
                $client->save();

                if ($client->influencer != $old) {
                    DB::table('influencer_changes')->insert([
                        'client_id' => $client->id,
                        'admin_id' => auth()->id(),
                        'from' => $client->influencer ? 'client' : 'influencer',
                        'to' => $client->influencer ? 'influencer' : 'client',
                        'date' => now()
                    ]);
                }

                $user->telephone = PhoneNumberFormatter::format($request->phone, $country->phonecode);
                if ($request->email) {
                    $user->email = $request->email;
                }
                $user->username = $request->username;

                $user->save();

                event(new ClientUpdated($client));

                $user->tokens()->where('name', $user->telephone)->delete();

                return redirect()->route('clients.show', $client->id)->with('success', "Informations mis à jour!");

            } catch (\Exception $exception) {
                Log::error("Erreur survenue, ligne: " . $exception->getLine() . " | Fichier: " . $exception->getFile() . " | Message: " . $exception->getMessage());
                return back()->withInput($request->input())->with('error', "Erreur l'hors de la mise à jour !");
            }

        }

        return redirect()->route('clients.index')->with('error', "Client non trouvé !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {

        Client::find($id)->delete();

        return redirect()->route('clients.index')
            ->with('success', 'Client désactivé');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function changeStatut($id, Request $rq)
    {
        /** @var Client $client */
        $client = Client::find($id);
        $client->status = $rq->statut;
        $client->updated_by = Auth::user()?->id;
        $client->save();
        event(new ClientUpdated($client));
        if ($client->status == 0) {
            $user = User::find($client->user_id);
            if ( $user ) {
                $user->tokens()->where('name', $user->telephone)->delete();
            }
            event(new ClientDisabled($client));
//            $client->user->tokens()->delete();
        }
        return redirect()->route('clients.show', $client->id)
            ->with('success', 'Statut Client modifié');
//        return redirect()->route('clients.index')
//            ->with('success', 'Statut Client modifié');
    }

    public function toggleStatut(int $id)
    {

        try {

            $data = Client::with('user')->find($id);

            $data->status = !$data->status;
            $data->save();
            if ($data->status == 0) {
                $data->user->tokens()->delete();
                event(new ClientLock($data));
            } else {
                try {
                    Notification::locale('fr')
                        ->sendNow(
                            $data->user,
                            new ClientUnlockedNotification()
                        );
                } catch (Exception $e) {
                    Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
                }
            }
            event(new ClientUpdated($data));
            $status = $data->status ? "débloqué" : "bloqué";
            return back()->with('success', "Compte $status avec succès !");

        } catch (\Exception $exception) {
            Log::error("Erreur survenue, ligne: " . $exception->getLine() . " | Fichier: " . $exception->getFile() . " | Message: " . $exception->getMessage());
            return back()->with('error', "Une erreur est survenue !!!");
        }
    }

    public function toggleInfluencer(int $id)
    {

        try {

            $data = Client::find($id);

            $data->influencer = !$data->influencer;
            $data->save();
            event(new ClientUpdated($data));
            $status = $data->status ? "transformé en influenceur" : "rétablit à la normale";
            return back()->with('success', "Compte $status");

        } catch (\Exception $exception) {
            Log::error("Erreur survenue, ligne: " . $exception->getLine() . " | Fichier: " . $exception->getFile() . " | Message: " . $exception->getMessage());
            return back()->with('error', "Une erreur est survenue !!!");
        }
    }

    public function listeClientsSelect2(Request $request)
    {
        return response()->json($this->rpClient->getClientSelect2($request), Response::HTTP_OK);
    }


    public function deletionRequests()
    {
        $data = DeletionRequest::with(['client'])->whereHas('client')->orderBy('created_at')->get();
        return view('backend.clients.deletions', compact('data'));
    }

    public function acceptDeletionRequests(Request $request, DeletionRequest $deletionRequest)
    {
        try {
            $client = $deletionRequest->client();
            $client->status = false;
            $client->save();
            $deletionRequest->processed = true;
            $deletionRequest->processed_by = $request->user()->id;
            $deletionRequest->save();
            return redirect()->back()->with('success', 'Request processed successfully');
        } catch (\Exception $exception) {
            Log::error("Erreur traitement requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur traitement requete.');
        }
    }

    public function cancelDeletionRequests(Request $request, DeletionRequest $deletionRequest)
    {
        try {
            $client = $deletionRequest->client;
            $client->status = true;
            $client->save();
            $deletionRequest->processed = true;
            $deletionRequest->processed_by = $request->user()->id;
            $deletionRequest->save();
            return redirect()->back()->with('success', 'Request processed successfully');
        } catch (\Exception $exception) {
            Log::error("Erreur traitement requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur traitement requete.');
        }
    }

    public function resetDeletionRequests(DeletionRequest $deletionRequest)
    {
        try {
            $client = $deletionRequest->client;
            $client->status = true;
            $client->save();
            $deletionRequest->processed = false;
            $deletionRequest->save();
            return redirect()->back()->with('success', 'Request reset successfully');
        } catch (\Exception $exception) {
            Log::error("Erreur reinitialisation requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur reinitialisation requete.');
        }
    }

}
