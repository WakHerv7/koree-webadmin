<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Pays;
use App\Repositories\Backend\V1\BoostRepository;
use App\Repositories\Backend\V1\CommerceRepository;
use DB;
use Illuminate\Http\Request;
use App\Models\PackageBoost;
use App\Repositories\Backend\V1\PackageRepository;
use Illuminate\Support\Facades\Validator;

class PackageController extends Controller
{
    protected $rpPackage;
    protected  $rpCommerce;
    protected  $rpPays;
    protected $rpBoost;
    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(PackageRepository $rpPack, CommerceRepository $rpCom, BoostRepository $bst)
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->rpPackage= $rpPack;
        $this->rpCommerce= $rpCom;
        $this->rpBoost= $bst;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->rpPackage->getAll();
        return view('backend.packages.index', compact('data'));
    }

    /**
     * Display a listing of boost commerce
     *
     * @return \Illuminate\Http\Response
     */
    public function listCommerceBoostes()
    {
        $data = $this->rpBoost->getAllBoostCommerces();

        return view('backend.packages.liste-boost', compact('data'));
    }

    /**
     * Display a listing of boost commerce
     *
     * @return \Illuminate\Http\Response
     */
    public function historiqueCommerceBoostes()
    {
        $data = $this->rpBoost->getHistoriqueBoostCommerces();
        return view('backend.packages.histo-boost', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pays = Pays::get();

        return view('backend.packages.create', compact('pays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'prix' => 'required|numeric',
            'duree' => 'required|numeric',
            'country_id' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes="";
            $errors = $validator->errors();
            foreach ($errors->all() as $er){
                $mes.= $er.' ';
            }
            return redirect()->route('package.index')
                ->with('error', $mes);
        }

        $inputs= $request->all();
        $desc= (isset($request['description']))?$request['description']:'';

        $role = PackageBoost::create(['nom' => $inputs['nom'],'prix' => $inputs['prix'],
            'duree' => $inputs['duree'],'description' => $desc,'country_id' => $inputs['country_id']]);

        return redirect()->route('package.index')
            ->with('success', 'package boost créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->rpPackage->getForId($id);
            //dd($data);
        return view('backend.packages.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pays = Pays::get();
        $data = PackageBoost::with('pays')->find($id);

        return view('backend.packages.edit', compact('data','pays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'prix' => 'required|numeric',
            'duree' => 'required|numeric',
            'country_id' => 'required|numeric',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes="";
            $errors = $validator->errors();
            foreach ($errors->all() as $er){
                $mes.= $er.' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $data = PackageBoost::find($id);
        $data->nom = $request->input('nom');
        $data->prix = $request->input('prix');
        $data->duree = $request->input('duree');
        $data->description = $request->input('description');
        $data->country_id = $request->input('country_id');
        $data->save();

        return redirect()->route('package.index')
            ->with('success', 'Package de boost mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PackageBoost::find($id)->delete();

        return redirect()->route('package.index')
            ->with('success', 'Package supprimé');
    }
}
