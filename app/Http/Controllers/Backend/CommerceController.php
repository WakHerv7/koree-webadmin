<?php

namespace App\Http\Controllers\Backend;

use Kreait\Firebase\Factory as FirebaseFactory;
use App\Events\Pusher\VendorUpdated;
use App\Helpers\PhoneNumberFormatter;
use App\Http\Controllers\Controller;
use App\Models\CategoryCommerce;
use App\Models\Commerce;
use App\Models\DeletionRequest;
use App\Models\Pays;
use App\Models\PointVente;
use App\Models\Programme;
use App\Models\Quartier;
use App\Models\Region;
use App\Models\ShopConfiguration;
use App\Models\ShopSuggestion;
use App\Models\User;
use App\Models\Vendeur;
use App\Models\Ville;
use App\Models\WalletVendeur;
use App\Repositories\Backend\V1\CommerceRepository;
use App\Repositories\Backend\V1\NoteRepository;
use App\Repositories\Frontend\V1\VendeurRepository;

use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Http;

class CommerceController extends Controller
{
    protected $rpCommerce;
    protected $rpVendeur;
    protected $rpNote;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(
        CommerceRepository $rpcom,
        VendeurRepository $rpv,
        NoteRepository $nt,
        )
    {
        $this->middleware('permission:commerce-list|commerce-create|commerce-edit|commerce-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:commerce-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:commerce-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:commerce-delete', ['only' => ['destroy']]);

        $this->rpCommerce = $rpcom;
        $this->rpVendeur = $rpv;
        $this->rpNote = $nt;
    }

    /**
     * Display a listing commerce actif.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $is_actif = true;
        $data = $this->rpCommerce->getAllActif();
        // dd($data);
        return view('backend.commerces.index', compact('data', 'is_actif'));
    }

    public function getInactif(){
        $is_actif = false;
        $data = $this->rpCommerce->getAllInacActif();
        // dd($data);
        return view('backend.commerces.index', compact('data', 'is_actif'));
    }

    public function deletionRequests()
    {
        $data = DeletionRequest::with(['commerce.categorie', 'vendor'])->whereHas('vendor')->orderBy('created_at')->get();
        return view('backend.commerces.deletions', compact('data'));
    }

    public function acceptDeletionRequests(Request $request, DeletionRequest $deletionRequest)
    {
        try {
            if ($deletionRequest->vendor) {
                $vendor = $deletionRequest->ve;
                $vendor->status = false;
                $vendor->save();
                $deletionRequest->processed = true;
                $deletionRequest->processed_by = $request->user()->id;
                $deletionRequest->save();
                return redirect()->back()->with('success', 'Request processed successfully');
            }
        } catch (\Exception $exception) {
            Log::error("Erreur traitement requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur traitement requete.');
        }
    }

    public function cancelDeletionRequests(Request $request, DeletionRequest $deletionRequest)
    {
        try {
            if ($deletionRequest->vendor) {
                $vendor = $deletionRequest->vendor;
                $vendor->status = true;
                $vendor->save();
                $deletionRequest->processed = true;
                $deletionRequest->processed_by = $request->user()->id;
                $deletionRequest->save();
            }


            return redirect()->back()->with('success', 'Request processed successfully');
        } catch (\Exception $exception) {
            Log::error("Erreur traitement requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur traitement requete.');
        }
    }

    public function resetDeletionRequests(DeletionRequest $id)
    {
        try {
            $vendor = $id->vendor;
            $vendor->status = true;
            $vendor->save();
            $id->processed = false;
            $id->save();
            return redirect()->back()->with('success', 'Request reset successfully');
        } catch (\Exception $exception) {
            Log::error("Erreur reinitialisation requete: " . $exception->getMessage());
            return redirect()->back()->with('error', 'Erreur reinitialisation requete.');
        }
    }

    public function notationCommerce()
    {
        $data = $this->rpNote->getAllDetails();
        //dd($data);
        return view('backend.commerces.histo_notation', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $pays = Pays::get();
        $vendeur = $this->rpVendeur->getAllFree();
        $categories = CategoryCommerce::get();
        // dd($vendeur);

        return view('backend.commerces.create', compact('pays', 'vendeur', 'categories'));
    }

    public function createFirebaseUser($phone){
        $serviceAccount = [
            'type' => 'service_account',
            'project_id' => env('FIREBASE_PROJECT_ID'),
            'private_key_id' => env('FIREBASE_PRIVATE_KEY_ID'),
            'private_key' => Str::replace('\n', "\n", env('FIREBASE_PRIVATE_KEY')),
            'client_email' => env('FIREBASE_CLIENT_EMAIL'),
            'client_id' => env('FIREBASE_CLIENT_ID'),
            'auth_uri' => env('FIREBASE_AUTH_URI'),
            'token_uri' => env('FIREBASE_TOKEN_URI'),
            'auth_provider_x509_cert_url' => env('FIREBASE_AUTH_PROVIDER_CERT_URL'),
            'client_x509_cert_url' => env('FIREBASE_CLIENT_CERT_URL'),
        ];

        $factory      = (new FirebaseFactory)->withServiceAccount($serviceAccount);
        $firebaseAuth = $factory->createAuth();

        try{
            $userProperties = [
                'phoneNumber' => $phone
            ];

            $firebaseUser = $firebaseAuth->createUser($userProperties);
            return $firebaseUser->uid;
        }catch(Exception $e){
            return response()->json(['message' => 'An error occurred while migration: ' . $e->getMessage()], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $country = Pays::where('id', '=', $request->input('country_id'))->firstOrFail();
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('error', 'Impossible de créer le commerce : Pays introuvable !');
        }

        $telephone_validators = ['required', 'numeric', 'unique:users'];
        if ($country->phonelength) {
            $telephone_validators[] = 'digits:' . $country->phonelength;
        }

        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'city_id' => 'required|numeric',
            'card' => 'nullable|file|max:1024|image',
            'background' => 'nullable|file|max:1024|image',
            'telephone' => $telephone_validators
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->route('commerces.index')
                ->with('error', $mes);
        }

        $inputs = $request->all();

        try {

            $phone =  PhoneNumberFormatter::format($inputs['telephone'], $country->phonecode);
            //create firebase user
             try {
                if (Str::startsWith($phone, '00')) {
                    $phoneNumber = str_replace("00", "+", $phone);
                } else {
                    $phoneNumber = $phone;
                }
               // $firebase_uid = $firebaseUser->uid;
                $firebase_uid  =  $this->createFirebaseUser($phoneNumber);
            } catch(Exception $e){
                Log::error("Erreur creation firebase user" . $e->getMessage());
                return redirect()->back()->withInput($request->input())->with('error', 'erreur création merge.');
            }

            DB::beginTransaction();

            $user = User::create([
                'username' => $inputs['nom'],
                'email' => (isset($inputs['email'])) ? $inputs['email'] : '',
                'telephone' => PhoneNumberFormatter::format($inputs['telephone'], $country->phonecode),
                'password' => bcrypt($inputs['password']),
                'type_user' => 'vendeur',
                'uuid' => Str::uuid()->toString(),
                'created_by' => Auth::user()->id,
                'firebase_uid'=> $firebase_uid,
            ]);

            $vendeur = Vendeur::create([
                'nom' => $inputs['nom'],
                'prenom' => (isset($inputs['prenom'])) ? $inputs['prenom'] : '',
                'adresse' => (isset($inputs['adresse'])) ? $inputs['adresse'] : '',
                'rccm' => (isset($inputs['rccm'])) ? $inputs['rccm'] : '',
                'code_transaction' => (isset($inputs['code_transaction'])) ? $inputs['code_transaction'] : '',
                'code_postal' => (isset($inputs['code_postal'])) ? $inputs['code_postal'] : '',
                'telephone1' => (isset($inputs['telephone1'])) ? PhoneNumberFormatter::format($inputs['telephone1'], $country->phonecode) : '',
                'telephone2' => (isset($inputs['telephone2'])) ? PhoneNumberFormatter::format($inputs['telephone2'], $country->phonecode) : '',
                'horaires' => (isset($inputs['horaires'])) ? $inputs['horaires'] : '',
                'contact_compte' => (isset($inputs['contact_compte'])) ? $inputs['contact_compte'] : '',
                'city_id' => (isset($inputs['city_id'])) ? $inputs['city_id'] : '',
                'country_id' => (isset($inputs['country_id'])) ? $inputs['country_id'] : '',
                'created_by' => Auth::user()->id,
                'user_id' => $user->id,
                'status' => 1
            ]);

            $commerce = Commerce::create([
                'nom' => $inputs['nom_commerce'],
                'adresse' => (isset($inputs['adresse_commerce'])) ? $inputs['adresse_commerce'] : '',
                'referer_amount' => $inputs['referer_amount'] ?? 500,
                'referee_amount' => $inputs['referee_amount'] ?? 500,
                'contact' => (isset($inputs['contact'])) ? PhoneNumberFormatter::format($inputs['contact'], $country->phonecode) : '',
                'description' => (isset($inputs['description_commerce'])) ? $inputs['description_commerce'] : '',
                'is_boost' => 0,
                'localisation' => (isset($inputs['localisation'])) ? $inputs['localisation'] : '',
                'horaires' => (isset($inputs['horaires'])) ? $inputs['horaires'] : '',
                'couleurs' => (isset($inputs['couleurs'])) ? $inputs['couleurs'] : '',
                'city_id' => (isset($inputs['city_id'])) ? $inputs['city_id'] : '',
                'category_id' => (isset($inputs['category_id'])) ? $inputs['category_id'] : '',
                'quartier_id' => $inputs['quartier_id'],
                'created_by' => Auth::user()->id,
                'vendeur_id' => $vendeur->id
            ]);

            $fileNameLogo = null;
            if ($request->file('logo')) {
                $file = $request->file('logo');

                $ext = $file->getClientOriginalExtension();
                $fileNameLogo = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_COMMERCES .  $fileNameLogo;

                $this->storeImage($imageUploadPath, $file, 200, 200);
            }

            $fileNameCard = null;
            if ($request->file('card')) {
                $file = $request->file('card');

                $ext = $file->getClientOriginalExtension();
                $fileNameCard = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_CARDS .  $fileNameCard;

                $this->storeImage($imageUploadPath, $file, 640, 320);
            }

            $fileNameBackground = null;
            if ($request->file('background')) {
                $file = $request->file('background');

                $ext = $file->getClientOriginalExtension();
                $fileNameBackground = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_BACKGROUNDS .  $fileNameBackground;

                $this->storeImage($imageUploadPath, $file, 640, 320);
            }

            $commerce->update([
                'logo'          => $fileNameLogo,
                'card'          => $fileNameCard,
                'background'    => $fileNameBackground,
            ]);

            $datefin = (isset($inputs['duree'])) ? Carbon::now()->addMonths($inputs['duree']) : '';
            $programme = Programme::create([
                'libelle' => $inputs['nom_commerce'],
                'description' => (isset($inputs['description_commerce'])) ? $inputs['description_commerce'] : '',
                'duree' => $inputs['duree'],
                'date_debut' => (isset($inputs['date_debut'])) ? $inputs['date_debut'] : '',
                'date_fin' => $datefin,
                'is_cashback' => 1,
                'taux_commission' => $inputs['taux_commission'],
                'taux_cashback' => $inputs['taux_cashback'],
                'taux_cashback_vip' => $inputs['taux_cashback_vip'],
                'montant_abonnement' => (isset($inputs['montant_abonnement'])) ? $inputs['montant_abonnement'] : 0,
                'bonus_wallet' => 500,
                'max_cashback' => $inputs['max_cashback'],
                'max_cashback_vip' => $inputs['max_cashback_vip'],
                'mnt_max_facture' => $inputs['mnt_max_facture'],
                'commerce_id' => $commerce->id,
                'created_by' => Auth::user()->id
            ]);

            $datar = [
                'id_fidelite' => $this->generateUniqueNumber(), 'cashback' => 0, 'topup' => 0, 'passages' => 0,
                'vendeur_id' => $vendeur->id, 'programme_id' => $programme->id
            ];

            // ici on cree le wallet du vendeur
            WalletVendeur::create($datar);

            DB::commit(); // on persiste tout
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error("Erreur creation generale commerce - point vente - vendeur: " . $e->getMessage());
            return redirect()->back()->withInput($request->input())->with('error', 'erreur création merge.');
        }

        // //create firebase user
        // try {
        //     if (Str::startsWith($user->telephone, '00')) {
        //         $phoneNumber = str_replace("00", "+", $user->telephone);
        //     } else {
        //         $phoneNumber = $user->telephone;
        //     }
        //     $firebaseUser = $this->firebase->createUserWithPhoneNumber($phoneNumber);
        //     $user->firebase_uid = $firebaseUser->uid;
        //     $user->save();
        // } catch(Exception $e){
        //     Log::error("Erreur creation firebase user" . $e->getMessage());
        //     return redirect()->back()->withInput($request->input())->with('error', 'erreur création merge.');
        // }

        return redirect()->route('commerces.index')
            ->with('success', 'commerce créé avec succès.');
    }

    protected  function storeImage($path, $file, $width, $height): void
    {
        if ($width > 0 && $height > 0) {
            $image = Image::make($file)->resize($width, $height);
            $image->encode();
            // $url = Storage::disk('s3')->put($path, $image->__toString());
            Storage::disk('s3')->put($path, $image->__toString());
        } else {
            Storage::disk('s3')->put($path, file_get_contents($file));
        }
    }

    protected function removeImage($path): void
    {
        Storage::disk('s3')->delete($path);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $data = $this->rpCommerce->getCommerceFromId($id);
        // echo json_encode($data); die;
        return view('backend.commerces.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $pays = Pays::orderBy('name')->get();
        $data = $this->rpCommerce->getCommerceFromId($id);
        $ville = Ville::find($data->city_id);
        $pays_id = $ville->country_id;
        $vendeur = $this->rpVendeur->getAllFree();
        $categories = CategoryCommerce::get();

        $regions = Region::where('country_id', $pays_id)->orderBy('name')->get();
        $state_id = $ville->state_id;
        $villes = Ville::where('state_id', $state_id)->orderBy('name')->get();
        $quartiers = Quartier::where('city_id', $data->city_id)->orderBy('name')->get();
        $nbr_pv = (isset($data->pointVentes) && count($data->pointVentes) > 0) ? count($data->pointVentes) : 0;

        //        dd($data->configuration->toArray());
        return view('backend.commerces.edit', compact('data', 'pays', 'quartiers', 'pays_id', 'regions', 'state_id', 'villes', 'categories', 'nbr_pv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $com = Commerce::with('vendeur.user')->findOrFail($id);
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'city_id' => 'required|numeric',
            'ticket_scan_quota_per_user_daily' => 'sometimes|numeric',
            'ticket_scan_quota_all_user_daily' => 'sometimes|numeric|gt:ticket_scan_quota_per_user_daily',
            'category_id' => 'required|numeric',
            'card' => 'sometimes|file|image',
            'background' => 'sometimes|file|image',
            'max_top_up' => ['required', 'numeric'],
            'accept_cot' => ['sometimes'],
            'accept_cashback' => ['sometimes'],
            'button_scan_cashbak' => ['sometimes'],
            'cot_rate_basic' => ['required', 'numeric', 'gte:0', 'lte:100'],
            'cot_rate_vip' => ['required', 'numeric', 'gte:0', 'lte:100'],
            'max_cashback_over_topup' =>  ['required', 'numeric'],
            'max_cashback_vip_over_topup' => ['required', 'numeric'],
            'max_topup_vip' => ['required', 'numeric'],
            'telephone' => [
                'required',
                Rule::unique('users', 'telephone')
                    ->ignore($com->vendeur->user_id)
            ]
        ], [
            'telephone.required' => 'Le numéro de téléphone est obligatoire',
            'telephone.unique' => 'Le numéro de téléphone est déjà utilisé',
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        try {
            $country = Pays::where('id', '=', $request->input('country_id'))->firstOrFail();
        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())->with('error', 'Impossible de créer le commerce : Pays introuvable !');
        }

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->withInput($request->input())->with('error', $mes);
        }

        try {
            $inputs = $request->all();

            DB::beginTransaction();

            /** @var Commerce $commerce */
            $commerce = Commerce::find($id);

            $fileNameLogo = $commerce->logo;
            if ($request->file('logo')) {
                $file = $request->file('logo');

                $ext = $file->getClientOriginalExtension();
                $fileNameLogo = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_COMMERCES .  $fileNameLogo;

                if ($fileNameLogo) {
                    $this->removeImage($imageUploadPath);
                }

                $this->storeImage($imageUploadPath, $file, 200, 200);
            }

            $fileNameCard = $commerce->card;
            if ($request->file('card')) {
                $file = $request->file('card');

                $ext = $file->getClientOriginalExtension();
                $fileNameCard = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_CARDS .  $fileNameCard;

                if ($fileNameCard) {
                    $this->removeImage($imageUploadPath);
                }

                $this->storeImage($imageUploadPath, $file, 640, 320);
            }

            $fileNameBackground = $commerce->background;
            if ($request->file('background')) {
                $file = $request->file('background');

                $ext = $file->getClientOriginalExtension();
                $fileNameBackground = $commerce->id . '-' . date('ymdHis') . '.' . $ext;
                $imageUploadPath = Commerce::FOLDER_BACKGROUNDS .  $fileNameBackground;

                if ($fileNameBackground) {
                    $this->removeImage($imageUploadPath);
                }

                $this->storeImage($imageUploadPath, $file, 640, 320);
            }
            $commerce->logo = $fileNameLogo;
            $commerce->nom  = $inputs['nom_commerce'];
            $commerce->card = $fileNameCard;
            $commerce->background = $fileNameBackground;
            $commerce->contact = (isset($inputs['contact'])) ? PhoneNumberFormatter::format($inputs['contact'], $country->phonecode) : '';
            $commerce->description = (isset($inputs['description_commerce'])) ? $request->input('description_commerce') : '';
            $commerce->city_id = $request->input('city_id');
            $commerce->quartier_id = $request->input('quartier_id');
            $commerce->referee_amount = $request->input('referee_amount');
            $commerce->referer_amount = $request->input('referer_amount');
            $commerce->category_id = $request->input('category_id');
            $commerce->localisation = (isset($inputs['localisation'])) ? $inputs['localisation'] : '';
            $commerce->adresse =  $request->input('adresse_commerce', $commerce->adresse);
            $commerce->website = $request->input('website', $commerce->website);
            $commerce->horaires = (isset($inputs['horaires'])) ? $request->input('horaires') : '';
            $commerce->couleurs = (isset($inputs['couleurs'])) ? $inputs['couleurs'] : '';
            $commerce->save();

            $programme = Programme::where('commerce_id', '=', $commerce->id)->first();
            $datedebut = Carbon::parse($inputs['date_debut']);
            $datefin = $datedebut->addMonths($inputs['duree']);
            $programme->libelle = $inputs['nom_commerce'];
            $programme->duree = $inputs['duree'];
            $programme->date_debut = $inputs['date_debut'];
            $programme->date_fin = $datefin;
            $programme->status = 1;
            $programme->is_cashback = $request->boolean('accept_cashback');
            $programme->button_scan_cashbak = $request->boolean('button_scan_cashbak');
            $programme->cashback_over_topup = $request->boolean('accept_cot');
            if ( ! $programme->is_cashback &&  $programme->cashback_over_topup ) {
                $programme->is_cashback = 1;
            }
            $programme->cot_vip_percentage = $request->input('cot_rate_vip');
            $programme->cot_percentage = $request->input('cot_rate_basic');
            $programme->max_top_up = $request->input('max_top_up');
            $programme->taux_commission = $inputs['taux_commission'];
            $programme->taux_cashback = $inputs['taux_cashback'];
            $programme->taux_cashback_vip = $inputs['taux_cashback_vip'];
            $programme->montant_abonnement = $inputs['montant_abonnement'];
            $programme->mnt_max_facture = $inputs['mnt_max_facture'];
            $programme->max_cashback = $inputs['max_cashback'];
            $programme->max_cashback_vip = $inputs['max_cashback_vip'];
            $programme->max_cashback_vip_over_topup = $inputs['max_cashback_vip_over_topup'];
            $programme->max_cashback_over_topup     = $inputs['max_cashback_over_topup'];
            $programme->max_topup_vip               = $inputs['max_topup_vip'];
            $programme->save();

            ShopConfiguration::query()->updateOrCreate([
                'shop_id' => $commerce->id,
            ], [
                'ticket_scan_quota_per_user_daily' => $request->ticket_scan_quota_per_user_daily ?? 1,
                'ticket_scan_quota_all_user_daily' => $request->ticket_scan_quota_all_user_daily ?? 100,
                'allow_cashback_over_ticket_scan' => $request->has('allow_cashback_over_ticket_scan'),
            ]);


            $vendeur = Vendeur::find($commerce->vendeur_id);
            $vendeur->nom = $request->input('nom');
            $vendeur->prenom = $request->input('prenom');
            $vendeur->adresse = (isset($inputs['adresse'])) ? $inputs['adresse'] : '';
            $vendeur->rccm = (isset($inputs['rccm'])) ? $request->input('rccm') : '';
            $vendeur->code_transaction = $request->input('code_transaction');
            $vendeur->code_postal = (isset($inputs['code_postal'])) ? $request->input('code_postal') : '';
            $vendeur->telephone1 = (isset($inputs['telephone1'])) ? PhoneNumberFormatter::format($request->input('telephone1'), $country->phonecode) : '';
            $vendeur->telephone2 = (isset($inputs['telephone2'])) ? PhoneNumberFormatter::format($request->input('telephone2'), $country->phonecode) : '';
            $vendeur->contact_compte = $request->input('contact_compte');
            $vendeur->city_id = $request->input('city_id');
            $vendeur->country_id = $request->input('country_id');

            $vendeur->save();


            $user = User::find($vendeur->user_id);
            if ($user) {
                if ($request->input('email')) {
                    $user->email = $request->input('email');
                }
                if ($request->input('nom')) {
                    $user->username = $request->input('nom');
                }
                if ($request->input('telephone')) {
                    $telephone = PhoneNumberFormatter::format($request->input('telephone'), $country->phonecode);

                    if ( $user->telephone !== $telephone ) {
                        $user->tokens()->where('name', $user->telephone)->delete();
                    }
                    $user->telephone =  PhoneNumberFormatter::format($request->input('telephone'), $country->phonecode);
                }
                $user->save();
                //                Rollbar::
            }
            DB::commit(); // on persiste tout
            event(new VendorUpdated($vendeur));

            $apiUrl = config('services.koree.api.url');

            throw_if(blank($apiUrl), new \Exception('config services.koree.api.url must to be set with a value'));

            /** @var User $authenticated */
            $authenticated = $request->user();
            $token = $authenticated->createToken(name: "Admin Console {$authenticated->telephone}", expiresAt: now()->addMinute());

            $data = Http::withHeaders([
                'version' => 'koree-next',
            ])->acceptJson()->withToken($token->plainTextToken)->get("{$apiUrl}/api/shops/{$commerce->id}/sent-event");

            $token->accessToken->forceDelete();

        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
            Log::error("Erreur creation generale commerce - point vente - vendeur: " . $e->getMessage() . " File: " . $e->getFile() . " Line: " . $e->getLine());
            Log::info('info_err'.$e);
            return redirect()->back()->withInput($request->input())->with('error', 'erreur mise  à jour info commerce.');
        }

        return redirect()->route('commerces.index')
            ->with('success', 'commerce mis à jour avec succès.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus($id)
    {
        try {
            $commerce = Commerce::find($id);
            if ($commerce->status === 1){
                $commerce->status = false;
                $vendeur = Vendeur::find($commerce->vendeur_id);
                if ( $vendeur ) {
                    $user = User::find($vendeur->user_id);
                    if ($user) {
                        $user->tokens()->where('name', $user->telephone)->delete();
                    }
                }
            } else {
                $commerce->status = true;
            }
            $commerce->save();
            DB::commit();
            return redirect()->route('commerces.index')
            ->with('success', 'commerce mis à jour avec succès.');
        }catch(\Exception $e){
            report($e);
            DB::rollBack();
            Log::error("Erreur lors de la mise à jour du satut du commere" . $e->getMessage() . " File: " . $e->getFile() . " Line: " . $e->getLine());
            return redirect()->back()->withInput($request->input())->with('error', 'erreur mise  à jour du statut du commerce.');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $commerce = Commerce::find($id);
        if ($commerce->logo) {
            $this->removeImage(Commerce::FOLDER_COMMERCES . $commerce->logo);
        }
        if ($commerce->card) {
            $this->removeImage(Commerce::FOLDER_CARDS . $commerce->card);
        }
        if ($commerce->background) {
            $this->removeImage(Commerce::FOLDER_BACKGROUNDS . $commerce->background);
        }
        $commerce->delete();

        return redirect()->route('commerces.index')
            ->with('success', 'Commerce supprimé');
    }

    // genere un unique code de parrainage et recherche sil nya pas un suer qui la deja
    public function generateUniqueNumber()
    {
        $code = "";
        do {
            $code = strtoupper(Str::random(6));
        } while (WalletVendeur::where("id_fidelite", "=", $code)->first());

        return $code;
    }

    public function suggested()
    {
        $data = ShopSuggestion::with(['city', 'client', 'district'])->latest()->get();
        return view("backend.commerces.suggested", compact('data'));
    }

    public function categories()
    {
        $data = CategoryCommerce::withCount('commerces')->latest()->get();
        // dd($data);
        // session($data);
        return view("backend.commerces.categories.index", compact('data'));
    }

    public function createCategory()
    {
        return view('backend.commerces.categories.create');
    }

    public function editCategory(CategoryCommerce $id)
    {
        return view('backend.commerces.categories.edit', [
            'category' => $id
        ]);
    }

    public function saveCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'libelle_en' => 'required',
        ], [
            'libelle.required' => 'Le libellé est requis'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                ->withInput($request->input());
        }

        $category = new CategoryCommerce;
        $category->libelle = $request->get('libelle');
        $category->libelle_en = $request->get('libelle_en');
        $category->created_by = auth()->id();
        $category->parent_id = null;
        $category->created_at = now();
        $category->updated_at = now();
        $category->save();
        session()->flash('success', "Catégorie ajoutée avec succès");
        return redirect()->route('commerces.categories');
    }

    public function updateCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'libelle' => 'required',
            'libelle_en' => 'required',
            'category' => 'required|exists:categories_commerces,id'
        ], [
            'libelle.required' => 'Le libellé est requis',
            'category.required' => 'Catégorie manquante',
            'category.exists' => 'Catégorie manquante',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors())
                ->withInput($request->input());
        }

        $category = CategoryCommerce::find($request->get('category'));
        $category->libelle = $request->get('libelle');
        $category->libelle_en = $request->get('libelle_en');
        $category->updated_by = auth()->id();
        $category->updated_at = now();
        $category->save();
        Commerce::with('vendeur.user')->whereCategoryId($category->id)->get()->each(function (Commerce $commerce) {
            event(new VendorUpdated($commerce->vendeur));
        });
        session()->flash('success', "Catégorie mis à jour avec succès");
        return redirect()->route('commerces.categories');
    }

    public function listeCommercesSelect2(Request $request)
    {
        return response()->json($this->rpCommerce->getCommerceSelect2($request), Response::HTTP_OK);
    }
}
