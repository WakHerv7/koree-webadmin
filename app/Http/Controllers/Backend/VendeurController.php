<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Pays;
use App\Models\Region;
use App\Models\User;
use App\Models\Vendeur;
use App\Models\Ville;
use App\Repositories\Frontend\V1\VendeurRepository;
use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Throwable;

class VendeurController extends Controller
{
    protected $rpVendeur;
    protected $rpUser;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(UsersRepository $rpU, VendeurRepository $rpCli)
    {
//        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
//        $this->middleware('permission:role-create', ['only' => ['create','store']]);
//        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

        $this->rpVendeur = $rpCli;
        $this->rpUser = $rpU;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->rpVendeur->getAllVendeurActif();
        dd($this->rpUser->getAll()[0]);
        return view('backend.vendeurs.index', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pays = Pays::get();

        return view('backend.vendeurs.create', compact('pays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'telephone' => 'required|numeric',
            'password' => 'required|numeric|digits:6',
            'code_transaction' => 'required|numeric|digits:6',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        $inputs = $request->all();
        try {
            \Illuminate\Support\Facades\DB::beginTransaction();

            $lepays = Pays::where('id', '=', $inputs['country_id'])->first();

            $user = User::create([
                'username' => $inputs['nom'],
                'email' => (isset($inputs['email'])) ? $inputs['email'] : '',
                'telephone' => "00" . $lepays->phonecode . $inputs['telephone'],
                'password' => bcrypt($inputs['password']),
                'type_user' => 'vendeur',
                'created_by' => Auth::user()->id,
            ]);

            // $phonecode= substr($inputs['telephone'],2,3);


            $client = Vendeur::create([
                'nom' => $inputs['nom'],
                'adresse' => (isset($inputs['adresse'])) ? $inputs['adresse'] : '',
                'rccm' => (isset($inputs['rccm'])) ? $inputs['rccm'] : '',
                'code_transaction' => (isset($inputs['code_transaction'])) ? $inputs['code_transaction'] : '',
                'code_postal' => (isset($inputs['code_postal'])) ? $inputs['code_postal'] : '',
                'tel_fixe' => (isset($inputs['tel_fixe'])) ? $inputs['tel_fixe'] : '',
                'fonction' => (isset($inputs['fonction'])) ? $inputs['fonction'] : '',
                'contact_compte' => (isset($inputs['contact_compte'])) ? $inputs['contact_compte'] : '',
                'city_id' => (isset($inputs['city_id'])) ? $inputs['city_id'] : '',
                'country_id' => (isset($inputs['country_id'])) ? $inputs['country_id'] : '',
                'created_by' => Auth::user()->id,
                'user_id' => $user->id,
                'status' => 1
            ]);
            \Illuminate\Support\Facades\DB::commit();

        } catch (Throwable $r) {
            \Illuminate\Support\Facades\DB::rollBack();
            Log::error("Erreur creation vendeur: " . $r->getMessage());
            return redirect()->back()->with('error', "Erreur dans le processus de creation vendeur.");

        }

        return redirect()->route('vendeurs.index')
            ->with('success', 'proprietaire créé avec succès.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->rpVendeur->getVendeur($id);
        return view('backend.vendeurs.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pays = Pays::get();
        $data = Vendeur::with('pays', 'ville', 'user')->find($id);
        $ville = Ville::find($data->city_id);
        $pays_id = $ville->country_id;

        $regions = Region::where('country_id', $pays_id)->get();
        $state_id = $ville->state_id;
        $villes = Ville::where('state_id', $state_id)->get();

        return view('backend.vendeurs.edit', compact('data', 'pays', 'villes', 'state_id', 'regions', 'pays_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'country_id' => 'required|numeric',
            'code_transaction' => 'required|numeric|digits:6',
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'numeric' => 'Le champ :attribute doit être numérique.',
            'digits' => 'Le champ :attribute doit etre de :digits caracteres.',
        ]);

        if ($validator->fails()) {
            $mes = "";
            $errors = $validator->errors();
            foreach ($errors->all() as $er) {
                $mes .= $er . ' ';
            }
            return redirect()->back()->with('error', $mes);
        }

        try {

            $data = Vendeur::find($id);
            $data->nom = $request->input('nom');
            $data->adresse = $request->input('adresse');
            $data->rccm = $request->input('rccm');
            $data->code_transaction = $request->input('code_transaction');
            $data->code_postal = $request->input('code_postal');
            $data->tel_fixe = $request->input('tel_fixe');
            $data->fonction = $request->input('fonction');
            $data->contact_compte = $request->input('contact_compte');
            $data->city_id = $request->input('city_id');
            $data->country_id = $request->input('country_id');
            $data->updated_by = Auth::user()->id;
            $data->save();

        } catch (\Illuminate\Database\QueryException $e) {
            //    Log::info($e->getMessage());
            return redirect()->back()->with('error', "Erreur de mise a jour");
        } catch (\Throwable $r) {
            //    Log::info($r->getMessage());
            return redirect()->back()->with('error', "Erreur de mise a jour");
        }

        return redirect()->route('vendeurs.index')->with('success', 'Proprietaire mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vendeur::find($id)->delete();

        return redirect()->route('vendeurs.index')
            ->with('success', 'Proprietaire supprimé');
    }
}
