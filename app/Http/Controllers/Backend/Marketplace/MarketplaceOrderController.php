<?php

namespace App\Http\Controllers\Backend\Marketplace;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class MarketplaceOrderController extends Controller
{
    protected $url = '/api/marketplace/orders/manage';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($shop_id=null)
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];
            // dd($data);
            // $filteredOrders =[];
            if(!is_null($shop_id)) {
                $filteredOrders = array_filter($data, function($order) use ($shop_id) {
                    return $order["shop_id"] == $shop_id;
                });

                $data = $filteredOrders;
            }

            return view('backend.marketplace.order.index', compact('data', 'shop_id'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->withError('Failed to fetch order data from the API');
        }
    }

    public function indexDeleted()
    {
        
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });
            
            return view('backend.marketplace.order.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = session('currentOrder');
        
        // dd($data);

        return view('backend.marketplace.order.show', compact('data'));
    }

}
