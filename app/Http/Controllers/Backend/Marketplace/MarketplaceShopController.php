<?php

namespace App\Http\Controllers\Backend\Marketplace;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;


class MarketplaceShopController extends Controller
{
    protected $url = '/api/marketplace/shop';

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];

            // $data = array_filter($rawdata, function($object) {
            //     return !$object['is_active'];
            // });
            return view('backend.marketplace.shop.index', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->withError('Failed to fetch shop data from the API');
        }
    }

    public function indexDeleted()
    {
        
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });
            
            return view('backend.marketplace.shop.index-deleted', compact('data'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = session('currentValue');
        // dd($data);
        return view('backend.marketplace.shop.show', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.marketplace.shop.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = session('currentEdit');
        return view('backend.marketplace.shop.edit', compact('data'));
    }
    // public function edit($id)
    // {
    //     getToken();
    //     try {
    //         $client = new Client();
    //         $token = session('accessToken');
    //         $response = $client->request('GET', env('API_URL').$this->url, [
    //             'headers' => [
    //                 'Authorization' => 'Token '.$token,
    //                 'Accept' => 'application/json',
    //             ],
    //             'query' => [
    //                 'shop_id' => $id,
    //             ],
    //         ]);
    //         $res = json_decode($response->getBody()->getContents(), true);
    //         $data = $res[0]['results'][0];
    //         return view('backend.marketplace.shop.edit', compact('data'));
    //     } catch (ServerException $e) {
    //         $response = $e->getResponse();
    //         $responseBodyAsString = $response->getBody()->getContents();
    //         return back()->withError('Failed to fetch shop data from the API');
    //         // dd($responseBodyAsString);
    //         // return back()->withErrors($responseBodyAsString);
    //         // $this->errorMessage = "Une erreur est survenue";
    //     }
    // }
}
