<?php

namespace App\Http\Controllers\Backend\Marketplace;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;


class MarketplaceArticleController extends Controller
{
    protected $url = '/api/marketplace/articles';
    protected $product_categories = [];

    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($shop_id)
    {
        //$this->get_product_categories();

        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL') . $this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                    'X-App-Type' => 'koree/merchant',
                    'shop_id' => $shop_id,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            // dd($rawdata);
            if (is_null($rawdata)) {
                $rawdata = [];
            }


            $data = array_filter($rawdata, function ($object) {
                return $object['is_active'];
            });

            $shop = session('currentValue');

            return view('backend.marketplace.article.index', compact('data', 'shop'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->withError('Failed to fetch article data from the API');
        }
    }


    public function indexDeleted($shop_id)
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL') . $this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                    'X-App-Type' => 'koree/merchant',
                    'shop_id' => $shop_id,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            // dd($rawdata);
            if (is_null($rawdata)) {
                $rawdata = [];
            }

            $data = array_filter($rawdata, function ($object) {
                return !$object['is_active'];
            });

            $shop = session('currentValue');
            return view('backend.marketplace.article.index-deleted', compact('data', 'shop'));
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }

    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = session('currentArticle');
        $addons = null;
        if ($data['is_menu'] == true) {
            $addons = $this->get_article_addons($data['id']);
        }
        return view('backend.marketplace.article.show', compact('data', 'addons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shop = session('currentValue');
        return view('backend.marketplace.article.create', compact('shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = session('currentEdit');
        $addons = null;
        if ($data['is_menu'] == true) {
            $addons = $this->get_article_addons($data['id']);
            $addons = array_map(function ($item) {
                $is_required = var_export($item['required'], true);
                $item['is_required'] = $is_required;
                $item['article_id'] = $item['article'];
                unset($item['required']);
                unset($item['article']);
                unset($item['is_active']);
                unset($item['created_by']);
                unset($item['updated_by']);
                return $item;
            }, $addons);
        }
        $shop = session('currentValue');
        return view('backend.marketplace.article.edit', compact('data', 'addons', 'shop'));
    }


    protected function get_article_addons($id)
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL') . '/api/marketplace/articles/addons', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'article_id' => $id
                ]
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];
            return $data;

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            return [];
            // return back()->withError('Failed to fetch product-category data from the API');
        }
    }

    protected function get_product_categories()
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL') . '/api/marketplace/product-categories', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $data = $res[0]['results'];
            $this->product_categories = $data;

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            // return back()->withError('Failed to fetch product-category data from the API');
        }
    }

}
