<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Commerce;
use App\Models\QrCode;
use App\Models\QrCodeCampagne;
use App\Repositories\CampagneRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CampagneController extends Controller
{
    protected $campagneRepo;

    /**
     * create a new instance of the class
     *
     * @return void
     */
    function __construct(CampagneRepository $campagneRepository)
    {
//        $this->middleware('permission:commerce-list|commerce-create|commerce-edit|commerce-delete', ['only' => ['index', 'store']]);
//        $this->middleware('permission:commerce-create', ['only' => ['create', 'store']]);
//        $this->middleware('permission:commerce-edit', ['only' => ['edit', 'update']]);
//        $this->middleware('permission:commerce-delete', ['only' => ['destroy']]);

        $this->campagneRepo = $campagneRepository;
    }

    /**
     * Display a listing commerce actif.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $data = $this->campagneRepo->query()->with(['commerce'])->orderBy('nom')->latest()->get();
        return view('backend.campagnes.index', compact('data'));
    }

    public function create(Request $request)
    {
        $commerces = Commerce::query()->orderBy('nom')->get();
        return view('backend.campagnes.create', compact('commerces'));
    }


    public function store(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'nom' => 'required',
                'date_from' => 'required',
                'quantite' => 'required',
                'duree' => 'required',
                'montant' => 'required|integer|min:1',
            ], [
                'required' => 'Le champ :attribute est obligatoire.',
            ]);

            if ($validator->fails()) {
                $mes = "";
                $errors = $validator->errors();
                foreach ($errors->all() as $er) {
                    $mes .= $er . ' ';
                }
                return redirect()->back()->with('error', $mes);
            }

            $inputs = $request->all();
            $date_debut = Carbon::createFromDate($inputs['date_from']);
            $date_fin = $date_debut->copy()->addMonths(intval($inputs['duree']));
            //on recupere le vendeur
            $commerce = Commerce::find($inputs['commerce_id']);

            $groupNumber = Str::random();


            DB::beginTransaction();

            /** @var QrCodeCampagne $campagne */
            $campagne = QrCodeCampagne::create([
                'id' => Str::uuid()->toString(),
                'nom' => $request->nom,
                'quantite' => $request->quantite,
                'height_in_cm' => $request->height_in_cm,
                'shape' => $request->shape,
                'commerce_id' => $commerce->id,
                'date_until' => $date_fin->toDateTimeString(),
                'date_from' => $date_debut->toDateTimeString(),
            ]);

//        dd($campagne->toArray(), $request->all(), $date_fin->toString());
//
//            dd($campagne    );
            for ($i = 1; $i <= $request->quantite; $i++) {

                $qrcode = QrCode::query()->create([
                    'type' => QrCode::$qr_codes_campagnes,
                    'group_number' => $groupNumber,
                    'date_until' => $date_fin->toDateTimeLocalString(),
                    'date_from' => $date_debut->toDateTimeLocalString(),
//                    'date_used' => $request->date_used,
                    'number' => $i,
                    'campagne_id' => $campagne->id,
                    'data' => ['montant' => $request->montant],
//                    'data' => [
//                        'montant' => $request->montant,
//                        'date_from' => $request->date_from,
//                        'date_until' => $request->date_until,
//                        'campagne_id' => $campagne->id,
//                        'commerce_id' => $campagne->commerce_id,
//                    ],
                ]);
            }

            DB::commit();

        } catch (\Throwable $exception) {
            DB::rollBack();
            Log::error('Erreur creation campagne qrcode', [
                'message' => $exception->getMessage(),
                'libe' => $exception->getLine(),
                'file' => $exception->getFile(),
                'requestData' => $request->all(),
            ]);
        }
        return redirect()->route('campagnes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function detail($id)
    {
        $data = $this->campagneRepo->query()->with(['commerce', 'qrcodes'])->findOrFail($id);


//        dd($data);
        return view('backend.campagnes.show', compact('data'));
    }

    public function download($id)
    {
        $data = $this->campagneRepo->query()->with(['commerce', 'qrcodes'])->findOrFail($id);

        $images = $this->campagneRepo->generateQrcodesImg($data);
//        dd($data, $images);

//        return $images;

        $pdf = App::make('dompdf.wrapper');
//        $pdf->setPaper('L', 'landscape');
        $pdf->loadView("backend.campagnes.pdf", [
            'images' => $images,
            'campagne' => $data,
        ]);
        return $pdf->stream();
    }
}
