<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use App\Models\TicketsCancelReason;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Psy\Util\Str;

class TicketController extends Controller
{


    public function index(Request $request)
    {
        $status = $request->input('status');
        $data = Ticket::query()
            ->latest()
            ->when($status, function ($query, $status) {
                $query->where('status', $status);
            })
            ->with('wallet.programme.commerce', 'wallet.client')->get();

        return view('backend.tickets.index', compact('data', 'status'));
    }


    public function show(Ticket $ticket)
    {
        $data = $ticket->load('allower.vendeur', 'canceller.vendeur', 'wallet.programme.commerce', 'wallet.client', 'transaction');

        $cancel_reasons = TicketsCancelReason::query()->get();

        return view('backend.tickets.show', compact('data', 'cancel_reasons'));
    }

    public function apply_cashback(Request $request, Ticket $ticket, string $apply)
    {

        $apiUrl = config('services.koree.api.url');

        throw_if(blank($apiUrl), new \Exception('config services.koree.api.url must to be set with a value'));

        /** @var User $authenticated */
        $authenticated = $request->user();
        $token = $authenticated->createToken(name: "Admin Console {$authenticated->telephone}", expiresAt: now()->addMinute());

        $data = Http::withHeaders([
            'version' => 'koree-next',
        ])->acceptJson()->withToken($token->plainTextToken)->post("{$apiUrl}/api/tickets/{$apply}", [
            'hash' => $ticket->content_hash,
            'cancel_reason' => $request->cancel_reason,
        ]);

        $token->accessToken->forceDelete();

        return redirect()->route('tickets.show', $ticket)->with('success', $data->successful() ? ($apply === 'charge' ? 'Cashback créé avec succès' : 'Cashback réjété avec succès') : $data->json('message'));
    }


    public function indexCancellationReasons(Request $request)
    {
        $data = TicketsCancelReason::query()->get();

        return view('backend.tickets-cancel-reasons.index', compact('data'));
    }

    public function createCancellationReasons(Request $request)
    {
        return view('backend.tickets-cancel-reasons.create');
    }
    public function storeCancellationReasons(Request $request)
    {
        try {

            TicketsCancelReason::query()->create([
                'text' => $request->input('text'),
                'value' => \Illuminate\Support\Str::snake($request->input('value')),
            ]);
            return redirect()->route('tickets.cancel-reasons.index')->with('success', "Motif ajouté avec succès !");
        } catch (\Throwable $exception) {
            return redirect()->route('tickets.cancel-reasons.index')->with('error', $exception->getMessage());
        }
    }
    public function deleteCancellationReasons(Request $request, TicketsCancelReason $model)
    {
        $model->forceDelete();
        return redirect()->route('tickets.cancel-reasons.index')->with('success', "Motif supprimé avec succès !");
    }
}
