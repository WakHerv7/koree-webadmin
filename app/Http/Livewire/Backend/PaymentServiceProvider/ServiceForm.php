<?php

namespace App\Http\Livewire\Backend\PaymentServiceProvider;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class ServiceForm extends Component
{
    use WithFileUploads;

    protected $url = '/api/payment/provider/serivice';

    public $servicetypes = [
        ['id'=>'TRANSFERT',  'name'=>'TRANSFERT'],
        ['id'=>'COLLECT',  'name'=>'COLLECT'],
        ['id'=>'AIRTIME',  'name'=>'AIRTIME'],
        ['id'=>'CANAL',  'name'=>'CANAL'],
        ['id'=>'ENEO_PREPAY',  'name'=>'ENEO_PREPAY'],
        ['id'=>'ENEO_POSTPAY',  'name'=>'ENEO_POSTPAY'],
    ];
    public $servicelibelle;
    public $servicerate;
    public $servicesens;
    public $servicetype = "null";
    public $serviceimage;
    public $serviceprovider_id = null;
    public $mountserviceimage;

    public $form_title = "Création d'un nouveau service de paiement";
    public $button_text = "Créer";
    public $service_id = null;


    public function submitData() 
    {
        $this->validate([
            "servicelibelle" => ['required', 'string'],
            "servicerate" => ['required'],
            // "servicesens" => ['required'],
            "servicetype" => 'required|string|not_in:null',
            "serviceimage" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
        ]);

        // dd($this->servicelibelle,
        // $this->servicerate,
        // $this->servicesens,
        // $this->servicetype,
        // $this->serviceimage,
        // $this->serviceprovider_id);

        $serviceimagePath = $this->serviceimage ? $this->serviceimage->getRealPath() : $this->mountserviceimage;
        $serviceimagePathType = $this->serviceimage ? "local" : "online";
        $serviceimageExtension = $this->serviceimage ? $this->serviceimage->getClientOriginalExtension() : pathinfo($this->mountserviceimage, PATHINFO_EXTENSION);

        $token = session('accessToken');
        $client = new Client();

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $this->servicelibelle,
            ],
            [
                'name'    => 'rate',
                'contents' => strval($this->servicerate),
            ],
            [
                'name'    => 'sens',
                'contents' => $this->servicesens,
            ],
            [
                'name'    => 'service_type',
                'contents' => $this->servicetype,
            ],
            [
                'name'    => 'provider_id',
                'contents' => $this->serviceprovider_id,
            ],
            [
                'name'    => 'image',
                'contents' => $serviceimagePathType == "local" ? fopen($serviceimagePath, 'r') : $serviceimagePath,
                'filename' => getRandomCode(8).'.'.$serviceimageExtension
            ],
        ];

        if (is_null($this->service_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("payment-service-provider.index")
                    ->with('success', 'Creation d\'un nouveau service de paiement réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'service_id' => $this->service_id,
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("payment-service-provider.index")
                    ->with('success', 'Modification de service de paiement réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($provider, $sens, $service = null) 
    {
        $this->form_title = $this->form_title. ' pour '.$provider['libelle'];
        $this->serviceprovider_id  = $provider['id'];
        $this->servicesens  = $sens;
        // dd($service);
        if (!is_null($service)) {
            
            $this->service_id  = $service['id'];
            $this->servicelibelle    = $service['libelle'];
            $this->servicerate    = $service['rate'];
            $this->servicesens    = $service['sens'];
            $this->servicetype    = $service['service_type'];
            // $this->serviceprovider_id    = $service['provider_id'];
            $this->mountserviceimage       = $service['image'];
            
            $this->form_title = "Modifier un service de paiement". ' pour '.$provider['libelle'];
            $this->button_text = "Modifier";
        }
    }

    public function render()
    {
        return view('livewire..backend.payment-service-provider.service-form');
    }
}
