<?php

namespace App\Http\Livewire\Backend\PaymentServiceProvider;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowList extends Component
{
    protected $url = '/api/payment/provider';

    public $data;

    public $checkedServices = [];
    public $allServices = [];
    public $allProviders = [];
    

    public function mount($data = null) 
    {
        $this->data = $data;

        $checkedServices = [];
        $allServices = [];
        $allProviders = [];

        foreach ($data as $pKey => $p) {
            $allProviders[$p['id']]['in'] = false;
            $allProviders[$p['id']]['out'] = false;
            if (array_key_exists('services', $p)) {
                foreach ($p['services']['in'] as $sKey => $s) {
                    if (isset($p['id']) && isset($s['id'])) {
                        $checkedServices[$p['id']][$s['id']] = $s['is_active'];
                        $allServices[$p['id']][$s['id']] = ['edit'=>false, 'delete'=>false];
                    }
                }
                foreach ($p['services']['out'] as $sKey => $s) {
                    if (isset($p['id']) && isset($s['id'])) {
                        $checkedServices[$p['id']][$s['id']] = $s['is_active'];
                        $allServices[$p['id']][$s['id']] = ['edit'=>false, 'delete'=>false];
                    }
                }
            }
        }

        $this->checkedServices = $checkedServices;
        $this->allServices = $allServices;
        $this->allProviders = $allProviders;
        // dd($data);
        // dd($checkedServices);
        // dd($allServices);
    }

    public function toggleDeleteModal($pid, $sid) 
    {
        $this->allServices[$pid][$sid]['delete'] = !$this->allServices[$pid][$sid]['delete'];
        if ($this->allServices[$pid][$sid]['delete'])
        {
            $this->dispatchBrowserEvent('showmodalscreen');
        } else {
            $this->dispatchBrowserEvent('hidemodalscreen');
        }
    }

    public function toggleEditModal($pid, $sid) 
    {
        $this->allServices[$pid][$sid]['edit'] = !$this->allServices[$pid][$sid]['edit'];
        if ($this->allServices[$pid][$sid]['edit'])
        {
            $this->dispatchBrowserEvent('showmodalscreen');
        } else {
            $this->dispatchBrowserEvent('hidemodalscreen');
        }
    }

    public function toggleAddServiceModal($pid, $sens) 
    {
        $this->allProviders[$pid][$sens] = !$this->allProviders[$pid][$sens];
        if ($this->allProviders[$pid][$sens])
        {
            $this->dispatchBrowserEvent('showmodalscreen');
        } else {
            $this->dispatchBrowserEvent('hidemodalscreen');
        }
    }

    public function show($val) 
    {
        session(['currentValue' => $val]);

        return redirect()->route('payment-service-provider.show');
    }

    public function updatedCheckedServices($value, $outerIndex)
    {
        // dd($value, $outerIndex);
        // $indexes = explode(".", "1.4");
        // $outerIndex = intval($indexes[0]);
        // $innerIndex = intval($indexes[1]);
        
        // $this->checkedServices[$outerIndex][$innerIndex] = $value;
    }

    public function toggleService($pid, $sid)
    {
        $this->handleActivation($sid, $this->checkedServices[$pid][$sid]);
    }

    public function handleActivation($id, $value) 
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url.'/serivice', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'service_id' => $id,
                ],
                'form_params' => [
                    "is_active" => $value,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                $this->dispatchBrowserEvent('alertsuccess', [
                    'type' => 'success', 
                    'message' => $value ? 'Activation réussie':'Désactivation réussie'
                ]);
            }
            
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($esponseBodyAsString);
        }
    }

    public function handleProviderDeletion($id) 
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('DELETE',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'provider_id' => $id,
                ],
            ]);
            // dd($id);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("payment-service-provider.index")
                ->with('success', 'Suppresion fournisseur réussie.');
            }

        } catch (ServerException $e) {
            
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            // return back()->with('error','Failed to fetch data from the API');
        }
    }

    public function handleServiceDeletion($id) 
    {
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('DELETE',env('API_URL').$this->url.'/serivice', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'service_id' => $id,
                ],
            ]);
            // dd($id);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("payment-service-provider.index")
                ->with('success', 'Suppresion service réussie.');
            }

        } catch (ServerException $e) {
            
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            // return back()->with('error','Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.payment-service-provider.show-list');
    }
}
