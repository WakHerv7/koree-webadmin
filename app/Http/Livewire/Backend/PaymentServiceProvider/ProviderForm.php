<?php

namespace App\Http\Livewire\Backend\PaymentServiceProvider;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class ProviderForm extends Component
{
    use WithFileUploads;

    protected $url = '/api/payment/provider';

    public $providerlibelle;
    public $providerimage;
    public $mountproviderimage;

    public $form_title = "Création d'un nouveau fournisseur de paiement";
    public $button_text = "Créer";
    public $provider_id = null;

    public function submitData() 
    {
        $this->validate([
            "providerlibelle" => ['required', 'string'],
            "providerimage" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
        ]);

        $providerimagePath = $this->providerimage ? $this->providerimage->getRealPath() : $this->mountproviderimage;
        $providerimagePathType = $this->providerimage ? "local" : "online";
        $providerimageExtension = $this->providerimage ? $this->providerimage->getClientOriginalExtension() : pathinfo($this->mountproviderimage, PATHINFO_EXTENSION);

        $token = session('accessToken');
        $client = new Client();

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $this->providerlibelle,
            ],
            [
                'name'    => 'image',
                'contents' => $providerimagePathType == "local" ? fopen($providerimagePath, 'r') : $providerimagePath,
                'filename' => getRandomCode(8).'.'.$providerimageExtension
            ],
        ];

        if (is_null($this->provider_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("payment-service-provider.index")
                    ->with('success', 'Creation d\'un nouveau fournisseur de paiement réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'provider_id' => $this->provider_id,
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("payment-service-provider.index")
                    ->with('success', 'Modification de fournisseur de paiement réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($provider = null) 
    {
        if (!is_null($provider)) {
            
            $this->provider_id  = $provider['id'];
            $this->providerlibelle    = $provider['libelle'];
            $this->mountproviderimage       = $provider['image'];
            
            $this->form_title = "Modifier un fournisseur de paiement";
            $this->button_text = "Modifier";
        }
    }

    public function render()
    {
        return view('livewire..backend.payment-service-provider.provider-form');
    }
}
