<?php

namespace App\Http\Livewire\Backend\Delivery\PaymentMethod;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;


class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/delivery/payment-method';
    
    public $logo_image;
    public $bg_image;
    public $rccm;
    public $identity_document;
    public $identity_document_2;

    public $manager_phone;
    public $manager_email;
    public $manager_name;
    public $payment_method_name;
    public $city_payment_method;
    public $payment_method_phone;
    public $neighborhood_payment_method;
    public $longitude;
    public $latitude;
    public $amount_per_kilometer;

    public $errorMessage = null;
    public $form_title = "Création d'une nouvelle entreprise";
    public $button_text = "Créer";
    public $payment_method_id = null;

    public function submitData() 
    {
        $this->validate([
            "manager_phone"=>['required', 'string'],
            "manager_email"=>['required', 'string'],
            "manager_name"=>['required', 'string'],
            "payment_method_name"=>['required', 'string'],
            "city_payment_method"=>['required', 'string'],
            "payment_method_phone"=>['required', 'string'],
            "neighborhood_payment_method"=>['required', 'string'],
            "longitude"=>['required', 'string'],
            "latitude"=>['required', 'string'],
            "amount_per_kilometer"=>['required', 'string'],
            'logo_image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'bg_image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'identity_document' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'identity_document_2' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'rccm' => 'nullable|file|max:2048', // 2MB Max
        ]);

        
        $logo_image_base64 = $this->logo_image ? base64_encode(file_get_contents($this->logo_image->getRealPath())) : null;

        $bg_image_base64 = $this->bg_image ? base64_encode(file_get_contents($this->bg_image->getRealPath())) : null;

        $identity_document_base64 = $this->identity_document ? base64_encode(file_get_contents($this->identity_document->getRealPath())) : null;

        $identity_document_2_base64 = $this->identity_document_2 ? base64_encode(file_get_contents($this->identity_document_2->getRealPath())) : null;

        $rccm_base64 = $this->rccm ? base64_encode(file_get_contents($this->rccm->getRealPath())) : null;


        $token = session('accessToken');
        $client = new Client();
        if (is_null($this->payment_method_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'form_params' => [
                        "manager_phone"=> $this->manager_phone,
                        "manager_email"=> $this->manager_email,
                        "manager_name"=> $this->manager_name,
                        "payment_method_name"=> $this->payment_method_name,
                        "city_payment_method"=> $this->city_payment_method,
                        "payment_method_phone"=> $this->payment_method_phone,
                        "neighborhood_payment_method"=> $this->neighborhood_payment_method,
                        "longitude"=> $this->longitude,
                        "latitude"=> $this->latitude,
                        "amount_per_kilometer"=> $this->amount_per_kilometer,
                        "image" => $logo_image_base64,
                        "bg_image" => $bg_image_base64,
                        "identity_document" => $identity_document_base64,
                        "identity_document_2" => $identity_document_2_base64,
                        "rccm" => $rccm_base64,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.payment_method.index")
                    ->with('success', 'Modification entreprise réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'payment_method_id' => $this->payment_method_id,
                    ],
                    'form_params' => [
                        "manager_phone" => $this->manager_phone,
                        "manager_email" => $this->manager_email,
                        "manager_name" => $this->manager_name,
                        "payment_method_name" => $this->payment_method_name,
                        "city_payment_method" => $this->city_payment_method,
                        "payment_method_phone" => $this->payment_method_phone,
                        "neighborhood_payment_method" => $this->neighborhood_payment_method,
                        "longitude" => $this->longitude,
                        "latitude" => $this->latitude,
                        "amount_per_kilometer" => $this->amount_per_kilometer,
                        "image" => $logo_image_base64,
                        "bg_image" => $bg_image_base64,
                        "identity_document" => $identity_document_base64,
                        "identity_document_2" => $identity_document_2_base64,
                        "rccm" => $rccm_base64,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.payment_method.index")
                    ->with('success', 'Creation d\'une nouvelle entreprise réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($payment_method = null) 
    {
        if (!is_null($payment_method)) {
            $this->manager_phone        = $payment_method['manager_phone'];
            $this->manager_email        = $payment_method['manager_email'];
            $this->manager_name         = $payment_method['manager_name'];
            $this->payment_method_name         = $payment_method['payment_method_name'];
            $this->city_payment_method         = $payment_method['city_payment_method'];
            $this->payment_method_phone        = $payment_method['payment_method_phone'];
            $this->neighborhood_payment_method = $payment_method['neighborhood_payment_method'];
            $this->longitude            = strval($payment_method['location']['long']);
            $this->latitude             = strval($payment_method['location']['lat']);
            $this->amount_per_kilometer = strval($payment_method['amount_per_kilometer']);

            $this->button_text = "Modifier";
            $this->form_title = "Modifier une methode de paiement";
            
        }
    }

    public function render()
    {
        return view('livewire..backend.delivery.payment_method.form');
    }
}




