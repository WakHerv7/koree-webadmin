<?php

namespace App\Http\Livewire\Backend\Delivery\Deliver;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/delivery/deliver';
    
    public $phone;
    public $first_name;
    public $last_name;
    public $name;
    public $longitude;
    public $latitude;

    public $photo;
    public $driving_license;
    public $driving_license_2;
    public $identity_document;
    public $identity_document_2;

    public $form_title = "Création d'une nouvelle entreprise";
    public $button_text = "Créer";
    public $deliver_id;

    public function submitData() 
    {
        $this->validate([
            "phone"=>['required', 'string'],
            "first_name"=>['required', 'string'],
            "last_name"=>['required', 'string'],
            "longitude"=>['required', 'string'],
            "latitude"=>['required', 'string'],
            'photo' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max

            'driving_license' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'driving_license_2' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max

            'identity_document' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'identity_document_2' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            
        ]);

        $photo_base64 = $this->photo ? base64_encode(file_get_contents($this->photo->getRealPath())) : null;

        $driving_license_base64 = $this->driving_license ? base64_encode(file_get_contents($this->driving_license->getRealPath())) : null;

        $driving_license_2_base64 = $this->driving_license_2 ? base64_encode(file_get_contents($this->driving_license_2->getRealPath())) : null;

        $identity_document_base64 = $this->identity_document ? base64_encode(file_get_contents($this->identity_document->getRealPath())) : null;

        $identity_document_2_base64 = $this->identity_document_2 ? base64_encode(file_get_contents($this->identity_document_2->getRealPath())) : null;


        $token = session('accessToken');
        
        $client = new Client();

        if (is_null($this->deliver_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'form_params' => [
                        "phone"=> $this->phone,
                        "first_name"=> $this->first_name,
                        "last_name"=> $this->last_name,
                        "latitude"=> $this->latitude,
                        "longitude"=> $this->longitude,
                        "image" => $photo_base64,
                        "driving_license" => $driving_license_base64,
                        "driving_license_2" => $driving_license_2_base64,
                        "identity_document" => $identity_document_base64,
                        "identity_document_2" => $identity_document_2_base64,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.deliver.index")
                    ->with('success', 'Creation livreur réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'deliver_id' => $this->deliver_id,
                    ],
                    'form_params' => [
                        "phone"=> $this->phone,
                        "first_name"=> $this->first_name,
                        "last_name"=> $this->last_name,
                        "latitude"=> $this->latitude,
                        "longitude"=> $this->longitude,
                        "image" => $photo_base64,
                        "driving_license" => $driving_license_base64,
                        "driving_license_2" => $driving_license_2_base64,
                        "identity_document" => $identity_document_base64,
                        "identity_document_2" => $identity_document_2_base64,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.deliver.index")
                    ->with('success', 'Modification livreur réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($deliver = null) 
    {
        if (!is_null($deliver)) {
            $this->phone             = $deliver['user']['phone'];
            $this->first_name        = $deliver['user']['first_name'];
            $this->last_name         = $deliver['user']['last_name'];
            $this->longitude         = strval($deliver['location']['long']);
            $this->latitude          = strval($deliver['location']['lat']);
            $this->driving_license        = $deliver['driving_license'];
            $this->driving_license_2      = $deliver['driving_license_2'];
            $this->identity_document      = $deliver['identity_document'];
            $this->identity_document_2    = $deliver['identity_document_2'];

            $this->button_text = "Modifier";
            $this->form_title = "Modifier un livreur";
            
        }
    }

    public function render()
    {
        return view('livewire.backend.delivery.deliver.form');
    }
}
