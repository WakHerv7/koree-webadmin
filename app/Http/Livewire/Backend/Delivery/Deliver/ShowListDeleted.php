<?php

namespace App\Http\Livewire\Backend\Delivery\Deliver;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;


class ShowListDeleted extends Component
{
    protected $url = '/api/delivery/deliver';

    public $data;

    public function mount($data = null) 
    {
        $this->data = $data;
    }

    public function show($val) 
    {
        session(['currentValue' => $val]);

        return redirect()->route('delivery.deliver.show');
    }


    public function handleActivation($id) 
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'deliver_id' => $id,
                ],
                'form_params' => [
                    "is_active" => true,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("delivery.deliver.index")
                ->with('success', 'Creation d\'une nouvelle entreprise réussie.');
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error','Failed to fetch data from the API');
        }
    }

    public function render()
    {
        return view('livewire..backend.delivery.deliver.show-list-deleted');
    }
}
