<?php

namespace App\Http\Livewire\Backend\Delivery\Subscription;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/delivery/subscription';
    
    public $companies;

    public $start_date;
    public $end_date;
    public $commission_rate;
    public $company;
    public $company_id = 'null';

    
    public $form_title = "Création d'un nouveau contrat";
    public $button_text = "Créer";
    public $subscription_id = null;

    public function submitData() 
    {
        // dd($this->company_id);

        $this->validate([
            "start_date"=>['required', 'string'],
            "end_date"=>['required', 'string'],
            "commission_rate"=>['required'],
            "company_id"=>['required'],
        ]);

        // dd($this->company_id);

        $token = session('accessToken');
        $client = new Client();
        if (is_null($this->subscription_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'form_params' => [
                        "start_date"=> $this->start_date,
                        "end_date"=> $this->end_date,
                        "commission_rate"=> strval($this->commission_rate),
                        "company"=> $this->company_id,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.subscription.index")
                    ->with('success', 'Creation d\'une nouveau contrat réussie.');
                    
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'subscription_id' => $this->subscription_id,
                    ],
                    'form_params' => [
                        "start_date"=> $this->start_date,
                        "end_date"=> $this->end_date,
                        "commission_rate"=> $this->commission_rate,
                        "company"=> $this->company_id,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.subscription.index")
                    ->with('success', 'Modification du contrat réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($subscription = null) 
    {
        $data;
        $rawdata;
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL').'/api/delivery/company', [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $data = array_filter($rawdata, function($object) {
                return !$object['is_active'];
            });

            $this->companies = $data;
            // dd($this->companies);

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue.']);
        }

        if (!is_null($subscription)) {
            
            $data = array_filter($rawdata, function($object) {
                return $object['is_active'];
            });
            $this->companies = $data;

            // dd($subscription['company']);
            
            $this->subscription_id  = $subscription['id'];
            $this->start_date        = $subscription['start_date'];
            $this->end_date        = $subscription['end_date'];
            $this->commission_rate         = $subscription['commission_rate'];
            $this->company         = $subscription['company'];
            $this->company_id        = $subscription['company'][0]['id'];

            $this->button_text = "Modifier";
            $this->form_title = "Modifier un contrat";
            
        }
    }

    
    public function render()
    {
        return view('livewire..backend.delivery.subscription.form');
    }
}


