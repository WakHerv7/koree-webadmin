<?php

namespace App\Http\Livewire\Backend\Delivery\Subscription;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowList extends Component
{
    protected $url = '/api/delivery/subscription';

    public $data;

    public function mount($data = null) 
    {
        $this->data = $data;
    }

    public function show($val) 
    {
        session(['currentValue' => $val]);

        return redirect()->route('delivery.subscription.show');
    }

    public function handleDeletion($data) 
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'subscription_id' => $data['id'],
                ],
                'form_params' => [
                    // "start_date"=> $data['start_date'],
                    // "end_date"=> $data['end_date'],
                    // "commission_rate"=> $data['commission_rate'],
                    // "company"=> $data['company'][0]['id'],
                    "is_active" => false,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("delivery.subscription.index")
                ->with('success', 'Désactivation réussie.');
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error','Failed to fetch data from the API');
        }
    }

    public function render()
    {
        return view('livewire..backend.delivery.subscription.show-list');
    }
}
