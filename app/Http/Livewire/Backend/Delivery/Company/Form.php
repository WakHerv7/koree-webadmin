<?php

namespace App\Http\Livewire\Backend\Delivery\Company;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;


class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/delivery/company';
    
    public $countries;
    public $cities;
    public $neighborhoods;

    // public $logo;
    // public $background;
    // public $mount;
    // public $mountbackground;

    public $logo_image;
    public $bg_image;
    public $rccm;
    public $identity_document;
    public $identity_document_2;

    public $mountlogo_image;
    public $mountbg_image;
    public $mountrccm;
    public $mountidentity_document;
    public $mountidentity_document_2;

    public $manager_phone;
    public $manager_email;
    public $manager_name;
    public $company_name;
    public $company_phone;
    public $country;
    public $city;
    public $neighborhood;
    public $longitude;
    public $latitude;
    public $amount_per_kilometer;
    public $identity_call = "237";

    public $errorMessage = null;
    public $form_title = "Création d'une nouvelle entreprise";
    public $button_text = "Créer";
    public $company_id = null;


    public function submitData() 
    {
        
        // dd('Country:'.$this->country.' - City:'.$this->city.' - Neighbirhood:'.$this->neighborhood);

        $this->validate([
            "manager_phone"=>['required', 'string'],
            "manager_email"=>['required', 'string'],
            "manager_name"=>['required', 'string'],
            "company_name"=>['required', 'string'],
            // "country"=>['required', 'string'],
            "city"=>['required'],
            "neighborhood"=>['required'],
            "company_phone"=>['required'],
            // "longitude"=>['nullable', 'string'],
            // "latitude"=>['nullable', 'string'],
            "amount_per_kilometer"=>['required', 'string'],
            'logo_image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'bg_image' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'identity_document' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'identity_document_2' => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            'rccm' => 'nullable|file|max:2048', // 2MB Max
        ]);

        
        

        $logo_imagePath = $this->logo_image ? $this->logo_image->getRealPath() : $this->mountlogo_image;
        $logo_imagePathType = $this->logo_image ? "local" : "online";
        $logo_imageExtension = $this->logo_image ? $this->logo_image->getClientOriginalExtension() : pathinfo($this->mountlogo_image, PATHINFO_EXTENSION);

        $bg_imagePath = $this->bg_image ? $this->bg_image->getRealPath() : $this->mountbg_image;
        $bg_imagePathType = $this->bg_image ? "local" : "online";
        $bg_imageExtension = $this->bg_image ? $this->bg_image->getClientOriginalExtension() : pathinfo($this->mountbg_image, PATHINFO_EXTENSION);

        $rccmPath = $this->rccm ? $this->rccm->getRealPath() : $this->mountrccm;
        $rccmPathType = $this->rccm ? "local" : "online";
        $rccmExtension = $this->rccm ? $this->rccm->getClientOriginalExtension() : pathinfo($this->mountrccm, PATHINFO_EXTENSION);

        $identity_documentPath = $this->identity_document ? $this->identity_document->getRealPath() : $this->mountidentity_document;
        $identity_documentPathType = $this->identity_document ? "local" : "online";
        $identity_documentExtension = $this->identity_document ? $this->identity_document->getClientOriginalExtension() : pathinfo($this->mountidentity_document, PATHINFO_EXTENSION);

        $identity_document_2Path = $this->identity_document_2 ? $this->identity_document_2->getRealPath() : $this->mountidentity_document_2;
        $identity_document_2PathType = $this->identity_document_2 ? "local" : "online";
        $identity_document_2Extension = $this->identity_document_2 ? $this->identity_document_2->getClientOriginalExtension() : pathinfo($this->mountidentity_document_2, PATHINFO_EXTENSION);

        // dd($backgroundPath);

        $multipart = [
            [
                'name'    => 'manager_phone',
                'contents' => $this->manager_phone,
            ],            
            [
                'name'    => 'manager_email',
                'contents' => $this->manager_email,
            ],
            [
                'name'    => 'manager_name',
                'contents' => $this->manager_name,
            ],
            [
                'name'    => 'company_name',
                'contents' => $this->company_name,
            ],
            [
                'name'    => 'city_company',
                'contents' => strval($this->city),
            ],
            [
                'name'    => 'neighborhood_company',
                'contents' => strval($this->neighborhood),
            ],
            [
                'name'    => 'company_phone',
                'contents' => $this->company_phone,
            ],
            [
                'name'    => 'amount_per_kilometer',
                'contents' => strval($this->amount_per_kilometer),
            ],
            [
                'name'    => 'identity_call',
                'contents' => strval($this->identity_call),
            ],
            [
                'name'    => 'longitude',
                'contents' => "4.0777712",
            ],
            [
                'name'    => 'latitude',
                'contents' => "9.6723884",
            ],
         ];
         
         if (!empty($this->logo_image)) {
            $multipart[] = [
                'name'    => 'image',
                'contents' => $logo_imagePathType == "local" ? fopen($logo_imagePath, 'r') : $logo_imagePath,
                'filename' => getRandomCode(8).'.'.$logo_imageExtension
            ];
         }

         if (!empty($this->bg_image)) {
            $multipart[] = [
                'name'    => 'bg_image',
                'contents' => $bg_imagePathType == "local" ? fopen($bg_imagePath, 'r') : $bg_imagePath,
                'filename' => getRandomCode(8).'.'.$bg_imageExtension
            ];
         }

         if (!empty($this->rccm)) {
            $multipart[] = [
                'name'    => 'rccm',
                'contents' => $rccmPathType == "local" ? fopen($rccmPath, 'r') : $rccmPath,
                'filename' => getRandomCode(8).'.'.$rccmExtension
            ];
         }

         if (!empty($this->identity_document)) {
            $multipart[] = [
                'name'    => 'identity_document',
                'contents' => $identity_documentPathType == "local" ? fopen($identity_documentPath, 'r') : $identity_documentPath,
                'filename' => getRandomCode(8).'.'.$identity_documentExtension
            ];
         }

         if (!empty($this->identity_document_2)) {
            $multipart[] = [
                'name'    => 'identity_document_2',
                'contents' => $identity_document_2PathType == "local" ? fopen($identity_document_2Path, 'r') : $identity_document_2Path,
                'filename' => getRandomCode(8).'.'.$identity_document_2Extension
            ];
         }

        //  dd($multipart);



        $selectedCountry = array_filter($this->countries, function($object) {
            return $object['id'] == $this->country;
        });

        // dd($selectedCountry[0]['code_call']);

        $token = session('accessToken');
        $client = new Client();

        if (is_null($this->company_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart,
                    // 'form_params' => [
                    //     "manager_phone"=> $this->manager_phone,
                    //     "manager_email"=> $this->manager_email,
                    //     "manager_name"=> $this->manager_name,
                    //     "company_name"=> $this->company_name,
                    //     "city_company"=> strval($this->city),
                    //     "company_phone"=> $this->company_phone,
                    //     "neighborhood_company"=> strval($this->neighborhood),
                    //     "longitude"=> "4.0777712",
                    //     "latitude"=> "9.6723884",
                    //     "amount_per_kilometer"=> $this->amount_per_kilometer,
                    //     "image" => $logo_image_base64,
                    //     "bg_image" => $bg_image_base64,
                    //     "identity_document" => $identity_document_base64,
                    //     "identity_document_2" => $identity_document_2_base64,
                    //     "rccm" => $rccm_base64,
                    //     // "is_active" => "true",
                    //     "identity_call"=> "237", // $selectedCountry[0]['code_call']
                    // ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.company.index")
                    ->with('success', 'Creation d\'une nouvelle entreprise réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'company_id' => $this->company_id,
                    ],
                    'multipart' => $multipart,
                    // 'form_params' => [
                    //     "manager_phone" => $this->manager_phone,
                    //     "manager_email" => $this->manager_email,
                    //     "manager_name" => $this->manager_name,
                    //     "company_name" => $this->company_name,
                    //     "city_company" => strval($this->city),
                    //     "company_phone" => $this->company_phone,
                    //     "neighborhood_company" => strval($this->neighborhood),
                    //     "longitude" => $this->longitude,
                    //     "latitude" => $this->latitude,
                    //     "amount_per_kilometer" => $this->amount_per_kilometer,
                    //     "image" => $logo_image_base64,
                    //     "bg_image" => $bg_image_base64,
                    //     "identity_document" => $identity_document_base64,
                    //     "identity_document_2" => $identity_document_2_base64,
                    //     "rccm" => $rccm_base64,
                    // ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("delivery.company.index")
                    ->with('success', 'Modification entreprise réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($company = null) 
    {
        $this->getCountries();
        // $this->getCities();
        // $this->getNeighborhoods();
        
        // dd($company);
        if (!is_null($company)) {
            $this->company_id           = $company['id'];
            $this->manager_phone        = $company['manager_phone'];
            $this->manager_email        = $company['manager_email'];
            $this->manager_name         = $company['manager_name'];
            $this->company_name         = $company['company_name'];
            $this->city                 = intval($company['city_company']);
            $this->company_phone        = $company['company_phone'];
            // $this->neighborhood         = $company['neighborhood'] ? intval($company['neighborhood']['name']) : null;
            $this->country              = $company['neighborhood'] ? intval($company['neighborhood']['country']) : null;
            $this->longitude            = $company['location'] ? strval($company['location']['long']) : null;
            $this->latitude             = $company['location'] ? strval($company['location']['lat']) : null;
            $this->amount_per_kilometer = strval($company['amount_per_kilometer']);

            $this->mountlogo_image = $company['image'];
            $this->mountbg_image = $company['bg_image'];
            $this->mountrccm = $company['rccm'];
            $this->mountidentity_document = $company['identity_document'];
            $this->mountidentity_document_2 = $company['identity_document_2'];

            $this->button_text = "Modifier";
            $this->form_title = "Modifier une entreprise";
        }
    }

    public function updatedCountry($value){
        // dd($value);
        $this->getCities(intval($value));
        // dd($this->cities);
    }

    public function updatedCity($value){
        $this->getNeighborhoods(intval($value));
    }

    // public function updatedNeighborhood($value){
    //     dd('Country:'.$this->country.' - City:'.$this->city.' - Neighbirhood:'.$this->neighborhood);
    // }
    

    public function getCountries()
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').'/api/delivery/countries/public', [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'items' => 1000
                ]
            ]);            
            $res = json_decode($response->getBody()->getContents(), true);
            $this->countries = $res[0]['results'];
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }

    public function getCities($country_id = null)
    {
        try {
            $client = new Client();
            $token = session('accessToken');

            if (!is_null($country_id)) {
                $response = $client->request('GET', env('API_URL').'/api/delivery/cities/public', [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'country_id' => $country_id,
                        'items' => 100
                    ]
                ]);
            } else {
                $response = $client->request('GET', env('API_URL').'/api/delivery/cities/public', [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'items' => 1000
                    ]
                ]);
            }
            
            $res = json_decode($response->getBody()->getContents(), true);
            // dd($res[0]['results']);
            $this->cities = $res[0]['results'];
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }

    public function getNeighborhoods($city_id = null)
    {
        try {
            $client = new Client();
            $token = session('accessToken');

            if ($city_id) {
                $response = $client->request('GET', env('API_URL').'/api/delivery/neihborhood/public', [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'citie_id' => $city_id,
                        'items' => 100
                    ]
                ]);
            } else {
                $response = $client->request('GET', env('API_URL').'/api/delivery/neihborhood/public', [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'items' => 1000
                    ]
                ]);
            }
            $res = json_decode($response->getBody()->getContents(), true);
            $this->neighborhoods = $res[0]['results'];
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }

    public function render()
    {
        return view('livewire.backend.delivery.company.form');
    }
}