<?php

namespace App\Http\Livewire\Backend\Marketplace\Category;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;
use App\Models\CategoryCommerce;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/category';
  
    public $libelle;
    public $logo;
    public $mountlogo;
    public $bg_color;
    public $logo_color;
    
    public $form_title = "Création d'une nouvelle categorie de commerce";
    public $button_text = "Créer";
    public $category_id = null;

    // public $shop_categories = [];

    public function submitData() 
    {
        $this->validate([
            
            "libelle" => ['required', 'string'],
            "logo" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            "bg_color" => ['required', 'string'],
            "logo_color" => ['required', 'string'],
        ]);

        $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        $logoPathType = $this->logo ? "local" : "online";
        $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);

        $token = session('accessToken');
        $client = new Client();

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $this->libelle,
            ],
            [
                'name'    => 'bg_color',
                'contents' => str_replace('#', '', $this->bg_color),
            ],
            [
                'name'    => 'logo_color',
                'contents' => str_replace('#', '', $this->logo_color),
            ],
            [
                'name'    => 'logo',
                'contents' => $logoPathType == "local" ? fopen($logoPath, 'r') : $logoPath,
                'filename' => getRandomCode(8).'.'.$logoExtension
            ],
        ];


        if (is_null($this->category_id)) {                
            try {
                    $response = $client->request('POST',env('API_URL').$this->url, [
                        'headers' => [
                            'Authorization' => 'Token '.$token,
                            'Accept' => 'application/json',
                        ],
                        'multipart' => $multipart,
                    ]);

                    $res = json_decode($response->getBody()->getContents(), true);
                    
                    if ($res[0]['success'] == 0) {
                        $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                    } else {
                        // $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'message' => 'Catégorie créée.']);
                        return redirect()->route("marketplace.category.index")
                        ->with('success', 'Creation d\'une categorie produit réussie.');
                    }

                } catch (ServerException $e) {
                    $response = $e->getResponse();
                    $responseBodyAsString = $response->getBody()->getContents();
                    dd($responseBodyAsString);
                }
            }
        else {
            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'category_id' => $this->category_id,
                    ],
                    'multipart' => $multipart,
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.category.index")
                    ->with('success', 'Modification de categorie commerce réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($category = null) 
    {
        
        if (!is_null($category)) {
            
            $this->category_id  = $category['id'];

            $this->libelle    = $category['libelle'];
            // $this->logo       = $category['logo'];
            $this->mountlogo       = $category['logo'];            
            $this->bg_color   = '#'.$category['bg_color'];
            $this->logo_color = '#'.$category['logo_color'];
            
            $this->button_text = "Modifier";
            $this->form_title = "Modifier une categorie commerce";
            
        }
    }

    // public function getCategories()
    // {
    //     try {
    //         $client = new Client();
    //         $token = session('accessToken');
    //         $response = $client->request('GET', env('API_URL').'/api/marketplace/category', [
    //             'headers' => [
    //                 'Authorization' => 'Token '.$token,
    //                 'Accept' => 'application/json',
    //             ],
    //             'query' => [
    //                 'items' => 100
    //             ]
    //         ]);            
    //         $res = json_decode($response->getBody()->getContents(), true);
    //         return $res[0]['results'];
    //     } catch (ServerException $e) {
    //         $response = $e->getResponse();
    //         $responseBodyAsString = $response->getBody()->getContents();
    //         return back()->with('error', 'Failed to fetch data from the API');
    //     }
    // }



    public function render()
    {
        return view('livewire..backend.marketplace.category.form');
    }
}





// $apiCategoryList = [];
//         $apicategories = $this->getCategories();
//         foreach ($apicategories as $cat) {
//             $apiCategoryList[] = [
//                 'id' => $cat['id'],
//                 'libelle' => $cat['libelle']
//             ];
//         }
//         $data = CategoryCommerce::withCount('commerces')->latest()->get();
//         $laravelCategoryList = [];
//         foreach ($data as $cat) {
//             $laravelCategoryList[] = [
//                 'id' => $cat->id,
//                 'libelle' => $cat->libelle
//             ];
//         }
//         // dd("mergedCategoryList: ",$mergedCategoryList);
//         // Create a new list with unique categories based on 'libelle'
//         $uniqueCategoryList = [];
//         // Loop through the second cat list and check if "libelle" exists in the first list
//         foreach ($laravelCategoryList as $cat) {
//             $libelleExistsInFirstList = false;
//             foreach ($apiCategoryList as $firstCategory) {
//                 if ($cat['libelle'] === $firstCategory['libelle'] || $cat['libelle'] == '' || $cat['libelle'] == 'Restaurants') {
//                     $libelleExistsInFirstList = true;
//                     break;
//                 }
//             }
//             if (!$libelleExistsInFirstList) {
//                 $uniqueCategoryList[] = $cat;
//             }
//         }
        
//         $this->shop_categories = $uniqueCategoryList;
//         // dd($uniqueCategoryList);
        
//         // $this->shop_categories
//         // dd($this->shop_categories[0]->libelle);