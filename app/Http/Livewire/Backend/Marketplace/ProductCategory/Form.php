<?php

namespace App\Http\Livewire\Backend\Marketplace\ProductCategory;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/product-categories';
  
    public $libelle;
    public $logo;
    public $mountlogo;
    public $bg_color;
    public $logo_color;
    
    public $form_title = "Création d'une nouvelle categorie de produit";
    public $button_text = "Créer";
    public $product_category_id = null;

    public function submitData() 
    {
        $this->validate([
            
            "libelle" => ['required', 'string'],
            "logo" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            "bg_color" => ['required', 'string'],
            "logo_color" => ['required', 'string'],
        ]);

        $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        $logoPathType = $this->logo ? "local" : "online";
        $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);

        $token = session('accessToken');
        $client = new Client();

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $this->libelle,
            ],
            [
                'name'    => 'bg_color',
                'contents' => str_replace('#', '', $this->bg_color),
            ],
            [
                'name'    => 'logo_color',
                'contents' => str_replace('#', '', $this->logo_color),
            ],
            [
                'name'    => 'logo',
                'contents' => $logoPathType == "local" ? fopen($logoPath, 'r') : $logoPath,
                'filename' => getRandomCode(8).'.'.$logoExtension
            ],
        ];

        if (is_null($this->product_category_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart,
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.product_category.index")
                    ->with('success', 'Creation d\'une categorie produit réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'id' => $this->product_category_id,
                    ],
                    'multipart' => $multipart,
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.product_category.index")
                    ->with('success', 'Modification de categorie produit réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($product_category = null) 
    {
        if (!is_null($product_category)) {
            
            $this->product_category_id  = $product_category['id'];

            $this->libelle    = $product_category['libelle'];
            // $this->logo       = $product_category['logo'];
            $this->mountlogo       = $product_category['logo'];            
            $this->bg_color   = '#'.$product_category['bg_color'];
            $this->logo_color = '#'.$product_category['logo_color'];
            
            $this->button_text = "Modifier";
            $this->form_title = "Modifier une categorie produit";
            
        }
    }



    public function render()
    {
        return view('livewire..backend.marketplace.product-category.form');
    }
}
