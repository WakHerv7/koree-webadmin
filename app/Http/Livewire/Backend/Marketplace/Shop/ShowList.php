<?php

namespace App\Http\Livewire\Backend\Marketplace\Shop;

use App\Models\Commerce;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowList extends Component
{
    protected $url = '/api/marketplace/shop';

    public $data;

    public function mount($data = null) 
    {        
        // $data = Commerce::with('categorie','vendeur.user','ville.pays','programme.wallets','pointVentes')
        //                     ->orderBy('created_at', 'desc')
        //                     ->where('status', '=', true)
        //                     ->get();

        // $commerces = [];
        // foreach ($data as $p) {
        //     $commerces[] = [
        //         "libelle" => $p->nom,
        //         "logo" => asset($p->logo),
        //         "background" => public_path($p->background),
        //         "contact" => $p->contact,
        //         "adresse" => $p->adresse,
        //         "description" => $p->description,
        //         "category_id" => $p->categorie->id,
        //         "fidelity_id" => $p->programme->wallets_vendeur->id_fidelite,
        //         "shop_cashback_id" => $p->id,
        //     ];
        // }
        // dd($commerces);

        $this->data = $data;

    }

    public function show($val) 
    {
        session(['currentValue' => $val]);

        return redirect()->route('marketplace.shop.show');
    }

    public function edit($val) 
    {
        session(['currentEdit' => $val]);

        return redirect()->route('marketplace.shop.edit', ['id' => $val['id']]);
    }

    public function handleDeletion($id) 
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'id' => $id,
                ],
                'form_params' => [
                    "is_active" => false,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("marketplace.shop.index")
                ->with('success', 'Désactivation commerce réussie.');
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error','Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.shop.show-list');
    }
}
