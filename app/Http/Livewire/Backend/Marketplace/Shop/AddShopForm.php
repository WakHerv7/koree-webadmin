<?php

namespace App\Http\Livewire\Backend\Marketplace\Shop;

use App\Models\Commerce;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class AddShopForm extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/shop';
    
    public $accept_delivery = "false";
    public $accept_external_delivery = "false";
    public $provider = "KOREE";
    public $identity_call = "237";
    public $longitude = "4.0777712";
    public $latitude = "9.6723884";
    public $form_title = "Ajouter  un commerce à la marketplace";
    public $button_text = "Ajouter";

    public $logo;
    public $mountlogo;
    public $background;
    public $mountbackground;
    public $all_shops;
    public $shop_id = null;

    public function submitData() 
    {
        $this->validate([
            "shop_id" => ['required', 'string'],
            "logo" => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            "background" => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
        ]);

        $token = session('accessToken');
        $client = new Client();

        $shop_to_add = array_values(array_filter($this->all_shops, function($shop) {
            return $shop['id'] == $this->shop_id;
        }));

        $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        $logoPathType = $this->logo ? "local" : "online";
        $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);

        $backgroundPath = $this->background ? $this->background->getRealPath() : $this->mountbackground;
        $backgroundPathType = $this->logo ? "local" : "online";
        $backgroundExtension = $this->background ? $this->background->getClientOriginalExtension() : pathinfo($this->mountbackground, PATHINFO_EXTENSION);

        // dd($shop_to_add[0]['libelle']);

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $shop_to_add[0]['libelle'],
            ],            
            [
                'name'    => 'contact',
                'contents' => $shop_to_add[0]['contact'],
            ],
            [
                'name'    => 'description',
                'contents' => $shop_to_add[0]['description'],
            ],
            [
                'name'    => 'adresse',
                'contents' => $shop_to_add[0]['adresse'],
            ],
            [
                'name'    => 'category_id',
                'contents' => strval($shop_to_add[0]['category_id']),
            ],
            [
                'name'    => 'accept_delivery',
                'contents' => strval($this->accept_delivery),
            ],
            [
                'name'    => 'accept_external_delivery',
                'contents' => strval($this->accept_external_delivery),
            ],
            [
                'name'    => 'longitude',
                'contents' => strval($this->longitude),
            ],
            [
                'name'    => 'latitude',
                'contents' => strval($this->latitude),
            ],
            [
                'name'    => 'provider',
                'contents' => strval($this->provider),
            ],
            [
                'name'    => 'fidelity_id',
                'contents' => strval($shop_to_add[0]['fidelity_id']),
            ],
            [
                'name'    => 'shop_cashback_id',
                'contents' => strval($shop_to_add[0]['shop_cashback_id']),
            ],
            [
                'name'    => 'identity_call',
                'contents' => strval($this->identity_call),
            ],
            [
                'name'    => 'logo',
                'contents' => fopen($logoPath, 'r'),
                'filename' => getRandomCode(5).'logo'.'.'.$logoExtension
            ],
            [
                'name'    => 'background',
                'contents' => fopen($backgroundPath, 'r'),
                'filename' => getRandomCode(5).'background'.'.'.$backgroundExtension
            ],
         ];


        try {
            $response = $client->request('POST',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'multipart' => $multipart
            ]);

            $res = json_decode($response->getBody()->getContents(), true);
            
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'Ajout d\'un commerce réussi.']);
                // return redirect()->route("payment-service-shop.index")
                // ->with('success', 'Ajout d\'un commerce réussi.');
                unset($this->all_shops[$this->shop_id]);
                foreach ($this->all_shops as $key => $item) {
                    if ($item['id'] == $this->shop_id) {
                        unset($this->all_shops[$key]);
                    }
                 }
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
        }

    }

    public function mount($shop = null) 
    {           
        $data = Commerce::with('categorie','vendeur.user','ville.pays','programme.wallets','pointVentes')
                            ->orderBy('created_at', 'desc')
                            ->where('status', '=', true)
                            ->get();

        
        // $p = $data[0];
        $commerces = [];
        foreach ($data as $p) {
            $contact = str_replace("00237", "", $p->contact);
            $commerces[] = [
                "id" => $p->id,
                "libelle" => $p->nom,
                "logo" => asset($p->logo),
                "background" => public_path($p->background),
                "contact" => $contact,
                "adresse" => $p->adresse,
                "description" => $p->description,
                "category_id" => $p->categorie->id,
                "fidelity_id" => $p->programme->wallets_vendeur->id_fidelite,
                "shop_cashback_id" => $p->id,
            ];
        }
        $this->all_shops = $commerces;

        // dd($commerces);

        // if (!is_null($shop)) {
            
        //     $this->shop_id  = $shop['id'];
        //     $this->shoplibelle    = $shop['libelle'];
        //     $this->mountshopimage       = $shop['image'];
            
        //     $this->form_title = "Modifier un fournisseur de paiement";
        //     $this->button_text = "Modifier";
        // }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.shop.add-shop-form');
    }
}
