<?php

namespace App\Http\Livewire\Backend\Marketplace\Shop;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/shop';
    
    public $categories;

    public $libelle;
    public $logo;
    public $background;
    public $mountlogo;
    public $mountbackground;
    public $contact;
    public $description;
    public $adresse;
    public $category_id;
    public $note;
    public $accept_delivery = "false";
    public $accept_external_delivery = "false";
    public $provider = "KOREE";
    public $identity_call = "237";
    
    public $longitude = "4.0777712";
    public $latitude = "9.6723884";
    public $fidelity_id;
    public $shop_cashback_id;

    
    public $form_title = "Création d'un nouveau commerce";
    public $button_text = "Créer";
    public $shop_id = null;

    public function submitData() 
    {
        $this->validate([
            "libelle" => ['required', 'string'],
            "logo" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            "background" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
            "contact" => ['required', 'string'],
            "description" => ['nullable', 'string'],
            "adresse" => ['required', 'string'],
            "category_id" => ['required'],
            "note" => ['nullable'],
            "accept_delivery" => ['required'],
            "accept_external_delivery" => ['required'],

            "longitude" => ['nullable'],
            "latitude" => ['nullable'],
            "provider" => ['nullable'],
            "fidelity_id" => ['nullable'],
            "shop_cashback_id" => ['nullable'],
        ]);

        $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        $logoPathType = $this->logo ? "local" : "online";
        $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);

        $backgroundPath = $this->background ? $this->background->getRealPath() : $this->mountbackground;
        $backgroundPathType = $this->logo ? "local" : "online";
        $backgroundExtension = $this->background ? $this->background->getClientOriginalExtension() : pathinfo($this->mountbackground, PATHINFO_EXTENSION);

        // dd($backgroundPath);

        $multipart = [
            [
                'name'    => 'libelle',
                'contents' => $this->libelle,
            ],            
            [
                'name'    => 'contact',
                'contents' => $this->contact,
            ],
            [
                'name'    => 'description',
                'contents' => $this->description,
            ],
            [
                'name'    => 'adresse',
                'contents' => $this->adresse,
            ],
            [
                'name'    => 'category_id',
                'contents' => strval($this->category_id),
            ],
            [
                'name'    => 'note',
                'contents' => strval($this->note),
            ],
            [
                'name'    => 'accept_delivery',
                'contents' => strval($this->accept_delivery),
            ],
            [
                'name'    => 'accept_external_delivery',
                'contents' => strval($this->accept_external_delivery),
            ],
            [
                'name'    => 'longitude',
                'contents' => strval($this->longitude),
            ],
            [
                'name'    => 'latitude',
                'contents' => strval($this->latitude),
            ],
            [
                'name'    => 'provider',
                'contents' => strval($this->provider),
            ],
            [
                'name'    => 'fidelity_id',
                'contents' => strval($this->fidelity_id),
            ],
            [
                'name'    => 'shop_cashback_id',
                'contents' => strval($this->shop_cashback_id),
            ],
            [
                'name'    => 'identity_call',
                'contents' => strval($this->identity_call),
            ],
            // [
            //     'name'    => 'logo',
            //     'contents' => fopen($logoPath, 'r'),
            //     'filename' => getRandomCode(5).'logo'.'.'.$logoExtension
            // ],
            // [
            //     'name'    => 'background',
            //     'contents' => fopen($backgroundPath, 'r'),
            //     'filename' => getRandomCode(5).'background'.'.'.$backgroundExtension
            // ],
         ];
         
         if (!empty($this->logo)) {
            $multipart[] = [
                'name'    => 'logo',
                'contents' => $logoPathType == "local" ? fopen($logoPath, 'r') : $logoPath,
                'filename' => getRandomCode(8).'.'.$logoExtension
            ];
         }

         if (!empty($this->background)) {
            $multipart[] = [
                'name'    => 'background',
                'contents' => $backgroundPathType == "local" ? fopen($backgroundPath, 'r') : $backgroundPath,
                'filename' => getRandomCode(8).'.'.$backgroundExtension
            ];
         }

        //  dd($multipart);

        $token = session('accessToken');
        $client = new Client();
        if (is_null($this->shop_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart,
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.shop.index")
                    ->with('success', 'Creation d\'un nouveau commerce réussie.');
                    
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'id' => $this->shop_id,
                    ],
                    'multipart' => $multipart,
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.shop.index")
                    ->with('success', 'Modification du commerce réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($shop = null) 
    {
        $this->getCategories();
        if (!is_null($shop)) {
            
            // dd($shop);
            $this->shop_id  = $shop['id'];

            // dd($shop['background']);

            $this->libelle              = $shop['libelle'];
            // $this->logo                 = $shop['logo'];
            // $this->background           = $shop['background'];
            $this->mountlogo            = $shop['logo']; 
            $this->mountbackground      = $shop['background']; 
            $this->contact              = $shop['contact'];
            $this->description          = $shop['description'];
            $this->adresse              = $shop['adresse'];
            $this->category_id          = $shop['category']['id'];
            $this->note                 = $shop['note'];
            $this->accept_delivery      = ($shop['accept_delivery']) ? "true" : "false";
            $this->accept_external_delivery     = ($shop['accept_external_delivery']) ? "true" : "false";

            $this->longitude                = $shop['location']['longitude'];
            $this->latitude                 = $shop['location']['latitude'];
            $this->provider                 = $shop['provider'];
            $this->fidelity_id              = $shop['fidelity_id'];
            $this->shop_cashback_id         = $shop['shop_cashback_id'];

            $this->button_text = "Modifier";
            $this->form_title = "Modifier un commerce";
            
        }
    }



    public function getCategories()
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('GET', env('API_URL').'/api/marketplace/category', [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'items' => 1000
                ]
            ]);            
            $res = json_decode($response->getBody()->getContents(), true);
            $this->categories = $res[0]['results'];
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error', 'Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.shop.form');
    }
}
