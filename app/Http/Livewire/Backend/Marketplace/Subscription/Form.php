<?php

namespace App\Http\Livewire\Backend\Marketplace\Subscription;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/subscription';
    
    public $companies;

    public $start_date;
    public $end_date;
    public $operator_load;
    public $platform_load;
    public $taxes;
    public $company;
    public $company_id = "null";

    
    public $form_title = "Création d'un nouveau contrat";
    public $button_text = "Créer";
    public $subscription_id = null;

    public function submitData() 
    {
        $this->validate([
            "start_date"=>['required', 'string'],
            "end_date"=>['required', 'string'],
            "operator_load"=>['required'],
            "platform_load"=>['required'],
            "taxes"=>['required'],
            "company_id"=> 'required|string|not_in:null',
        ]);

        // dd($this->company_id);

        $token = session('accessToken');
        $client = new Client();
        if (is_null($this->subscription_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'form_params' => [
                        "strat_date"=> $this->start_date,
                        "end_date"=> $this->end_date,
                        "operator_load"=> strval($this->operator_load),
                        "platform_load"=> strval($this->platform_load),
                        "taxes"=> strval($this->taxes),
                        "shop_id"=> $this->company_id,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.subscription.index")
                    ->with('success', 'Creation d\'une nouveau contrat réussie.');
                    
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'id' => $this->subscription_id,
                    ],
                    'form_params' => [
                        "strat_date"=> $this->start_date,
                        "end_date"=> $this->end_date,
                        "operator_load"=> strval($this->operator_load),
                        "platform_load"=> strval($this->platform_load),
                        "taxes"=> strval($this->taxes),
                        "shop_id"=> $this->company_id,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.subscription.index")
                    ->with('success', 'Modification du contrat réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

    }

    public function mount($subscription = null) 
    {
        $data;
        $rawdata;
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL').'/api/marketplace/shop', [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            // $data = array_filter($rawdata, function($object) {
            //     return !$object['is_active'];
            // });

            $this->companies = $rawdata;
            // dd($this->companies);

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue.']);
        }

        if (!is_null($subscription)) {
            
            // $data = array_filter($rawdata, function($object) {
            //     return $object['is_active'];
            // });
            // $this->companies = $data;

            // dd($subscription['company']);
            
            $this->subscription_id  = $subscription['id'];
            
            $this->start_date       = date('Y-m-d', strtotime($subscription['strat_date']));
            $this->end_date         = date('Y-m-d', strtotime($subscription['end_date']));
            $this->operator_load    = $subscription['operator_load'];
            $this->platform_load    = $subscription['platform_load'];
            $this->taxes    = $subscription['taxes'];
            $this->company         = $subscription['shop'];
            // $this->company_id        = $subscription['company'][0]['id'];

            $this->button_text = "Modifier";
            $this->form_title = "Modifier un contrat";

            // dd($this->company);
            
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.subscription.form');
    }
}
