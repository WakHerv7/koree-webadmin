<?php

namespace App\Http\Livewire\Backend\Marketplace\Article;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/articles';
    protected $addonurl = '/api/marketplace/articles/addons';

    public $categories;
    public $shop;

    public $logo = [];
    public $mountlogo;

    public $tags;
    public $libelle;
    public $description;
    public $item_unit;
    public $is_menu = "false";
    public $is_ticket = "false";
    public $stock_quantity;
    public $unit_per_price;
    public $product_category_id = "null";
    public $has_exp_date = "false";
    public $expiration_date = null;

    public $form_title = "Création d'un nouveau produit";
    public $button_text = "Créer";
    public $article = null;
    public $article_id = null;

    protected $messages = [
        'product_category_id.required' => 'Product category ID is required.',
        'product_category_id.string' => 'Product category ID must be a string.',
        'product_category_id.not_in' => 'Product category ID cannot be null.',
        'libelle.required' => 'Libelle is required.',
        'stock_quantity.required' => 'Stock quantity is required.',
        'unit_per_price.required' => 'Unit per price is required.',
        'product_category_id.required' => 'Product category ID is required.',
        'components.*.libelle.required' => 'Libelle is required for components.',
        'components.*.is_required.required' => 'Is required field is required for components.',
        'components.*.quantity.required' => 'Quantity is required for components.',
        'components.*.type.required' => 'Type is required for components.',
        'logo.*.image' => 'Logo must be an image.',
        'logo.*.mimes' => 'Logo must be a file of type: jpg, jpeg, png, svg, gif.',
        'logo.*.max' => 'Logo may not be greater than 2048 kilobytes.',

        // 'components.*.libelle.required' => 'Libelle is required for this component.',
        // 'components.*.is_required.required' => 'Is required field is required for this component.',
        // 'components.*.quantity.required' => 'Quantity is required for this component.',
        // 'components.*.type.required' => 'Type is required for this component.',
        // 'components.*.type.string' => 'Type must be a string.',
        // 'components.*.type.not_in' => 'Type cannot be null.',
        // 'components.*.description.nullable' => 'Description is optional for this component.',
    ];

    public function rules()
    {
        $rules = [
            "product_category_id" => 'required|string|not_in:null',
            "tags" => 'nullable',
            "libelle" => 'required',
            "description" => 'nullable',
            'item_unit' => 'nullable',
            "is_menu" => 'required',
            "is_ticket" => 'required',
            "stock_quantity" => 'required',
            "unit_per_price" => 'required',
            "product_category_id" => 'required',
            "expiration_date" => 'nullable',
            "logo.*" => 'nullable|image|mimes:jpg,jpeg,png,svg,gif|max:2048', // 2MB Max
        ];

        if ($this->is_menu == "true") {
            foreach ($this->components as $index => $input) {
                // $rules["components.$index.email"] = 'required|email';
                $rules["components.$index.libelle"] = 'required';
                $rules["components.$index.is_required"] = 'required';
                $rules["components.$index.quantity"] = 'required';
                $rules["components.$index.type"] = 'required|string|not_in:null';
                $rules["components.$index.description"] = 'nullable';
            }
        }

        return $rules;
    }

    public function submitData()
    {
        $this->validate();

        // $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        // $logoPathType = $this->logo ? "local" : "online";
        // $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);

        $token = session('accessToken');
        $client = new Client();


        // $tagsArray = array_map('trim', explode(';', $this->tags));

        // $tagParts = [];
        // foreach ($tagsArray as $tag) {
        //     $tagParts[] = [
        //         'name'     => 'tags[]',
        //         'contents' => $tag
        //     ];
        // }

        // dd(json_encode($tagsArray));

        $images = array_map(function ($item) {
            return [
                'name' => 'images',
                'contents' => fopen($item->getRealPath(), 'r'),
                'filename' => $this->libelle . 'logo' . '.' . $item->getClientOriginalName()
            ];
        }, $this->logo);



        $multipart = [
            [
                'name' => 'tags',
                'contents' => $this->tags,
            ],
            [
                'name' => 'libelle',
                'contents' => $this->libelle,
            ],
            [
                'name' => 'item_unit',
                'contents' => $this->item_unit,
            ],
            [
                'name' => 'description',
                'contents' => $this->description,
            ],
            [
                'name' => 'unit_per_price',
                'contents' => $this->unit_per_price,
            ],
            [
                'name' => 'is_menu',
                'contents' => $this->is_menu,
            ],
            [
                'name' => 'is_ticket',
                'contents' => $this->is_ticket,
            ],
            [
                'name' => 'stock_quantity',
                'contents' => strval($this->stock_quantity),
            ],
            [
                'name' => 'product_category_id',
                'contents' => strval($this->product_category_id),
            ],
            ...$images,
        ];

        //  if (!empty($this->logo) || !empty($this->mountlogo)) {
        //     $multipart[] = [
        //         'name'    => 'image',
        //         'contents' => $logoPathType == "local" ? fopen($logoPath, 'r') : $logoPath,
        //         'filename' => getRandomCode(8).'.'.$logoExtension
        //     ];
        //  }

        if ($this->has_exp_date == "true" && !empty($this->expiration_date)) {
            $multipart[] = [
                'name' => 'expiration_date',
                'contents' => $this->expiration_date,
            ];
        }

//          dd($multipart);

        $current_article_id = null;

        if (is_null($this->article_id)) {

            try {
                $response = $client->request('POST', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);

                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
                } else {
                    $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'message' => "Creation d'article réussie."]);
                    // return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])
                    // ->with('success', 'Creation d\'un produit réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } else {

            try {
                $response = $client->request('PUT', env('API_URL') . $this->url, [
                    'headers' => [
                        'Authorization' => 'Token ' . $token,
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'id' => $this->article_id,
                    ],
                    'multipart' => $multipart
                ]);

                $res = json_decode($response->getBody()->getContents(), true);

                // dd($res);

                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
                } else {
                    $current_article_id = $res[0]['results'][0]['id'];
                    $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'message' => "Modification d'article réussie."]);
                    // return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])
                    // ->with('success', 'Modification de produit réussie.');
                }
            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }
        }

        if ($this->is_menu == "true") {
            $components_to_create = array_filter($this->components, function ($comp) {
                return is_null($comp['id']);
            });
            $components_to_edit = array_filter($this->components, function ($comp) {
                return !is_null($comp['id']);
            });

            foreach ($components_to_create as $index => $comp) {

                $compMultipart = [
                    [
                        'name' => 'libelle',
                        'contents' => $comp['libelle'],
                    ],
                    [
                        'name' => 'required',
                        'contents' => $comp['is_required'],
                    ],
                    [
                        'name' => 'quantity',
                        'contents' => $comp['quantity'],
                    ],
                    [
                        'name' => 'type',
                        'contents' => $comp['type'],
                    ],
                    [
                        'name' => 'description',
                        'contents' => $comp['description'],
                    ],
                    [
                        'name' => 'article_id',
                        'contents' => $current_article_id,
                    ],
                ];
                try {
                    $response = $client->request('POST', env('API_URL') . $this->addonurl, [
                        'headers' => [
                            'Authorization' => 'Token ' . $token,
                            'Accept' => 'application/json',
                        ],
                        'multipart' => $compMultipart
                    ]);

                    $res = json_decode($response->getBody()->getContents(), true);

                    if ($res[0]['success'] == 0) {
                        $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
                    } else {
                        $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'message' => "Creation de composant réussie."]);
                        // return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])
                        // ->with('success', 'Creation d\'un produit réussie.');
                    }

                } catch (ServerException $e) {
                    $response = $e->getResponse();
                    $responseBodyAsString = $response->getBody()->getContents();
                    dd($responseBodyAsString);
                }
            }

            foreach ($components_to_edit as $index => $comp) {

                $compMultipart = [
                    [
                        'name' => 'libelle',
                        'contents' => $comp['libelle'],
                    ],
                    [
                        'name' => 'required',
                        'contents' => $comp['is_required'],
                    ],
                    [
                        'name' => 'quantity',
                        'contents' => $comp['quantity'],
                    ],
                    [
                        'name' => 'type',
                        'contents' => $comp['type'],
                    ],
                    [
                        'name' => 'description',
                        'contents' => $comp['description'],
                    ],
                    [
                        'name' => 'article_id',
                        'contents' => $current_article_id,
                    ],
                ];
                try {
                    $response = $client->request('PUT', env('API_URL') . $this->addonurl, [
                        'headers' => [
                            'Authorization' => 'Token ' . $token,
                            'Accept' => 'application/json',
                        ],
                        'query' => [
                            'id' => $comp['id'],
                        ],
                        'multipart' => $compMultipart
                    ]);

                    $res = json_decode($response->getBody()->getContents(), true);

                    if ($res[0]['success'] == 0) {
                        $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
                    } else {
                        $this->dispatchBrowserEvent('alertsuccess', ['type' => 'success', 'message' => "Modification de composant réussie."]);
                        // return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])
                        // ->with('success', 'Creation d\'un produit réussie.');
                    }

                } catch (ServerException $e) {
                    $response = $e->getResponse();
                    $responseBodyAsString = $response->getBody()->getContents();
                    dd($responseBodyAsString);
                }
            }
        }


        return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])->with('success', 'Opération réussie.');
    }

    public function mount($article = null, $addons = null, $shop = null)
    {
        $this->shop = $shop;

        $data;
        $rawdata;
        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL') . '/api/marketplace/product-categories', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $this->categories = $rawdata;
            // dd($this->companies);

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue.']);
        }

        if (!is_null($article)) {
            $this->article_id = $article['id'];
            // dd($article);
            // $this->tags = $article['tags'];
            $this->libelle = $article['libelle'];
            $this->description = $article['description'];
            $this->item_unit = $article['item_unit'];
            $this->is_menu = var_export($article['is_menu'], true);
            $this->is_ticket = var_export($article['is_ticket'], true);
            $this->stock_quantity = $article['stock_quantity'];
            $this->unit_per_price = $article['unit_per_price'];
            $this->product_category_id = $article['product_category'];
            $this->expiration_date = $article['expiration_date'];

            $this->has_exp_date = $article['expiration_date'] ? "true" : "false";

            $this->mountlogo = $article['image'];

            // dd(json_decode($article['tags']));
            $tagsText = implode('; ', array_map(function ($tag) {
                return $tag['name'];
            }, $article['tags']));

            $this->tags = $tagsText;


            $this->button_text = "Modifier";
            $this->form_title = "Modifier un produit";
        }

        if (!is_null($addons)) {
            $this->components = $addons;

            // dd($this->components);
        }
    }

    public function setArticleImage($article_id = null)
    {
        $token = session('accessToken');
        $client = new Client();

        $logoPath = $this->logo ? $this->logo->getRealPath() : $this->mountlogo;
        $logoPathType = $this->logo ? "local" : "online";
        $logoExtension = $this->logo ? $this->logo->getClientOriginalExtension() : pathinfo($this->mountlogo, PATHINFO_EXTENSION);


        $multipart = [
            [
                'name' => 'article_id',
                'contents' => $article_id,
            ],
        ];
        if (!empty($this->logo) || !empty($this->mountlogo)) {
            $multipart[] = [
                'name' => 'image',
                'contents' => $logoPathType == "local" ? fopen($logoPath, 'r') : $logoPath,
                'filename' => getRandomCode(8) . '.' . $logoExtension
            ];
        }

        //  dd($multipart);

        try {
            $response = $client->request('POST', env('API_URL') . $this->url . '/images', [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'article_id' => $article_id,
                ],
                'multipart' => $multipart
            ]);

            $res = json_decode($response->getBody()->getContents(), true);

            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route('marketplace.article.index', ['shop_id' => $this->shop['id']])
                    ->with('success', 'Creation d\'un produit réussie.');
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
        }
    }


    public $component_libelle;
    public $component_is_required;
    public $component_quantity;
    public $component_type_is_new = false;
    public $component_new_type;
    public $component_type_id;
    public $component_description;

    public $component_types = [
        ['id' => 'BOISSON', 'name' => 'BOISSON'],
        ['id' => 'SUPPLEMENT', 'name' => 'SUPPLEMENT'],
        ['id' => 'ACCOMPAGNEMENT', 'name' => 'ACCOMPAGNEMENT'],
    ];

    protected $component_rules = [];
    public $components = [
        [
            'id' => null,
            'libelle' => null,
            'is_required' => "false",
            'quantity' => null,
            'type' => "null",
            'article_id' => null,
            'description' => "",
            // 'type_is_new' => false,
            // 'new_type' => null,
        ],
    ];

    public function addComponent()
    {
        $new_component = [
            'id' => null,
            'libelle' => null,
            'is_required' => "false",
            'quantity' => null,
            'type' => "null",
            'article_id' => null,
            'description' => "",
            // 'type_is_new' => false,
            // 'new_type' => null,
        ];

        array_push($this->components, $new_component);
    }

    public function removeComponent($index)
    {
        unset($this->components[$index]);
    }

    public function handleAddonDeletion($id, $index)
    {

        getToken();
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('DELETE', env('API_URL') . $this->addonurl, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'id' => $id,
                ],
                // 'form_params' => [
                //     "is_active" => false,
                // ],
            ]);
            // dd($id);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  ' . $res[0]['errors'][0]['error_msg']]);
            } else {
                $this->dispatchBrowserEvent('alertsuccess', [
                    'type' => 'success',
                    'message' => 'Suppression du composant réussie'
                ]);
                unset($this->components[$index]);
            }

        } catch (ServerException $e) {

            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            // return back()->with('error','Failed to fetch data from the API');
        }


    }

    public function render()
    {
        return view('livewire..backend.marketplace.article.form');
    }
}
