<?php

namespace App\Http\Livewire\Backend\Marketplace\Article;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowList extends Component
{
    protected $url = '/api/marketplace/articles';

    public $data;
    public $shop;

    public function mount($data = null, $shop = null) 
    {
        $this->data = $data;
        $this->shop = $shop;
    }

    public function show($val) 
    {
        session(['currentArticle' => $val]);

        return redirect()->route('marketplace.article.show', ['id' => $val['id']]);
    }

    public function edit($val) 
    {
        session(['currentEdit' => $val]);

        return redirect()->route('marketplace.article.edit', ['id' => $val['id']]);
    }

    public function handleDeletion($id) 
    {
        
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'id' => $id,
                ],
                'form_params' => [
                    "is_active" => false,
                ],
            ]);
            // dd($id);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("marketplace.article.index", ['shop_id' => $this->shop['id']])
                ->with('success', 'Désactivation categorie produit réussie.');
            }

        } catch (ServerException $e) {
            
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            dd($responseBodyAsString);
            // return back()->with('error','Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.article.show-list');
    }
}
