<?php

namespace App\Http\Livewire\Backend\Marketplace\ShopSubCategory;

use Livewire\Component;
use Livewire\WithFileUploads;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7;

class Form extends Component
{
    use WithFileUploads;

    protected $url = '/api/marketplace/shop-subcategories';
  
    public $categories;
    public $shopsubcategories;
    public $category;
    public $category_id = "null";
    public $shop_id;
    
    public $form_title = "Création d'une nouvelle categorie de produit";
    public $button_text = "Créer";
    public $shop_sub_category_id = null;

    public function submitData() 
    {
        $this->validate([
            
            "category_id"=> 'required|string|not_in:null',
        ]);

        $token = session('accessToken');
        $client = new Client();

        if (is_null($this->shop_sub_category_id)) {

            try {
                $response = $client->request('POST',env('API_URL').$this->url, [
                    'headers' => [
                        'Authorization' => 'Token '.$token,
                        'Accept' => 'application/json',
                    ],
                    'form_params' => [
                        "shop_id"=> $this->shop_id,
                        "category_id"=> $this->category_id,
                    ],
                ]);

                $res = json_decode($response->getBody()->getContents(), true);
                
                if ($res[0]['success'] == 0) {
                    $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
                } else {
                    return redirect()->route("marketplace.shop-sub-category.index", ['shop_id' => $this->shop_id])
                    ->with('success', 'Creation d\'une categorie produit réussie.');
                }

            } catch (ServerException $e) {
                $response = $e->getResponse();
                $responseBodyAsString = $response->getBody()->getContents();
                dd($responseBodyAsString);
            }

        } 
        // else {

        //     try {
        //         $response = $client->request('PUT', env('API_URL') . $this->url, [
        //             'headers' => [
        //                 'Authorization' => 'Token ' . $token,
        //                 'Accept' => 'application/json',
        //             ],
        //             'query' => [
        //                 'id' => $this->shop_sub_category_id,
        //             ],
        //             'form_params' => [
        //                 "category_id"=> $this->category_id,
        //             ],
        //         ]);

        //         $res = json_decode($response->getBody()->getContents(), true);
                
        //         if ($res[0]['success'] == 0) {
        //             $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
        //         } else {
        //             return redirect()->route("marketplace.shop-sub-category.index")
        //             ->with('success', 'Modification de categorie produit réussie.');
        //         }
        //     } catch (ServerException $e) {
        //         $response = $e->getResponse();
        //         $responseBodyAsString = $response->getBody()->getContents();
        //         dd($responseBodyAsString);
        //     }
        // }

    }

    public function mount($category = null, $shopsubcategories=null) 
    {
        // $this->shopsubcategories = $shopsubcategories;

        $data;
        $rawdata;
        try {
            $client = new Client();
            $token = session('accessToken');

            $response = $client->request('GET', env('API_URL').'/api/marketplace/product-categories', [
                'headers' => [
                    'Authorization' => 'Token '.$token,
                    'Accept' => 'application/json',
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            $rawdata = $res[0]['results'];

            $filteredProducts = array_filter($rawdata, function($product) use ($shopsubcategories) {
                foreach ($shopsubcategories as $article) {
                    if ($article["article_category"] == $product["libelle"]) {
                        return false; // Exclude matching products
                    }
                }
                return true; // Include products with category mismatches
            });

            $this->categories = $filteredProducts;
            // dd($this->companies);

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue.']);
        }

        // if (!is_null($category)) {
        //     $this->category_id  = $category['id'];
        //     $this->button_text = "Modifier";
        //     $this->form_title = "Modifier une categorie produit";
        // }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.shop-sub-category.form');
    }
}
