<?php

namespace App\Http\Livewire\Backend\Marketplace\ShopSubCategory;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowListDeleted extends Component
{
    protected $url = '/api/marketplace/shop-subcategories';

    public $data;
    public $shop;

    public function mount($data = null, $shop = null) 
    {
        $this->data = $data;
        $this->shop = $shop;
    }


    public function handleActivation($id) 
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'id' => $id,
                ],
                'form_params' => [
                    "is_active" => true,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("marketplace.shop-sub-category.index", ['shop_id' => $this->shop['id']])
                ->with('success', 'Activation categorie produit réussie.');
            }

        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error','Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.shop-sub-category.show-list-deleted');
    }
}
