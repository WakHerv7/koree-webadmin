<?php

namespace App\Http\Livewire\Backend\Marketplace\Order;

use Livewire\Component;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

class ShowList extends Component
{
    protected $url = '/api/marketplace/orders/manage';

    public $data;
    public $shop_id;

    public function mount($data = null, $shop_id=null) 
    {
        $this->data = $data;
        $this->shop_id = $shop_id;
    }

    public function show($val) 
    {
        session(['currentOrder' => $val]);

        return redirect()->route('marketplace.order.show');
    }

    public function handleDeletion($id) 
    {
        try {
            $client = new Client();
            $token = session('accessToken');
            $response = $client->request('PUT',env('API_URL').$this->url, [
                'headers' => [
                    'Authorization' => 'Token ' . $token,
                    'Accept' => 'application/json',
                ],
                'query' => [
                    'order_id' => $id,
                ],
                'form_params' => [
                    "is_active" => false,
                ],
            ]);
            $res = json_decode($response->getBody()->getContents(), true);
            if ($res[0]['success'] == 0) {
                $this->dispatchBrowserEvent('alerterror', ['type' => 'error', 'message' => 'Une erreur est survenue:  '.$res[0]['errors'][0]['error_msg']]);
            } else {
                return redirect()->route("marketplace.order.index")
                ->with('success', 'Désactivation Commande réussie.');
            }
            
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            return back()->with('error','Failed to fetch data from the API');
        }
    }


    public function render()
    {
        return view('livewire..backend.marketplace.order.show-list');
    }
}
