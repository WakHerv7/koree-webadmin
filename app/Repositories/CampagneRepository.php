<?php

namespace App\Repositories;

use App\Models\QrCodeCampagne;
use App\Models\User;
use App\Models\Region;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\HtmlString;

class CampagneRepository extends ResourceRepository
{

    /**
     * @param User $qrCodeCampagne
     */
    public function __construct(QrCodeCampagne $qrCodeCampagne)
    {
        $this->model = $qrCodeCampagne;
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $user = QrCodeCampagne::find($id);
        $user->telephone = mt_rand(700000000, 780000000);
        $user->save();
        $this->getById($id)->delete();
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    public function generatePdf(QrCodeCampagne $campagne)
    {

    }

    public function generateSvgs(QrCodeCampagne $campagne)
    {

        $qrcodesImages = collect();


        foreach ($campagne->qrcodes as $qrcode) {

            /** @var HtmlString $img */
            $img = \QrCode::encoding("UTF-8")->size(150)
                ->backgroundColor(255, 55, 0)
                ->generate($qrcode->uuid);

            $qrcodesImages->push($img);

        }
        return $qrcodesImages->toArray();
    }

    public function generateQrcodesImg(QrCodeCampagne|Model $campagne)
    {

        $qrcodesImages = collect();

        $default_90_pixel_in_cm = 2.38125;
        $one_cm_pixel = 37.795276;

//        dd($campagne->toArray());

        foreach ($campagne->qrcodes as $qrcode) {
            $size = $one_cm_pixel * ($campagne->height_in_cm ?? $default_90_pixel_in_cm);
            $circle_shape_padding = $size * 0.17;

            /** @var HtmlString $img */
            $pngImg = \QrCode::encoding("UTF-8")->size($campagne->shape === 'circle' ? $size - $circle_shape_padding * 2 : $size)
//                ->backgroundColor(255,55,0)
                ->format('png')
                ->generate($qrcode->uuid);

            $img = base64_encode($pngImg);

            $imgHtmlTag = match ($campagne->shape) {
                default => "<img alt=\"hidden\" src=\"data:image/png;base64,$img\">",
                'circle' => "<div style='position:relative;background: red; height: {$size}px; width: {$size}px; border: 1px solid black; border-radius: 50%'>
                    <img alt=\"hidden\" style=\"height: auto; position: absolute; top: {$circle_shape_padding}px;left: {$circle_shape_padding}px;\" src=\"data:image/png;base64,$img\">
                 </div>",
            };

            $qrcodesImages->push($imgHtmlTag);

        }

        $bladeA4SizePixel = 21 * $one_cm_pixel;

        $chunkSize = intval($bladeA4SizePixel / ($size + 3));

        return $qrcodesImages->chunk($chunkSize)->toArray();
    }


}

