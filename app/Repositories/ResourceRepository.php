<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

abstract class ResourceRepository
{


    /**
     * @var Model
     */
    protected $model;

    /**
     * @param $n
     * @return mixed
     */
    public function getPaginate($n)
    {
        return $this->model->paginate($n);
    }

    /**
     * @param array $inputs
     * @return mixed
     */
    public function store(array $inputs)
    {
        return $this->model->create($inputs);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $id
     * @param array $inputs
     */
    public function update($id, array $inputs)
    {
        return $this->getById($id)->update($inputs);
    }

    public function update_password($id, $password)
    {
        return $this->getById($id)->update(['password' => Hash::make($password)]);
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    /**
     * @param $id
     * @return Builder
     */
    public function newQuery()
    {
        return $this->model->newQuery();
    }
    /**
     * @param $id
     * @return Builder
     */
    public function query()
    {
        return $this->model->query();
    }
}
