<?php

namespace App\Repositories\Frontend\V1;

use App\Models\Client;
use App\Models\Transaction;
use App\Models\User;
use App\Repositories\ResourceRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use DataTables;

class ClientRepository extends ResourceRepository
{

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->model = $client;
    }

    /**
     * Recupere les infos du user
     * @param $idUser
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByPhone($idUser)
    {
        return $this->model->with('user', 'ville', 'ville.pays')->where('user_id', $idUser)->first();
    }

    // Retourne les infos de profil dun client en fonction de son Id
    public function getProfil($userId)
    {
        return Client::with(['user', 'pays', 'ville'])->whereUserId($userId)->first();
    }

    public function getFilteredQuery(Request $request, $onlyDeleted = false)
    {

        $columns = array(
            '0' => 'code_parrain',
            '1' => 'code_parrainage',
            '2' => 'clients.nom',
            '3' => 'clients.prenom',
            '4' => 'users.telephone',
            '5' => 'users.email',
            '6' => 'clients.date_naissance',
            '7' => 'clients.sexe',
            '8' => 'clients.status',
            '9' => 'clients.country_id',
            '10' => 'clients.city_id',
            '11' => 'clients.quartier_id',
            '12' => 'clients.created_at',
        );


//        $query = Transaction::query()
        $query = DB::table('clients')
            ->leftJoin('users', 'users.id', '=', 'clients.user_id')
            ->leftJoin('parrainage', 'parrainage.filleul_id', '=', 'clients.id')
            ->leftJoin('countries', 'countries.id', '=', 'clients.country_id')
            ->leftJoin('quartiers', 'quartiers.id', '=', 'clients.quartier_id')
            ->leftJoin('cities', 'cities.id', '=', 'clients.city_id')
            ->leftJoin('deletion_requests', 'cities.id', '=', 'deletion_requests.customer_id')
            ->where('clients.status', (int)$request->query('status', '1'))
            ->when($request->input('start_date'), function ($query, $date) {
                $date = Carbon::parse($date)->toDateTimeString();
                return $query->whereDate('clients.created_at', '>=', $date);
            })
            ->when($request->input('end_date'), function ($query, $date) {
                $date = Carbon::parse($date)->toDateTimeString();
                return $query->whereDate('clients.created_at', '<=', $date);
            })
            ->when($request->input('search.value'), function ($query, $searchValue) {
                return $query->where(function ($query) use ($searchValue) {

                    return $query
                        ->orWhere('clients.nom', 'like', "%$searchValue%")
                        ->orWhere('clients.prenom', 'like', "%$searchValue%")
                        ->orWhere('users.telephone', 'like', "%$searchValue%");
                });
            })
            ->when($request->input('order.0.column', '8'), function ($query, $orderColumnNumber) use ($request, $columns) {
                $dir = $request->input('order.0.dir', 'desc');
                $columnName = $columns[$orderColumnNumber] ?? null;

                return $columnName ? $query->orderBy($columnName, $dir) : $query->orderByDesc('created_at', $dir);
            })
            ->whereNull('deletion_requests.id')
            ->selectRaw("
                            clients.id,
                            parrainage.code_parrainage as code_parrain,
                            parrainage.id as parrainnage_id,
                            clients.code_parrainage,
                            clients.nom,
                            clients.prenom,
                            users.telephone,
                            users.email as email,
                            clients.date_naissance,
                            clients.sexe,
                            clients.status,
                            countries.name as country,
                            cities.name as city,
                            quartiers.name as district,
                            clients.created_at,
                            clients.updated_at

            ");

        return $query;


    }

    public function getClientsDatatable(Request $request, $onlyDeleted = false)
    {

        $query = $this->getFilteredQuery($request, $onlyDeleted);

        return DataTables::queryBuilder($query)
            ->filter(function ($query) {
                // to disable filter
            })
            ->addColumn('password', function ($itemRow) {
                return null;
            })
            ->addColumn('created_at', function ($itemRow) {
                return Carbon::parse($itemRow->created_at)->format('Y-m-d H:i:s');
            })
            ->addColumn('country', function ($itemRow) {
                return $itemRow->country;
            })
            ->addColumn('city', function ($itemRow) {
                return $itemRow->city;
            })
            ->addColumn('district', function ($itemRow) {
                return $itemRow->district;
            })
            ->addColumn('status', function ($itemRow) {
                return '<span class="label label-lg font-weight-bold label-light-' . ($itemRow->status == 1 ? 'success' : 'danger') . ' label-inline">' . ($itemRow->status == 1 ? 'Actif' : 'Inatif') . '</span>';
            })
            ->addColumn('actions', function ($itemRow) {
                $actionEdit = '<a class="btn btn-sm btn-clean btn-icon mr-2" title="Edit detais" href="' . route('clients.edit', $itemRow->id) . '">
                            <i class="flaticon2-pen icon-md"></i>
                        </a>';

                $actionView = '<a class="btn btn-sm btn-clean btn-icon mr-2" title="Voir details" href="' . route('clients.show', $itemRow->id) . '">
                            <i class="flaticon-eye icon-md"></i>
                        </a>';

                return $actionView . $actionEdit;
            })
            ->toJson();

    }

    public function getInfluencerVariations($id)
    {
        return DB::table('influencer_changes')
            ->where('client_id', $id)
            ->orderByDesc('date')
            ->get();
    }

    public function getAll()
    {
        return $this->model->with('user', 'ville', 'pays', 'createdBy', 'updatedBy', 'parrain.parrain')->get();
    }

    public function getAllClientActif()
    {
        return $this->model->with('user', 'ville', 'pays', 'createdBy', 'updatedBy', 'parrain.parrain')->where('status', '=', 1)->get();
    }

    public function getAllClientInActif()
    {
        return $this->model->with('user', 'ville', 'pays')->where('status', '=', 0)->paginate(10000);
    }

    public function getClient($id)
    {
        return $this->model->with('user', 'ville', 'pays')->where('id', '=', $id)->first();
    }

    public function paginateAllInfluencers($per_page = 100): LengthAwarePaginator
    {
        return $this->queryInfluencers()
            ->latest()->paginate($per_page);
    }

    public function getAllInfluencers()
    {
        return $this->queryInfluencers()
            ->latest()->get();
    }

    public function queryInfluencers(): Builder
    {
        return $this->model->whereNotNull('influencer')
            ->where('influencer', '=', '1')
            ->with('user')
            ->withCount('parrainages')
            ->withCount('parrainages_nonactives')
            ->withCount('parrainages_actives');
    }

    /**
     * @param $id
     * @param array $inputs
     */
    public function updateClient($id, array $inputs)
    {
        return $this->model->where('user_id', $id)->update($inputs);
    }

    // retourne les X dernieres transactions dun vendeur
    public function getClientSelect2($request, $page = 0, $size = 10)
    {
        $requestAjax = $request->all();
        $page = isset($requestAjax["page"]) ? intval($requestAjax["page"]) : $page;

        $search = $requestAjax["q"];
        $offset = $page * $size;
        $sqlClient = "select clt.id,concat(clt.nom, ' ',clt.prenom) full_name,usr.telephone
                        from clients clt,users usr
                        where clt.user_id=usr.id and (clt.nom like '%$search%' or clt.prenom like '%$search%')
                        order by full_name limit  $size offset $offset ";
        $result = DB::select($sqlClient);

        return json_decode(json_encode($result), true);
    }
}

