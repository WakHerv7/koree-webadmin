<?php

namespace App\Repositories\Frontend\V1;

use App\Models\PointVente;
use App\Models\User;
use App\Models\Vendeur;
use App\Models\WalletClient;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Facades\DB;

class VendeurRepository extends ResourceRepository
{

    /**
     * @param User $user
     */
    public function __construct(Vendeur $user)
    {
        $this->model = $user;
    }

    /**
     * Recupere les infos du user
     * @param $idUser
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getByPhone($idUser)
    {
        return $this->model->with('user', 'ville', 'ville.pays')->where('user_id', $idUser)->first();
    }

    public function getProfil($idUser)
    {
        $datas = $this->model->with('user', 'ville.pays', 'commerce.categorie.parent', 'commerce.quartier', 'programme'/*'commerce.pointVentes','commerce.pointVentes.agents'*/)->where('user_id', $idUser)->first();
        // on verifie sil ya des points de ventes

        if ($datas->commerce != null) {
            $datas['points_ventes'] = PointVente::where('commerce_id', '=', $datas->commerce->id)->with('agents')->get() ?: [];
            if ($datas->commerce->programme) {
                $wallets = WalletClient::query()->where('programme_id', $datas->commerce->programme->id)->get() ?: [];
                $datas['programme']['wallets'] = $wallets;
            }
        }
        return $datas;
    }


    // Retourne les infos de profil dun client en fonction de son Id
    public function getProfile($userId)
    {

        $res = DB::table('users')
            ->select('users.id', 'users.username', 'users.email', 'users.telephone', 'users.type_user',
                'users.created_at', 'vendeurs.id as vendeur_id', 'vendeurs.nom as nom', 'vendeurs.adresse', 'vendeurs.status',
                'vendeurs.code_postal', 'vendeurs.tel_fixe', 'vendeurs.rccm', 'vendeurs.contact_compte', 'vendeurs.fonction', 'cities.id as ville_id',
                'cities.name as ville', 'countries.id as pays_id', 'countries.name as pays',
                'countries.iso3 as pays_iso3', 'countries.iso2 as pays_iso2', 'countries.phonecode as pays_phonecode')
            ->leftJoin('vendeurs', 'users.id', '=', 'vendeurs.user_id')
            ->join('cities', 'cities.id', '=', 'vendeurs.city_id')
            ->join('countries', 'countries.id', '=', 'cities.country_id')
            ->where('users.id', '=', $userId)
            //->where('client.actif', '=', 1)
            ->first();

        return $res;

    }

    public function getAllFree()
    {
        // on recupere tous les vendeurs dans commerce
        $tbIdCom = DB::table('commerces')->select('vendeur_id')->get();
        $dtId = [];
        foreach ($tbIdCom as $i) {
            //dd($i);
            $dtId[] = $i->vendeur_id;
        }
        return $this->model->whereNotIn('id', $dtId)->get();

    }

    /**
     * @param $id
     * @param array $inputs
     */
    public function updateVendeur($id, array $inputs)
    {
        return $this->model->where('user_id', $id)->update($inputs);
    }

    public function getAllVendeurActif()
    {
        return $this->model->with('user', 'ville', 'pays')->where('status', '=', 1)->paginate(100);
    }

    public function getVendeur($id)
    {
        return $this->model->with('user', 'ville', 'pays')->where('id', '=', $id)->first();
    }

}

