<?php

namespace App\Repositories\Frontend\V2;

use App\Helpers\StringHelper;
use App\Models\Client;
use App\Models\User;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Str;

class ClientRepository extends ResourceRepository
{

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->model = $client;
    }

    // Retourne les infos de profil dun client en fonction de son Id
    public function getProfil($userId)
    {
        return Client::with(['user', 'pays', 'ville'])->whereUserId($userId)->first();
    }


    public function getAllClientActif()
    {
        return $this->model->with('user', 'ville', 'pays', 'createdBy', 'updatedBy')->get();
    }

    public function getAllClientInActif()
    {
        return $this->model->with('user', 'ville', 'pays')->where('status', '=', 0)->paginate(100);
    }

    public function getClient($id)
    {
        return $this->model->with('user', 'ville', 'pays')->where('id', '=', $id)->first();
    }

    /**
     * @param $id
     * @param array $inputs
     */
    public function updateClient($id, array $inputs)
    {
        return $this->model->where('user_id', $id)->update($inputs);
    }


    public function generateReferralCode($lastname, $firstname, $phone): string
    {
        $code1Name = trim(Str::substr($firstname, 0, 4));
        $code1Phone = Str::substr($phone, -2, 2);

        $code1 = StringHelper::transformString(Str::padRight(Str::upper(Str::padRight($code1Name, 4, "0") . $code1Phone), 6, Str::substr($phone, -1, 1)));

        $code2Name = trim(Str::substr($lastname, 0, 4));
        $code2Phone = Str::substr($phone, -4, 2);

        $code2 = StringHelper::transformString(Str::padRight(Str::upper(Str::padRight($code2Name, 4, "0") . $code2Phone), 6, Str::substr($phone, -3, 1)));

        if (!$this->model->where('code_parrainage', $code1)->exists()) {
            return $code1;
        }

        if (!$this->model->where('code_parrainage', $code2)->exists()) {
            return $code2;
        }

        do {
            $code3 = StringHelper::transformString(Str::padRight(Str::upper(Str::random(4) . rand(10, 100)), 6, "0"));
        } while (Client::where('code_parrainage', $code3)->exists());
        return $code3;

    }

}

