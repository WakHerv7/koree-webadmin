<?php

namespace App\Repositories\Backend\V2;

use App\Models\CategoryCommerce;
use App\Models\Vendeur;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class VendeursRepository extends ResourceRepository
{

    /**
     * @param Vendeur $vendeur
     */
    public function __construct(Vendeur $ven)
    {
        $this->model = $ven;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
     *
     * @return Collection
     */
    public function getAllFree()
    {
        // on recupere tous les vendeurs dans commerce
        $tbIdCom = DB::table('commerces')->select('vendeur_id')->get();
        $dtId = [];


        return $this->model->whereNotIn('id', $tbIdCom)->get();

    }

    /**
     *
     * @return Collection
     */
    public function getCommerceFromId($id)
    {
        return $this->model->where('id', '=', $id)->with('categorie', 'vendeur', 'ville.pays')->first();
    }

    public function getAllCategories()
    {
        return CategoryCommerce::orderBy('libelle', 'asc')->get();
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }


}
