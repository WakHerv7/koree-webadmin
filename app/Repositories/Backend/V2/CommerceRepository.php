<?php

namespace App\Repositories\Backend\V2;

use App\Models\CategoryCommerce;
use App\Models\Commerce;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class CommerceRepository extends ResourceRepository
{

    /**
     * @param Commerce $commerce
     */
    public function __construct(Commerce $commerce)
    {
        $this->model = $commerce;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
     *
     * @return Collection
     */
    public function getAllActif()
    {
        return $this->model->with('categorie','vendeur.user','ville.pays','programme.wallets','pointVentes')->get();
    }

    /**
     *
     * @return Collection
     */
    public function getCommerceFromId($id)
    {
        return $this->model->where('id','=',$id)->with('categorie','vendeur','vendeur.user','quartier','ville.region.pays','programme','pointVentes')->first();
    }

    public function getAllCategories(){
        return CategoryCommerce::orderBy('libelle','asc')->get();
    }


    public function getAllCommerceFromPays($paysId){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.ville', function ( $query) use ($paysId) {
            $query->where('country_id', '=', $paysId);
        })->get();
    }

    public function getAllCommerceFromVille($villeId){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.ville', function ( $query) use ($villeId) {
            $query->where('city_id', '=', $villeId);
        })->get();
    }


    public function getAllBoostCommerceFromPays($paysId=null){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.pays', function ( $query) use ($paysId) {
            $query->where('country_id', '=', $paysId);
        })->where('is_boost','=',1)->get()/*->paginate(500)*/;
    }

    public function getAllBoostCommerces(){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->where('is_boost','=',1)->get()/*->paginate(500)*/;
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function setNoteCommerce($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getAllFree()
    {
        // on recupere tous les commerce dans programmes
        $tbIm= DB::table('programmes')->select('commerce_id')->get();
        $dtId=[];
        foreach ( $tbIm as $i) {
            //dd($i);
            $dtId[]=$i->commerce_id;
        }

        if(count($dtId)>0){
            //dd($tbIm);
            return $this->model->whereNotIn('id', $dtId)->get();
        }else{
            return array();
        }
    }

}
