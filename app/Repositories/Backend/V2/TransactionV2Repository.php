<?php

namespace App\Repositories\Backend\V2;

use App\Models\Achat;
use App\Models\Agent;
use App\Models\Transaction;
use App\Repositories\Backend\V1\TransactionRepository;
use App\Repositories\ResourceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class TransactionV2Repository extends ResourceRepository
{


    /**
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->active()->orderByDesc('created_at')->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getVendeurLastTransactions($Wallet_vendeurId, ?Agent $agent = null, $limite = 10)
    {
        $data = Transaction::where('wallet_vendeur_id', $Wallet_vendeurId)->orderBy('created_at', 'desc');
        if ($agent != null && !$agent->is_admin) {
            $data = $data->where('agent_id', $agent->id);
        }
        return $data->active()->limit($limite)->get();
    }

    public function getVendeurLatestTransactions($Wallet_vendeurId, ?Agent $agent = null,)
    {
        $payments = Transaction::active()->whereTypeTransaction('paiement')->where('wallet_vendeur_id', $Wallet_vendeurId);
        $topups = Transaction::active()->whereTypeTransaction('topup')->where('wallet_vendeur_id', $Wallet_vendeurId);
        $cashbacks = Transaction::active()->whereTypeTransaction('cashback')->where('wallet_vendeur_id', $Wallet_vendeurId);
        $bonuses = Transaction::active()->whereIn('type_transaction', ['parrainage', 'first_buy'])->where('wallet_vendeur_id', $Wallet_vendeurId);

        if ($agent != null && !$agent->is_admin) {
            $payments = $payments->whereAgentId($agent->id);
            $topups = $topups->whereAgentId($agent->id);
            $cashbacks = $cashbacks->whereAgentId($agent->id);
            $bonuses = $bonuses->whereAgentId($agent->id);
        }

        $payments = $payments->latest()->limit(10)->get();
        $topups = $topups->latest()->limit(10)->get();
        $cashbacks = $cashbacks->latest()->limit(10)->get();
        $bonuses = $bonuses->latest()->limit(10)->get();
        return $payments->merge($topups)->merge($cashbacks)->merge($bonuses)->sortByDesc('created_at');
    }

    // retourne les X dernieres transactions dun vendeur
    public function getLastTransactions($limite = 10)
    {
        //return $this->model->where('wallet_vendeur_id', $Wallet_vendeurId)->orderBy('created_at','desc')->limit($limite)->get();
        return DB::table('transactions')->orderBy('created_at', 'desc')->limit($limite)->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getClientLastXTransactions($Wallet_clientId, $nbre = -1)
    {
        $data = $this->model->active()->where('wallet_id', $Wallet_clientId)->orderBy('created_at', 'desc');
        if ($nbre != -1)
            $data = $data->limit($nbre);
        return $data->get();
    }

    // on retourne les statistique de transaction dun commerce
    public function getSellerStatistique($Wallet_Id, ?Agent $agent, $type_transaction = 'cashback', $periode = 'annee')
    {
        $in = $type_transaction == 'parrainage' ? ['parrainage', 'first_buy'] : [$type_transaction];
        if ($periode == 'jour') {
            $fromDate = Carbon::now()->startOfMonth()->toDateString();
            $tillDate = Carbon::now()->endOfMonth()->toDateString();
            $data = $this->model
                ->active()
                ->where('wallet_vendeur_id', $Wallet_Id)
                ->whereIn('type_transaction', $in)
                ->whereBetween('created_at', [$fromDate, $tillDate])
                ->without(['commerce', 'wallet_vendeur'])
                ->orderBy('created_at');
        } else {
            $data = $this->model
                ->active()
                ->where('wallet_vendeur_id', $Wallet_Id)
                ->whereIn('type_transaction', $in)
                ->whereYear('created_at', date('Y'))
                ->without(['commerce', 'wallet_vendeur'])
                ->orderBy('created_at');
        }
        if ($agent != null && !$agent->is_admin) {
            $data = $data->where('agent_id', $agent->id);
        }

        return $data->limit(5000)
            ->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getForLastYear()
    {
        return $this->model->active()->orderBy('created_at', 'desc')->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getForLastYearLimits($limit = 100)
    {
        $transactions = $this->model->active()->whereYear('created_at', date('Y'))->orderBy('created_at', 'desc');
        if ($limit != -1) {
            $transactions = $transactions->limit($limit);
        }
        return $transactions->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getRevenuForLastYearLimits($limit = 100)
    {
        $transactions = $this->model->active()->whereYear('created_at', date('Y'))->where('gain', '<>', 0)->orderBy('created_at', 'desc');
        if ($limit != -1) {
            $transactions = $transactions->limit($limit);
        }
        return $transactions->get();
    }

    public function getByAchat(?Achat $achat)
    {
        if ($achat == null) return [];
        return Transaction::whereAchatId($achat->id)->orderByDesc('created_at')->get();
    }


}
