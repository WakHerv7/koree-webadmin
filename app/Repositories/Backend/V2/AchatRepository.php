<?php

namespace App\Repositories\Backend\V2;

use App\Models\Achat;
use App\Repositories\ResourceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;


class AchatRepository extends ResourceRepository
{

    /**
     * @param Achat $pack
     */
    public function __construct(Achat $pack)
    {
        $this->model = $pack;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }


    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    // on retourne les statistique de transaction dun commerce
    public function getTotalAchatsMois()
    {

        $data = $this->model->whereMonth('created_at', Carbon::now())->sum('montant_paye');

        return $data;
    }

    // retourne les X dernieres transactions dun vendeur
    public function getForLastYearLimits($limit = 100)
    {
        return $this->model->with('commerce', 'client')->whereYear('created_at', date('Y'))->orderBy('created_at', 'desc')->limit($limit)->get();
    }


}
