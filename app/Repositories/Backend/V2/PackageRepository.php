<?php

namespace App\Repositories\Backend\V2;

use App\Models\CategoryCommerce;
use App\Models\PackageBoost;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class PackageRepository extends ResourceRepository
{

    /**
     * @param PackageBoost $pack
     */
    public function __construct(PackageBoost $pack)
    {
        $this->model = $pack;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('pays')->get();
    }

    public function getForId($id)
    {
        return $this->model->with('pays')->where('id', $id)->first();
    }

    public function getAllCategories()
    {
        return CategoryCommerce::orderBy('libelle', 'asc')->get();
    }


    public function getAllPackFromPays($paysId)
    {
        return $this->model->where('country_id', $paysId)->get();
    }


    public function getAllCommerceFromVille($villeId)
    {
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie', 'vendeur')->whereHas('vendeur.ville', function ($query) use ($villeId) {
            $query->where('id', '=', $villeId);
        })->get()/*->paginate(500)*/ ;
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->with('villes')->where('id', $id)->first();
    }


}
