<?php

namespace App\Repositories\Backend\V2;

use App\Models\Notes;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class NoteRepository extends ResourceRepository
{

    /**
     * @param Notes $note
     */
    public function __construct(Notes $note)
    {
        $this->model = $note;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
     *
     * @return Collection
     */
    public function getAllDetails()
    {
        return $this->model->with('client', 'commerce')->get();
    }


    /**
     * @param $dt
     * @return mixed
     * @see permet de set une note a un commerce
     */
    public function setNoteCommerce($dt)
    {
        $tab = '';
        $deja = Notes::where('client_id', '=', $dt['client_id'])->where('commerce_id', $dt['commerce_id'])->first();
        if ($deja) {
            $deja->note = $dt['note'];
            $deja->save();

            $tab = "ce client a déjà noté ce commerce. Note mise à jour.";
        } else {
            $user = Notes::create([
                'client_id' => $dt['client_id'],
                'commerce_id' => $dt['commerce_id'],
                'note' => $dt['note'],
                'created_at' => now(),
            ]);
            $tab = "Commerce noté";
        }

        return $tab;
    }

}
