<?php

namespace App\Repositories\Backend\V2;

use App\Models\WalletVendeur;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class WalletVendeurRepository extends ResourceRepository
{

    /**
     * @param WalletVendeur $wallet
     */
    public function __construct(WalletVendeur $wallet)
    {
        $this->model = $wallet;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('programme', 'vendeur')->paginate(100);
    }

    public function getByVendeurId($id)
    {
        return $this->model->where('vendeur_id', '=', $id)->with(['vendeur', 'bonusTransactions'])->first();
    }

    public function getByProgrammeId($id)
    {
        return $this->model->where('programme_id', '=', $id)->with('programme', 'vendeur')->first();
    }


}
