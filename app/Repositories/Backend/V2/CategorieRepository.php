<?php

namespace App\Repositories\Backend\V2;

use App\Models\CategoryCommerce;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class CategorieRepository extends ResourceRepository
{

    /**
     * @param CategoryCommerce $note
     */
    public function __construct(CategoryCommerce $note)
    {
        $this->model = $note;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }


}
