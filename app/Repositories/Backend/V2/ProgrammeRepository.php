<?php

namespace App\Repositories\Backend\V2;

use App\Models\Programme;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class ProgrammeRepository extends ResourceRepository
{

    /**
     * @param Programme $programme
     */
    public function __construct(Programme $programme)
    {
        $this->model = $programme;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('commerce', 'commerce.ville.pays')->get();
    }

    public function getAllProgrammeFromPays($paysId)
    {
        return $this->model->with('commerce')->whereHas('commerce.ville', function ($query) use ($paysId) {
            $query->where('country_id', '=', $paysId)->orWhereRaw('country_id', '=', strtolower($paysId));
        })->paginate(50);
    }


    public function getAllPopularProgrammeFromPays($paysId)
    {
        return $this->model->whereHas('commerce.ville', function ($query) use ($paysId) {
                $query->where('country_id', '=', $paysId)->orWhereRaw(sprintf("LOWER(country_code)='%s'", strtolower($paysId)));
            })
            ->withCount('wallets')->orderByDesc('wallets_count')->limit(3)->get();
    }



    // recupere les programmes dune ville
    public function getAllProgrammeFromVille($villeId)
    {
        return $this->model->with('commerce')->whereHas('commerce.ville', function ($query) use ($villeId) {
            $query->where('id', '=', $villeId);
        })->paginate(50);
    }


    public function getById($id)
    {
        return $this->model->with('commerce')->where('id', $id)->first();
    }


}
