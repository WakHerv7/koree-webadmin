<?php

namespace App\Repositories\Backend\V1;

use App\Models\Pays;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class PaysRepository extends ResourceRepository
{

    /**
     * @param Pays $pays
     */
    public function __construct(Pays $pays)
    {
        $this->model = $pays;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    public function getListePays()
    {
        return $this->model->select('id', 'name', 'iso3', 'iso2', 'phonecode', 'capital', 'currency')->orderBy('name', 'ASC')->get();
    }

    public function getByName($name)
    {
        return $this->model->with('regions')->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->with('regions')->where('id', $id)->first();
    }

    public function getByIds($lesid)
    {
        return $this->model->with('regions')->whereIn('id', $lesid)->get();
    }

    public function getByIso3($iso)
    {
        return $this->model->where('iso3', $iso)->first();
    }

    // retourne laliste des ville dun pays
    public function getVilleFromPaysId($id)
    {
        return $this->model->with('villes')->where('id', $id)->get();
    }


}
