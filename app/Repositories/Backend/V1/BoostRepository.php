<?php

namespace App\Repositories\Backend\V1;

use App\Models\Boost;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class BoostRepository extends ResourceRepository
{

    /**
     * @param Boost $commerce
     */
    public function __construct(Boost $bt)
    {
        $this->model = $bt;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }


    public function getAllBoostCommerceFromPays($paysId = null)
    {
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie', 'vendeur')->whereHas('vendeur.pays', function ($query) use ($paysId) {
            $query->where('country_id', '=', $paysId);
        })->where('is_boost', '=', 1)->get()/*->paginate(500)*/ ;
    }

    public function getAllBoostCommerces()
    {
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model/*->select(DB::raw('id, ordre, date_debut, date_fin, status, distinct commerce_id, package_id'))*/
        ->with('package', 'commerce', 'package.pays')->whereHas('commerce', function ($query) {
            $query->where('is_boost', '=', 1);
        })->where('status', '=', 1)->orderBy('created_at', 'asc')->get();
    }

    public function getHistoriqueBoostCommerces()
    {
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('package', 'commerce', 'package.pays')->orderBy('created_at', 'desc')->get();
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->with('villes')->where('id', $id)->first();
    }


}
