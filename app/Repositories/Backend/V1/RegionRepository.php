<?php

namespace App\Repositories\Backend\V1;

use App\Models\Region;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class RegionRepository extends ResourceRepository
{

    /**
     * @param Region $region
     */
    public function __construct(Region $region)
    {
        $this->model = $region;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->with('villes')->where('id', $id)->first();
    }

    public function getByCountryId($id)
    {
        return $this->model->where('country_id', $id)->get();
    }


}
