<?php

namespace App\Repositories\Backend\V1;

use App\Models\Parrainage;
use App\Repositories\ResourceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;


class ParrainageRepository extends ResourceRepository
{

    /**
     * @param Parrainage $parrainage
     */
    public function __construct(Parrainage $parrainage)
    {
        $this->model = $parrainage;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }


    public function getAllBoostCommerceFromPays($paysId = null)
    {
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie', 'vendeur')->whereHas('vendeur.pays', function ($query) use ($paysId) {
            $query->where('country_id', '=', $paysId);
        })->where('is_boost', '=', 1)->get()/*->paginate(500)*/ ;
    }

    public function getMesFilleuls($id)
    {
        return $this->model->where('parrain_id', $id)->with('filleul', 'filleul.user')->get();
    }

    public function getFromLastMonth()
    {
        return $this->model->whereMonth('created_at', '>=', Carbon::now()->subMonth(1))->orderBy('created_at', 'asc')->get();
    }

}
