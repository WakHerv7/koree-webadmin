<?php

namespace App\Repositories\Backend\V1;

use App\Models\CategoryCommerce;
use App\Models\Commerce;
use App\Repositories\ResourceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class CommerceRepository extends ResourceRepository
{

    /**
     * @param Commerce $commerce
     */
    public function __construct(Commerce $commerce)
    {
        $this->model = $commerce;
    }

    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
     *
     * @return Collection
     */
    public function getAllActif()
    {
        return $this->model->with('categorie','vendeur.user','ville.pays','programme.wallets','pointVentes')
                            ->orderBy('created_at', 'desc')
                            ->where('status', '=', true)
                            ->get();
    }

    /**
     *
     * @return Collection
     */
    public function getAllInacActif()
    {
        return $this->model->with('categorie','vendeur.user','ville.pays','programme.wallets','pointVentes')
                            ->orderBy('created_at', 'desc')
                            ->where('status', '=', false)
                            ->get();
    }

    /**
     *
     * @return Collection
     */
    public function getCommerceFromId($id)
    {
        $commerce = $this->model->where('id','=',$id)->with('categorie','vendeur','vendeur.user','quartier','ville.region.pays','programme','pointVentes')->first();
        if ( ! $commerce->programme->is_cashback  && $commerce->programme->cashback_over_topup ) {
            $commerce->programme->is_cashback  = True;
            $commerce->save();
        }
        return $commerce;
    }

    public function getAllCategories(Request $request){
        $parent_id = $request->parent_id;
        return CategoryCommerce::query()->orderBy('libelle','asc')
            ->when($parent_id, function ($q, $parent_id) {
                return $q->where('parent_id', $parent_id);
            })->get();
    }


    public function getAllCommerceFromPays($paysId){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.ville', function ( $query) use ($paysId) {
            $query->whereIn('country_id', $paysId);
        })->get();
    }

    public function getAllCommerceFromVille($villeId){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.ville', function ( $query) use ($villeId) {
            $query->where('city_id', '=', $villeId);
        })->get();
    }


    public function getAllBoostCommerceFromPays($paysId=null){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->whereHas('vendeur.pays', function ( $query) use ($paysId) {
            $query->where('country_id', '=', $paysId);
        })->where('is_boost','=',1)->get()/*->paginate(500)*/;
    }

    public function getAllBoostCommerces(){
        //return $this->model->with('vendeur','')->where('id', $id)->first();
        return $this->model->with('categorie','vendeur')->where('is_boost','=',1)->get()/*->paginate(500)*/;
    }


    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function setNoteCommerce($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getAllFree()
    {
        // on recupere tous les commerce dans programmes
        $tbIm= DB::table('programmes')->select('commerce_id')->get();
        $dtId=[];
        foreach ( $tbIm as $i) {
            //dd($i);
            $dtId[]=$i->commerce_id;
        }

        if(count($dtId)>0){
            //dd($tbIm);
            return $this->model->whereNotIn('id', $dtId)->get();
        }else{
            return array();
        }
    }

    // retourne les X dernieres transactions dun vendeur
    public function getCommerceSelect2($request, $page=0, $size=10)
    {
        $requestAjax = $request->all();
        $page = isset($requestAjax["page"]) ? intval($requestAjax["page"]) : $page;

        $search = $requestAjax["q"];
        $sqlClient = 'select vd.id, com.id as commerce_id, com.nom as  commerce_nom , concat( com.nom , "/", vd.nom, " ",vd.prenom) full_name,usr.telephone
                        from commerces com, vendeurs vd,users usr
                        where  com.vendeur_id =  vd.id and vd.user_id=usr.id and (com.nom like "%' . $search .'%" or vd.nom like "%' . $search .'%" or vd.prenom like "%' . $search .'%")
                        order by full_name, commerce_nom
                        limit '.$size.' offset ' . ($page*$size) ;
        $result = DB::select($sqlClient);

        return json_decode(json_encode($result), true);
    }
}
