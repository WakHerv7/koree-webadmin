<?php

namespace App\Repositories\Backend\V1;

use App\Models\Pays;
use App\Models\Ville;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class VilleRepository extends ResourceRepository
{

    /**
     * @param Ville $ville
     */
    public function __construct(Ville $ville)
    {
        $this->model = $ville;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getVilleFromPaysId($idList)
    {


        if (is_string($idList)) {
            $idList = explode(',', strtoupper($idList));
        }
        $country_ids = Pays::query()->whereIn('iso2', $idList)->orWhereIn("id", $idList)->pluck('id')->toArray();

        return $this->model->with('quartiers')->whereIn('country_id', $country_ids)
            ->get();

    }

    public function getVilleQuartiersFromPaysId($idList)
    {
        if (is_string($idList)) {
            $idList = explode(',', strtoupper($idList));
        }

        $country_ids = Pays::query()->whereIn('iso2', $idList)->orWhereIn("id", $idList)->pluck('id')->toArray();

        return $this->model->with('pays', 'region', 'quartiers')->whereIn('country_id', $country_ids)
            ->get();
    }

    public function getByRegionId($id)
    {
        return $this->model->where('state_id', $id)->get();
    }

    public function getByArrayRegionId($arrayRegionId)
    {
        return $this->model->whereIn('state_id', $arrayRegionId)->get();
    }

    public function getByNameAndCountry($ville, $pays_id)
    {
        return $this->model->where([['name', $ville], ['country_id', $pays_id]])->first();
    }

    public function getByNameAndStateId($ville, $state_id)
    {
        return $this->model->where([['name', $ville], ['state_id', $state_id]])->first();
    }

}
