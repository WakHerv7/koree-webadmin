<?php

namespace App\Repositories\Backend\V1;

use App\Models\PointVente;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;


class PointVenteRepository extends ResourceRepository
{

    /**
     * @param PointVente $pack
     */
    public function __construct(PointVente $pack)
    {
        $this->model = $pack;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('commerce')->get();
    }

    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->with('commerce')->where('id', $id)->first();
    }


}
