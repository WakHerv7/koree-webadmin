<?php

namespace App\Repositories\Backend\V1;

use App\Models\Agent;
use App\Repositories\ResourceRepository;
use Illuminate\Support\Collection;

class AgentRepository extends ResourceRepository
{

    /**
     * @param Agent $agent
     */
    public function __construct(Agent $agent)
    {
        $this->model = $agent;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('pointVente')->get();
    }

    public function getByName($name)
    {
        return $this->model->where('name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function checkExistCode($code){
        $instanceAgent = $this->model->where('code', $code)->first();
        if ($instanceAgent === null){
            return false;
        } else {
            return true;
        }
    }


}
