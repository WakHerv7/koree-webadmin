<?php

namespace App\Repositories\Backend\V1;

use App\Events\Pusher\NewClientTransaction;
use App\Events\Pusher\NewVendorTransaction;
use App\Helpers\TransactionHelper;
use App\Models\Agent;
use App\Models\Commerce;
use App\Models\Programme;
use App\Models\Transaction;
use App\Models\WalletClient;
use App\Models\WalletVendeur;
use App\Notifications\PaymentClientNotification;
use App\Notifications\PaymentVendorNotification;
use App\Repositories\ResourceRepository;
use App\Services\FraudModeration;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class TransactionRepository extends ResourceRepository
{


    /**
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }


    public function checkPaiementFraud(Programme $programme, WalletVendeur $walletVendeur, int $id_fidelite, int $totalAmount, int $orderAmount)
    {

        $t = new TransactionHelper(
            $id_fidelite,
            $totalAmount,
            $orderAmount,
            $programme->commerce_id,
            $walletVendeur->vendeur_id,
            null
        );

        $fraudModeration = new FraudModeration($t);

        return $fraudModeration->process();

    }

    public function notifyAfterPaiement(WalletClient $walletClient, WalletVendeur $walletVendeur, Transaction $transaction)
    {
        try {
            Notification::locale('fr')->sendNow($walletClient->client->user, new PaymentClientNotification($walletClient, $transaction, false));
        } catch (\Throwable $e) {
            Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
        }

        try {
            Notification::locale('fr')->sendNow($walletVendeur->vendeur->user, new PaymentVendorNotification($transaction, false));
        } catch (\Throwable $e) {
            Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
        }

    }

    public function pushEventAfterPaiement(WalletClient $walletClient, WalletVendeur $walletVendeur)
    {


        try {
            event(new NewVendorTransaction($walletVendeur->vendeur, $walletVendeur));
        } catch (\Throwable $e) {
            Log::error("Echec de notifcation vendeur | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
        }

        try {
            event(new NewClientTransaction($walletClient->client, $walletClient));
        } catch (\Throwable $e) {
            Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
        }
    }

    public function addPaiement(WalletClient $walletClient, WalletVendeur $walletVendeur, int $montant)
    {

        // on fait les mouvement de wallet client vers vendeur
        $walletVendeur->payment = $walletVendeur->payment + $montant;
        $walletVendeur->save();

        if ($montant > $walletClient->topup) {
            $newcash = $walletClient->cashback - ($montant - $walletClient->topup);
            $walletClient->topup = 0;
            $walletClient->cashback = $newcash;
        } else {
            $walletClient->topup = $walletClient->topup - $montant;
        }

        $walletClient->montant = $walletClient->montant - $montant;
        $walletClient->save();

        // on cree la transaction de paiement
        $transTopup = Transaction::create([
            'wallet_id' => $walletClient->id,
            'wallet_vendeur_id' => $walletVendeur->id,
            'montant' => $montant,
            'gain' => 0,
            'status' => 1,
            'solde_wallet' => $walletClient->montant,
            'type_transaction' => Transaction::TYPE_DEBIT_PAIEMENT,
            'created_by' => $walletClient->client_id
        ]);

        return $transTopup;

    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->active()->orderBy('created_at', 'desc')->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getVendeurLastTransactions($Wallet_vendeurId, ?Agent $agent = null, $limite = 10)
    {
        $data = Transaction::where('wallet_vendeur_id', $Wallet_vendeurId)->orderBy('created_at', 'desc');
        if ($agent != null && !$agent->is_admin) {
            $data = $data->where('agent_id', $agent->id);
        }
        return $data->limit($limite)->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getLastTransactions($limite = 10)
    {
        //return $this->model->where('wallet_vendeur_id', $Wallet_vendeurId)->orderBy('created_at','desc')->limit($limite)->get();
        return DB::table('transactions')->orderBy('created_at', 'desc')->limit($limite)->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getClientLastXTransactions($Wallet_clientId, $nbre = -1)
    {
        $query = $this->model->newQuery()->where('wallet_id', $Wallet_clientId)
            ->where('status', 1)
            ->when($nbre != -1, function ($q) use ($nbre) {
                return $q->limit($nbre);
            })
            ->latest();

        return $query->get();
    }

    // on retourne les statistique de transaction dun commerce
    public function getSellerStatistique($Wallet_Id, ?Agent $agent, $type_transaction = 'cashback', $periode = 'annee')
    {
        $in = $type_transaction == Transaction::TYPE_CREDIT_BONUS_PARRAINAGE ? [Transaction::TYPE_CREDIT_BONUS_PARRAINAGE, Transaction::TYPE_CREDIT_FIRSTBUY] : [$type_transaction];
        if ($periode == 'jour') {
            $fromDate = Carbon::now()->startOfMonth()->toDateString();
            $tillDate = Carbon::now()->endOfMonth()->toDateString();
            $data = $this->model
                ->where('wallet_vendeur_id', $Wallet_Id)
                ->whereIn('type_transaction', $in)
                ->whereBetween('created_at', [$fromDate, $tillDate])
                ->without(['commerce', 'wallet_vendeur'])
                ->orderBy('created_at');

            //
        } else {
            $data = $this->model
                ->where('wallet_vendeur_id', $Wallet_Id)
                ->whereIn('type_transaction', $in)
                ->whereYear('created_at', date('Y'))
                ->without(['commerce', 'wallet_vendeur'])
                ->orderBy('created_at');
        }

        if ($agent != null && !$agent->is_admin) {
            $data = $data->where('agent_id', $agent->id);
        }

        return $data->limit(5000)
            ->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getForLastYear()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getForLastYearLimits($limit = 100)
    {
        $transactions = $this->model->whereYear('created_at', date('Y'))->orderBy('created_at', 'desc');
        if ($limit != -1) {
            $transactions = $transactions->limit($limit);
        }
        return $transactions->get();
    }

    // retourne les X dernieres transactions dun vendeur
    public function getRevenuForLastYearLimits($limit = 100)
    {
        $transactions = $this->model->whereYear('created_at', date('Y'))->where('gain', '<>', 0)->orderBy('created_at', 'desc');
        if ($limit != -1) {
            $transactions = $transactions->limit($limit);
        }
        return $transactions->get();
    }

    public function getFilteredQueryForExport(Request $request, $onlyDeleted = false)
    {

        $sqlCommerceVille = 'select CONCAT(cty.name,\',\',ctr.name)
                        from cities cty
                        left join countries ctr on ctr.id=cty.country_id
                        where cty.id=commerces.city_id limit 1';

//        $query = Transaction::query()
        $query = DB::table('transactions')
            ->leftJoin('achat', 'achat.id', '=', 'transactions.achat_id')
            ->leftJoin('agents', 'agents.id', '=', 'transactions.agent_id')
            ->leftJoin('points_ventes', 'points_ventes.id', '=', 'agents.point_vente_id')
            ->leftJoin('wallets', 'wallets.id', '=', 'transactions.wallet_id')
            ->leftJoin('clients', 'clients.id', '=', 'wallets.client_id')
            ->leftJoin('users', 'users.id', '=', 'clients.user_id')
            ->leftJoin('parrainage', 'parrainage.filleul_id', 'clients.id')
            ->leftJoin('clients as referrer_cli', 'referrer_cli.id', 'parrainage.parrain_id')
            ->leftJoin('users as referrer_usr', 'referrer_usr.id', 'referrer_cli.user_id')
            ->leftJoin('programmes', 'programmes.id', '=', 'wallets.programme_id')
            ->leftJoin('commerces', 'commerces.id', '=', 'programmes.commerce_id')
            ->leftJoin('wallets_vendeur', 'wallets_vendeur.id', '=', 'transactions.wallet_vendeur_id')


            ->when($request->input('deleted'), function (Builder $query) {
                return $query->where('transactions.status', '!=', 1);
            })
            ->when(!$request->input('deleted'), function (Builder $query) {
                return $query->where('transactions.status', 1);
            })
            ->when($request->filled('start_date'), function ($query) use ($request) {
                $date = Carbon::parse($request->get('start_date'))->toDateTimeString();
                return $query->whereDate('transactions.created_at', '>=', $date);
            })
            ->when($request->filled('end_date'), function ($query) use ($request)  {
                $date = Carbon::parse($request->get('end_date'))->toDateTimeString();
                return $query->whereDate('transactions.created_at', '<=', $date);
            })
//            ->orderByDesc('transactions.created_at')
            ->selectRaw('
                            transactions.id,
                            IF(transactions.type_transaction="'.Transaction::TYPE_CREDIT_TOPUP.'", transactions.montant, 0) topup,
                            IF(transactions.type_transaction="'.Transaction::TYPE_CREDIT_CASHBACK_SUR_MONNAIE.'", transactions.montant, 0) cashback_over_topup,
                            IF(transactions.type_transaction="'.Transaction::TYPE_CREDIT_CASHBACK.'", transactions.montant, 0) cashback,
                            IF(transactions.type_transaction="'.Transaction::TYPE_DEBIT_PAIEMENT.'", transactions.montant, 0) paiement,
                            IF(transactions.type_transaction="'.Transaction::TYPE_CREDIT_FIRSTBUY.'", transactions.montant, 0) firstbuy,
                            IF(transactions.type_transaction="'.Transaction::TYPE_CREDIT_BONUS_PARRAINAGE.'", transactions.montant, 0) bonus_parrainage,
                            transactions.gain,
                            achat.montant_facture,
                            transactions.wallet_id,
                            transactions.wallet_vendeur_id,
                            transactions.created_at,
                            transactions.updated_at,
                            transactions.solde_wallet,
                            transactions.status,
                            transactions.type_transaction,
                            CONCAT(referrer_cli.nom, \' \', referrer_cli.prenom) referrer_fullName,
                            referrer_usr.telephone as referrer_telephone,
                            CONCAT(clients.nom, \' \', clients.prenom) client,
                            users.telephone,
                            commerces.nom commerce,
                            CONCAT(agents.nom_prenom, \'--\', points_ventes.nom, \'--\', points_ventes.adresse) agent,
                            (' . $sqlCommerceVille . ') commerce_ville

            ');
        return $query;


    }

    public function getFilteredQuery(Request $request, $onlyDeleted = false)
    {

//        dd(!$request->input('deleted'));
        $columns = array(
            'commerce' => 'commerce',
            '1' => 'client',
            '2' => 'customer_vip',
            '3' => 'telephone',
            '4' => 'agent',
            '5' => 'type_transaction',
            '6' => 'montant_facture',
            '7' => 'montant',
            '8' => 'montant',
            '9' => 'montant',
            '10' => 'montant',
            '11' => 'commerce',
            '12' => 'commerce',
            '13' => 'transactions.created_at',
            '14' => 'created_at',
        );

        $sqlCommerce = 'select CONCAT(cme.nom,\':KR:\',ctr.name,\':KR:\',cty.name)
                        from wallets_vendeur wvd
                        left join commerces cme on cme.vendeur_id=wvd.vendeur_id
                        left join cities cty on cty.id=cme.city_id
                        left join countries ctr on ctr.id=cty.country_id
                        where wvd.id=transactions.wallet_vendeur_id limit 1';
        $sqlClient = 'select CONCAT(clt.nom, \' \', clt.prenom) fullname
                        from wallets wlt
                        left join clients clt on clt.id=wlt.client_id
                        where wlt.id=transactions.wallet_id';
        $sqlAgent = 'select CONCAT(agt.nom_prenom, \'||\', ptv.nom, \'||\', ptv.adresse) agent
                        from agents agt
                        left join points_ventes ptv on agt.point_vente_id=ptv.id
                        where agt.id=transactions.agent_id';
        $sqlAchat = 'select ach.montant_facture
                        from achat ach
                        where ach.id=transactions.achat_id';

//        $query = Transaction::query()

        $query = DB::table('transactions')
            ->orderByDesc('transactions.created_at')
            ->leftJoin('wallets', 'wallets.id', '=', 'transactions.wallet_id')
//            ->leftJoin('programmes', 'programmes.id', '=', 'wallets.programme_id')
//            ->leftJoin('commerces', 'commerces.id', '=', 'programmes.commerce_id')
            ->leftJoin('clients', 'clients.id', '=', 'wallets.client_id')
            ->leftJoin('users', 'users.id', '=', 'clients.user_id')
            ->leftJoin('wallets_vendeur', 'wallets_vendeur.id', '=', 'transactions.wallet_vendeur_id')
            ->when($request->input('wallet_client_id'), function ($query, $wallet_client_id) {
                return $query->where('transactions.wallet_id', $wallet_client_id);
            })
            ->when($request->input('wallet_vendeur_id'), function ($query, $wallet_vendeur_id) {
                return $query->where('transactions.wallet_vendeur_id', $wallet_vendeur_id);
            })
            ->when($request->input('deleted'), function (Builder $query, $onlyDeleted) {
                return $query->where('transactions.status', '!=', 1);
            })
            ->when(!$request->input('deleted'), function (Builder $query, $onlyDeleted) {
                return $query->where('transactions.status',  1);
            })
            ->when($request->input('start_date'), function ($query, $date) {
                $date = Carbon::parse($date)->toDateTimeString();
                return $query->whereDate('transactions.created_at', '>=', $date);
            })
            ->when($request->input('end_date'), function ($query, $date) {
                $date = Carbon::parse($date)->toDateTimeString();
                return $query->whereDate('transactions.created_at', '<=', $date);
            })
            ->when($request->input('vendeur'), function ($query, $idVendeur) {
                return $query->where(function ($query) use ($idVendeur) {
                    return $query
                        ->orWhere('wallets_vendeur.vendeur_id', $idVendeur);
                });
            })
            ->when($request->input('client'), function ($query, $idClient) {
                return $query->where(function ($query) use ($idClient) {
                    return $query
                        ->orWhere('wallets.client_id', $idClient);
                });
            })
            ->when($request->input('search.value'), function ($query, $searchValue) {
                return $query->where(function ($query) use ($searchValue) {

                    return $query
//                        ->orWhere('commerce', 'like', "%$searchValue%")
                        ->orWhere('transactions.montant', 'like', "%$searchValue%")
                        ->orWhere('transactions.type_transaction', 'like', "%$searchValue%")
                        ->orWhere('transactions.created_at', 'like', "%$searchValue%");
                });
            })
            ->when($request->input('order.0.column', '10'), function ($query, $orderColumnNumber) use ($request, $columns) {
                $dir = $request->input('order.0.dir', 'desc');
//                $columnName = $request->input("columns.$orderColumnNumber.data");
                $columnName = $columns[$orderColumnNumber] ?? null;

                return $columnName ? $query->orderBy($columnName, $dir) : $query->orderByDesc('created_at', $dir);
            })
            //->orderByDesc('transactions.created_at')
            ->selectRaw('
                            transactions.*,
                            users.telephone,
                            (' . $sqlCommerce . ') commerce,
                            (' . $sqlClient . ') client,
                            (' . $sqlAgent . ') agent,
                            (' . $sqlAchat . ') montant_facture
            ');

        return $query;


    }

    // retourne les X dernieres transactions dun vendeur
    public function getTransactionsDatatable(Request $request, $onlyDeleted = false)
    {

        $query = $this->getFilteredQuery($request, $onlyDeleted);

        return DataTables::queryBuilder($query)
            ->filter(function ($query) {
                // to disable filter
            })
            ->addColumn('commerce', function ($itemRow) {
                $commerceArr = explode(":KR:", $itemRow->commerce);
                return isset($commerceArr[1]) ? $commerceArr[0] : '';
            })
            ->addColumn('pays', function ($itemRow) {
                $commerceArr = explode(":KR:", $itemRow->commerce);
                return isset($commerceArr[1]) ? $commerceArr[1] : '';
            })
            ->addColumn('ville', function ($itemRow) {
                $commerceArr = explode(":KR:", $itemRow->commerce);
                return isset($commerceArr[1]) ? $commerceArr[2] : '';
            })
            ->addColumn('montant_cashback', function ($itemRow) {
                return $itemRow->type_transaction == Transaction::TYPE_CREDIT_CASHBACK ? $itemRow->montant : 0;
            })
            ->addColumn('montant_topup', function ($itemRow) {
                return $itemRow->type_transaction == Transaction::TYPE_CREDIT_TOPUP ? $itemRow->montant : 0;
//                return $itemRow['type_transaction'] == Transaction::TYPE_TOPUP ? $itemRow['montant'] : 0;
            })
            ->addColumn('montant_cashback_over_topup', function ($itemRow) {
                return $itemRow->type_transaction == Transaction::TYPE_CREDIT_CASHBACK_SUR_MONNAIE ? $itemRow->montant : 0;
//                return $itemRow['type_transaction'] == Transaction::TYPE_TOPUP ? $itemRow['montant'] : 0;
            })

            ->addColumn('montant_paiement', function ($itemRow) {
                return $itemRow->type_transaction == Transaction::TYPE_DEBIT_PAIEMENT ? $itemRow->montant : 0;
//                return $itemRow['type_transaction'] == Transaction::TYPE_PAIEMENT ? $itemRow['montant'] : 0;
            })
            ->addColumn('montant_parrainage', function ($itemRow) {
                return $itemRow->type_transaction == Transaction::TYPE_CREDIT_BONUS_PARRAINAGE ? $itemRow->montant : 0;
//                return $itemRow['type_transaction'] == Transaction::TYPE_PARRAINAGE ? $itemRow['montant'] : 0;
            })
            ->addColumn('created_at', function ($itemRow) {
                return Carbon::parse($itemRow->created_at)->format('Y-m-d H:i:s');
            })
            ->addColumn('actions', function ($itemRow) use ($onlyDeleted) {
                $html = '<a title="Revert Transaction" href="' . route('transaction.revert.init', ['id' => $itemRow->id]) . '">
                            <i class="fa text-danger fa-trash-restore-alt"></i>
                        </a>';

                $htmlRestore = '<a title="Restore Transaction" href="' . route('transaction.restore.init', ['id' => $itemRow->id]) . '">
                            <i class="fa text-warning fa-trash-restore"></i>
                        </a>';

                return $onlyDeleted ? $htmlRestore : $html;
            })
            ->toJson();

    }
}
