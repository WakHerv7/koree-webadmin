<?php

namespace App\Repositories\Backend\V1;

use App\Models\WalletClient;
use App\Repositories\ResourceRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;


class WalletRepository extends ResourceRepository
{

    /**
     * @param WalletClient $wallet
     */
    public function __construct(WalletClient $wallet)
    {
        $this->model = $wallet;
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->with('programme', 'client')->get();
    }

    // retourne tous les wallets dun client
    public function getAllForCustomer($id)
    {
        return $this->model->with('programme.commerce')->has('programme.commerce')->where('client_id', $id)->where('deleted', false)->get();
    }

    // retourne tous les wallets dun programme
    public function getAllForProgramme($id)
    {
        return $this->model->where('programme_id', $id)->with('client')->get();
    }

    // retourne le id , nom ,prenom du client
    public function getClientInfo($id)
    {
        $walcli = $this->model->where('id', $id)->with('client')->first();

        return array('id' => $walcli->client->id, 'nom' => $walcli->client->nom, 'prenom' => $walcli->client->prenom);
    }

    // retourne les abonnements du mois courant
    public function getCurrentMonthAbonnement()
    {
        $fromDate = Carbon::now()->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->endOfMonth()->toDateString();
        return $this->model->whereBetween('created_at', [$fromDate, $tillDate])->get();
    }


}
