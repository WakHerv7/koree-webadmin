<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Region;
use App\Repositories\Frontend\V2\ClientRepository;
use App\Repositories\Frontend\V2\VendeurRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class UsersRepository extends ResourceRepository
{
    private ClientRepository $clientRepository;
    private VendeurRepository $vendeurRepository;

    /**
     * @param User $user
     */
    public function __construct(User $user, ClientRepository $clientRepository, VendeurRepository $vendeurRepository)
    {
        $this->model = $user;
        $this->clientRepository = $clientRepository;
        $this->vendeurRepository = $vendeurRepository;
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->telephone = mt_rand(700000000, 780000000);
        $user->save();
        $this->getById($id)->delete();
    }

    /**
     * @return User[]
     */
    public function getAdmins()
    {
        return User::role('Super-Admin')->get();
    }


    /**
     *
     * @return Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getByPhone($phone)
    {
        return $this->model->where('telephone', $phone)->first();
    }

    public function getClientProfileByType($user_id, $user_type)
    {
        $data = array();
        if ($user_type == User::TYPE_CLIENT) {
            $data = $this->clientRepository->getProfil($user_id);
        } else if ($user_type == User::TYPE_VENDEUR) {
            $data = $this->vendeurRepository->getProfil($user_id);
        }

        return $data;
    }

    /**
     * @return VendeurRepository
     */
    public function getVendeurRepository(): VendeurRepository
    {
        return $this->vendeurRepository;
    }

    /**
     * @return ClientRepository
     */
    public function getClientRepository(): ClientRepository
    {
        return $this->clientRepository;
    }

}

