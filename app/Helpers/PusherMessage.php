<?php

namespace App\Helpers;

class PusherMessage
{

    public $type;
    public $content;
    public $owner;

    public function __construct($type, $content, $owner)
    {
        $this->content = $content;
        $this->type = $type;
        $this->owner = $owner;
    }
}
