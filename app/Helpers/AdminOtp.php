<?php

namespace App\Helpers;


use App\Mail\OtpMail;
use App\Mail\SimpleMessageMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AdminOtp
{


    const OTP_LENGTH = 6;

    private string $signature;
    private string $code;
    private string $message;

    public function __construct()
    {
        $request = request();
        $this->signature = $request->has('signature') ? $request->get('signature') : "d4PolGkbs7/";
        $this->code = Str::upper(Str::random(self::OTP_LENGTH));

    }

    public function toEmail(string $email)
    {
        try {

            Mail::to($email)->send(
                (new SimpleMessageMail)
                    ->setCustomMessageBody("Code de confirmation est {$this->code}")
                    ->from('contact@koree.africa')
                    ->subject('Votre de code sécurite - koree'));

            DB::table('admin_otps')->insert([
                'to' => $email,
                'code' => $this->code,
                'date' => now(),
                'channel' => 'email',
                'pin_id' => Str::uuid()->toString(),
            ]);

            return true;

        } catch (\Exception $exception) {
            Log::critical("SecureOptEmailSendFailure::" . $exception->getMessage());
            return false;
        }

    }


    public function toSms(string $msisdn)
    {
        try {

            $this->message = "{$this->code} est votre code de sécurite. Merci\n{$this->signature}";

            $sms = new SMS($msisdn, $this->message);
            $send = $sms->send();

            DB::table('admin_otps')->insert([
                'phone_number' => $msisdn,
                'code' => $this->code,
                'date' => now(),
                'channel' => 'sms',
                'pin_id' => Str::uuid()->toString(),
            ]);

            return $send;
        } catch (\Exception $exception) {
            Log::info("SecureOptSmsSendFailure::" . $exception->getMessage());
            return false;

        }
    }


}
