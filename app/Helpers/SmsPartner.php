<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class SmsPartner
{

   // protected $url = config('services.smspartner.url');

    public static function smsBalance()
    {
        $url = config('services.smspartner.url').'/balance';

        return Http::withHeaders(self::getHeaders())->get($url);

    }

    private static function getHeaders()
    {
        return [
            'Authorization' => 'Basic '.config('services.d7network.secret'),
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
    }

    public static function envoiSMS($data){
        $endpoint = config('services.smspartner.url')."/send";
        try {
            $body=[
                'phoneNumbers' => $data['msisdn'],
                'message' => $data['message'],
                'sender'=>config('services.smspartner.sender'),
                'apiKey'=>config('services.smspartner.secret'),
            ];
           //$response = Http::withHeaders(self::getHeaders())->post($endpoint, $body);
           $response = Http::post($endpoint, $body);
            //Log::info(json_encode($response));
            return $response;
        } catch (\Exception $e) {
            Log::error("Message erreur SMS: ". $e->getMessage());
            return false;
        }
    }

    public static function sendSMS($data)
    {
        $endpoint = config('services.d7network.url')."/send"; // "https://rest-api.d7networks.com/secure/send";
        //dd($endpoint);
        $headers = ['Content-Type' => 'application/json','Authorization'=> 'Basic ZmVxYzM4ODI6QlB1amFPZUo=' ];
        $client = new \GuzzleHttp\Client();
        $theQuery = ["to" => $data['msisdn'],   "content" => json_encode($data['message']), "from"=>"Koree",
           // 'dlr'=> 'no'
        ];

        try {
            $response = $client->request('POST', $endpoint, ['form_params' => $theQuery,
            'headers' => self::getHeaders()]);
            $statusCode = $response->getStatusCode();
            $content = $response->getBody();

            Log::info($content);

            return $content;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            //dd($e);
            Log::error("Message erreur SMS: ". $e->getMessage());
            return false;
        }
    }

}
