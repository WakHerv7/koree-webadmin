<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;

/**
 * Get token
 *
 * @param  Classe $class
 * @param  mixed $value
 * @return mixed
 */
function getToken()
{
    $token_expired = false;

    if (session()->has('accessToken')) {
        $accessTokenDateTime = session('accessTokenDateTime');
        $currentDateTime = new DateTime();
        $tokenDateTime = new DateTime($accessTokenDateTime);
        
        $interval = $tokenDateTime->diff($currentDateTime);

        if ($interval->i >= 5) {
            $token_expired = true;
        }
    } else {
        $token_expired = true;
    }

    if ($token_expired) {
        $client = new Client();
        try {
            $response = $client->get('http://api.dev.Koree.africa/api/migrate-user');
            dd($response);
        } catch (ServerException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            
            $errorMessage = $responseBodyAsString; // Your error message here

            // Find the position of "idToken"
            $pos = strpos($errorMessage, 'idToken');
            
            if ($pos !== false) {
            // Extract the part of the string that follows "idToken"
            $afterIdToken = substr($errorMessage, $pos);

            // Use a regular expression to match the content of the <span>
            preg_match('/<span class=sf-dump-str title="1000 characters">(.*?)<\/span>/', $afterIdToken, $matches);
                
                if (count($matches) > 1) {
                    $idToken = $matches[1];
                    $currentDateTime = new DateTime();
                    session(['accessToken' => $idToken]);
                    session(['accessTokenDateTime' => $currentDateTime->format('Y-m-d H:i:s')]);
                    
                    // $accessToken = [
                    // "token" => $idToken,
                    // "datetime" => $currentDateTime->format('Y-m-d H:i:s'),
                    // ];
                    // $token = $request->user()->createToken($idToken);
                    // dd($idToken);
                } else {
                    session(['accessToken' => null]);
                }
            } else {
                session(['accessToken' => null]);
            }
        }
    }
}


function rand_color() {
    return str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
}


function getFormattedDate($val)
{
    // '2023-11-21T07:55:26.316302Z'
    $date = new DateTime($val);
    return $date->format('Y-m-d H:i:s');
}

function getRandomCode($nb)
{
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $referralCode = substr(str_shuffle($characters), 0, $nb);

    return $referralCode;
}
