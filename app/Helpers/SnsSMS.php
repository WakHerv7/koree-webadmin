<?php

namespace App\Helpers;

use App\Services\SnsService;
use Aws\Exception\AwsException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SnsSMS
{

    public static function send(SMS $sms): bool
    {

        $phone =  $sms->getMsisdn();
        $msg =  $sms->getMessage();


        $service = new SnsService;

        $result = $service->sendSMSSns('Koree', $phone, $msg);

        return !$result instanceof AwsException;

    }

    public static function balance()
    {

        return 'N/A';
    }
}
