<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GeneralFunctions
{
    public static function getCommerceByWalletId($walletVendeurId,$walletId){

        try {
            if($walletVendeurId){
                $wallet_v= \App\Models\WalletVendeur::where('id','=',$walletVendeurId)->with('programme.commerce.ville')->first();
            }else{
                $wallet_v= \App\Models\WalletClient::where('id','=',$walletId)->with('programme.commerce.ville')->first();
            }
            if($wallet_v){
                return $wallet_v;
            }else{
                return '';
            }
        } catch (\Exception $e) {
            Log::error("Erreur recuperation du commerce a partir du wallet id : ". $e->getMessage());
            return false;
        }
    }

    public static function getClientByWalletId($walletId){

        try {
            $wallet_v= \App\Models\WalletClient::where('id','=',$walletId)->with('client.ville.pays')->first();

            if($wallet_v){
               // return $wallet_v->client->nom." ".$wallet_v->client->prenom;
                return $wallet_v;
            }else{
                return false;
            }
        } catch (\Exception $e) {
            Log::error("Erreur recuperation du commerce a partir du wallet id : ". $e->getMessage());
            return false;
        }
    }


}
