<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class   NexahSMS
{

    public static function send(SMS $sms): bool
    {
        $request = Http::post(config('services.nexah.url'), [
            'user' => config('services.nexah.username'),
            'senderid' => config('services.nexah.sender'),
            'password' => config('services.nexah.password'),
            'sms' => $sms->getMessage(),
            'mobiles' => $sms->getMsisdn()
        ]);


        return $request->status() === 200;
    }

    public static function balance()
    {

//        $request = Http::post(config('services.nexah.url'), [
//            'user' => config('services.nexah.username'),
//            'senderid' => config('services.nexah.sender'),
//            'password' => config('services.nexah.password'),
//        ]);
//
//
//        Log::alert("Nexah SMS balance", ['responseData' => $request->body()]);
//
//        if ($request->json('balance')) return $request->json('balance');

        return 'N/A';
    }
}
