<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TermiiSMS
{

    public static function send(SMS $sms): bool
    {

        $data = [

            "to" => $sms->getMsisdn(),
            'from' => config('services.termii.sender_id'),
            'sms' => $sms->getMessage(),
            "type" => "plain",
            "channel" => "generic",
            'api_key' => config('services.termii.api_key'),
        ];
        $request = Http::asJson()->post(config('services.termii.url')."/sms/send", $data);


        return $request->status() === 200;
    }

    public static function balance()
    {

        $data = [
            'api_key' => config('services.termii.api_key'),
        ];

        $request = Http::asJson()->get(config('services.termii.url')."/get-balance", $data);

//        Log::alert("Termii SMS balance", ['responseData' => $request->body()]);

        if ($request->json('balance')) return $request->json('balance');

        return 'N/A';
    }

}
