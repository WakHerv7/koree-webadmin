<?php

namespace App\Helpers;

use Illuminate\Support\Str;

abstract class PhoneNumberFormatter
{
    public static function format(string $phone, string $calling_code): string
    {
        return Str::start(ltrim(ltrim(ltrim($phone, '+'), '00'), $calling_code), $calling_code);
    }
}
