<?php

namespace App\Helpers;


class TransactionHelper
{
    private string $idFidelity;
    private float $totalAmount;
    private float $orderAmount;
    private int $shopId;
    private int $vendorId;
    private ?int $agentId;

    /**
     * @param string $idFidelity
     * @param float $totalAmount
     * @param float $orderAmount
     * @param int $shopId
     * @param int $vendorId
     * @param ?int $agentId
     */
    public function __construct(string $idFidelity, float $totalAmount, float $orderAmount, int $shopId, int $vendorId, ?int $agentId = null)
    {
        $this->idFidelity = $idFidelity;
        $this->totalAmount = $totalAmount;
        $this->orderAmount = $orderAmount;
        $this->shopId = $shopId;
        $this->vendorId = $vendorId;
        $this->agentId = $agentId;
    }

    /**
     * @return string
     */
    public function getIdFidelity(): string
    {
        return $this->idFidelity;
    }

    /**
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    /**
     * @return float
     */
    public function getOrderAmount(): float
    {
        return $this->orderAmount;
    }

    /**
     * @return int
     */
    public function getShopId(): int
    {
        return $this->shopId;
    }

    /**
     * @return int
     */
    public function getVendorId(): int
    {
        return $this->vendorId;
    }

    /**
     * @return ?int
     */
    public function getAgentId(): ?int
    {
        return $this->agentId;
    }


}
