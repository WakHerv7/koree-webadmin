<?php

namespace App\Helpers;

use App\Models\User;
use Aws\Exception\AwsException;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Netflie\WhatsAppCloudApi\Response\ResponseException;

class SMS
{
    const CAMEROUN_CALLING_CODE = '237';
    const COTE_DIVOIRE_CALLING_CODE = '225';

    const SMS_PROVIDER_TECHSOFT = 'TECHSOFT';
    const SMS_PROVIDER_TERMII = 'TERMII';
    const SMS_PROVIDER_NEXAH = 'NEXAH';
    const SMS_PROVIDER_SNS = 'SNS';
    const SMS_PROVIDER_AUTOMATIC = 'AUTOMATIC';
    const SMS_PROVIDER_WHATSAPP = 'WHATSAPP';
    const SMS_PROVIDER_TWILIO   = 'TWILIO';

    const SMS_PROVIDER_MESAGOO = 'MESAGOO';
    const RETRY_TIMEOUT_MINUTE = 10;

    private string $msisdn;
    private string $message;
    private string $phone_country_code;
    private array $errorMessageBag = [];
    private mixed $params;
    private string $errorMessage = '';

    public function __construct(string $msisdn, string $message)
    {

        $msisdn = str_replace(" ", "", $msisdn);

        $user = User::query()->where('telephone', $msisdn)->first();

        if ($user) {

            $phonecode = $user?->client?->pays?->phonecode;
            $this->phone_country_code = $phonecode ?? '';
            $this->msisdn = $phonecode . ltrim(ltrim($msisdn, '00'), $phonecode);

        } else {

            $phone = ltrim($msisdn, '00');
            $this->phone_country_code = Str::substr($phone, 0, 3);
            $this->msisdn = $phone;

        }


        $this->message = trim($message);

        $this->params = DB::table('parametrage')->select("sms_provider", "sms_provider_fallback")->first();

    }

    public static function getBalances()
    {
        $data = [
            'termii' => strtolower(TermiiSMS::balance()),
            'techsoft' => strtolower(TechSoftSMS::balance()),
            'nexah' => strtolower(NexahSMS::balance()),
            'sns' => strtolower(SnsSMS::balance()),
            'whatsapp' => strtolower(WhatsappSMS::balance()),

        ];

        Log::alert("SMS Provider balance", ['data' => $data]);

        return $data;
    }

    /**
     * @return string
     */
    public function getMsisdn(): string
    {
        return $this->msisdn;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getPhonecountrycode(): string
    {
        return $this->phone_country_code;
    }

    public function send(): bool
    {
        $send_result = false;
        $key = 'sms_retry_count_for_' . $this->msisdn;

        try {
            $send_result = $this->provider_send_sms($this->getSmsProviderByRetry(Cache::get($key)));
            throw_unless($send_result, new Exception("SMS SEND FAILED"));
        } catch (Exception $exception) {
            $this->handleException($exception, $this->params->sms_provider);
            $send_result = $this->sendFallback();
        }

        if (Cache::has($key))
            Cache::increment($key);
        else
            Cache::set($key, 1, Carbon::now()->addMinutes(self::RETRY_TIMEOUT_MINUTE));

        return $send_result;
    }

    public function sendFallback(): bool
    {
        $send_result = false;

        try {
            $send_result = $this->provider_send_sms($this->params->sms_provider_fallback);
            throw_unless($send_result, new Exception("SMS SEND FAILED"));
        } catch (\Throwable $exception) {
            $this->handleException($exception, "{$this->params->sms_provider_fallback} FALLBACK");
        }

        return $send_result;
    }

    private function getSmsProviderByRetry($count): string
    {
        return match ($count) {
            '1' => self::SMS_PROVIDER_SNS,
            '2' => self::SMS_PROVIDER_WHATSAPP,
            '3' => self::SMS_PROVIDER_MESAGOO,
            '4' => self::SMS_PROVIDER_TECHSOFT,
            default => $this->params->sms_provider,
        };
    }

    private function provider_send_sms($sms_provider)
    {
        return match ($sms_provider) {
//            self::SMS_PROVIDER_SNS => SnsSMS::send($this),
            self::SMS_PROVIDER_WHATSAPP => WhatsappSMS::send($this),
            self::SMS_PROVIDER_MESAGOO => MesagooSMS::send($this),
            self::SMS_PROVIDER_NEXAH => NexahSMS::send($this),
            self::SMS_PROVIDER_TERMII => TermiiSMS::send($this),
            self::SMS_PROVIDER_TECHSOFT => TechSoftSMS::send($this),
            self::SMS_PROVIDER_AUTOMATIC => match ($this->phone_country_code) {
                self::CAMEROUN_CALLING_CODE => $this->isMtnCameroon()
                    ? SnsSMS::send($this) : (
                    $this->isOrangeCameroon()
                        ? MesagooSMS::send($this)
                        : TechSoftSMS::send($this)
                    ),
                self::COTE_DIVOIRE_CALLING_CODE => TermiiSMS::send($this),
                default => SnsSMS::send($this),

            },
            default => SnsSMS::send($this),
        };
    }

    private function handleException(\Throwable $exception, $provider)
    {

        match (get_class($exception)) {
            ResponseException::class => $this->errorLog($exception->response()->body(), $provider),
            AwsException::class => $this->errorLog($exception->getResponse()->getBody(), $provider),
            default => $this->errorLog($exception->getMessage(), $provider),
        };

    }

    private function errorLog($message, $provider = '')
    {
        $this->errorMessageBag[] = "[[$provider]]$message";
        $this->errorMessage = 'SMS SEND ERROR::' . implode('::', $this->errorMessageBag);
        Log::error($this->errorMessage);
    }


    /**
     * @return string
     */
    public function hasError(): string
    {
        return count($this->errorMessageBag);
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    private function isOrangeCameroon(): bool
    {
        return in_array($this->phonePrefix(), ['655', '656', '657', '658', '659', '690', '691', '692', '693', '694', '695', '696', '697', '698', '699']);
    }

    private function isMtnCameroon(): bool
    {
        return in_array($this->phonePrefix(), ['650', '651', '652', '653', '654', '670', '671', '672', '673', '674', '675', '676', '677', '678', '679', '680', '681', '682', '683', '684', '685', '686', '687', '688', '689']);
    }


    private function phonePrefix(): string
    {
        return substr($this->msisdn, 3, 3);
    }
}
