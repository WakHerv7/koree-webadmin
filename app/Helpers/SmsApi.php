<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SmsApi
{
    public static function sendSMS($data)
    {
        $endpoint = config('services.smsapi.url');
        try {
            $params = [
                'action' => "send-sms",
                'api_key' => config("services.smsapi.key"),
                'from' => config('services.smsapi.sender'),
                'to' => $data['msisdn'],
                'sms' => $data['message'],
            ];
            $response = Http::get($endpoint, $params);

            return $response;
        } catch (\Exception $e) {
            Log::error("Message erreur SMS: " . $e->getMessage());
            return false;
        }
    }

}
