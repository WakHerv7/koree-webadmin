<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Netflie\WhatsAppCloudApi\Response\ResponseException;
use Netflie\WhatsAppCloudApi\WhatsAppCloudApi;
use NotificationChannels\WhatsApp\Component;
use NotificationChannels\WhatsApp\WhatsAppTemplate;

class WhatsappSMS
{

    public static function send(SMS $sms): bool
    {
        $phone = $sms->getMsisdn();
        $msg = $sms->getMessage();

        /** @var WhatsAppCloudApi $whatsapp */
        $whatsapp = App::make(WhatsAppCloudApi::class);

        $response = $whatsapp->sendTextMessage(
            $phone,
            $msg
        );

        return !$response->isError();

    }

    public static function balance()
    {
        return 'N/A';
    }
}
