<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TechSoftSMS
{
    public static function send(SMS $sms): bool
    {
        $request = Http::get(config('services.smsapi.url'), [
            'action' => "send-sms",
            'api_key' => config("services.smsapi.key"),
            'from' => config('services.smsapi.sender'),
            'to' => $sms->getMsisdn(),
            'sms' => $sms->getMessage(),
        ]);

        return $request->status() === 200;
    }

    public static function balance()
    {
        $request = Http::get(config('services.smsapi.url'), [
            'action' => 'check-balance',
            'api_key' => config("services.smsapi.key"),
            'response' => 'json'
        ]);

//        Log::alert("Techsoft SMS balance", ['responseData' => $request->body()]);

        if ($request->json('balance')) return $request->json('balance');

        return 'N/A';
    }
}
