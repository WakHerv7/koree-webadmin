<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class   MesagooSMS
{

    public static function send(SMS $sms): bool
    {
        $request = Http::acceptJson()
            ->contentType('application/json')
            ->post(config('services.mesagoo.url'), [
                'message' => $sms->getMessage(),
                'phone' => $sms->getMsisdn(),
                'api_token' => config('services.mesagoo.token'),
                'sender_code' => config('services.mesagoo.sender'),
                'type' => 'text',
                'gateway' => "premium",
            ]);

        return $request->status() === 200;
    }

    public static function balance()
    {
        return 'N/A';
    }
}
