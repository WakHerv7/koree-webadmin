<?php

namespace App\Jobs;

use App\Models\Restriction;
use App\Notifications\ClientUnlockedNotification;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use Log;

class UnlockClientJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Restriction::with('client.user')
            ->whereProcessed(false)
            ->where('end_date', '<', now())
            ->get()
            ->each(function (Restriction $restriction) {
                $client = $restriction->client;
                $client->status = true;
                $client->save();
                $restriction->processed = true;
                $restriction->save();

                try {
                    Notification::locale('fr')
                        ->sendNow(
                            $client->user,
                            new ClientUnlockedNotification()
                        );
                } catch (Exception $e) {
                    Log::error("Echec de notifcation client | ERROR///" . $e->getMessage() . " Line: " . $e->getFile() . " | " . $e->getLine());
                }
            });


    }
}
