<?php

namespace App;

use App\Models\Client;
use App\Models\Restriction;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class AccountRestriction
{
    /**
     * @param Collection $clients
     * @param Carbon $date
     * @param string $batch
     * @return void
     */
    public static function schedule(Collection $clients, Carbon $date, string $batch)
    {
        foreach ($clients as $client) {
            if ($client instanceof Client) {
                Restriction::create([
                    'batch' => $batch,
                    'end_date' => $date,
                    'processed' => false,
                    'client_id' => $client->id
                ]);
            }
        }
    }
}
