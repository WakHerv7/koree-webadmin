<?php

namespace App\Events\Pusher;

use App\Models\User;
use App\Models\Vendeur;
use App\Models\WalletVendeur;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewVendorTransaction implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $type;
    public array $content;
    public string $owner;

    private User $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Vendeur $vendeur, WalletVendeur $wallet)
    {
        $this->type = "VENDOR_TRANSACTION";
        $this->content = ['vendor' => $vendeur->id, 'wallet' => $wallet->id];
        $this->owner = $vendeur->user_id;
        $this->user = $vendeur->user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['koree-vendor-channel-' . $this->user->uuid];
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'x-vendor-transaction';
    }
}
