<?php

namespace App\Events\Pusher;

use App\Models\Client;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ClientUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $type;
    public array $content;
    public string $owner;

    private User $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->type = "CLIENT_UPDATED";
        $this->content = ['client' => $client->id];
        $this->owner = $client->user_id;
        $this->user = $client->user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['koree-client-channel-' . $this->user->uuid];
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'x-client-update-data';
    }
}
