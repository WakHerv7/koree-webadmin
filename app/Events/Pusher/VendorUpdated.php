<?php

namespace App\Events\Pusher;

use App\Models\User;
use App\Models\Vendeur;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VendorUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $type;
    public array $content;
    public string $owner;

    private User $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Vendeur $vendor)
    {
        $this->type = "VENDOR_UPDATED";
        $this->content = ['vendor' => $vendor->id];
        $this->owner = $vendor->user_id;
        $this->user = $vendor->user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['koree-vendor-channel-' . $this->user->uuid];
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'x-vendor-update-data';
    }
}
