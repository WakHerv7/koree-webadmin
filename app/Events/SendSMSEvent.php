<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendSMSEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public mixed $target;
    public string $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(mixed $target, string $message)
    {
        $this->message = $message;
        $this->target = $target;

    }

}
