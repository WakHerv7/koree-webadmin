<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

abstract class CollectionExport implements FromView
{
    protected string $collectionKey = 'items';

    protected string $filename = 'koree';

    protected string $viewPath;

    final public function __construct(
        private readonly Collection $collection
    )
    {
    }

    final public function __invoke(Request $request): BinaryFileResponse|RedirectResponse
    {
        try {
            $now = now()->toDateTimeLocalString();
            ini_set('memory_limit', '1G');
            return Excel::download(
                export: new static(static::getCollection($request)),
                fileName: "{$this->filename}_{$now}.xlsx"
            );
        } catch (\Exception $exception) {
            $classname = static::class;
            Log::error("An error encountered when trying download collection from class :: {$classname}. Exception: {$exception->getMessage()}");
            return redirect()->back();
        }
    }

    final public function view(): View
    {
        return view($this->viewPath, [
            $this->collectionKey => $this->collection
        ]);
    }

    abstract protected function getCollection(Request $request): Collection;
}
