<?php

namespace App\Exports;

use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

final class ClientsExport extends CollectionExport
{
    protected string $collectionKey = 'clients';

    protected string $filename = 'koree_clients_list';

    protected string $viewPath = 'backend.exports.clients';

    protected function getCollection(Request $request): Collection
    {
        return Client::with('user', 'pays', 'quartier.ville', 'parrain.parrain', 'wallets.programme.commerce')
            ->when($request->filled('period_start'), function (Builder $query) use ($request) {
                $date = Carbon::parse($request->get('period_start'))->toDateTimeString();
                $date && $query->whereDate('created_at', '>=', $request->input('period_start'));
            })
            ->when($request->filled('period_end'), function (Builder $query) use ($request) {
                $date = Carbon::parse($request->get('period_end'))->toDateTimeString();
                $date && $query->whereDate('created_at', '<=', $request->input('period_end'));
            })->latest()->get();
    }
}
