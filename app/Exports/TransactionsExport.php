<?php

namespace App\Exports;

use App\Repositories\Backend\V1\TransactionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

final class TransactionsExport extends CollectionExport
{
    protected string $viewPath = 'backend.exports.transactions';

    protected string $filename = 'koree_transactions_list';

    protected string $collectionKey = 'transactions';

    protected function getCollection(Request $request): Collection
    {
        return app(TransactionRepository::class)
            ->getFilteredQueryForExport($request, $request->filled('deleted'))
            ->orderByDesc('transactions.created_at')
            ->get();
    }
}
