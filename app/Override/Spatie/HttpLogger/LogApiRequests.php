<?php

namespace App\Override\Spatie\HttpLogger;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\HttpLogger\LogProfile;

class LogApiRequests implements LogProfile
{
    public function shouldLogRequest(Request $request): bool
    {
        if (Str::contains($request->url(), 'api/v1') || Str::contains($request->url(), 'api/v2')) {
            return in_array(strtolower($request->method()), ['get', 'post', 'put', 'patch', 'delete']);
        }

        return false;
    }
}
