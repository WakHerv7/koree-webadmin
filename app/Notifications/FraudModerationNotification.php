<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class FraudModerationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private string $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Alerte - Fraud",
                'en' => "Fraud - Alert",
            ])
            ->setBody([
                'fr' => $this->message,
                'en' => $this->message,
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
//            ->setData('type', get_class($this))
        ;


        return $message;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }
}
