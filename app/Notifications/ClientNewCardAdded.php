<?php

namespace App\Notifications;

use App\Models\Commerce;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class ClientNewCardAdded extends Notification implements ShouldQueue
{
    use Queueable;

    private Commerce $commerce;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Commerce $commerce)
    {
        $this->commerce = $commerce;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $commerce = $this->commerce->nom;
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Carte enregistrée",
                'en' => "Carte enregistrée",
//                'en' => "New payment",
            ])
            ->setBody([
                'fr' => "Votre carte de fidélité $commerce a bien été enregistrée",
                'en' => "Votre carte de fidélité $commerce a bien été enregistrée",
//                'en' => "Your new payment has been accepted ",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }
}
