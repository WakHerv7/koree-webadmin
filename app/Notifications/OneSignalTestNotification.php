<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class OneSignalTestNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    /**
     * @var $data
     */
    private $data;
    /**
     * @var bool
     */
    private $persistTodatabase;

    public function __construct(bool $persistTodatabase = false)
    {
        $this->persistTodatabase = $persistTodatabase;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [
            OneSignalChannel::class,
        ];
        return /*$this->persistTodatabase ? array_merge($channels, ['database']) :*/ $channels;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->toOneSignal($notifiable)->toArray();
        return $data;
//    return [
//      'id' => $this->data->id,
//      'sender' => $notifiable->toArray(),
//      'data' => $this->data->toArray(),
//      'code' => 'CommandeCreatedNotification',
//      'redirectUrl' => url().'/office/courses',
//    ];
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "ONe signal Test",
                'en' => "ONe signal Test",
            ])
            ->setBody([
                'fr' => "Nouvelle notification 00 " . app()->environment(),
                'en' => "New delivery notification 00 " . app()->environment(),
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
//            ->setData('type', get_class($this))
        ;


        return $message;

    }


}
