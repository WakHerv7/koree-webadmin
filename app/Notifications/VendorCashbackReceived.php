<?php

namespace App\Notifications;

use App\Models\Client;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class VendorCashbackReceived extends Notification implements ShouldQueue
{
    use Queueable;

    private Transaction $transaction;
    private Client $client;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Client $client, Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->client = $client;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $montant = intval($this->transaction->montant);
        $client = $this->client->getFullname();

        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Cashback versé",
                'en' => "Cashback versé",
//                'en' => "New payment",
            ])
            ->setBody([
                'fr' => "Vous venez de verser un cashback de $montant à $client",
                'en' => "Vous venez de verser un cashback de $montant à $client",
//                'en' => "Your new payment has been accepted ",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }
}
