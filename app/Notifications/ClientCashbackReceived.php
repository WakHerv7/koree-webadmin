<?php

namespace App\Notifications;

use App\Models\Commerce;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class ClientCashbackReceived extends Notification implements ShouldQueue
{
    use Queueable;

    private Commerce $commerce;
    private Transaction $transaction;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Commerce $commerce, Transaction $transaction)
    {
        $this->commerce = $commerce;
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $commerce = $this->commerce->nom;
        $montant = intval($this->transaction->montant);
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Cashback reçu",
                'en' => "Cashback reçu",
//                'en' => "New payment",
            ])
            ->setBody([
                'fr' => "Vous venez de recevoir un cashback chez $commerce de $montant à utiliser dès à présent en boutique!",
                'en' => "Vous venez de recevoir un cashback chez $commerce de $montant à utiliser dès à présent en boutique!",
//                'en' => "Your new payment has been accepted ",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }
}
