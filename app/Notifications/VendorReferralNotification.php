<?php

namespace App\Notifications;

use App\Models\Client;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class VendorReferralNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private Client $client;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $name = $this->client->getFullname();
        return OneSignalMessage::create()
            ->setSubject([
                'fr' => "Bonus de parrainage",
                'en' => "Bonus de parrainage",
            ])
            ->setBody([
                'fr' => "Votre client $name a gagné un bonus de parrainage dans votre boutique de 500!",
                'en' => "Votre client $name a gagné un bonus de parrainage dans votre boutique de 500!",
            ])
            ->setData('type', get_class($this));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }
}
