<?php

namespace App\Notifications;

use App\Models\Commerce;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class ClientTopupReceived extends Notification implements ShouldQueue
{
    use Queueable;

    private bool $persistToDatabase;
    private Transaction $transaction;
    private Commerce $commerce;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction, Commerce $commerce, bool $persistToDatabase = false)
    {
        $this->transaction = $transaction;
        $this->persistToDatabase = $persistToDatabase;
        $this->commerce = $commerce;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [OneSignalChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->toOneSignal($notifiable)->toArray();
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $montant = intval($this->transaction->montant);
        $commerce = $this->commerce->nom;
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Top up effectué",
                'en' => "Top up effectué",
//                'en' => "New payment",
            ])
            ->setBody([
                'fr' => "Nous avons enregistré un top up de $montant chez $commerce à utiliser à présent en boutique.",
                'en' => "Nous avons enregistré un top up de $montant chez $commerce à utiliser à présent en boutique.",
//                'en' => "Your new payment has been accepted ",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }
}
