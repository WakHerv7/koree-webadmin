<?php

namespace App\Notifications;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class PaymentVendorNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    /**
     * @var bool
     */
    private $persistTodatabase;

    /**
     * @var Transaction
     */
    private Transaction $transaction;

    public function __construct(Transaction $transaction, bool $persistTodatabase = false)
    {
        $this->persistTodatabase = $persistTodatabase;

        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [
            OneSignalChannel::class,
        ];
        return/* $this->persistTodatabase ? array_merge($channels, ['database']) : */ $channels;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->toOneSignal($notifiable)->toArray();
        return $data;
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $client = $this->transaction->wallet->client->getFullname();
        $montant = intval($this->transaction->montant);
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Paiement effectué",
                'en' => "Paiement effectué",
//                'en' => "New payment",
            ])
            ->setBody([
                'fr' => "Votre client $client vient de payer $montant! 💸",
                'en' => "Votre client $client vient de payer $montant! 💸",
//                'en' => "Your new payment has been accepted ",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }
}
