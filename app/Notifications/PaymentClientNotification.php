<?php

namespace App\Notifications;

use App\Models\Transaction;
use App\Models\User;
use App\Models\WalletClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;

class PaymentClientNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    /**
     * @var bool
     */
    private $persistTodatabase;

    /**
     * @var WalletClient
     */
    private WalletClient $walletClient;
    /**
     * @var Transaction
     */
    private Transaction $transTopup;

    public function __construct(WalletClient $walletClient, Transaction $transTopup, bool $persistTodatabase = false)
    {
        $this->persistTodatabase = $persistTodatabase;

        $this->walletClient = $walletClient;
        $this->transTopup = $transTopup;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $channels = [
            OneSignalChannel::class,
        ];
        return/* $this->persistTodatabase ? array_merge($channels, ['database']) : */$channels;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = $this->toOneSignal($notifiable)->toArray();
        return $data;
    }

    /**
     * @param User $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        $montant = (int) $this->transTopup->montant;
        $commerce = $this->transTopup->wallet->programme->libelle;
        $message = OneSignalMessage::create()
            ->setSubject([
                'fr' => "Paiement effectué",
                'en' => "Paiement effectué",
            ])
            ->setBody([
                'fr' => "Votre paiement de {$montant} chez ${commerce} a bien été effectué",
                'en' => "Votre paiement de {$montant} chez ${commerce} a bien été effectué",
            ])
//            ->setData('notifiable', $notifiable->only(['id', 'email', 'telephone']))
//            ->setData('notifiable_id', $notifiable->id)
//            ->setData('notifiable_type', get_class($notifiable))
            ->setData('type', get_class($this));


        return $message;

    }


}
