<?php

namespace App\Mail;

use App\Models\FraudConfig;
use App\Models\TransactionTrace;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FraudMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public FraudConfig $config;
    public TransactionTrace $trace;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FraudConfig $config, TransactionTrace $trace)
    {
        $this->config = $config;
        $this->trace = $trace;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@koree.africa', "Koree")->subject("Alerte - Nouvelle fraude")->view("mails.fraud")->text("mails.fraud-text");
    }
}
