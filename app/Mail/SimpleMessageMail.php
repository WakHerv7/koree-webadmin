<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SimpleMessageMail extends Mailable
{
    use Queueable, SerializesModels;

    private string $customMessageBody;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('mails.simple-message', [
                'messageBody' => $this->customMessageBody
            ]);
    }

    /**
     * @return string
     */
    public function getCustomMessageBody(): string
    {
        return $this->customMessageBody;
    }

    /**
     * @param string $customMessageBody
     */
    public function setCustomMessageBody(string $customMessageBody)
    {
        $this->customMessageBody = $customMessageBody;
        return $this;
    }
}
