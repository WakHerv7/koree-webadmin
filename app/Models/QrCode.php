<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property-read QrCodeCampagne $campagne
 * @property-read int $montant
 * @mixin IdeHelperQrCode
 */
class QrCode extends Model
{
    use HasFactory, HasUuids;

    protected $table = "qr_codes";


    static $qr_codes_campagnes = 'qr_codes_campagnes';

    protected $guarded = [];

    protected $casts = [
        'data' => 'array',
    ];

    public function campagne()
    {
        return $this->belongsTo(QrCodeCampagne::class, 'campagne_id');
    }

    public function getMontantAttribute()
    {
        return (int) data_get($this->data, 'montant');
    }
}
