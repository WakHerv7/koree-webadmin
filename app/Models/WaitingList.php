<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WaitingList
 *
 * @package App\Models
 * @property string $tel
 * @mixin IdeHelperWaitingList
 */
class WaitingList extends Model
{
    use HasFactory;
//    use SoftDeletes;

    protected $table = 'waiting_lists';

    protected $fillable = [
        'tel'
    ];
}
