<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperRestriction
 */
class Restriction extends Model
{

    protected $fillable = ['batch', 'client_id', 'processed', 'end_date'];

    protected $casts = [
        'end_date' => 'datetime'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
