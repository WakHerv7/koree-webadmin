<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int note
 * @mixin IdeHelperCategoryCommerce
 */
class CategoryCommerce extends Model
{
    use HasFactory;

    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories_commerces';

    protected $guarded = ['id'];

    protected $appends = ['total_commerces'];

    public function notes()
    {
        return $this->hasManyThrough(Notes::class, Commerce::class, 'category_id', 'commerce_id');

//        return $this->hasMany(Notes::class, 'category_id');
    }

    public function commerces()
    {
        return $this->hasMany(Commerce::class, 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\CategoryCommerce', 'parent_id');
    }

    public function getTotalCommercesAttribute()
    {
        return $this->commerces()->count();
    }


    public function getTotalAttribute()
    {
        return $this->getTotalNotesAttribute();
    }

    public function getTotalNotesAttribute()
    {
        return $this->notes()->sum('note');
    }

}
