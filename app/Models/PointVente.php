<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperPointVente
 */
class PointVente extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'points_ventes';

    protected $guarded = ['id'];


    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce', 'commerce_id');
    }


    public function agents()
    {
        return $this->hasMany('App\Models\Agent', 'point_vente_id');
    }

    public function district()
    {
        return $this->belongsTo(Quartier::class, 'district_id');
    }

    public function city()
    {
        return $this->belongsTo(Ville::class, 'city_id');
    }


}
