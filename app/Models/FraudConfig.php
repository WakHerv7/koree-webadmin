<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @mixin IdeHelperFraudConfig
 */
class FraudConfig extends Model
{

    protected $casts = [
        'global' => 'boolean',
        'look_agent' => 'boolean',
        'flag_relatives' => 'boolean',
        'notify_shop_owner' => 'boolean',
        'notify_admin' => 'boolean',
        'compare_to_shop_limit' => 'boolean',
        'authorize' => 'boolean',
        'active' => 'boolean'
    ];

    public function displayWhoToNotify()
    {
        $notify = [];
        if ($this->notify_shop_owner) {
            $notify[] = "VENDEUR";
        }
        if ($this->notify_admin) {
            $notify[] = "ADMIN";
        }
        return join(",", $notify);
    }

    public function displayWhatToDo()
    {
        return $this->restrict == "yes" ? "BLOQUER" : ($this->restrict == "no" ? "RIEN FAIRE" : "LAISSER L'ADMIN DÉCIDER");
    }

    public function displayLimit()
    {
        if ($this->compare_to_shop_limit) {
            return Str::upper(($this->transaction_type == "all" ? "transaction" : $this->transaction_type) . " - limite commerce");
        }
        $limit = $this->value . " " . ($this->transaction_type == "all" ? "transaction" : $this->transaction_type);
        return Str::upper($limit);
    }

    public function displayGlobal()
    {
        return $this->global ? "TOUT COMMERCE CONFONDU" : "POUR LE MÊME COMMERCE";
    }

    public function displayInterval(bool $restriction = false)
    {
        if ($restriction)
            return ($this->restriction_duration_unit == "infinity" || !$this->flag_relatives) ? "PAS DE LIMITE" : $this->restriction_duration_value . " " . Str::upper($this->pluralize($this->restriction_duration_value, $this->restriction_duration_unit));
        return $this->interval_unit == "infinity" ? "PAS DE LIMITE" : $this->interval_value . " " . Str::upper($this->pluralize($this->interval_value, $this->interval_unit));
    }

    private function pluralize(int $count, string $singular, string $plural = null)
    {
        if ($plural == null) $plural = $singular . "s";
        return $count < 2 ? $singular : $plural;
    }
}
