<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property-read Vendeur $vendeur
 * @property-read Collection $bonusTransactions
 * @mixin IdeHelperWalletVendeur
 */
class WalletVendeur extends Model
{
    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wallets_vendeur';

    protected $guarded = ['id'];


    public function vendeur()
    {
        return $this->belongsTo('App\Models\Vendeur', 'vendeur_id');
    }


    public function programme()
    {
        return $this->belongsTo('App\Models\Programme', 'programme_id');
    }

    public function bonusTransactions()
    {
        return $this->hasMany(Transaction::class, 'wallet_vendeur_id', 'id')->whereIn('type_transaction', ['first_buy', 'parrainage']);
    }


}
