<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperTransactionTrace
 */
class TransactionTrace extends Model
{

    public function fraud()
    {
        return $this->belongsTo(FraudConfig::class, 'fraud_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function vendeur()
    {
        return $this->belongsTo(Vendeur::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }
}
