<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperQuartier
 */
class Quartier extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quartiers';

    protected $guarded = ['id'];

    // retourne le pays de la ville
    public function pays()
    {
        return $this->belongsTo('App\Models\Pays', 'country_id');
    }

    public function ville() {
        return $this->belongsTo(Ville::class, 'city_id');
    }


}
