<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @mixin IdeHelperProgramme
 */
class Programme extends Model
{

    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'programmes';

    protected $guarded = ['id'];

    protected $appends = ['nombreclients'];

    protected $casts = [
        'is_cashback' => 'boolean'
    ];



    public function shop()
    {
        return $this->commerce();
    }

    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce', 'commerce_id');
    }

    public function wallets_vendeur()
    {
        return $this->hasOne('App\Models\WalletVendeur');
    }

    public function wallets()
    {
        return $this->hasMany('App\Models\WalletClient');
    }

    public function getNombreclientsAttribute()
    {

        return $this->wallets()->count();
        // on recupere toutes les notes effectues sur ce commerce
//        $total = 0;
//        $total = DB::table('wallets')->where('programme_id', $this->id)->count();
//        return $total;

    }

    public function getExpiresAttribute()
    {
        return \Illuminate\Support\Carbon::parse($this->getAttribute('date_fin'))->isPast();

    }


}
