<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Transaction
 *
 * @package App\Models
 * @property-read WalletClient $wallet
 * @property-read string $uuid
 * @mixin IdeHelperTransaction
 */
class Transaction extends Model
{

    use HasUuids;

    const TYPE_CREDIT_BONUS_PARRAINAGE = 'parrainage';
    const TYPE_CREDIT_CASHBACK = 'cashback';
    const TYPE_DEBIT_PAIEMENT = 'paiement';
    const TYPE_CREDIT_TOPUP = 'topup';
    const TYPE_CREDIT_FIRSTBUY = 'first_buy';
    const TYPE_CREDIT_CASHBACK_SUR_MONNAIE = 'cashback_over_topup';

    const STATUS_REVERTED = 0;
    const STATUS_DONE = 1;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    protected $guarded = ['id'];

    protected $appends = [/*'tabclient','achat',*/
        //'commerce'
    ];

    protected $casts = [
        'created_at' => 'datetime'
    ];

    public static function makeCashbackFromPaiementAmount(WalletClient $walletClient, WalletVendeur $walletVendeur, float $totalAmount)
    {
        if (!$walletClient->programme->is_cashback) {
            return false;
        }

        $montantCashback = $walletClient->programme->is_cashback ? max(0, ($totalAmount * ($walletClient->programme->taux_cashback / 100))) : 0;


        /** @var CustomCashback $customCashbackRule */
        $customCashbackRule = $walletClient->programme->commerce->activeCustomCashbacks()->where('max_amount', '<=', $totalAmount)->first();
        if ($customCashbackRule) {
            $customCashbackCeil = config('business.cashback.custom_cashback_amount_ceil');
            $montantCashback = $walletClient->programme->is_cashback ? min($customCashbackCeil, ($totalAmount * ($customCashbackRule->cashback_pct / 100))) : 0;
        }

        return self::makeCashback($walletClient, $walletVendeur, $montantCashback);
    }

    public static function makeCashback(WalletClient $walletClient, WalletVendeur $walletVendeur, float $montantCashback)
    {


        $walletClient->deleted = false;
        $walletClient->montant = $walletClient->montant + $montantCashback;
        $walletClient->cashback = $walletClient->cashback + $montantCashback;
        $walletClient->save();

        $walletVendeur->cashback = $walletVendeur->cashback + $montantCashback;
        $walletVendeur->save();


        // creation  transaction de  cashback

        return Transaction::create([
            'wallet_id' => $walletClient->id,
            'wallet_vendeur_id' => $walletVendeur->id,
            'montant' => $montantCashback,
            'solde_wallet' => $walletClient->montant,
            'created_by' => $walletClient->client->user->id,
            'type_transaction' => Transaction::TYPE_CREDIT_CASHBACK,
            'gain' => 0,
            'status' => 1,
        ]);

    }

    public function achat()
    {
        return $this->belongsTo(Achat::class);
    }

    /**
     * @return BelongsTo<WalletVendeur>
     */
    public function wallet_vendeur()
    {
        return $this->belongsTo(WalletVendeur::class, 'wallet_vendeur_id');
    }

    public function wallet()
    {
        return $this->belongsTo('App\Models\WalletClient', 'wallet_id');
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactive(Builder $query)
    {
        return $query->where('status', 0);
    }

    public function programme()
    {
        return $this->wallet_vendeur()->programme;
    }

    public function commerce()
    {
        return $this->programme()->commerce;
    }

//    public function getCommerceAttribute()
//    {
//        // on recupere lachat
//        if ($this->wallet_vendeur_id === null) {
//            $comm = $this->wallet->programme->commerce;
//            return $comm;
//        } else {
//            $comm = $this->wallet_vendeur->programme->commerce;
//            return $comm;
//        }
//    }





}
