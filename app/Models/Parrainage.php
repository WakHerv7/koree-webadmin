<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperParrainage
 */
class Parrainage extends Model
{
   // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
   // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'parrainage';

    protected $guarded = ['id'];

    protected $casts = [
        'status' => 'boolean'
    ];


    public function parrain()
    {
        return $this->belongsTo('App\Models\Client','parrain_id');
    }

    public function filleul()
    {
        return $this->belongsTo('App\Models\Client','filleul_id');
    }
}
