<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

/**
 * Class Client
 *
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property User $user
 * @property-read string $email
 * @mixin IdeHelperClient
 */
class Client extends Model
{
    // use SoftDeletes;

    const FOLDER = 'clients';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    protected $guarded = ['id'];

    protected $casts = [
        'date_naissance' => 'datetime',
        'first_achat' => 'boolean',
        'influencer' => 'boolean',
    ];

    public function getUpdatedPhoto(): ?string
    {
        return $this->getProfileImgUrl();
    }

    private function getProfileImgUrl(): string
    {
        return config('filesystems.disks.s3.url') . Client::FOLDER . $this->photo;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the user's email
     *
     * @return string
     */
    public function getEmailAttribute()
    {
        // dd($this->user);
        return $this->user ? $this->user->email : null;
    }

    public function email()
    {
        return $this->getEmailAttribute();
    }


    public function ville()
    {
        return $this->belongsTo(Ville::class, 'city_id');
    }

    public function pays()
    {
        return $this->belongsTo(Pays::class, 'country_id');
    }

    public function quartier()
    {
        return $this->belongsTo(Quartier::class, 'quartier_id');
    }

    public function getFullname(): string
    {
        return Str::ucfirst($this->prenom) . " " . Str::upper($this->nom);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function parrain()
    {
        return $this->belongsTo(Parrainage::class, 'id', 'filleul_id');
    }

    public function wallets()
    {
        return $this->hasMany(WalletClient::class, 'client_id', 'id');
    }

    public function parrainages()
    {
        return $this->hasMany(Parrainage::class, 'parrain_id', 'id');
    }

    public function parrainages_nonactives()
    {
        return $this->hasMany(Parrainage::class, 'parrain_id', 'id')->where('status', '=', '0');
    }

    public function parrainages_actives()
    {
        return $this->hasMany(Parrainage::class, 'parrain_id', 'id')->where('status', '=', '1');
    }

    public function filleuls()
    {
        return $this
//            ->whereNotNull('influencer')
//            ->where('influencer', '=', '1')
            ->belongsToMany(Client::class, 'parrainage', 'parrain_id', 'filleul_id');
    }

    public function deletion_request(): HasOne
    {
        return $this->hasOne(DeletionRequest::class,  'customer_id');
    }
}
