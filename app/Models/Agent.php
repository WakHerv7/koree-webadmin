<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperAgent
 */
class Agent extends Model
{
    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agents';

    protected $guarded = ['id'];

    protected $casts = [
        'is_admin' => 'boolean'
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'agent_id', 'id');
    }


    public function pointVente()
    {
        return $this->belongsTo('App\Models\PointVente', 'point_vente_id');
    }

//    public function commerce()
//    {
//        return $this->hasOneThrough(Commerce::class, PointVente::class);
//    }


}
