<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperPays
 */
class Pays extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    protected $guarded = ['id'];

    protected function phonecode(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => '00' . $value,
        );
    }

    // recupere les villes du pays
    public function villes()
    {
        return $this->hasMany('App\Models\Ville', 'country_id');
    }

    // recupere les villes du pays
    public function regions()
    {
        return $this->hasMany('App\Models\Region', 'country_id');
    }

    // manuy clients
    public function clients()
    {
        return $this->hasMany('App\Models\Client', 'country_id');
    }



    /**
     * Get all of the vendeurs for the country.
     */
    public function vendeurs()
    {
        return $this->hasManyThrough('App\Models\Vendeur', 'App\Models\Ville');
    }

}
