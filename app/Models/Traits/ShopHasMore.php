<?php

namespace App\Models\Traits;

use App\Models\Commerce;

/**
 * Traits ShopHasMore
 *
 * @property string $logo_url
 * @property string $card_url
 * @property string $background_url
 * @property string $rating
 * @property int $vendeur_id
 * @property int $created_by
 */
trait ShopHasMore
{

    private function getUrlImage($folder, $filename, $defaultFile): string
    {
        return $filename ? config('filesystems.disks.s3.url') . $folder . $filename : config('filesystems.disks.s3.url') . Commerce::FOLDER_DEFAULTS . $defaultFile;
    }

    public function getRatingAttribute(): float
    {
        return $this->notes()->average('note') ?? 0;
    }

    public function getLogoUrlAttribute(): ?string
    {
        return $this->getUrlImage(Commerce::FOLDER_COMMERCES, $this->logo, 'logo.png');
    }

    public function getCardUrlAttribute(): ?string
    {
        return $this->getUrlImage(Commerce::FOLDER_CARDS, $this->card, 'card.png');
    }

    public function getBackgroundUrlAttribute(): ?string
    {
        return $this->getUrlImage(Commerce::FOLDER_BACKGROUNDS, $this->background, 'background.png');
    }

    public function getTicketScanQuotaPerUserDailyAttribute()
    {
        return $this->configuration?->ticket_scan_quota_per_user_daily ?? 0;
    }

    public function getTicketScanQuotaAllUserDailyAttribute()
    {
        return $this->configuration?->ticket_scan_quota_all_user_daily ?? 100;
    }


}
