<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperShopReferral
 */
class ShopReferral extends Model
{
    use HasFactory;

    public function commerce()
    {
        return $this->hasOne(Commerce::class, 'id', 'commerce_id');
    }

    public function parrain()
    {
        return $this->hasOne(Client::class, 'id', 'parrain_id');
    }

    public function filleul()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function transactions()
    {
        return $this->hasManyThrough(Transaction::class, WalletClient::class, 'id', 'wallet_id');
    }
}
