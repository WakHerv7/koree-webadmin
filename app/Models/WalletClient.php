<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Wallet
 *
 * @package App\Models
 * @property int $id
 * @property int $client_id
 * @property int $programme_id
 * @property int $id_fidelite
 * @property Client $client
 * @property-read Programme $programme
 * @mixin IdeHelperWalletClient
 */
class WalletClient extends Model
{
    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'wallets';

    protected $dateFormat = 'Y-m-d';

    protected $guarded = ['id'];

    protected $casts = ['updated_at' => 'timestamp', 'vip' => 'boolean'];

    protected $appends = [
        'walletvendeur',
    ];


    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id');
    }

    public function programme()
    {
        return $this->belongsTo('App\Models\Programme', 'programme_id');
    }


    public function getWalletvendeurAttribute()
    {
        // on recupere toutes les notes effectues sur ce commerce
        $wal = DB::table('wallets_vendeur')->where('programme_id', $this->programme_id)->first();

        if ($wal) {
            return $wal->id_fidelite;
        } else {
            return null;
        }
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d');
    }

}
