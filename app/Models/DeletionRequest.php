<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperDeletionRequest
 * @property-read string $label
 * @property-read Client $client
 * @property-read Vendeur $vendor
 */
class DeletionRequest extends Model
{
    use HasFactory;

    public function vendor()
    {
        return $this->belongsTo(Vendeur::class, 'vendeur_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'customer_id', 'id');
    }

    public function commerce()
    {
        return $this->belongsTo(Commerce::class, 'commerce_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function getLabelAttribute()
    {
        return blank($this->getAttribute('customer_id')) ? 'commerce/vendeur' : 'client';
    }
}
