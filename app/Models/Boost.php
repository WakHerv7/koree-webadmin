<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperBoost
 */
class Boost extends Model
{
   // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
   // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boosts';

    protected $guarded = ['id'];


    public function package()
    {
        return $this->belongsTo('App\Models\PackageBoost','package_id');
    }
    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce','commerce_id');
    }

}
