<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperCustomCashback
 */
class CustomCashback extends Model
{
    use HasFactory;

    protected $casts = [
        'active' => 'boolean'
    ];

    public function commerce()
    {
        return $this->belongsTo(Commerce::class);
    }
}
