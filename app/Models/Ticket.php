<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Provider
 *
 * @property int $id
 * @property int $amount
 * @property array $ocr_data
 * @property string $ocr_driver
 * @property string $content_hash
 * @property string $content_hash_algo
 * @property Carbon $created_at
 * @property Carbon $charged_at
 * @property Carbon $updated_at
 * @property int $wallet_id
 * @property WalletClient $wallet
 * @property User $canceller
 * @property User $allower
 * @property int $transaction_id
 * @property Transaction $transaction
 */
class Ticket extends Model
{
    use HasFactory;

    const FOLDER = '/tickets/';

    protected $fillable = [
        'amount',
        'wallet_id',
        'content_hash_algo',
        'content_hash',
        'ocr_data',
        'ocr_driver',
        'bill_ref',
        'charged_at',
        'charged_by',
        'cancelled_at',
        'cancelled_by',
        'cancel_reason',
        'image',
    ];

    protected $casts = [
        'amount' => 'int',
        'wallet_id' => 'int',
        'ocr_data' => 'array',
        'charged_at' => 'datetime',
        'charged_by' => 'int',
        'cancelled_at' => 'datetime',
        'cancelled_by' => 'int',

    ];

    public function wallet()
    {
        return $this->belongsTo(WalletClient::class, 'wallet_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function getMediaUrl()
    {
        return config('filesystems.disks.s3.url') . Ticket::FOLDER . $this->image;
    }

    public function canceller()
    {
        return $this->belongsTo(User::class, 'cancelled_by');
    }

    public function allower()
    {
        return $this->belongsTo(User::class, 'charged_by');
    }
}
