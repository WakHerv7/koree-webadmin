<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperPackageBoost
 */
class PackageBoost extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'package_boost';

    protected $guarded = ['id'];

    public function pays()
    {
        return $this->belongsTo('App\Models\Pays', 'country_id');
    }

}
