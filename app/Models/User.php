<?php

namespace App\Models;

use Barryvdh\LaravelIdeHelper\UsesResolver;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @package App\Models
 * @mixin UsesResolver
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, HasRoles, Notifiable, TwoFactorAuthenticatable;


    const TYPE_USER = 'user';
    const TYPE_ADMIN = 'admin';
    const ROLE_ADMIN = 'Administrateur';
    const TYPE_SUPER_ADMIN = 'super-admin';
    const ROLE_SUPER_ADMIN = 'Super-Admin';
    const TYPE_VENDEUR = 'vendeur';
    const ROLE_VENDEUR = 'Vendeur';
    const TYPE_CLIENT = 'client';
    const ROLE_CLIENT = 'Client';
    const TYPE_ANALYST = 'analyst';
    const ROLE_ANALYST = 'Analyst';
    const TYPE_DEVELOPER = 'developer';
    const ROLE_DEVELOPER = 'Developer';

    protected $appends = [
        'onesignal_email_auth_hash',
        'onesignal_sms_auth_hash',
    ];

    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function providers()
    {
        return $this->hasMany(Provider::class, 'user_id', 'id');
    }

    // retourne la liste des champs qui peuvent etre remplies
    public function getFillable()
    {
        return $this->fillable;
    }

    public function client()
    {
        return $this->hasOne(Client::class, 'user_id');
    }


    public function vendeur()
    {
        return $this->hasOne(Vendeur::class,'user_id');
    }

    public function routeNotificationForSns($notification)
    {
        // dd($notification->data['message']);
        return $this->getAttribute('telephone');
        // return $notification->data['msisdn'];
    }


    public function routeNotificationForOneSignal()
    {

        return [
            'include_external_user_ids' => [
                $this->getAttribute('telephone') ?? "{$this->id}"
            ]
        ];

    }

    public function routeNotificationForWhatsapp()
    {
        return $this->getAttribute('telephone');
    }

    public function getOnesignalEmailAuthHashAttribute()
    {
        $ONESIGNAL_REST_API_KEY = config('services.onesignal.rest_api_key');
        return hash_hmac('sha256', $this->email, $ONESIGNAL_REST_API_KEY);

    }

    public function getOnesignalSmsAuthHashAttribute()
    {
        $ONESIGNAL_REST_API_KEY = config('services.onesignal.rest_api_key');
        $tel = "+" . ltrim($this->telephone, "00");
        return hash_hmac('sha256', $tel, $ONESIGNAL_REST_API_KEY);

    }

    function hasTypeUser(string ...$roles): bool
    {
        $roles = array_map('strtolower', $roles);
        return in_array(strtolower($this->getAttribute('type_user')), $roles);

    }

    function hasBackendTypeUser(): bool
    {
        $roles = array_map('strtolower', [
            self::TYPE_USER,
            self::TYPE_DEVELOPER,
            self::TYPE_ADMIN,
            self::TYPE_SUPER_ADMIN,
        ]);
        return in_array(strtolower($this->getAttribute('type_user')), $roles);

    }

    function hasBackendUserRole(): bool
    {
        $roles = array_map('ucfirst', [
            self::ROLE_ADMIN,
            self::ROLE_DEVELOPER,
            self::ROLE_SUPER_ADMIN,
        ]);
        return $this->hasRole($roles);

    }


}
