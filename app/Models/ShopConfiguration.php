<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $ticket_scan_quota_per_user_daily
 * @property int $ticket_scan_quota_all_user_daily
 * @property bool allow_cashback_over_ticket_scan
 */
class ShopConfiguration extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'ticket_scan_quota_per_user_daily' => 'integer',
        'ticket_scan_quota_all_user_daily' => 'integer',
        'allow_cashback_over_ticket_scan' => 'boolean',
    ];


    protected $fillable = [
        'shop_id',
        'ticket_scan_quota_per_user_daily',
        'ticket_scan_quota_all_user_daily',
        'allow_cashback_over_ticket_scan',
    ];

    public function shop()
    {
        return $this->belongsTo(Commerce::class,  'shop_id');
    }
}
