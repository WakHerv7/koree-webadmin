<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperRegion
 */
class Region extends Model
{
    //use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    //protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'states';

    protected $guarded = ['id'];

    // recupere les villes du pays
    public function villes()
    {
        return $this->hasMany('App\Models\Ville');
    }

    public function pays()
    {
        return $this->belongsTo('App\Models\Pays', 'country_id');
    }
}
