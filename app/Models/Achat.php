<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperAchat
 */
class Achat extends Model
{
    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achat';

    protected $guarded = ['id'];

    protected $casts = [
        'updated_at' => 'timestamp',
        'created_at' => 'timestamp',
    ];


    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce', 'commerce_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'created_by');
    }

}
