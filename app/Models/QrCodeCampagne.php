<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class QrCodeCampaign
 *
 * @property int $id
 * @property int $commerce_id
 * @property string $nom
 * @property Carbon $date_from
 * @property Carbon $date_until
 * @property int $quantite
 * @property int $height_in_cm
 * @property string $shape
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Commerce $commerce
 * @property QrCode[] $qrcodes
 */
class QrCodeCampagne extends Model
{
    use HasUuids;
    use HasFactory;

    protected $table = 'qr_codes_campagnes';

    protected $casts = [
        'commerce_id' => 'int',
        'height_in_cm' => 'int',
        'quantite' => 'int',
    ];

    protected $guarded = [];

    public function commerce(): BelongsTo
    {
        return $this->belongsTo(Commerce::class, 'commerce_id');
    }

    public function qrcodes(): HasMany
    {
        return $this->hasMany(QrCode::class, 'campagne_id');
    }
}
