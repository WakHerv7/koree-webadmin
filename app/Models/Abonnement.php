<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperAbonnement
 */
class Abonnement extends Model
{
   // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
   // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'abonnement';

    protected $guarded = ['id'];


    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce','commerce_id');
    }



}
