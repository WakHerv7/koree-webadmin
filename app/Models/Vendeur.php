<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property-read User $user
 * @mixin IdeHelperVendeur
 */
class Vendeur extends Model
{
    use HasFactory;
   // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
   // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vendeurs';

    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }


    public function ville()
    {
        return $this->belongsTo('App\Models\Ville', 'city_id');
    }

    public function pays()
    {
        return $this->belongsTo('App\Models\Pays', 'country_id');
    }

    public function commerce()
    {
        return $this->hasOne(Commerce::class);
    }

    public function programme()
    {
        return $this->hasOneThrough('App\Models\Programme','App\Models\Commerce');
    }


}
