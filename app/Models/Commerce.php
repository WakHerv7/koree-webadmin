<?php

namespace App\Models;

use App\Models\Traits\ShopHasMore;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @property-read Programme $programme
 * @property-read Vendeur $vendeur
 * @property-read Collection $pointVentes
 * @property-read CategoryCommerce $category
 * @mixin IdeHelperCommerce
 */
class Commerce extends Model
{
    use HasFactory, ShopHasMore;

    const FOLDER_CARDS = '/cards/';
    const FOLDER_COMMERCES = '/commerces/';
    const FOLDER_BACKGROUNDS = '/backgrounds/';
    const FOLDER_DEFAULTS = '/defaults/';

    // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commerces';

    protected $guarded = ['id'];

    protected $appends = [
        'notes',
        'ticket_scan_quota_per_user_daily',
        'ticket_scan_quota_all_user_daily',
    ];

    public function categorie()
    {
        return $this->belongsTo(CategoryCommerce::class, 'category_id');
    }

    public function vendeur()
    {
        return $this->belongsTo('App\Models\Vendeur', 'vendeur_id');
    }

    public function ville()
    {
        return $this->belongsTo('App\Models\Ville', 'city_id');
    }

    public function quartier()
    {
        return $this->belongsTo('App\Models\Quartier', 'quartier_id');
    }


    public function agents()
    {
        return $this->hasManyThrough(
            Agent::class, PointVente::class,
            'commerce_id',
            'point_vente_id'
        );
    }

    public function programme()
    {
        return $this->hasOne(Programme::class, 'commerce_id');
    }


    public function configuration(): HasOne
    {
        return $this->hasOne(ShopConfiguration::class, 'shop_id');
    }

    public function pointVentes()
    {
        return $this->hasMany('App\Models\PointVente', 'commerce_id');
    }

    public function getNotesAttribute()
    {
        // on recupere toutes les notes effectues sur ce commerce
        $total = DB::table('notes')->where('commerce_id', $this->id)->get();
        $moyenne = 0.0;
        $nbr = 0;
        $somme = 0;
        foreach ($total as $to) {
            $nbr++;
            $somme += $to->note;
        }
        if ($nbr > 0) {
            $moyenne = $somme / $nbr;
        }
        return round($moyenne, PHP_ROUND_HALF_DOWN);

    }

    public function customCashbacks(): HasMany
    {
        return $this->hasMany(CustomCashback::class, 'commerce_id', 'id')->orderByDesc('max_amount');
    }

    public function activeCustomCashbacks(): HasMany
    {
        return $this->hasMany(CustomCashback::class, 'commerce_id', 'id')->where('active', true)->orderByDesc('max_amount');

    }

    public function config()
    {
        return $this->configuration();
    }
}
