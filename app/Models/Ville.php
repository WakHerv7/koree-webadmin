<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperVille
 */
class Ville extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cities';

    protected $guarded = ['id'];

    // retourne le pays de la ville
    public function pays(): BelongsTo
    {
        return $this->belongsTo(Pays::class, 'country_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'state_id');
    }

    public function quartiers()
    {
        return $this->hasMany(Quartier::class, 'city_id');
    }
}
