<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperShopSuggestion
 */
class ShopSuggestion extends Model
{

    protected $table = 'suggest_vendors';
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime'
    ];

    public function getFullname()
    {
        return $this->shop_owner_firstname . " " . $this->shop_owner_lastname;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function city()
    {
        return $this->belongsTo(Ville::class, 'shop_city_id');
    }

    public function district()
    {
        return $this->belongsTo(Quartier::class, 'shop_district_id');
    }

}
