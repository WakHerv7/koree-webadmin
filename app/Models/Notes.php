<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin IdeHelperNotes
 */
class Notes extends Model
{
   // use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
   // protected $dates = ['deleted_at'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notes';

    protected $guarded = ['id'];


    public function client()
    {
        return $this->belongsTo('App\Models\Client','client_id');
    }

    public function commerce()
    {
        return $this->belongsTo('App\Models\Commerce','commerce_id');
    }

}
