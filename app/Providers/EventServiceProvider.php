<?php

namespace App\Providers;

use App\Events\Pusher\ClientDisabled;
use App\Events\SendSMSEvent;
use App\Listeners\ClientDisabledListener;
use App\Listeners\SendSMSListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        ClientDisabled::class => [
            ClientDisabledListener::class
        ],
        SendSMSEvent::class => [
            SendSMSListener::class
        ],
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
