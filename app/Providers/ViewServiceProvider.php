<?php

namespace App\Providers;

use App\View\Components\Ui\SideNavigation;
use App\View\Components\Ui\SideNavigationIcon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('side-navigation', SideNavigation::class);
        Blade::component('side-navigation-icon', SideNavigationIcon::class);
    }
}
