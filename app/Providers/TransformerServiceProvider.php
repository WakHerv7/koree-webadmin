<?php

namespace App\Providers;

use Flugg\Responder\Transformers\TransformerResolver;
use Illuminate\Support\ServiceProvider;

class TransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->app->make(TransformerResolver::class)->bind([
//            \App\Models\Commerce::class => \App\Transformers\CommerceTransformer::class,
//        ]);
    }
}
