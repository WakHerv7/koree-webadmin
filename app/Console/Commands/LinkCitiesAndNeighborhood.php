<?php

namespace App\Console\Commands;

use App\Models\Quartier;
use App\Models\Ville;
use Illuminate\Console\Command;

class LinkCitiesAndNeighborhood extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cities:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a default quartier for cities with none';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cities = Ville::doesntHave('quartiers')->get();
        foreach ($cities as $city) {
            $quartier = new Quartier;
            $quartier->longitude = $city->longitude;
            $quartier->latitude = $city->latitude;
            $quartier->country_id = $city->country_id;
            $quartier->city_id = $city->id;
            $quartier->name = $city->name;
            $quartier->save();
        }
        $this->info("Command executed");
        return 0;
    }
}
