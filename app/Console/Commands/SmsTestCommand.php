<?php

namespace App\Console\Commands;

use App\Helpers\MesagooSMS;
use App\Helpers\SMS;
use App\Helpers\SnsSMS;
use App\Helpers\WhatsappSMS;
use App\Services\SnsService;
use Aws\Exception\AwsException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Netflie\WhatsAppCloudApi\Response\ResponseException;
use function Pest\Laravel\instance;

class SmsTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan sms:send 237654306774 "Houhhaaa sms via sns work fine"
     *
     * @var string
     */
    protected $signature = 'sms:send {phone} {msg}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $phone = $this->argument('phone');
        $msg = $this->argument('msg') ?? 'NO MESSAGE';


        if (empty($phone)) {
            $this->error('Give a valid  phone number to send test message. CMD: ' . $this->signature);
        }

        $sms = new SMS($phone, $msg);

        try {
            $sms->send() ? $this->info('SMS SEND SUCCESS') : $this->error('SMS SEND FAILURE');
            if ($sms->hasError()) {
                $this->info('WITH FOLLOWINGS ERRORS');
                $this->error($sms->getErrorMessage());
            }
        } catch (\Exception $exception) {
            $this->errorLog('SMS TEST CMD ERROR::' . $exception->getMessage());
        }

//        try {
//            WhatsappSMS::send($sms) ? $this->info('WHATSAPP SMS TEST SUCCESS') : $this->error('WHATSAPP SMS TEST FAILURE');
//        } catch (\Exception $exception) {
//            ($exception instanceof ResponseException) ?
//                $this->errorLog('WHATSAPP SMS TEST CMD ERROR::' . $exception->response()->body()) :
//                $this->errorLog('WHATSAPP SMS TEST CMD ERROR::' . $exception->getMessage());
//        }
//
//        try {
//            MesagooSMS::send($sms) ? $this->info('MESAGOO SMS TEST SUCCESS') : $this->error('MESAGOO SMS TEST FAILURE');
//        } catch (\Exception $exception) {
//            $this->errorLog('MESAGOO SMS TEST CMD ERROR::' . $exception->getMessage());
//        }
//
//        try {
//            SnsSMS::send($sms) ? $this->info('SNS SMS TEST SUCCESS') : $this->error('SNS SMS TEST FAILURE');
//        } catch (\Exception $exception) {
//            ($exception instanceof AwsException) ?
//                $this->errorLog('SNS SMS TEST CMD ERROR::' . $exception->getResponse()->getBody()) :
//                $this->errorLog('SNS SMS TEST CMD ERROR::' . $exception->getMessage());
//        }

        return 0;
    }

    private function errorLog($message)
    {
        Log::error($message);
        $this->error($message);
    }
}
