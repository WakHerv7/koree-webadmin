<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class CreateSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the super admin to manage the add';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $roles = [
            [
                'id' => 1,
                'name' => 'Super-Admin',
                'guard_name' => 'web'
            ], [
                'id' => 2,
                'name' => 'Administrateur',
                'guard_name' => 'web'
            ]
        ];
        foreach ($roles as $r) {
            $role = new Role;
            $role->id = $r['id'];
            $role->name = $r['name'];
            $role->guard_name = $r['guard_name'];
            $role->created_at = now();
            $role->updated_at = now();
            $role->save();
        }

        $user = new User;
        $user->username = "Super Admin KOREE";
        $user->email = "admin@koree.africa";
        $user->type_user = "user";
        $user->telephone = "000000000000";
        $user->created_at = now();
        $user->updated_at = now();
        $user->password = bcrypt("000000");
        $user->save();
        $user->assignRole(Role::where('name', 'Super-Admin')->first());
        $user->save();
        $this->info("Operation done");
        return 0;
    }
}
