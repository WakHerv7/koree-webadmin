<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class GenerateUUID extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uuid:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate UUID for users that does not have !';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (User::whereNull('uuid')->get() as $user) {
            $user->uuid = Str::uuid()->toString();
            $user->save();
        }
        $this->info("UUID Generated successfully");
        return 0;
    }
}
