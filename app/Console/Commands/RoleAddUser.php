<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Role;

class RoleAddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * EXAMPLE
     * =======
     *          php artisan role:adduser coursier api.coursier --userid=-100 --userid=-200
     *          php artisan role:adduser coursier api.coursier --userid=-1
     *          php artisan role:adduser coursier --userid=-1
     *
     *
     * @var string
     */
    protected $signature = 'role:adduser
                                {role*} : The role names list
                                {--userid=*} : The ID (primary key) of the user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ajouter des roles a des utilisateurs sans detachement' . PHP_EOL . ' ex: php artisan role:adduser cousier api.cousier web.coursier --userid=100';

    private $roles = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $roleNames = $this->argument('role');
        $userIds = $this->options()['userid'] ?? [];

        if (!empty($userIds) && !empty($roleNames)) {
            if ($this->confirm('Voulez vous continuer?')) {
                User::whereIn('id', $userIds)->get()->each(function ($user) use ($roleNames) {

                    $user->roles()->syncWithoutDetaching($this->getRolesIds($roleNames));

                    $this->line('les roles de ' . ($user->fullname ?? $user->id) . ' ont été modifiés');
                });
            }

        }
    }

    /**
     * @param array $roleNames
     * @return mixed
     */
    private function getRolesIds(array $roleNames)
    {
        $this->getRoles($roleNames);
        return $this->roles->pluck('id');
    }

    /**
     * @param array $roleNames
     * @return null|Collection
     */
    private function getRoles(array $roleNames)
    {
        if ($this->roles === null) {
            $this->roles = Role::whereIn('name', $roleNames)->get();
        }

        return $this->roles;
    }

}
