<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;


class UserPasswordCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan user:password email@tld.com newpass
     *
     * @var string
     */
    protected $signature = 'user:password {email} {passwd} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change le mot de passse d\'un user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        // $options = $this->options();

        $passwd = $this->argument('passwd') ?? 'NO passwd';
        $email = $this->argument('email');

        if (empty($email)) {
            $this->error('Give an Email to match CMD: ' . $this->signature);
        }


        $user = User::query()->where(compact('email'))->first();
        if (!$user) {
            $this->error('Aucun compte touver avec cet email');
            return 0;
        }
        $user->password = Hash::make($passwd);
        $user->save();
        $this->info('Mise a jour du mot de passe effectuée');

    }
}
