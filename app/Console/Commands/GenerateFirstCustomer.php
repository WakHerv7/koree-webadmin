<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Pays;
use App\Models\Quartier;
use App\Models\User;
use App\Models\Ville;
use Illuminate\Console\Command;

class GenerateFirstCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate first customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $country = Pays::where('iso2', 'CM')->first();
        $cities = Ville::where('country_id', $country->id)->get();
        $districts = Quartier::where('country_id', $country->id)->get();
        $user = User::create([
            'username' => 'underscore',
            'email' => 'leonelkahameni@camaroes.org',
            'telephone' => "00237696852116",
            'password' => bcrypt("000000"),
            'type_user' => 'client',
            'created_by' => 1
        ]);

        $client = Client::create([
            'nom' => 'KAHAMENI',
            'prenom' => 'Léonel',
            'date_naissance' => '1998-08-06',
            'sexe' => 'M',
            'status' => '1',
            'code_parrainage' => 'LEON16', // $this->generateUniqueNumber(),
            'user_id' => $user->id,
            'country_id' => $country->id,
            'created_by' => 1,
            'city_id' => $cities->random()->id,
            'quartier_id' => $districts->random()->id,
        ]);
        return 0;
    }
}
