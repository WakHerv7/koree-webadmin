<?php

namespace App\Console\Commands;

use App\Models\Pays;
use App\Models\Quartier;
use App\Models\Region;
use App\Models\Ville;
use Illuminate\Console\Command;

class ImportAllData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import old data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $countries = collect(json_decode(file_get_contents(resource_path("data/countries.json"))));
        $states = collect(json_decode(file_get_contents(resource_path("data/states.json"))));
        $cities = collect(json_decode(file_get_contents(resource_path("data/cities.json"))));
        $districts = collect(json_decode(file_get_contents(resource_path("data/quartiers.json"))));

        foreach ($countries as $c) {
            $country = new Pays;
            $country->id = $c->id;
            $country->name = $c->name;
            $country->iso2 = $c->iso2;
            $country->iso3 = $c->iso3;
            $country->numeric_code = $c->numeric_code;
            $country->phonecode = $c->phonecode;
            $country->capital = $c->capital;
            $country->currency = $c->currency;
            $country->currency_name = $c->currency_name;
            $country->currency_symbol = $c->currency_symbol;
            $country->tld = $c->tld;
            $country->native = $c->native;
            $country->region = $c->region;
            $country->subregion = $c->subregion;
            $country->timezones = $c->timezones;
            $country->translations = $c->translations;
            $country->latitude = $c->latitude;
            $country->longitude = $c->longitude;
            $country->emoji = $c->emoji;
            $country->emojiU = $c->emojiU;
            $country->flag = $c->flag;
            $country->wikiDataId = $c->wikiDataId;
            $country->created_at = now();
            $country->updated_at = now();
            $country->save();
        }
        foreach ($states as $s) {
            $state = new Region;
            $state->id = $s->id;
            $state->name = $s->name;
            $state->country_code = $s->country_code;
            $state->country_id = $s->country_id;
            $state->fips_code = $s->fips_code;
            $state->iso2 = $s->iso2;
            $state->type = $s->type;
            $state->latitude = $s->latitude;
            $state->longitude = $s->longitude;
            $state->flag = $s->flag;
            $state->wikiDataId = $s->wikiDataId;
            $state->created_at = now();
            $state->updated_at = now();
            $state->save();
        }

        foreach ($cities as $c) {
            $city = new Ville;
            $city->id = $c->id;
            $city->name = $c->name;
            $city->state_id = $c->state_id;
            $city->state_code = $c->state_code;
            $city->country_id = $c->country_id;
            $city->country_code = $c->country_code;
            $city->latitude = $c->latitude;
            $city->longitude = $c->longitude;
            $city->flag = $c->flag;
            $city->wikiDataId = $c->wikiDataId;
            $city->created_at = now();
            $city->updated_at = now();
            $city->save();
        }

        foreach ($districts as $d) {
            $district = new Quartier;
            $district->id = $d->id;
            $district->name = $d->name;
            $district->city_id = $d->city_id;
            $district->country_id = $d->country_id;
            $district->latitude = $d->latitude;
            $district->longitude = $d->longitude;
            $district->updated_at = now();
            $district->created_at = now();
            $district->deleted_at = null;
            $district->save();
        }
        $this->info("Old data imported");
        return 0;
    }
}
