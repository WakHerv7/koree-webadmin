<?php

namespace App\Console\Commands;

use App\Models\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class NormalizeReferralCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'referral:normalize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Normalize referral code to make them be 6 digits long';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Client::with('user')->get()->filter(function (Client $client){
            return mb_strlen(str_replace(' ', '', $client->code_parrainage)) != 6;
        })->each(function (Client $client) {
            $code = $this->generateReferralCode($client->nom, $client->prenom, $client->user->telephone);
            $client->code_parrainage = $code;
            $client->save();
        });
        $this->info("Referral codes normalized");
        return 0;
    }

    private function generateReferralCode($lastname, $firstname, $phone): string
    {
        $code1Name = trim(Str::substr($firstname, 0, 4));
        $code1Phone = Str::substr($phone, -2, 2);

        $code1 = $this->transformString(Str::padRight(Str::upper(Str::padRight($code1Name, 4, "0") . $code1Phone), 6, Str::substr($phone, -1, 1)));

        $code2Name = trim(Str::substr($lastname, 0, 4));
        $code2Phone = Str::substr($phone, -4, 2);

        $code2 = $this->transformString(Str::padRight(Str::upper(Str::padRight($code2Name, 4, "0") . $code2Phone), 6, Str::substr($phone, -3, 1)));

        if (!Client::where('code_parrainage', $code1)->exists()) {
            return $code1;
        }

        if (!Client::where('code_parrainage', $code2)->exists()) {
            return $code2;
        }

        do {
            $code3 = $this->transformString(Str::padRight(Str::upper(Str::random(4) . rand(10, 100)), 6, "0"));
        } while (Client::where('code_parrainage', $code3)->exists());
        return $code3;

    }

    private function transformString(string $string): string
    {
        return str_replace('ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy', $string);
    }

}
