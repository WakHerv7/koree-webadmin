<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;


class SendMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan mail:send email@tld.com "Mail Transport Test" "Houhhaaa it work"
     *
     * @var string
     */
    protected $signature = 'mail:send {email} {title} {msg} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoyer un simple message de test SMTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        // $options = $this->options();

        $title = $this->argument('title') ?? 'NO TITLE';
        $email = $this->argument('email');
        $msg = $this->argument('msg') ?? 'NO MESSAGE';

        if (empty($email)) {
            $this->error('Give an Email to send test message. CMD: ' . $this->signature);
        }

        $data['title'] = $title;
        $data['msg'] = $msg;



        Mail::send('emails.hello', $data, function($message) use ($email, $title) {

            $message->to($email, '')

                ->subject($title);
        });

        if (Mail::failures()) {
            $this->error('Sorry! Please try again latter');

        }else{
            $this->info('Great! Successfully send in your mail to ' . $email);
        }
    }
}
