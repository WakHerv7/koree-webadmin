<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class StorageLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'link:storage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if the storage folder exists in the public folder and call the artisan:link command otherwise';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!File::exists(public_path("storage"))) {
            Artisan::call("storage:link");
        }
        return 0;
    }
}
