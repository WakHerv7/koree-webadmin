<?php

namespace App\Console\Commands;

use App\Models\CategoryCommerce;
use Illuminate\Console\Command;

class LoadCommerceCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load categories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = collect(json_decode(file_get_contents(resource_path("data/categories_commerces.json"))));
        foreach ($categories as $c) {
            $category = new CategoryCommerce;
            $category->id = $c->id;
            $category->libelle = $c->libelle;
            $category->created_by = $c->created_by;
            $category->updated_by = $c->updated_by;
            $category->parent_id = $c->parent_id;
            $category->created_at = now();
            $category->updated_at = now();
            $category->save();
        }
        $this->info("Old categories imported");
        return 0;
    }
}
