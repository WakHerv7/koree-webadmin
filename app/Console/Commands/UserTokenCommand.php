<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;


class UserTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan user:token --userid=3
     *
     * @var string
     */
    protected $signature = 'user:token
                             {--userid=*} : The ID (primary key) of the user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "creer un token personnel d'acces pour un user";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $names = $this->options()['name'] ?? [];
        $userIds = $this->options()['userid'] ?? [];
        $scopes = $this->options()['scopes'] ?? ['*'];

        if (!empty($userIds)) {

            if ($this->confirm('Voulez vous continuer?')) {
                $userId = $userIds[0];

                /** @var User $user */
                $user = User::query()->find($userId);

                if (blank($user)) {
                    $this->error("User not found");
                    return;
                }

                $name = $this->ask("What is the personnal  acces token name to create for user {$user->full_name} ({$user->email})?");


                $token = $user->createToken($name, $scopes)->accessToken;

                $this->line("Personnal Access Token=");
                $this->line("=======================");
                $this->line($token);
            }
        } else {
            $this->error("Verifier les arguments et les options");
        }

    }
}
