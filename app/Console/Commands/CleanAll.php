<?php

namespace App\Console\Commands;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CleanAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean data and reset with a fresh database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call("migrate:fresh");
        $this->info('Database migrated');
        Artisan::call("data:import");
        $this->info('Countries and etc... loaded');
        Artisan::call("categories:load");
        $this->info('Commerce categories loaded');
        Artisan::call("cities:link");
        $this->info('Cities and neighbourhood linked');
        Artisan::call("admin:create");
        $this->info('Admin user created');
        Artisan::call("customer:generate");
        $this->info('First customer generated');
        Artisan::call("users:generate");
        $this->info('Customers and shops generated');
        Artisan::call("make:wallets");
        $this->info('Clients wallet generated');
        $this->info('Data regenerated');
        return 0;
    }
}
