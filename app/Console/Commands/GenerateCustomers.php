<?php

namespace App\Console\Commands;

use App\Models\CategoryCommerce;
use App\Models\Client;
use App\Models\Commerce;
use App\Models\Pays;
use App\Models\Programme;
use App\Models\Quartier;
use App\Models\User;
use App\Models\Vendeur;
use App\Models\Ville;
use App\Models\WalletVendeur;
use Faker\Factory;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GenerateCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faker = Factory::create();
        $clients = 120;
        $shops = 15;
        $parrain = Client::first();
        $country = Pays::where('iso2', 'CM')->first();
        $cities = Ville::where('country_id', $country->id)->get();
        $districts = Quartier::where('country_id', $country->id)->get();

        for ($i = 0; $i < $clients; $i++) {
            $user = User::create([
                'username' => $faker->userName,
                'email' => $faker->safeEmail,
                'telephone' => "00237" . $faker->numberBetween(600000000, 699999999),
                'password' => bcrypt("000000"),
                'type_user' => 'client',
                'created_by' => 1
            ]);

            $gender = $faker->randomElement(['male', 'female']);
            $firstname = $faker->firstName($gender);
            $gender = $gender == "male" ? "M" : "F";
            $client = Client::create([
                'nom' => Str::upper($faker->lastName),
                'prenom' => $firstname,
                'date_naissance' => $faker->dateTimeBetween('-45 years', '-18 years')->format('Y-m-d'),
                'sexe' => $gender,
                'status' => '1',
                'code_parrainage' => Str::upper(Str::substr($firstname, 0, 2). Str::random(2) . Str::substr($user->telephone, -1, 2)),
                'user_id' => $user->id,
                'country_id' => $country->id,
                'created_by' => 1,
                'city_id' => $cities->random()->id,
                'quartier_id' => $districts->random()->id,
            ]);

            if ($i % 2 == 0) {
                DB::table('parrainage')
                    ->insert([
                        'parrain_id' => $parrain->id,
                        'filleul_id' => $client->id,
                        'status' => 0,
                        'code_parrainage' => $parrain->code_parrainage,
                        'created_at' => now()
                    ]);
            }

        }

        $categories = CategoryCommerce::all();

        for ($i = 0; $i < $shops; $i++) {
            $user = User::create([
                'username' => $faker->userName,
                'email' => $faker->safeEmail,
                'telephone' => "00237" . $faker->numberBetween(600000000, 699999999),
                'password' => bcrypt("000000"),
                'type_user' => 'vendeur',
                'created_by' => 1,
            ]);

            $vendeur = Vendeur::create([
                'nom' => Str::upper($faker->lastName),
                'prenom' => $faker->firstName,
                'adresse' => $faker->streetAddress,
                'rccm' => Str::upper(Str::random()),
                'code_transaction' => "000000",
                'code_postal' => $faker->postcode,
                'telephone1' => "00237" . $faker->numberBetween(600000000, 699999999),
                'telephone2' => "00237" . $faker->numberBetween(600000000, 699999999),
                'horaires' => '',
                'contact_compte' => "00237" . $faker->numberBetween(600000000, 699999999),
                'city_id' => $cities->random()->id,
                'country_id' => $country->id,
                'created_by' => 1,
                'user_id' => $user->id,
                'status' => 1
            ]);

            $name = $faker->company;
            $description = $faker->sentence;
            $commerce = Commerce::create([
                'nom' => $name,
                'adresse' => $faker->streetAddress,
                'logo' => null,
                'card' => null,
                'referer_amount' => $faker->numberBetween(500, 5000),
                'referee_amount' => $faker->numberBetween(500, 5000),
                'background' => null,
                'contact' => "00237" . $faker->numberBetween(600000000, 699999999),
                'description' => $description,
                'is_boost' => 0,
                'localisation' => $faker->url,
                'horaires' => '',
                'couleurs' => '#FA9D36-#121318',
                'city_id' => $cities->random()->id,
                'category_id' => $categories->random()->id,
                'quartier_id' => $districts->random()->id,
                'created_by' => 1,
                'vendeur_id' => $vendeur->id
            ]);

            $duration = $faker->randomElement([6, 9, 12, 24, 36]);
            $start = $faker->dateTimeBetween('-3 months');
            $program = Programme::create([
                'libelle' => $name,
                'description' => $description,
                'duree' => $duration,
                'date_debut' => $start,
                'date_fin' => Carbon::createFromTimestamp($start->getTimestamp())->addMonths($duration),
                'is_cashback' => 1,
                'taux_commission' => $faker->numberBetween(1, 10),
                'taux_cashback' => $faker->numberBetween(1, 10),
                'montant_abonnement' => $duration * 5000,
                'bonus_wallet' => 500,
                'mnt_max_facture' => $faker->numberBetween(25000, 250000),
                'commerce_id' => $commerce->id,
                'created_by' => 1
            ]);

            $wallet = WalletVendeur::create([
                'programme_id' => $program->id,
                'cashback' => 0,
                'topup' => 0,
                'passages' => 0,
                'vendeur_id' => $vendeur->id,
                'id_fidelite' => Str::upper(Str::random(6))
            ]);
        }
        return 0;
    }



}
