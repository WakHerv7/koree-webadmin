<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;


class UserFindCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan user:find nkaurelien
     *
     * @var string
     */
    protected $signature = 'user:find {searchTerm}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'recherche un user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        $searchTerm = $this->argument('searchTerm');

        if (empty($searchTerm)) {
            $this->error('Give an search term to match CMD: ' . $this->signature);
        }


        $user = User::query()
            ->where('email', 'like', "%$searchTerm%")
            ->first();

        !$user ? $this->error('Aucun utilisateur touver ') : $this->info($user->toJson());

        return 0;

    }
}
