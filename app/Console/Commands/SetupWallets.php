<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Programme;
use App\Models\WalletClient;
use App\Models\WalletVendeur;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SetupWallets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:wallets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create wallet and link to programmes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Creating wallets...');
        $clients = Client::all();
        $programmes = Programme::with('commerce.vendeur')->get();

        $clients->each(function ($client) use ($programmes) {
            $lap = $programmes->shuffle()->take(rand(4, 10));
            $lap->each(function ($programme) use ($client) {
                $wallet = new WalletClient();
                $wallet->client_id = $client->id;
                $wallet->programme_id = $programme->id;
                $wallet->id_fidelite = Str::upper(Str::random(10));
                $wallet->save();
            });
        });

        $programmes->each(function ($programme) {
            $wallet = new WalletVendeur();
            $wallet->vendeur_id = $programme->commerce->vendeur_id;
            $wallet->programme_id = $programme->id;
            $wallet->id_fidelite = Str::upper(Str::random(10));
            $wallet->save();
        });
        $this->info('Wallets created.');
        return 0;
    }
}
