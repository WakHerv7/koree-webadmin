<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class OneSignalTestNotification extends Command
{
    /*
     * php artisan slack:test
     */

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onesignal:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test OneSIgnal notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $contact = $this->argument('contact');
        $this->info('QUEUE DRIVER = ' . config('queue.default'));
        $this->info('ONE SIGNAL APP ID = ' . config('services.onesignal.app_id'));
        $user = User::query()
//            ->orWhere('id', $contact)
//            ->orWhere('telephone', $contact)
//            ->orWhere('email', $contact)
            ->orWhere('telephone', '00237654306774')
            ->first();
//        dd($user);
        Notification::locale('fr')->sendNow($user, new \App\Notifications\OneSignalTestNotification(true));

    }
}
