<?php

namespace App;

use Illuminate\Support\Facades\Http;

class DispatchSocket
{

    public static function dispatchMessage(string $to, string $event)
    {
        Http::post(config('app.socket_url'), [
            'room' => $to,
            'event' => $event
        ]);
    }
}
